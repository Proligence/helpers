﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PatternService.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    using Cache;

    public class PatternService : IPatternService
    {
        private readonly IPatternCache _cache;

        public PatternService(IPatternCache cache)
        {
            this._cache = cache;
        }

        public bool Match(string text, string pattern)
        {
            PatternMatch details;
            return Match(text, pattern, out details);
        }

        public bool Match(string text, string pattern, out PatternMatch details)
        {
            pattern = "{^}" + pattern + "{$}";
            var cachedPattern = _cache.GetOrAdd(pattern, () => Convert(pattern));
            var cachedGroups = _cache.GetOrAddGroups(pattern, () => this.GetGroups(pattern));

            var match = cachedPattern.Match(text);
            details = new PatternMatch
            {
                Groups = cachedGroups.Select(g => new KeyValuePair<string, string>(g, match.Groups[g].Value)),
                BaseExpression = cachedPattern,
                IsMatch = match.Success
            };

            return details.IsMatch;
        }

        /// <summary>
        /// Matches a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        public bool TryMatch(string text, string pattern)
        {
            PatternMatch details;
            return TryMatch(text, pattern, out details);
        }

        /// <summary>
        /// Matches a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="details">Object containing detailed match information.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        public bool TryMatch(string text, string pattern, out PatternMatch details)
        {
            try
            {
                return Match(text, pattern, out details);
            }
            catch (ArgumentException)
            {
                details = null;
                return false;
            }
        }

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a specified replacement string.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        public string Replace(string text, string pattern, string replacement)
        {
            var cached = _cache.GetOrAdd(pattern, () => Convert(pattern));
            return cached.Replace(text, replacement);
        }

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        public string Replace(string text, string pattern, Func<string, string> replacement)
        {
            var cached = _cache.GetOrAdd(pattern, () => Convert(pattern));
            return cached.Replace(text, match => replacement(match.ToString()));
        }

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        public string Replace(string text, string pattern, Func<PatternMatch, string> replacement)
        {
            var cached = _cache.GetOrAdd(pattern, () => Convert(pattern));
            var cachedGroups = _cache.GetOrAddGroups(pattern, () => GetGroups(pattern));

            return cached.Replace(
                text, 
                match =>
                {
                    var details = new PatternMatch
                    {
                        Groups = cachedGroups.Select(g => new KeyValuePair<string, string>(g, match.Groups[g].Value)),
                        BaseExpression = cached,
                        IsMatch = match.Success
                    };

                    return replacement(details);
                });
        }

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a specified replacement string.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        public bool TryReplace(string text, string pattern, string replacement, out string replaced)
        {
            try
            {
                replaced = Replace(text, pattern, replacement);
                return true;
            }
            catch (ArgumentException)
            {
                replaced = text;
                return false;
            }
        }

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        public bool TryReplace(string text, string pattern, Func<string, string> replacement, out string replaced)
        {
            try
            {
                replaced = Replace(text, pattern, replacement);
                return true;
            }
            catch (ArgumentException)
            {
                replaced = text;
                return false;
            }
        }

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        public bool TryReplace(
            string text, 
            string pattern, 
            Func<PatternMatch, string> replacement, 
            out string replaced)
        {
            try
            {
                replaced = Replace(text, pattern, replacement);
                return true;
            }
            catch (ArgumentException)
            {
                replaced = text;
                return false;
            }
        }

        /// <summary>
        /// Returns names of groups defined in a given pattern.
        /// </summary>
        /// <param name="pattern">Pattern to check.</param>
        /// <returns>List of group names. Does not include whitespace-only or groups without names.</returns>
        public IEnumerable<string> GetGroups(string pattern)
        {
            return ExtractGroups(pattern)
                .Where(t => !string.IsNullOrWhiteSpace(t.Item1))
                .Select(t => t.Item1.Trim());
        }

        /// <summary>
        /// Validates a given pattern
        /// </summary>
        /// <param name="pattern">Pattern to validate.</param>
        /// <returns>True if pattern is valid, false otherwise.</returns>
        public bool Validate(string pattern)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pattern))
                {
                    return false;
                }

                var regex = Convert(pattern);
                regex.Match(string.Empty);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        private static Regex Convert(string pattern)
        {
            var groups = ExtractGroups(pattern);
            var regexPattern = groups.Aggregate(
                EscapeSpecial(pattern),
                (current, replacement) =>
                {
                    var newString = ReplaceWildcards(UnescapeSpecial(replacement.Item2.Trim()));
                    var groupName = replacement.Item1.Trim();

                    return current.Replace(
                        "{" + EscapeSpecial(replacement.Item1 + replacement.Item2) + "}",
                        !string.IsNullOrEmpty(groupName)
                            ? "(?'" + groupName + "'" + newString + ")"
                            : newString.Length == 1 ? newString : "(" + newString + ")");
                });

            return new Regex(
                regexPattern,
                RegexOptions.IgnoreCase |
                RegexOptions.ExplicitCapture |
                RegexOptions.Singleline |
                RegexOptions.IgnorePatternWhitespace);
        }

        private static string EscapeSpecial(string text)
        {
            var sb = new StringBuilder(text);
            sb.Replace("\\", "\\\\")
              .Replace(".", "\\.")
              .Replace("[", "\\[")
              .Replace("]", "\\]")
              .Replace("(", "\\(")
              .Replace(")", "\\)")
              .Replace("|", "\\|")
              .Replace("/", "\\/")
              .Replace("*", "\\*")
              .Replace("$", "\\$")
              .Replace("^", "\\^")
              .Replace("?", "\\?");

            return sb.ToString();
        }

        private static string UnescapeSpecial(string text)
        {
            var sb = new StringBuilder(text);
            sb.Replace("\\\\", "\\")
              .Replace("\\.", ".")
              .Replace("\\[", "[")
              .Replace("\\]", "]")
              .Replace("\\(", "(")
              .Replace("\\)", ")")
              .Replace("\\|", "|")
              .Replace("\\/", "/")
              .Replace("\\*", "*")
              .Replace("\\$", "$")
              .Replace("\\^", "^")
              .Replace("\\?", "?");

            return sb.ToString();
        }

        private static IEnumerable<Tuple<string, string>> ExtractGroups(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                yield break;
            }

            var insideGroup = false;
            var groupStart = 0;
            var groupContentStart = 0;
            var insideGroupName = true;
            var groupName = "";

            for (var i = 0; i < text.Length; i++)
            {
                var c = text[i];

                if (c == '{')
                {
                    if (i + 1 < text.Length && text[i + 1] == '{')
                    {
                        text = text.Substring(0, i) + text.Substring(i + 1);
                        continue;
                    }
                }
                else if (c == '}')
                {
                    if (i + 1 < text.Length && text[i + 1] == '}')
                    {
                        text = text.Substring(0, i) + text.Substring(i + 1);
                        continue;
                    }
                }

                if (insideGroup)
                {
                    // Group name ends when first non-alphanumeric char is found
                    if (insideGroupName && (!char.IsLetter(c) || char.IsWhiteSpace(c)))
                    {
                        insideGroupName = false;

                        // If there is a pipe sign following, it's not a group name but text alternation
                        if (c == '|')
                        {
                            groupContentStart = groupStart + 1;
                            groupName = "";
                        }
                        else
                        {
                            groupContentStart = i;
                            groupName = text.Substring(groupStart + 1, i - groupStart - 1);
                        }
                    }

                    if (c == '}')
                    {
                        insideGroup = false;
                        var groupContent = text.Substring(groupContentStart, i - groupContentStart);
                        yield return new Tuple<string, string>(
                            groupName,
                            groupContent);
                    }
                }
                else if (c == '{')
                {
                    insideGroup = true;

                    // Group name starts just after opening {, 
                    insideGroupName = true;
                    groupStart = i;
                    groupName = "";
                }
            }
        }

        private static string ReplaceWildcards(string pattern)
        {
            var sb = new StringBuilder(pattern);

            while (sb.ToString().IndexOf("?*", StringComparison.Ordinal) != -1)
            {
                sb.Replace("?*", "*");
            }

            while (sb.ToString().IndexOf("**", StringComparison.Ordinal) != -1)
            {
                sb.Replace("**", "*");
            }

            while (sb.ToString().IndexOf(" ", StringComparison.Ordinal) != -1)
            {
                sb.Replace(" ", "\u0200");
            }

            // If the expressions are simple - change it to match any character (.)
            // * should match 0 or more characters
            var finalString = sb.ToString();
            if (finalString.Equals("*"))
            {
                return ".*";
            }

            // + should match 1 or more characters
            if (finalString.Equals("+"))
            {
                return ".+";
            }

            // ? should match exactly one character
            if (finalString.Equals("?"))
            {
                return ".";
            }

            // Default quantifier (if none is specified) should not be greedy
            return string.IsNullOrWhiteSpace(pattern) ? ".+?" : finalString;
        }
    }
}