﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PatternMatch.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Describes a result of pattern-matching operation.
    /// </summary>
    public class PatternMatch 
    {
        public PatternMatch() 
        {
            this.Groups = Enumerable.Empty<KeyValuePair<string, string>>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether there is a match.
        /// </summary>
        public bool IsMatch
        {
            get; 
            set;
        }

        /// <summary>
        /// Gets or sets a list of groups and their matching substrings.
        /// </summary>
        public IEnumerable<KeyValuePair<string, string>> Groups
        {
            get; 
            set;
        }

        /// <summary>
        /// Gets or sets the base regular expression.
        /// </summary>
        public Regex BaseExpression
        {
            get; 
            set;
        }
    }
}