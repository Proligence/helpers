﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPatternService.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.Core
{
    using System;
    using System.Collections.Generic;

    public interface IPatternService
    {
        /// <summary>
        /// Matches a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        bool Match(string text, string pattern);

        /// <summary>
        /// Matches a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="details">Object containing detailed match information.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        bool Match(string text, string pattern, out PatternMatch details);

        /// <summary>
        /// Tries to match a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        bool TryMatch(string text, string pattern);

        /// <summary>
        /// Tries to match a given text against a given wildcard pattern.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="details">Object containing detailed match information.</param>
        /// <returns>True if match is found or false otherwise.</returns>
        bool TryMatch(string text, string pattern, out PatternMatch details);

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a specified replacement string.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        string Replace(string text, string pattern, string replacement);

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        string Replace(string text, string pattern, Func<string, string> replacement);

        /// <summary>
        /// In a specified string, replaces all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <returns>The replaced string.</returns>
        string Replace(string text, string pattern, Func<PatternMatch, string> replacement);

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a specified replacement string.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        bool TryReplace(string text, string pattern, string replacement, out string replaced);

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        bool TryReplace(string text, string pattern, Func<string, string> replacement, out string replaced);

        /// <summary>
        /// In a specified string, tries to replace all strings that match a pattern 
        /// with a string returned by the replacement function.
        /// </summary>
        /// <param name="text">Text to match.</param>
        /// <param name="pattern">Pattern to match against.</param>
        /// <param name="replacement">Replacement string.</param>
        /// <param name="replaced">The replaced string.</param>
        /// <returns>True if replacement succeeded.</returns>
        bool TryReplace(string text, string pattern, Func<PatternMatch, string> replacement, out string replaced);

        /// <summary>
        /// Returns names of groups defined in a given pattern.
        /// </summary>
        /// <param name="pattern">Pattern to check.</param>
        /// <returns>List of group names. Does not include whitespace-only or groups without names.</returns>
        IEnumerable<string> GetGroups(string pattern);

        /// <summary>
        /// Validates a given pattern
        /// </summary>
        /// <param name="pattern">Pattern to validate.</param>
        /// <returns>True if pattern is valid, false otherwise.</returns>
        bool Validate(string pattern);
    }
}