﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPatternCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Simple wrapper for regular expression cache.
    /// </summary>
    public interface IPatternCache
    {
        /// <summary>
        /// Gets or sets a regex corresponding to a given pattern.
        /// </summary>
        /// <param name="pattern">Pattern to set regex for.</param>
        /// <param name="setter">Delegate used to set non-existent value.</param>
        /// <returns>Regex matching given pattern.</returns>
        Regex GetOrAdd(string pattern, Func<Regex> setter);

        /// <summary>
        /// Gets or sets a list of group names contained within a given pattern.
        /// </summary>
        /// <param name="pattern">Pattern to set group names for.</param>
        /// <param name="setter">Delegate used to set non-existent value.</param>
        /// <returns>List of groups in a given pattern.</returns>
        IEnumerable<string> GetOrAddGroups(string pattern, Func<IEnumerable<string>> setter);
    }
}