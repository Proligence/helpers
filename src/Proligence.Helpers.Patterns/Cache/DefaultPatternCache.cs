﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultPatternCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public class DefaultPatternCache : IPatternCache
    {
        private readonly IDictionary<string, Regex> _regexes = 
            new Dictionary<string, Regex>();

        private readonly IDictionary<string, IEnumerable<string>> _groups = 
            new Dictionary<string, IEnumerable<string>>();

        private volatile object _syncRegexes = new object();
        private volatile object _syncGroups = new object();

        public Regex GetOrAdd(string pattern, Func<Regex> setter)
        {
            if (string.IsNullOrEmpty(pattern))
            {
                throw new ArgumentNullException("pattern");
            }

            if (setter == null)
            {
                throw new ArgumentNullException("setter");
            }

            Regex retVal;
            if (_regexes.TryGetValue(pattern, out retVal) && retVal != null)
            {
                return retVal;
            }

            lock (_syncRegexes)
            {
                if (!_regexes.TryGetValue(pattern, out retVal) || retVal == null)
                {
                    retVal = setter();
                    _regexes[pattern] = retVal;
                }
            }

            return retVal;
        }

        public IEnumerable<string> GetOrAddGroups(string pattern, Func<IEnumerable<string>> setter)
        {
            if (string.IsNullOrEmpty(pattern))
            {
                throw new ArgumentNullException("pattern");
            }

            if (setter == null)
            {
                throw new ArgumentNullException("setter");
            }

            IEnumerable<string> retVal;
            if (_groups.TryGetValue(pattern, out retVal) && retVal != null)
            {
                return retVal;
            }

            lock (_syncGroups)
            {
                if (!_groups.TryGetValue(pattern, out retVal) || retVal == null)
                {
                    retVal = setter();
                    _groups[pattern] = retVal;
                }
            }

            return retVal;
        }
    }
}