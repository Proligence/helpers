﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System;
    using global::Autofac;

    /// <summary>
    /// Manages and executes add-in commands.
    /// </summary>
    public class CommandManager : ICommandManager
    {
        /// <summary>
        /// The add-in's dependency injection container.
        /// </summary>
        private readonly IComponentContext componentContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandManager"/> class.
        /// </summary>
        /// <param name="componentContext">The add-in's dependency injection container.</param>
        public CommandManager(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
        }

        /// <summary>
        /// Gets the command with the specified name.
        /// </summary>
        /// <param name="name">The name of the command to get.</param>
        /// <returns>The command instance or <c>null</c> if there is no command with the specified name.</returns>
        public IAddInCommand GetCommand(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            object command;
            if (this.componentContext.TryResolveNamed(name, typeof(IAddInCommand), out command))
            {
                return (IAddInCommand)command;
            }

            return null;
        }

        /// <summary>
        /// Executes the command with the specified name.
        /// </summary>
        /// <param name="name">The name of the command to get.</param>
        /// <param name="parameter">The parameter which will be passed to the command or <c>null</c>.</param>
        public void ExecuteCommand(string name, object parameter = null)
        {
            IAddInCommand command = this.GetCommand(name);
            if (command == null)
            {
                throw new ArgumentException("Command '" + name + "' is not registered.", "name");                
            }

            command.Execute(parameter);
        }
    }
}
