﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddInBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Forms;
    using global::Autofac;
    using Microsoft.Office.Core;
    using Microsoft.Office.Tools;
    using Microsoft.Office.Tools.Ribbon;
    using Proligence.Helpers.Autofac;
    
    /// <summary>
    /// The base class for Office add-in classes.
    /// </summary>
    public abstract class AddInBase : Microsoft.Office.Tools.AddInBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddInBase"/> class.
        /// </summary>
        /// <param name="factory">The Office factory.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="primaryCookie">The primary cookie.</param>
        /// <param name="identifier">The identifier.</param>
        protected AddInBase(Factory factory, IServiceProvider serviceProvider, string primaryCookie, string identifier)
            : base(factory, serviceProvider, primaryCookie, identifier)
        {
        }

        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        public IContainer DependencyContainer { get; private set; }

        /// <summary>
        /// Gets the ribbons registered by the add-in.
        /// </summary>
        public IEnumerable<IRibbonExtension> Ribbons { get; private set; }

        /// <summary>
        /// Gets or sets the options for building the add-in's dependency injection container.
        /// </summary>
        protected ContainerBuildOptions ContainerBuildOptions { get; set; }

        /// <summary>
        /// Registers additional components in the application's dependency injection container.
        /// </summary>
        /// <param name="builder">The dependency injection container builder.</param>
        protected virtual void BuildContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<OfficeCommonModule>();
        }

        /// <summary>
        /// Returns an array of <see cref="T:Microsoft.Office.Tools.Ribbon.OfficeRibbon"/> objects to the 
        /// <see cref="AddInBase.CreateRibbonExtensibilityObject"/> method.
        /// </summary>
        /// <returns>An array of <see cref="T:Microsoft.Office.Tools.Ribbon.OfficeRibbon"/> objects.</returns>
        protected override IRibbonExtension[] CreateRibbonObjects()
        {
            string excelExePath = Process.GetCurrentProcess().MainModule.FileName;
            int majorVersion = FileVersionInfo.GetVersionInfo(excelExePath).ProductMajorPart;

            var ribbons = new List<IRibbonExtension>();

            var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
            FieldInfo[] fields = this.GetType().GetFields(bindingFlags);

            foreach (FieldInfo field in fields)
            {
                if (typeof(IRibbonExtension).IsAssignableFrom(field.FieldType) && (field.GetValue(this) == null))
                {
                    var ribbon = (IRibbonExtension)Activator.CreateInstance(field.FieldType);

                    // Capitalize ribbon titles for newer Excel versions
                    if (majorVersion >= 15)
                    {
                        var officeRibbon = ribbon as RibbonBase;
                        if (officeRibbon != null)
                        {
                            foreach (RibbonTab tab in officeRibbon.Tabs)
                            {
                                tab.Label = tab.Label.ToUpper(CultureInfo.CurrentCulture);
                            }
                        }
                    }

                    field.SetValue(this, ribbon);
                    ribbons.Add(ribbon);
                }
            }

            this.Ribbons = ribbons.ToArray();
            
            return ribbons.ToArray();
        }

        /// <summary>
        /// Called when the add-in is started.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnStartup(object sender, EventArgs e)
        {
            try
            {
                this.DependencyContainer = AppDomain.CurrentDomain.BuildIocContainer(
                    this.ContainerBuildOptions,
                    this.BuildContainer);

                // Inject unsert properties for designer-based ribbons.
                if (this.Ribbons != null)
                {
                    foreach (IRibbonExtension ribbon in this.Ribbons)
                    {
                        this.DependencyContainer.InjectUnsetProperties(ribbon);
                    }
                }

                // Inject unsert properties for XML-based ribbons.
                var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
                FieldInfo[] fields = this.GetType().GetFields(bindingFlags);
                foreach (FieldInfo field in fields)
                {
                    if (typeof(IRibbonExtensibility).IsAssignableFrom(field.FieldType))
                    {
                        var ribbon = (IRibbonExtensibility)field.GetValue(this);
                        if (ribbon != null)
                        {
                            this.DependencyContainer.InjectUnsetProperties(ribbon);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;

                Exception exc = ex;
                while (exc != null)
                {
                    message += exc.Message;

                    var typeLoadException = exc as ReflectionTypeLoadException;
                    if (typeLoadException != null)
                    {
                        foreach (Exception loaderException in typeLoadException.LoaderExceptions.Take(10))
                        {
                            message += loaderException.Message;
                        }
                    }

                    exc = exc.InnerException;
                }

                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        /// <summary>
        /// Called when the add-in is shut down.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void OnShutdown(object sender, EventArgs e)
        {
            if (this.DependencyContainer != null)
            {
                this.DependencyContainer.Dispose();
            }
        }
    }
}