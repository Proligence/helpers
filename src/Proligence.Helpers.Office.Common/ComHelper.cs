﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;

    /// <summary>
    /// Implements helper methods for invoking COM interfaces.
    /// </summary>
    public static class ComHelper
    {
        /// <summary>
        /// The default number of times to retry failed COM calls.
        /// </summary>
        private const int DefaultRetryCount = 3;

        /// <summary>
        /// The default number of milliseconds to wait before retrying failed COM calls.
        /// </summary>
        private const int DefaultWaitTime = 100;

        /// <summary>
        /// Invokes a method of a COM interface.
        /// </summary>
        /// <typeparam name="T">The type of the COM interface.</typeparam>
        /// <param name="obj">The COM object instance.</param>
        /// <param name="action">The action to invoke.</param>
        /// <param name="retryCount">The number of times to retry failed calls.</param>
        /// <param name="waitTime">The number of milliseconds to wait before retrying the operation.</param>
        public static void ComInvoke<T>(
            this T obj,
            Action<T> action,
            int retryCount = DefaultRetryCount,
            int waitTime = DefaultWaitTime)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            try
            {
                action(obj);
            }
            catch (COMException)
            {
                if (retryCount > 0)
                {
                    if (waitTime > 0)
                    {
                        Thread.Sleep(waitTime);
                    }

                    ComInvoke(obj, action, retryCount - 1);                    
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Invokes a method of a COM interface.
        /// </summary>
        /// <typeparam name="T">The type of the COM interface.</typeparam>
        /// <typeparam name="TResult">The type of result returned from the COM interface.</typeparam>
        /// <param name="obj">The COM object instance.</param>
        /// <param name="action">The action to invoke.</param>
        /// <param name="retryCount">The number of times to retry failed calls.</param>
        /// <param name="waitTime">The number of milliseconds to wait before retrying the operation.</param>
        /// <returns>The value returned from the COM interface.</returns>
        public static TResult ComInvoke<T, TResult>(
            this T obj,
            Func<T, TResult> action,
            int retryCount = DefaultRetryCount,
            int waitTime = DefaultWaitTime)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            try
            {
                return action(obj);
            }
            catch (COMException)
            {
                if (retryCount > 0)
                {
                    if (waitTime > 0)
                    {
                        Thread.Sleep(waitTime);
                    }
                    
                    return ComInvoke(obj, action, retryCount - 1);
                }
                
                throw;
            }
        }
    }
}