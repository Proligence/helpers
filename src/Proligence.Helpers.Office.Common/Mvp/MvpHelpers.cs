﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MvpHelpers.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common.Mvp
{
    using System.Windows.Forms;

    /// <summary>
    /// Implements MPV-related helper methods.
    /// </summary>
    public static class MvpHelpers
    {
        /// <summary>
        /// Displays a model dialog box.
        /// </summary>
        /// <typeparam name="TDialog">The type which implements the dialog box.</typeparam>
        /// <typeparam name="TModel">The type which represents the model for the dialog box.</typeparam>
        /// <returns>
        /// The model after closing the dialog box using the OK button, or <c>null</c> if the dialog box was closed
        /// using the Cancel button.
        /// </returns>
        public static TModel ShowModalDialog<TDialog, TModel>()
            where TDialog : Form, IView<TModel>, new()
            where TModel : class, new()
        {
            var model = new TModel();

            using (var dialog = new TDialog())
            {
                dialog.DisplayModel(model);

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return null;
                }

                dialog.UpdateModel(model);
            }

            return model;
        }

        /// <summary>
        /// Displays a model dialog box.
        /// </summary>
        /// <typeparam name="TDialog">The type which implements the dialog box.</typeparam>
        /// <typeparam name="TModel">The type which represents the model for the dialog box.</typeparam>
        /// <param name="model">The model to display in the dialog box.</param>
        /// <returns>
        /// The model after closing the dialog box using the OK button, or <c>null</c> if the dialog box was closed
        /// using the Cancel button.
        /// </returns>
        public static TModel ShowModalDialog<TDialog, TModel>(TModel model)
            where TDialog : Form, IView<TModel>, new()
            where TModel : class
        {
            using (var dialog = new TDialog())
            {
                dialog.DisplayModel(model);

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return null;
                }

                dialog.UpdateModel(model);
            }

            return model;
        }

        /// <summary>
        /// Displays a model dialog box.
        /// </summary>
        /// <param name="dialog">The dialog which will be displayed.</param>
        /// <typeparam name="TDialog">The type which implements the dialog box.</typeparam>
        /// <typeparam name="TModel">The type which represents the model for the dialog box.</typeparam>
        /// <returns>
        /// The model after closing the dialog box using the OK button, or <c>null</c> if the dialog box was closed
        /// using the Cancel button.
        /// </returns>
        public static TModel ShowModalDialog<TDialog, TModel>(TDialog dialog)
            where TDialog : Form, IView<TModel>
            where TModel : class, new()
        {
            var model = new TModel();

            using (dialog)
            {
                dialog.DisplayModel(model);

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return null;
                }

                dialog.UpdateModel(model);
            }

            return model;
        }

        /// <summary>
        /// Displays a model dialog box.
        /// </summary>
        /// <param name="dialog">The dialog which will be displayed.</param>
        /// <typeparam name="TDialog">The type which implements the dialog box.</typeparam>
        /// <typeparam name="TModel">The type which represents the model for the dialog box.</typeparam>
        /// <param name="model">The model to display in the dialog box.</param>
        /// <returns>
        /// The model after closing the dialog box using the OK button, or <c>null</c> if the dialog box was closed
        /// using the Cancel button.
        /// </returns>
        public static TModel ShowModalDialog<TDialog, TModel>(TDialog dialog, TModel model)
            where TDialog : Form, IView<TModel>
            where TModel : class
        {
            using (dialog)
            {
                dialog.DisplayModel(model);

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return null;
                }

                dialog.UpdateModel(model);
            }

            return model;
        }
    }
}