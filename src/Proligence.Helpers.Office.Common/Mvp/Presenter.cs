﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Presenter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common.Mvp
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The base class for classes which implement Office MVP presenters.
    /// </summary>
    /// <typeparam name="TView">The interface of the view associated with the presenter.</typeparam>
    /// <typeparam name="TModel">The type which represents the domain model.</typeparam>
    public abstract class Presenter<TView, TModel> : IPresenter<TView, TModel>, INotifyPropertyChanged
        where TView : class, IView<TModel>
        where TModel : class
    {
        /// <summary>
        /// The view associated with the presenter.
        /// </summary>
        private TView view;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the view associated with the presenter.
        /// </summary>
        public TView View
        {
            get
            {
                return this.view;
            }

            set
            {
                if (this.view != null)
                {
                    this.view.ViewDataChanged -= this.OnViewDataChanged;
                }

                this.view = value;

                if (this.view != null)
                {
                    this.view.ViewDataChanged += this.OnViewDataChanged;
                }
            }
        }

        /// <summary>
        /// Gets or sets the domain model associated with the presenter.
        /// </summary>
        public TModel Model { get; set; }

        /// <summary>
        /// Initializes the presenter and its view.
        /// </summary>
        public virtual void Initialize()
        {
            this.Model = this.InitializeModel();
            if (this.Model != null)
            {
                this.DisplayModel(this.Model);
            }
        }

        /// <summary>
        /// Initializes the domain model associated with the presenter.
        /// </summary>
        /// <returns>The created domain model.</returns>
        protected virtual TModel InitializeModel()
        {
            return null;
        }

        /// <summary>
        /// Displays the current model in the current view.
        /// </summary>
        protected virtual void DisplayModel()
        {
            this.DisplayModel(this.Model);
        }

        /// <summary>
        /// Displays the specified model in the current view.
        /// </summary>
        /// <param name="model">The model to bind.</param>
        protected virtual void DisplayModel(TModel model)
        {
            this.View.DisplayModel(model);
        }

        /// <summary>
        /// Updates the current model with the data in the current view.
        /// </summary>
        protected virtual void UpdateModel()
        {
            this.UpdateModel(this.Model);
        }

        /// <summary>
        /// Updates the specified model with the data in the current view.
        /// </summary>
        /// <param name="model">The model to update.</param>
        protected virtual void UpdateModel(TModel model)
        {
            this.View.UpdateModel(model);
        }

        /// <summary>
        /// Called when the view's data has been changed by the user.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected virtual void OnViewDataChanged(object sender, EventArgs eventArgs)
        {
            if (this.Model != null)
            {
                this.UpdateModel(this.Model);
            }
        }

        /// <summary>
        /// Called when a property value is changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}