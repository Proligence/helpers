﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LocalizedString.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common.Localization
{
    using System.Globalization;
    using System.Linq;
    using System.Resources;

    /// <summary>
    /// Implements a wrapper for <see cref="string"/> instances which represent localized texts.
    /// </summary>
    public struct LocalizedString
    {
        /// <summary>
        /// The name of the string resource.
        /// </summary>
        private readonly string resourceName;

        /// <summary>
        /// The arguments for the string resource format string.
        /// </summary>
        private readonly object[] arguments;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString"/> struct.
        /// </summary>
        /// <param name="resourceName">The name of the string resource.</param>
        public LocalizedString(string resourceName)
        {
            this.resourceName = resourceName;
            this.arguments = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizedString"/> struct.
        /// </summary>
        /// <param name="resourceName">The name of the string resource.</param>
        /// <param name="arguments">The arguments for the string resource format string.</param>
        public LocalizedString(string resourceName, params object[] arguments)
        {
            this.resourceName = resourceName;
            this.arguments = arguments;
        }

        /// <summary>
        /// Gets or sets the <see cref="ResourceManager"/> which will be used to resolve string resources.
        /// </summary>
        public static ResourceManager ResourceManager { get; set; }

        /// <summary>
        /// Gets the name of the string resource.
        /// </summary>
        /// <value>The name of the resource.</value>
        public string ResourceName
        {
            get
            {
                return this.resourceName;
            }
        }

        /// <summary>
        /// Gets the localized text.
        /// </summary>
        public string Text
        {
            get
            {
                if (this.resourceName == null)
                {
                    return null;
                }

                string str = ResourceManager.GetString(this.resourceName);
                if (str == null)
                {
                    return null;
                }

                if (this.arguments != null)
                {
                    str = string.Format(CultureInfo.CurrentCulture, str, this.arguments);
                }

                return str;
            }
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="LocalizedString"/> to <see cref="string"/>.
        /// </summary>
        /// <param name="localizedString">The localized string.</param>
        /// <returns>The result of the conversion.</returns>
        public static implicit operator string(LocalizedString localizedString)
        {
            return localizedString.Text;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="localizedString">The localized string.</param>
        /// <param name="other">The other localized string.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(LocalizedString localizedString, LocalizedString other)
        {
            return localizedString.Equals(other);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="localizedString">The localized string.</param>
        /// <param name="other">The other localized string.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(LocalizedString localizedString, LocalizedString other)
        {
            return !localizedString.Equals(other);
        }    

        /// <summary>
        /// Determines whether the specified <see cref="object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is LocalizedString))
            {
                return false;
            }

            var localizedString = (LocalizedString)obj;
            
            if (localizedString.resourceName != this.resourceName)
            {
                return false;
            }

            if ((localizedString.arguments != null) && (localizedString.arguments.Length > 0))
            {
                if ((this.arguments == null) || (this.arguments.Length == 0))
                {
                    return false;
                }

                if (!localizedString.arguments.SequenceEqual(this.arguments))
                {
                    return false;
                }
            }
            else if ((this.arguments != null) && (this.arguments.Length > 0))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash
        /// table.
        /// </returns>
        public override int GetHashCode()
        {
            if (this.resourceName != null)
            {
                return this.resourceName.GetHashCode();   
            }

            return 0;
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string" /> that represents this instance.</returns>
        public override string ToString()
        {
            return this.resourceName;
        }
    }
}