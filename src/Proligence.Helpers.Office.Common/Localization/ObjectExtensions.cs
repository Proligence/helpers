﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common.Localization
{
    /// <summary>
    /// Implements localization-related extension methods for <see cref="object"/>.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Gets the value of a string resource with the specified name.
        /// </summary>
        /// <param name="obj">The context object.</param>
        /// <param name="name">The name of the resource to get.</param>
        /// <returns>The value of the string resource.</returns>
        public static LocalizedString R(this object obj, string name)
        {
            return new LocalizedString(name);
        }

        /// <summary>
        /// Gets the value of a string resource with the specified name.
        /// </summary>
        /// <param name="obj">The context object.</param>
        /// <param name="name">The name of the resource to get.</param>
        /// <param name="args">Format string arguments for the resource string.</param>
        /// <returns>The value of the string resource.</returns>
        public static LocalizedString R(this object obj, string name, params object[] args)
        {
            return new LocalizedString(name, args);
        }
    }
}