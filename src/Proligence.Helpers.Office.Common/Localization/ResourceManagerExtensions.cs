﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceManagerExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common.Localization
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Resources;
    using System.Windows.Forms;
    using Microsoft.Office.Tools.Ribbon;

    /// <summary>
    /// Implements extension methods for the <see cref="ResourceManager"/> class.
    /// </summary>
    public static class ResourceManagerExtensions
    {
        /// <summary>
        /// Localizes the specified form.
        /// </summary>
        /// <param name="resourceManager">The resource manager.</param>
        /// <param name="form">The form to localize.</param>
        /// <param name="prefix">The prefix which will be added to resource names, or <c>null</c>.</param>
        public static void LocalizeForm(this ResourceManager resourceManager, Form form, string prefix = null)
        {
            if (form == null)
            {
                throw new ArgumentNullException("form");
            }

            if (IsLocalizationSuppressed(form))
            {
                return;
            }

            form.Text = GetString(resourceManager, BuildPrefix(prefix, form.Name, "Text"));

            foreach (Control control in form.Controls)
            {
                resourceManager.LocalizeControl(control, BuildPrefix(prefix, form.Name));
            }
        }

        /// <summary>
        /// Localizes the specified control.
        /// </summary>
        /// <param name="resourceManager">The resource manager.</param>
        /// <param name="control">The control to localize.</param>
        /// <param name="prefix">The prefix which will be added to resource names, or <c>null</c>.</param>
        public static void LocalizeControl(this ResourceManager resourceManager, Control control, string prefix = null)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            if (IsLocalizationSuppressed(control))
            {
                return;
            }

            if (!string.IsNullOrEmpty(control.Text))
            {
                control.Text = GetString(resourceManager, BuildPrefix(prefix, control.Name, "Text"));
            }

            var tabControl = control as TabControl;
            if (tabControl != null)
            {
                foreach (TabPage tab in tabControl.TabPages)
                {
                    if (IsLocalizationSuppressed(tab))
                    {
                        continue;
                    }

                    tab.Text = GetString(resourceManager, BuildPrefix(prefix, control.Name, tab.Name, "Text"));
                    foreach (Control childControl in tab.Controls)
                    {
                        resourceManager.LocalizeControl(childControl, BuildPrefix(prefix, control.Name, tab.Name));
                    }
                }
            }

            foreach (Control childControl in control.Controls)
            {
                resourceManager.LocalizeControl(childControl, BuildPrefix(prefix, control.Name));
            }
        }

        /// <summary>
        /// Localizes the specified ribbon tab.
        /// </summary>
        /// <param name="resourceManager">The resource manager.</param>
        /// <param name="ribbonTab">The ribbon tab to localize.</param>
        /// <param name="prefix">The prefix which will be added to resource names, or <c>null</c>.</param>
        public static void LocalizeRibbonTab(
            this ResourceManager resourceManager,
            RibbonTab ribbonTab,
            string prefix = null)
        {
            if (ribbonTab == null)
            {
                throw new ArgumentNullException("ribbonTab");
            }

            foreach (RibbonGroup ribbonGroup in ribbonTab.Groups)
            {
                ribbonGroup.Label = GetString(
                    resourceManager,
                    BuildPrefix(prefix, ribbonTab.Name, ribbonGroup.Name, "Label"));

                foreach (RibbonButton button in ribbonGroup.Items.OfType<RibbonButton>())
                {
                    button.Label = GetString(
                        resourceManager,
                        BuildPrefix(prefix, ribbonTab.Name, ribbonGroup.Name, button.Name, "Label"));
                }
            }
        }

        /// <summary>
        /// Gets the localized string with the specified name parts.
        /// </summary>
        /// <param name="resourceManager">The resource manager</param>
        /// <param name="name">The name of the resource string.</param>
        /// <param name="prefix">The prefix which will be added to resource names, or <c>null</c>.</param>
        /// <returns>The localized string.</returns>
        public static string GetString(ResourceManager resourceManager, string name, string prefix = null)
        {
            if (resourceManager == null)
            {
                throw new ArgumentNullException("resourceManager");
            }

            string str = resourceManager.GetString(BuildPrefix(prefix, name));

            if (string.IsNullOrEmpty(str))
            {
                Trace.WriteLine("Unlocalized string detected: " + name);
            }

            return str;
        }

        /// <summary>
        /// Builds a prefix of the specified name parts.
        /// </summary>
        /// <param name="nameParts">The name parts.</param>
        /// <returns>The prefix string.</returns>
        public static string BuildPrefix(params string[] nameParts)
        {
            return string.Join("_", nameParts.Where(s => !string.IsNullOrEmpty(s)));
        }

        /// <summary>
        /// Determines whether localization should be suppressed for the specified control.
        /// </summary>
        /// <param name="control">The control instance.</param>
        /// <returns>
        /// <c>true</c> if localization should should be suppressed for the control; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsLocalizationSuppressed(Control control)
        {
            if ((control != null) && (control.Tag != null))
            {
                if (control.Tag.ToString().Equals("SuppressLocalization", StringComparison.Ordinal))
                {
                    return true;
                }
            }

            return false;
        }
    }
}