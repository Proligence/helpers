﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MessageBoxImpl.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System.Windows.Forms;

    /// <summary>
    /// Provides an interface to use the standard Windows message box.
    /// </summary>
    public class MessageBoxImpl : IMessageBox
    {
        /// <summary>
        /// Displays an error message.
        /// </summary>
        /// <param name="title">The message title.</param>
        /// <param name="message">The message to display.</param>
        public void ShowError(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Displays an error message.
        /// </summary>
        /// <param name="title">The message title.</param>
        /// <param name="message">The message to display.</param>
        public void ShowWarning(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// Displays an informational message.
        /// </summary>
        /// <param name="title">The message title.</param>
        /// <param name="message">The message to display.</param>
        public void ShowInfo(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Displays a warning and asks the user whether to continue execution.
        /// </summary>
        /// <param name="title">The message title.</param>
        /// <param name="message">The message to display.</param>
        /// <param name="icon">The icon to display.</param>
        /// <param name="buttons">The dialog buttons to display.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <returns><c>true</c> if the user clicked 'Yes' or 'OK'; otherwise, <c>false</c>.</returns>
        public bool Ask(
            string title,
            string message,
            MessageBoxIcon icon = MessageBoxIcon.Warning, 
            MessageBoxButtons buttons = MessageBoxButtons.YesNo,
            MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            DialogResult result = MessageBox.Show(message, title, buttons, icon, defaultButton);

            return (result == DialogResult.Yes) || (result == DialogResult.OK);
        }

        /// <summary>
        /// Shows a custom message box.
        /// </summary>
        /// <param name="title">The message title.</param>
        /// <param name="message">The message to display.</param>
        /// <param name="buttons">The dialog buttons to display.</param>
        /// <param name="icon">The icon to display.</param>
        /// <param name="defaultButton">The default button.</param>
        /// <param name="options">Additional message box options.</param>
        /// <returns>The dialog result.</returns>
        public DialogResult ShowCustomMessage(
            string title,
            string message,
            MessageBoxButtons buttons,
            MessageBoxIcon icon,
            MessageBoxDefaultButton defaultButton,
            MessageBoxOptions options)
        {
            return MessageBox.Show(message, title, buttons, icon, defaultButton, options);
        }
    }
}