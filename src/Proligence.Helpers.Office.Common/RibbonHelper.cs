﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RibbonHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Resources;
    using Microsoft.Office.Core;
    using Proligence.Helpers.Localization;

    /// <summary>
    /// Implements helper methods for implementing Office ribbons.
    /// </summary>
    public static class RibbonHelper
    {
        /// <summary>
        /// Maps command names to ribbon control names.
        /// </summary>
        private static readonly IDictionary<string, string> CommandsMap = new Dictionary<string, string>();

        /// <summary>
        /// Maps command names to their <see cref="IAddInCommand"/> objects.
        /// </summary>
        private static readonly IDictionary<string, IAddInCommand> Commands = new Dictionary<string, IAddInCommand>();

        /// <summary>
        /// Object used to synchronize access to <see cref="CommandsMap"/>.
        /// </summary>
        private static readonly object CommandsMapLock = new object();

        /// <summary>
        /// Gets or sets the ribbon implementation.
        /// </summary>
        public static IRibbonUI Ribbon { get; set; }

        /// <summary>
        /// Loads the XML markup, either from an XML customization file or from XML markup embedded in the procedure,
        /// that customizes the Ribbon user interface.
        /// </summary>
        /// <param name="assembly">The assembly which contains the ribbon XML markup.</param>
        /// <param name="ribbonType">The type which implements the ribbon.</param>
        /// <param name="resourceName">The name of the XML resource which contains the ribbon XML markup.</param>
        /// <returns>The XML markup of the ribbon.</returns>
        // ReSharper disable once InconsistentNaming
        public static string GetCustomUI(
            Assembly assembly,
            Type ribbonType,
            string resourceName = "Ribbon.xml")
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }

            if (ribbonType == null)
            {
                throw new ArgumentNullException("ribbonType");
            }

            if (resourceName == null)
            {
                throw new ArgumentNullException("resourceName");
            }

            using (Stream stream = assembly.GetManifestResourceStream(ribbonType, resourceName))
            {
                Debug.Assert(stream != null, "stream != null");
                return new StreamReader(stream).ReadToEnd();
            }
        }

        /// <summary>
        /// Gets the localized label for a ribbon element.
        /// </summary>
        /// <param name="control">The control for which label will be retrieved.</param>
        /// <param name="commandManager">The command manager service instance.</param>
        /// <param name="resourceManagerAccessor">The object which provides the add-in's resource manager.</param>
        /// <returns>The retrieved label or <c>control.Id</c> if control is not localized.</returns>
        public static string GetLabel(
            IRibbonControl control,
            ICommandManager commandManager,
            IResourceManagerAccessor resourceManagerAccessor)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            if (commandManager == null)
            {
                throw new ArgumentNullException("commandManager");
            }

            if (resourceManagerAccessor == null)
            {
                throw new ArgumentNullException("resourceManagerAccessor");
            }

            IAddInCommand command = commandManager.GetCommand(control.Id.Replace("Button", string.Empty));
            if (command != null)
            {
                RegisterCommandControl(control, command);
                return command.Label;
            }

            ResourceManager resourceManager = resourceManagerAccessor.GetResourceManager();
            string label = resourceManager.GetString("Ribbon_" + control.Id + "_Label");
            if (label != null)
            {
                return label;
            }

            Trace.WriteLine("Unlocalized ribbon label: " + control.Id);
            return control.Id;
        }

        /// <summary>
        /// Loads an image for a ribbon element.
        /// </summary>
        /// <param name="resourceManagerAccessor">The object which provides the add-in's resource manager.</param>
        /// <param name="imageName">The name of the image to load.</param>
        /// <returns>The bitmap which contains the loaded image.</returns>
        public static Bitmap GetImage(IResourceManagerAccessor resourceManagerAccessor, string imageName)
        {
            if (resourceManagerAccessor == null)
            {
                throw new ArgumentNullException("resourceManagerAccessor");
            }

            ResourceManager resourceManager = resourceManagerAccessor.GetResourceManager();
            return (Bitmap)resourceManager.GetObject(imageName);
        }

        /// <summary>
        /// Gets a value indicating whether the specified control should be visible or not.
        /// </summary>
        /// <param name="control">The control which will be displayed or concealed.</param>
        /// <returns><c>true</c> if the control should be displayed; otherwise, <c>false</c>.</returns>
        public static bool GetVisible(IRibbonControl control)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            lock (CommandsMapLock)
            {
                foreach (KeyValuePair<string, string> entry in CommandsMap)
                {
                    if (entry.Value == control.Id)
                    {
                        IAddInCommand command;
                        if (Commands.TryGetValue(entry.Key, out command))
                        {
                            return command.CanExecute(null);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Registers a ribbon control associated with an <see cref="IAddInCommand"/>.
        /// </summary>
        /// <param name="control">The ribbon control.</param>
        /// <param name="command">The add-in command.</param>
        private static void RegisterCommandControl(IRibbonControl control, IAddInCommand command)
        {
            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            lock (CommandsMapLock)
            {
                string controlName;
                if (!CommandsMap.TryGetValue(command.GetType().Name, out controlName))
                {
                    CommandsMap[command.GetType().Name] = control.Id;
                    Commands[command.GetType().Name] = command;
                    command.CanExecuteChanged -= OnCommandCanExecuteChanged;
                    command.CanExecuteChanged += OnCommandCanExecuteChanged;
                    
                    // Trigger the event to set the control's initial visibility.
                    OnCommandCanExecuteChanged(command, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Handles the <see cref="AddInCommand.CanExecuteChanged"/> event.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void OnCommandCanExecuteChanged(object sender, EventArgs eventArgs)
        {
            lock (CommandsMapLock)
            {
                string controlName;
                if (CommandsMap.TryGetValue(sender.GetType().Name, out controlName))
                {
                    if (Ribbon != null)
                    {
                        Ribbon.InvalidateControl(controlName);
                    }
                }
            }
        }
    }
}