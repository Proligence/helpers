﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Common
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Reflection;

    /// <summary>
    /// Reads the add-in's configuration.
    /// </summary>
    /// <typeparam name="TConfiguration">The type which represents the add-in's configuration.</typeparam>
    public class ConfigurationProvider<TConfiguration> : IConfigurationProvider<TConfiguration>
        where TConfiguration : class, new()
    {
        /// <summary>
        /// Gets the add-in's configuration.
        /// </summary>
        /// <returns>The <see cref="TConfiguration" /> object which represents the configuration.</returns>
        public TConfiguration GetConfiguration()
        {
            var configuration = (TConfiguration)Activator.CreateInstance(typeof(TConfiguration));

            foreach (string name in ConfigurationManager.AppSettings.AllKeys)
            {
                PropertyInfo propertyInfo = typeof(TConfiguration).GetProperty(name);
                if (propertyInfo == null)
                {
                    throw new InvalidOperationException(
                        "Failed to load add-in configuration. Setting '" + name + "' is not supported.");
                }

                object value;
                if (propertyInfo.PropertyType != typeof(string))
                {
                    value = Convert.ChangeType(
                        ConfigurationManager.AppSettings[name],
                        propertyInfo.PropertyType,
                        CultureInfo.InvariantCulture);
                }
                else
                {
                    value = ConfigurationManager.AppSettings[name];
                }

                propertyInfo.SetValue(configuration, value, new object[0]);
            }

            return configuration;
        }
    }
}