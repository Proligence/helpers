﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Proxy
{
    using System;
    using Castle.DynamicProxy;

    /// <summary>
    /// Assembly helper class.
    /// </summary>
    public static class ProxyExtensions
    {
        /// <summary>
        /// The proxy builder.
        /// </summary>
        private static readonly ProxyGenerator ProxyBuilder = new ProxyGenerator();

        /// <summary>
        /// Intercepts calls to the given target, and all interfaces this one implements or inherits,
        /// with given interceptors.
        /// </summary>
        /// <param name="target">
        /// The target.
        /// </param>
        /// <param name="interceptors">
        /// The interceptors.
        /// </param>
        /// <typeparam name="T">
        /// Type of an object being proxied.
        /// </typeparam>
        /// <returns>
        /// Proxy object.
        /// </returns>
        public static T Intercept<T>(this T target, params IInterceptor[] interceptors) where T : class
        {
            if (interceptors == null)
            {
                throw new ArgumentNullException("interceptors");
            }

            var proxyType = typeof(T);

            if (!proxyType.IsInterface)
            {
                throw new ArgumentException(
                    "Target type is not an interface. Only interface types can be proxied this way.", 
                    "target");
            }

            return (T)ProxyBuilder.CreateInterfaceProxyWithTarget(
                proxyType, 
                proxyType.GetInterfaces(), 
                target, 
                interceptors);
        }

        /// <summary>
        /// Builds a proxy object that intercepts calls to all members of a given interface 
        /// and all other interfaces this one implements or inherits.
        /// </summary>
        /// <param name="target">
        /// Target type to proxy.
        /// </param>
        /// <param name="interceptors">
        /// The interceptors.
        /// </param>
        /// <typeparam name="T">
        /// Interface to proxy.
        /// </typeparam>
        /// <returns>
        /// Proxy object.
        /// </returns>
        public static T Proxy<T>(this Type target, params IInterceptor[] interceptors)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            var proxyType = typeof(T);

            if (proxyType != target)
            {
                throw new ArgumentException("Target type does not equal requested proxy type.", "target");
            }

            if (!target.IsInterface)
            {
                throw new ArgumentException(
                    "Target is not an interface. Only interface types can be proxied this way.", 
                    "target");
            }

            return (T)ProxyBuilder.CreateInterfaceProxyWithoutTarget(target, proxyType.GetInterfaces(), interceptors);
        }
    }
}
