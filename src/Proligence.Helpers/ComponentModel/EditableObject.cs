﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EditableObject.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.ComponentModel
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.CompilerServices;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// The base class for wrapper objects which implement <see cref="IEditableObject" />.
    /// </summary>
    /// <typeparam name="T">The type of the wrapped object.</typeparam>
    public class EditableObject<T> : IEditableObject, INotifyPropertyChanged
    {
        /// <summary>
        /// Indicates whether the object is currently being edited.
        /// </summary>
        private bool edited;

        /// <summary>
        /// Indicates whether the object has been changed.
        /// </summary>
        private bool hasChanges;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditableObject{T}"/> class.
        /// </summary>
        /// <param name="wrapped">The wrapped object.</param>
        protected EditableObject(T wrapped)
        {
            this.Wrapped = wrapped;
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the wrapped object.
        /// </summary>
        public T Wrapped { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the object is currently being edited.
        /// </summary>
        public bool IsEdited
        {
            get
            {
                return this.edited;
            }

            private set
            {
                this.edited = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the object has been changed.
        /// </summary>
        public bool HasChanges
        {
            get
            {
                return this.hasChanges;
            }

            private set
            {
                this.hasChanges = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the snapshot of the wrapped object, created when <see cref="BeginEdit"/> was called for the last
        /// time.
        /// </summary>
        protected T Backup { get; private set; }

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        public virtual void BeginEdit()
        {
            if (!this.IsEdited)
            {
                this.Backup = this.CreateBackupSnapshot(this.Wrapped);
                this.IsEdited = true;
            }
        }

        /// <summary>
        /// Discards changes since the last <see cref="BeginEdit" /> call.
        /// </summary>
        public virtual void CancelEdit()
        {
            if (this.IsEdited)
            {
                this.Wrapped = this.Backup;
                this.IsEdited = false;
            }
        }

        /// <summary>
        /// Pushes changes since the last <see cref="BeginEdit" /> call into the underlying object.
        /// </summary>
        public virtual void EndEdit()
        {
            if (this.IsEdited)
            {
                this.Backup = default(T);
                this.IsEdited = false;
                this.HasChanges = true;
            }
        }

        /// <summary>
        /// Creates a backup snapshot of the specified object.
        /// </summary>
        /// <param name="obj">The wrapped object which should be copied.</param>
        /// <returns>The copied object.</returns>
        protected virtual T CreateBackupSnapshot(T obj)
        {
            // ReSharper disable once CompareNonConstrainedGenericWithNull
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            return obj.MemberwiseClone();
        }

        /// <summary>
        /// Called when a property value changes.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <summary>
    /// The non-generic base class for wrapper objects which implement <see cref="IEditableObject" />.
    /// </summary>
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass",
        Justification = "Generic and non-generic class with the same name.")]
    public class EditableObject : EditableObject<object>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EditableObject"/> class.
        /// </summary>
        /// <param name="wrapped">The wrapped object.</param>
        protected EditableObject(object wrapped)
            : base(wrapped)
        {
        }
    }
}