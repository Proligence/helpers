﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MethodInfoExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Extension methods for reflected MethodInfo object.
    /// </summary>
    public static class MethodInfoExtensions
    {
        /// <summary>
        /// Delegate cache.
        /// </summary>
        private static readonly ConcurrentDictionary<string, Delegate> DelegateCache = 
            new ConcurrentDictionary<string, Delegate>();

        /// <summary>
        /// Builds a ready-to-use generic method delegate based on a provided MethodInfo 
        /// object and type parameter list. Delegate type is inferred based on provided 
        /// method return type (<c>Action</c> or <c>Func</c>) and its parameters.
        /// </summary>
        /// <param name="method">MethodInfo object to create closed generic from.</param>
        /// <param name="target">Optional target to call the method on.</param>
        /// <param name="types">List of type to use as method type parameters.</param>
        /// <returns>Method delegate as <c>Action</c> or <c>Func</c>.</returns>
        /// <remarks>
        /// Delegate is created at first use and cached.
        /// Caching makes all subsequent calls to the same method not have the reflection-related 
        /// performance impact, 
        /// provided they have the same type parameters and target.
        /// To use the generated method delegate, cast it to <c>Func</c> or <c>Action</c> with the signature 
        /// matching the generic method.
        /// </remarks>
        /// <example>
        /// <code>
        /// public class MyClass
        /// {
        ///     public Foo{T} First{T}({T} param1, string param2)
        ///     {
        ///     }
        ///     public Foo Second(IBar param1)
        ///     {
        ///         // Builds a Func{Foo{IBarBar}, IBarBar, string} delegate and casts it 
        ///         // to Func{Foo, IBar, string
        ///         var func = (Func{Foo, IBar, string)this.GetType()
        ///                         .GetMethod("First")
        ///                         .ToDelegate(this, typeof(IBarBar));
        ///         return func(param1, "SomeText");
        ///     }
        /// }
        /// public interface IBarBar { }
        /// public interface IBar : IBarBar { }
        /// public class Foo{T} : Foo { }
        /// </code>
        /// </example>
        public static Delegate ToDelegate(this MethodInfo method, object target = null, params Type[] types)
        {
            string dictKey = string.Format(
                CultureInfo.InvariantCulture,
                "{0}_{1}_{2}_{3}",
                method.DeclaringType.AssemblyQualifiedName,
                method.Name,
                string.Join("_", types.Select(t => t.AssemblyQualifiedName)),
                target != null ? target.GetHashCode().ToString(CultureInfo.InvariantCulture) : string.Empty);

            return DelegateCache.GetOrAdd(
                dictKey,
                key =>
                    {
                        MethodInfo mi = method.IsGenericMethod 
                            ? method.MakeGenericMethod(types.ToArray())
                            : method;

                        List<Type> typeArgs = mi.GetParameters()
                            .Select(p => p.ParameterType)
                            .ToList();

                        Type delegateType;

                        // Builds a delegate type
                        if (mi.ReturnType == typeof(void))
                        {
                            delegateType = Expression.GetActionType(typeArgs.ToArray());
                        }
                        else
                        {
                            typeArgs.Add(mi.ReturnType);
                            delegateType = Expression.GetFuncType(typeArgs.ToArray());
                        }

                        // Creates a binded delegate if target is supplied
                        Delegate result = (target == null)
                            ? Delegate.CreateDelegate(delegateType, mi)
                            : Delegate.CreateDelegate(delegateType, target, mi);

                        return result;
                    });
        }
    }
}
