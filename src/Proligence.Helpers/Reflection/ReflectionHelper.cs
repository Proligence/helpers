﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Implements generic reflection-related helper methods.
    /// </summary>
    public static class ReflectionHelper
    {
        /// <summary>
        /// Gets the <see cref="PropertyInfo"/> object of the property specified by a lambda expression.
        /// </summary>
        /// <typeparam name="T">The type which the property belongs to.</typeparam>
        /// <param name="expr">The lambda expresion which specifies the property.</param>
        /// <returns>The <see cref="PropertyInfo"/> object of the specified property.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static PropertyInfo GetPropertyInfo<T>(Expression<Func<T, object>> expr)
        {
            if (expr == null)
            {
                throw new ArgumentNullException(nameof(expr));
            }

            var member = expr.Body as MemberExpression;
            if (member == null)
            {
                var nodeType = expr.Body.NodeType;
                if (nodeType == ExpressionType.Convert || nodeType == ExpressionType.ConvertChecked)
                {
                    var unaryExpr = expr.Body as UnaryExpression;
                    if (unaryExpr != null)
                    {
                        var oper = unaryExpr.Operand as MemberExpression;
                        if (oper != null)
                        {
                            var propertyInfo = oper.Member as PropertyInfo;
                            if (propertyInfo != null)
                            {
                                return propertyInfo;
                            }
                        }
                    }
                }

                throw new ArgumentException(
                    "Expression '" + expr + "' refers to a method, not a property.");
            }

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException(
                    "Expression '" + expr + "' refers to a field, not a property.");
            }

            return propInfo;
        }

        /// <summary>
        /// Gets the name of the property specified by a lambda expression.
        /// </summary>
        /// <typeparam name="T">The type which the property belongs to.</typeparam>
        /// <param name="expr">The lambda expresion which specifies the property.</param>
        /// <returns>The name of the specified property.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public static string GetPropertyName<T>(Expression<Func<T, object>> expr)
        {
            return GetPropertyInfo(expr).Name;
        }
    }
}