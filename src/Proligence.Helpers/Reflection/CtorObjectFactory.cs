﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CtorObjectFactory.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Creates new objects using a parameterless constructor.
    /// </summary>
    /// <typeparam name="T">The type of objects created by the factory.</typeparam>
    public class CtorObjectFactory<T> : IObjectFactory<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CtorObjectFactory{T}"/> class.
        /// </summary>
        public CtorObjectFactory()
        {
            this.Constructor = CompileConstructorDelegate();
        }

        /// <summary>
        /// Gets or sets the delegate which will be used to create new <typeparamref name="T"/> objects.
        /// </summary>
        protected Func<T> Constructor { get; set; }

        /// <summary>
        /// Generates a dynamic delegate which calls a parameterless constructor to create a new object of the
        /// specified type.
        /// </summary>
        /// <returns>The created delegate.</returns>
        public static Func<T> CompileConstructorDelegate()
        {
            // Generate default version constructor (we assume that TDataVersion has a public parameterless ctor)
            ConstructorInfo ctor = typeof(T).GetConstructor(new Type[0]);
            if (ctor == null)
            {
                throw new InvalidOperationException(
                    "Failed to find parameterless constructor for '" + typeof(T).FullName + "' type.");
            }

            return Expression.Lambda<Func<T>>(Expression.New(ctor)).Compile();
        }

        /// <summary>
        /// Creates a new instance of the <typeparamref name="T"/> type.
        /// </summary>
        /// <returns>The created object.</returns>
        public T Create()
        {
            return this.Constructor();
        }
    }
}