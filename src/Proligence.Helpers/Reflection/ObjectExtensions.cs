﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Implements extension method for the <see cref="object"/> class.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Delegate wrapper for <see cref="object.MemberwiseClone"/> method.
        /// </summary>
        private static readonly Func<object, object> MemberwiseCloneFunc = 
            (Func<object, object>)Delegate.CreateDelegate(
                typeof(Func<object, object>),
                typeof(object).GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic));

        /// <summary>
        /// Gets the value of an object's private field.
        /// </summary>
        /// <typeparam name="T">The type of the field to get.</typeparam>
        /// <param name="obj">The object which contains the field to get.</param>
        /// <param name="name">The name of the field to get.</param>
        /// <returns>The value of the field.</returns>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "obj",
            Justification = "By design.")]
        public static T GetField<T>(this object obj, string name)
        {
            return (T)GetPrivateField(obj, name).GetValue(obj);
        }

        /// <summary>
        /// Sets a value for an object's private field.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <param name="name">The name of the field to set.</param>
        /// <param name="value">The value to set.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "obj",
            Justification = "By design.")]
        public static void SetField(this object obj, string name, object value)
        {
            GetPrivateField(obj, name).SetValue(obj, value);
        }

        /// <summary>
        /// Calls an instance method on the given object.
        /// </summary>
        /// <param name="target">The object on which the method will be called.</param>
        /// <param name="methodName">The name of the method to call.</param>
        /// <param name="parameters">The parameters which will be passed to the method.</param>
        /// <returns>The value returned by the invoked method.</returns>
        public static object CallMethod(
            this object target,
            string methodName,
            params object[] parameters)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            if (methodName == null)
            {
                throw new ArgumentNullException("methodName");
            }

            if (parameters == null)
            {
                parameters = new object[0];
            }

            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            Type[] parameterTypes = parameters.Select(p => p.GetType()).ToArray();
            MethodInfo method = target.GetType().GetMethod(methodName, bindingFlags, null, parameterTypes, null);
            if (method == null)
            {
                string message = string.Format(
                    CultureInfo.CurrentCulture,
                    "Failed to find method '{0}' on object of type '{1}'.",
                    methodName,
                    target.GetType().FullName);

                throw new ArgumentException(message, "methodName");
            }

            return method.Invoke(target, parameters);
        }

        /// <summary>
        /// Calls an instance method on the given object.
        /// </summary>
        /// <typeparam name="TResult">The type of the value returned by the method.</typeparam>
        /// <param name="target">The object on which the method will be called.</param>
        /// <param name="methodName">The name of the method to call.</param>
        /// <param name="parameters">The parameters which will be passed to the method.</param>
        /// <returns>The value returned by the invoked method.</returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter",
            Justification = "By design.")]
        public static TResult CallMethod<TResult>(
            this object target,
            string methodName,
            params object[] parameters)
        {
            return (TResult)CallMethod(target, methodName, parameters);
        }

        /// <summary>
        /// Calls a given generic method on a given target object without returning result.
        /// </summary>
        /// <param name="target">Object to invoke method on.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="genericTypeParameters">Generic type parameters in order of appearance.</param>
        /// <param name="methodParameters">Optional method parameter list.</param>
        public static void CallGeneric(
            this object target, 
            string methodName, 
            Type[] genericTypeParameters, 
            params object[] methodParameters)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target", "Target object cannot be null.");
            }

            target.CreateDelegate(methodName, genericTypeParameters).DynamicInvoke(methodParameters);
        }

        /// <summary>
        /// Calls a given generic method on a given target object with result of a given type.
        /// </summary>
        /// <typeparam name="TReturn">Type of result.</typeparam>
        /// <param name="target">Object to invoke method on.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="genericTypeParameters">Generic type parameters in order of appearance.</param>
        /// <param name="methodParameters">Optional method parameter list.</param>
        /// <returns>Method return value.</returns>
        public static TReturn CallGeneric<TReturn>(
            this object target, 
            string methodName, 
            Type[] genericTypeParameters, 
            params object[] methodParameters)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target", "Target object cannot be null.");
            }

            var func = target.CreateDelegate(methodName, genericTypeParameters);
            return (TReturn)func.DynamicInvoke(methodParameters);
        }

        /// <summary>
        /// Creates a delegate for the method with the specified and generic type parameters.
        /// Created delegate can be easily cast to appropriate <c>Func{...}</c> or <c>Action{...}</c> type.
        /// </summary>
        /// <param name="target">Object to create a method delegate on.</param>
        /// <param name="methodName">The name of the method.</param>
        /// <param name="types">The method's generic type parameters.</param>
        /// <returns>The created delegate.</returns>
        public static Delegate CreateDelegate(this object target, string methodName, params Type[] types)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target", "Target object cannot be null.");
            }

            var type = target.GetType();
            var methodInfo = type.GetMethod(
                methodName, 
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

            return methodInfo.ToDelegate(target, types);
        }

        /// <summary>
        /// Creates a shallow copy of the specified <see cref="object"/>.
        /// </summary>
        /// <typeparam name="T">The type of the cloned object.</typeparam>
        /// <param name="obj">The <see cref="object"/> which will be cloned.</param>
        /// <returns>A shallow copy of the specified <see cref="object"/>.</returns>
        public static T MemberwiseClone<T>(this T obj)
        {
            // ReSharper disable once CompareNonConstrainedGenericWithNull
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            return (T)MemberwiseCloneFunc(obj);
        }

        /// <summary>
        /// Gets the specified private field of the specified object.
        /// </summary>
        /// <param name="obj">The object which contains the field to get.</param>
        /// <param name="name">The name of the field to get.</param>
        /// <returns>The <see cref="FieldInfo"/> object of the specified field.</returns>
        private static FieldInfo GetPrivateField(object obj, string name)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            FieldInfo fieldInfo = null;
            for (Type type = obj.GetType(); type != null; type = type.BaseType)
            {
                fieldInfo = type.GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);
                if (fieldInfo != null) 
                {
                    break;
                }
            }

            if (fieldInfo == null)
            {
                throw new ArgumentException("Failed to find a field with the name '" + name + "'.", "name");
            }

            return fieldInfo;
        }
    }
}