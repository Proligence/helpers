﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Implements extension methods for the <see cref="Type"/> type.
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Creates a new instance of the specified type.
        /// </summary>
        /// <typeparam name="T">The type of value to cast the result to.</typeparam>
        /// <param name="type">The type of object to create.</param>
        /// <param name="parameters">The parameters which will be passed to the constructor.</param>
        /// <returns>The created object instance.</returns>
        public static T CreateInstance<T>(this Type type, params object[] parameters)
        {
            return (T)CreateInstance(type, parameters);
        }

        /// <summary>
        /// Creates a new instance of the specified type.
        /// </summary>
        /// <param name="type">The type of object to create.</param>
        /// <param name="parameters">The parameters which will be passed to the constructor.</param>
        /// <returns>The created object instance.</returns>
        public static object CreateInstance(this Type type, params object[] parameters)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            Type[] types;
            if (parameters != null && (parameters.Length > 0))
            {
                types = parameters.Select(p => p.GetType()).ToArray();
            }
            else
            {
                types = Type.EmptyTypes;
            }

            BindingFlags bindingFlags = 
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance;
            
            ConstructorInfo ctor = type.GetConstructor(bindingFlags, null, types, null);
            if (ctor == null)
            {
                throw new ArgumentException(
                    "Failed to find constructor with specified parameter types.", "parameters");
            }

            return ctor.Invoke(parameters);
        }

        /// <summary>
        /// Checks if a type is provided by Microsoft.
        /// </summary>
        /// <param name="type">A type to check.</param>
        /// <returns>True if a type is provided by MS.</returns>
        public static bool IsMicrosoftType(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            return type.Assembly.IsMicrosoftAssembly();
        }

        /// <summary>
        /// Returns collection of types that appear in the inheritance chain of a given collection of types.
        /// </summary>
        /// <param name="types">Types to check.</param>
        /// <returns>Collection of types.</returns>
        public static IEnumerable<Type> WithSubtypes(this IEnumerable<Type> types)
        {
            if (types == null)
            {
                throw new ArgumentNullException("types");
            }

            var stack = new Stack<Type>();
            var set = new HashSet<Type>();

            foreach (var type in types)
            {
                stack.Push(type);
                set.Add(type);
            }

            while (stack.Any())
            {
                var nextType = stack.Pop();

                if (nextType.BaseType != null)
                {
                    stack.Push(nextType.BaseType);
                    set.Add(nextType.BaseType);
                }
            }

            return set;
        }
    }
}