﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyHelpers.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Assembly helper class.
    /// </summary>
    public static class AssemblyHelpers
    {
        /// <summary>
        /// Loads assemblies located in a given folder.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <param name="searchSubdirectories">Should also search in subdirectories?</param>
        /// <returns>List of loaded assemblies.</returns>
        public static IEnumerable<Assembly> LoadAssembliesFrom(string directoryPath, bool searchSubdirectories)
        {
            string binFolder = AppDomain.CurrentDomain.BaseDirectory + directoryPath;
            IList<string> dllFiles = Directory
                .GetFiles(
                    binFolder, 
                    "*.dll", 
                    searchSubdirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                .ToList();

            IList<Assembly> binFolderAssemblies = dllFiles
                .Select(AssemblyName.GetAssemblyName)
                .Select(Assembly.Load).ToList();
            return new ReadOnlyCollection<Assembly>(binFolderAssemblies); 
        }

        /// <summary>
        /// Gets loadable types from given assembly, suppressing any exception that might happen 
        /// when loading an incorrect type.
        /// </summary>
        /// <param name="assembly">Assembly to load types from</param>
        /// <returns>List of types</returns>
        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }

            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        /// <summary>
        /// Checks if an assembly is provided by Microsoft.
        /// </summary>
        /// <param name="assembly">An assembly to check</param>
        /// <returns>True, if assembly is provided by MS.</returns>
        public static bool IsMicrosoftAssembly(this Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }

            if (assembly.FullName.StartsWith("System", StringComparison.Ordinal) 
                || assembly.FullName.StartsWith("Microsoft", StringComparison.Ordinal))
            {
                return true;
            }

            object[] attrs = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);

            return attrs.OfType<AssemblyCompanyAttribute>()
                        .Any(attr => attr.Company.Contains("Microsoft"));
        }
    }
}
