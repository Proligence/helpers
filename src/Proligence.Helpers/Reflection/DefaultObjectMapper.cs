﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultObjectMapper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using global::Autofac;

    /// <summary>
    /// Default implementation of <see cref="IObjectMapper{TSource,TDestination}"/> interface.
    /// </summary>
    /// <typeparam name="TSource">The type of the source object.</typeparam>
    /// <typeparam name="TDestination">The type of the destination object.</typeparam>
    public class DefaultObjectMapper<TSource, TDestination> : IObjectMapper<TSource, TDestination>
    {
        /// <summary>
        /// The dependency injection container context.
        /// </summary>
        private readonly IComponentContext componentContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultObjectMapper{TSource, TDestination}"/> class.
        /// </summary>
        /// <param name="componentContext">The dependency injection container context.</param>
        public DefaultObjectMapper(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
            this.BindingFlags = BindingFlags.Instance | BindingFlags.Public;
            this.TypeConverter = new TypeConverter();
            this.PropertyMappings = this.GeneratePropertyMappings();
        }

        /// <summary>
        /// Gets or sets the binding flags used to retrieve the objects' properties.
        /// </summary>
        public BindingFlags BindingFlags { get; set; }

        /// <summary>
        /// Gets or sets the type converter used to convert property values.
        /// </summary>
        public ITypeConverter TypeConverter { get; set; }

        /// <summary>
        /// Gets the mappings between properties of the <typeparamref name="TSource"/> and
        /// <typeparamref name="TDestination"/> types.
        /// </summary>
        public IList<Tuple<PropertyInfo, PropertyInfo>> PropertyMappings { get; private set; }

        /// <summary>
        /// Copies property values from the source object to the destination object.
        /// </summary>
        /// <param name="source">The source object from which data will be copied.</param>
        /// <param name="destination">The destination object to which data will be copied.</param>
        public virtual void MapProperties(TSource source, TDestination destination)
        {
            foreach (Tuple<PropertyInfo, PropertyInfo> propertyMapping in this.PropertyMappings)
            {
                object value = propertyMapping.Item1.GetValue(source, new object[0]);
                if ((value != null) && !value.GetType().IsInstanceOfType(propertyMapping.Item2.PropertyType))
                {
                    if (Attribute.IsDefined(propertyMapping.Item1, typeof(MapAsNestedAttribute)) ||
                        Attribute.IsDefined(propertyMapping.Item2, typeof(MapAsNestedAttribute)))
                    {
                        Type nestedMapperType = typeof(IObjectMapper<,>).MakeGenericType(
                            propertyMapping.Item1.PropertyType,
                            propertyMapping.Item2.PropertyType);

                        Type nestedFactoryType = typeof(IObjectFactory<>).MakeGenericType(
                            propertyMapping.Item2.PropertyType);

                        dynamic factory = this.componentContext.Resolve(nestedFactoryType);
                        dynamic nestedMapper = this.componentContext.Resolve(nestedMapperType);
                        dynamic sourceValue = value;
                        dynamic resultValue = factory.GetType().GetMethod("Create").Invoke(factory, new object[0]);
                        nestedMapper.GetType().GetMethod("MapProperties")
                            .Invoke(nestedMapper, new[] { sourceValue, resultValue });

                        value = resultValue;
                    }
                    else
                    {
                        value = this.TypeConverter.Convert(
                            value,
                            propertyMapping.Item2.PropertyType,
                            CultureInfo.CurrentCulture);
                    }
                }

                propertyMapping.Item2.SetValue(destination, value, new object[0]);
            }
        }

        /// <summary>
        /// Creates default property mappings between the data and version objects.
        /// </summary>
        /// <returns>The created mappings.</returns>
        /// <remarks>
        /// The result of this method can be overridden in subclasses by setting the <see cref="PropertyMappings"/>
        /// property in the constructor.
        /// </remarks>
        protected IList<Tuple<PropertyInfo, PropertyInfo>> GeneratePropertyMappings()
        {
            var mappings = new List<Tuple<PropertyInfo, PropertyInfo>>();

            foreach (PropertyInfo sourceProperty in typeof(TSource).GetProperties(this.BindingFlags))
            {
                if (!sourceProperty.CanRead || (sourceProperty.GetIndexParameters().Length > 0))
                {
                    continue;
                }

                PropertyInfo destProperty = typeof(TDestination).GetProperty(sourceProperty.Name, this.BindingFlags);
                if (destProperty != null)
                {
                    if (!destProperty.CanWrite || (destProperty.GetIndexParameters().Length != 0))
                    {
                        continue;
                    }

                    if (Attribute.IsDefined(sourceProperty, typeof(ExcludeFromMappingAttribute)))
                    {
                        continue;
                    }

                    if (Attribute.IsDefined(destProperty, typeof(ExcludeFromMappingAttribute)))
                    {
                        continue;
                    }

                    mappings.Add(new Tuple<PropertyInfo, PropertyInfo>(sourceProperty, destProperty));
                }
            }

            return mappings;
        }
    }
}