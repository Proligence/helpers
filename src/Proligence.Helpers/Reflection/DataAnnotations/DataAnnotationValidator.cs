﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataAnnotationValidator.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Reflection.DataAnnotations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Implements a utility class to validate data annotation attributes.
    /// </summary>
    public class DataAnnotationValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataAnnotationValidator"/> class.
        /// </summary>
        public DataAnnotationValidator()
        {
            this.RecursiveMode = true;
            this.BindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            
            this.IgnoredAssemblies = new List<Assembly>();
            this.IgnoreAssemblies("System.");
        }

        /// <summary>
        /// Gets or sets a value indicating whether subobjects will be validated recursively.
        /// </summary>
        public bool RecursiveMode { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="BindingFlags"/> to use to discover subobjects during validation.
        /// </summary>
        public BindingFlags BindingFlags { get; set; }

        /// <summary>
        /// Gets the list of assemblies from which types will not be validated when processing subobjects.
        /// </summary>
        public IList<Assembly> IgnoredAssemblies { get; private set; }

        /// <summary>
        /// Validates data annotation attributes for the specified object and throws a
        /// <see cref="ValidationException"/> in case of any detected errors.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        public void ValidateObject(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            
            var validationContext = new ValidationContext(obj, null, null);
            Validator.ValidateObject(obj, validationContext, true);

            if (this.RecursiveMode)
            {
                var visited = new HashSet<object> { obj };
                this.ValidateSubobjects(obj, visited);
            }
        }

        /// <summary>
        /// Validates data annotation attributes for the specified object.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        /// <returns>The detected validation errors.</returns>
        public IEnumerable<ValidationResult> TryValidateObject(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            var context = new ValidationContext(obj, null, null);
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(obj, context, results, true);

            if (this.RecursiveMode)
            {
                var visited = new HashSet<object> { obj };
                this.TryValidateSubobjects(obj, results, visited);
            }

            return results;
        }

        /// <summary>
        /// Adds assemblies with the specified name to the <see cref="IgnoredAssemblies"/> list.
        /// </summary>
        /// <param name="assemblyName">The full or partial name of the assembly to ignore.</param>
        public void IgnoreAssemblies(string assemblyName)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(
                a => a.FullName.StartsWith(assemblyName, StringComparison.Ordinal));
            
            foreach (Assembly assembly in assemblies)
            {
                if (!this.IgnoredAssemblies.Contains(assembly))
                {
                    this.IgnoredAssemblies.Add(assembly);
                }
            }
        }

        /// <summary>
        /// Executes the specified action for each subobject of the specified object.
        /// </summary>
        /// <param name="obj">The object instance.</param>
        /// <param name="action">The action to invoke.</param>
        protected virtual void ForEachSubobject(object obj, Action<object> action)
        {
            if (this.IgnoredAssemblies.Contains(obj.GetType().Assembly))
            {
                return;
            }

            foreach (PropertyInfo propertyInfo in obj.GetType().GetProperties(this.BindingFlags))
            {
                if (!propertyInfo.CanRead)
                {
                    continue;
                }

                if (propertyInfo.GetIndexParameters().Length > 0)
                {
                    continue;
                }

                object subobject = propertyInfo.GetValue(obj, new object[0]);
                if (subobject != null)
                {
                    action(subobject);
                }
            }
        }

        /// <summary>
        /// Validates data annotation attributes for the subobjects of the specified object and throws a
        /// <see cref="ValidationException"/> in case of any detected errors.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        /// <param name="visited">Set of visited objects.</param>
        private void ValidateSubobjects(object obj, ISet<object> visited)
        {
            this.ForEachSubobject(
                obj,
                x =>
                {
                    if (!visited.Contains(x))
                    {
                        var context = new ValidationContext(x, null, null);
                        Validator.ValidateObject(x, context, true);
                        visited.Add(x);

                        this.ValidateSubobjects(x, visited);
                    }
                });
        }

        /// <summary>
        /// Validates data annotation attributes for the subobjects of the specified object.
        /// </summary>
        /// <param name="obj">The object to validate.</param>
        /// <param name="results">List which will be populated with validation errors.</param>
        /// <param name="visited">Set of visited objects.</param>
        private void TryValidateSubobjects(object obj, ICollection<ValidationResult> results, ISet<object> visited)
        {
            this.ForEachSubobject(
                obj,
                x =>
                {
                    if (!visited.Contains(x))
                    {
                        var context = new ValidationContext(x, null, null);
                        Validator.TryValidateObject(x, context, results, true);
                        visited.Add(x);

                        this.TryValidateSubobjects(x, results, visited);
                    }
                });
        }
    }
}