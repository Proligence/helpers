﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NetworkExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Network
{
    using System;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;

    /// <summary>
    /// Extension methods for UDP network operations.
    /// </summary>
    public static class NetworkExtensions
    {
        /// <summary>
        /// Dictionary of endpoint listeners.
        /// </summary>
        private static readonly ConcurrentDictionary<string, UdpClient> Clients = 
            new ConcurrentDictionary<string, UdpClient>();

        /// <summary>
        /// Creates and starts an UDP listener on the given endpoint.
        /// </summary>
        /// <param name="localEndpoint">
        /// The local endpoint to listen on.
        /// </param>
        /// <param name="packetProcessor">
        /// Action for processing a single packet.
        /// </param>
        /// <param name="enableBroadcast">
        /// Listen for broadcast packets.
        /// </param>
        public static void ListenStart(
            this IPEndPoint localEndpoint, 
            Action<byte[]> packetProcessor, 
            bool enableBroadcast = false)
        {
            if (localEndpoint == null)
            {
                throw new ArgumentNullException("localEndpoint");
            }

            var udpServer = new UdpClient(localEndpoint)
                                {
                EnableBroadcast = enableBroadcast
            };

            try
            {
                var state = new UdpState
                {
                    Client = udpServer, 
                    Endpoint = localEndpoint, 
                    Processor = packetProcessor
                };
                udpServer.BeginReceive(ReceiveCallback, state);

                Clients.AddOrUpdate(
                    localEndpoint.ToString(), 
                    key => udpServer, 
                    (key, value) =>
                    {
                        value.Close();
                        return udpServer;
                    });
            }
            catch
            {
                udpServer.Close();
                throw;
            }
        }

        /// <summary>
        /// Stop listening for packets.
        /// </summary>
        /// <param name="localEndpoint">
        /// Local endpoint.
        /// </param>
        /// <returns>
        /// True if stopping succeeded, false otherwise or if listener hasn't been started.
        /// </returns>
        public static bool ListenStop(this IPEndPoint localEndpoint)
        {
            if (localEndpoint == null)
            {
                throw new ArgumentNullException("localEndpoint");
            }

            UdpClient client;
            if (Clients.TryRemove(localEndpoint.ToString(), out client))
            {
                client.Close();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Serializes and sends given object to given network endpoint.
        /// </summary>
        /// <param name="objectToSend">
        /// The object to send.
        /// </param>
        /// <param name="remoteEndpoint">
        /// The remote endpoint.
        /// </param>
        /// <typeparam name="T">
        /// Type of serialized object.
        /// </typeparam>
        /// <returns>
        /// Number of bytes sent.
        /// </returns>
        public static int Send<T>(this T objectToSend, IPEndPoint remoteEndpoint)
        {
            var localReceiveEndPoint = new IPEndPoint(IPAddress.Any, 0);
            using (var udpServer = new UdpClient(localReceiveEndPoint))
            {
                using (var memorystream = new MemoryStream())
                {
                    var bf = new BinaryFormatter();
                    bf.Serialize(memorystream, objectToSend);
                    var bytes = memorystream.ToArray();
                    return udpServer.Send(bytes, bytes.Length, remoteEndpoint);
                }
            }
        }

        /// <summary>
        /// Converts a given item to string and sends this representation over the wire to given endpoint.
        /// </summary>
        /// <param name="objectToSend">
        /// The object to send.
        /// </param>
        /// <param name="remoteEndpoint">
        /// The remote endpoint.
        /// </param>
        /// <typeparam name="T">
        /// Type of the object to send.
        /// </typeparam>
        /// <returns>
        /// Number of bytes sent.
        /// </returns>
        public static int SendAsString<T>(this T objectToSend, IPEndPoint remoteEndpoint)
        {
            var localReceiveEndPoint = new IPEndPoint(IPAddress.Any, 0);
            using (var udpServer = new UdpClient(localReceiveEndPoint))
            {
                var bytes = Encoding.UTF8.GetBytes(objectToSend.ToString());
                return udpServer.Send(bytes, bytes.Length, remoteEndpoint);
            }
        }

        /// <summary>
        /// Receives the discovery request.
        /// </summary>
        /// <param name="ar">The async result.</param>
        private static void ReceiveCallback(IAsyncResult ar)
        {
            if (ar == null)
            {
                return;
            }

            var state = (UdpState)ar.AsyncState;
            var endpoint = state.Endpoint;

            try
            {
                var response = state.Client.EndReceive(ar, ref endpoint);
                state.Client.BeginReceive(ReceiveCallback, state);
                state.Processor(response);
            }
            catch (SocketException ex)
            {
                Debug.WriteLine(ex.Message);

                // Timeout has been reached
                if (ex.SocketErrorCode != SocketError.TimedOut)
                {
                    throw;
                }
            }
            catch (ObjectDisposedException ex)
            {
                // Need to catch and suppress this as needed by the (dumb) design of socket operations in .NET
                // Reference: http://stackoverflow.com/questions/4662553/how-to-abort-sockets-beginreceive
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Holds data for async UDP receive operation.
        /// </summary>
        internal class UdpState
        {
            /// <summary>
            /// Gets or sets Endpoint.
            /// </summary>
            public IPEndPoint Endpoint { get; set; }

            /// <summary>
            /// Gets or sets the underlying client.
            /// </summary>
            public UdpClient Client { get; set; }

            /// <summary>
            /// Gets or sets the packet processing action.
            /// </summary>
            public Action<byte[]> Processor { get; set; }
        }
    }
}