﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebClient.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Network
{
    using System;
    using System.Net;

    /// <summary>
    /// Extends the functionality of the standard <see cref="System.Net.WebClient"/>.
    /// </summary>
    public class WebClient : System.Net.WebClient
    {
        /// <summary>
        /// The last web request.
        /// </summary>
        private WebRequest request;

        /// <summary>
        /// Gets the status code of the last response.
        /// </summary>
        public HttpStatusCode? StatusCode
        {
            get
            {
                if (this.request != null)
                {
                    var response = GetWebResponse(this.request) as HttpWebResponse;
                    if (response != null)
                    {
                        return response.StatusCode;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Returns a <see cref="WebRequest" /> object for the specified resource.
        /// </summary>
        /// <param name="address">A <see cref="Uri" /> that identifies the resource to request.</param>
        /// <returns>A new <see cref="WebRequest" /> object for the specified resource.</returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            this.request = base.GetWebRequest(address);

            var webRequest = this.request as HttpWebRequest;
            if (webRequest != null)
            {
                webRequest.AllowAutoRedirect = false;
            }

            return this.request;
        }
    }
}