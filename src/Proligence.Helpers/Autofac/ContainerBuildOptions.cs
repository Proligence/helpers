﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContainerBuildOptions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Autofac
{
    using System;

    /// <summary>
    /// Represents options used by the <c>AppDomainExtensions.BuildIocContainer</c> method.
    /// </summary>
    public sealed class ContainerBuildOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerBuildOptions"/> class.
        /// </summary>
        public ContainerBuildOptions()
        {
            this.RegisterAssemblyTypes = true;
            this.RegisterAssemblyModules = true;
        }

        /// <summary>
        /// Gets or sets a value indicating whether all types in the assemblies of the specified
        /// <see cref="AppDomain"/> will be registered in the container.
        /// </summary>
        public bool RegisterAssemblyTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Autofac modules in the assemblies of the specified
        /// <see cref="AppDomain"/> will be registered in the container.
        /// </summary>
        public bool RegisterAssemblyModules { get; set; }
    }
}