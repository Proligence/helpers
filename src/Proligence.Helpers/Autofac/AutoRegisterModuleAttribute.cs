﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AutoRegisterModuleAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Autofac
{
    using System;

    /// <summary>
    /// Registers the class as a service in Autofac.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class AutoRegisterModuleAttribute : Attribute
    {
        /// <summary>
        /// Indicates whether the Autofac module should be registered in the dependency injection container.
        /// </summary>
        private readonly bool register;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoRegisterModuleAttribute" /> class.
        /// </summary>
        /// <param name="register">
        /// If set to <c>true</c> the module will be automatically registered in the dependency injection container.
        /// </param>
        public AutoRegisterModuleAttribute(bool register)
        {
            this.register = register;
        }

        /// <summary>
        /// Gets a value indicating whether the Autofac module should be registered in the dependency injection
        /// container.
        /// </summary>
        public bool Register
        {
            get { return this.register; }
        }
    }
}