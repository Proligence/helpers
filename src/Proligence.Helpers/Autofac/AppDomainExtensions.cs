﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppDomainExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Autofac
{
    using System;
    using System.Reflection;
    using global::Autofac;

    /// <summary>
    /// Implements extension helper methods for the <see cref="AppDomain"/> class.
    /// </summary>
    public static class AppDomainExtensions
    {
        /// <summary>
        /// Builds an Autofac IoC container for all types and modules in the specified <see cref="AppDomain"/>.
        /// </summary>
        /// <param name="appDomain">
        /// The <see cref="AppDomain"/> from which types and modules will be registered with Autofac.
        /// </param>
        /// <param name="builder">Handler for additional container configuration.</param>
        /// <returns>The created IoC container.</returns>
        public static IContainer BuildIocContainer(this AppDomain appDomain, Action<ContainerBuilder> builder = null)
        {
            return appDomain.BuildIocContainer(new ContainerBuildOptions(), builder);
        }

        /// <summary>
        /// Builds an Autofac IoC container for all types and modules in the specified <see cref="AppDomain"/>.
        /// </summary>
        /// <param name="appDomain">
        /// The <see cref="AppDomain"/> from which types and modules will be registered with Autofac.
        /// </param>
        /// <param name="options">Specifies additional options for building dependency injection container.</param>
        /// <param name="builder">Handler for additional container configuration.</param>
        /// <returns>The created IoC container.</returns>
        public static IContainer BuildIocContainer(
            this AppDomain appDomain,
            ContainerBuildOptions options,
            Action<ContainerBuilder> builder = null)
        {
            if (appDomain == null)
            {
                throw new ArgumentNullException("appDomain");
            }

            if (options == null)
            {
                options = new ContainerBuildOptions();
            }

            var containerBuilder = new ContainerBuilder();

            Assembly[] assemblies = appDomain.GetAssemblies();

            // Register all types from the current AppDomain.
            if (options.RegisterAssemblyTypes)
            {
                containerBuilder.RegisterAssemblyTypes(assemblies);
            }

            // Register types with Dependency attribute.
            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.GetTypes())
                {
                    var attribute = (DependencyAttribute)Attribute.GetCustomAttribute(
                        type,
                        typeof(DependencyAttribute));

                    if (attribute != null)
                    {
                        var entry = containerBuilder.RegisterType(type).As(attribute.InterfaceType);

                        switch (attribute.DependencyType)
                        {
                            case DependencyType.PerDependency:
                                entry.InstancePerDependency();
                                break;

                            case DependencyType.SingleInstance:
                                entry.SingleInstance();
                                break;

                            case DependencyType.PerLifetimeScope:
                                entry.InstancePerLifetimeScope();
                                break;
                        }
                    }
                }
            }

            // Register Autofac modules.
            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.IsSubclassOf(typeof(global::Autofac.Module)))
                    {
                        var attribute = (AutoRegisterModuleAttribute)Attribute.GetCustomAttribute(
                            type,
                            typeof(AutoRegisterModuleAttribute));

                        if ((attribute != null && attribute.Register) || options.RegisterAssemblyModules)
                        {
                            containerBuilder.RegisterModule((global::Autofac.Module)Activator.CreateInstance(type));
                        }
                    }
                }
            }

            // Allow container customization.
            if (builder != null)
            {
                builder(containerBuilder);
            }

            return containerBuilder.Build();
        }
    }
}