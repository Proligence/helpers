﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DependencyType.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Autofac
{
    /// <summary>
    /// Defines valid type of Autofac dependencies.
    /// </summary>
    public enum DependencyType
    {
        /// <summary>
        /// Also called 'transient' or 'factory' in other containers. Using per-dependency scope, a unique instance 
        /// will be returned from each request for a service.
        /// </summary>
        PerDependency,

        /// <summary>
        /// Using single instance scope, one instance is returned from all requests in the parent and all nested 
        /// containers.
        /// </summary>
        SingleInstance,

        /// <summary>
        /// This scope applies to nested lifetimes. A component with per-lifetime scope will have at most a single 
        /// instance per nested lifetime scope.
        /// </summary>
        PerLifetimeScope
    }
}