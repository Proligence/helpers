﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentContextExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Autofac
{
    using System;
    using System.Linq;
    using System.Reflection;
    using global::Autofac;
    using global::Autofac.Core;

    /// <summary>
    /// Implements extension methods for the <see cref="IComponentContext"/> interface.
    /// </summary>
    public static class ComponentContextExtensions
    {
        /// <summary>
        /// Resolves the specified type from the container passing the specified parameters.
        /// </summary>
        /// <typeparam name="T">The type to resolve from the container.</typeparam>
        /// <param name="ctx">The container instance.</param>
        /// <param name="parameters">
        /// An anonymous object which specifies the parameters for the resolved object.
        /// </param>
        /// <returns>The resolved object. </returns>
        public static T Resolve<T>(this IComponentContext ctx, object parameters)
        {
            if (ctx == null)
            {
                throw new ArgumentNullException("ctx");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            Parameter[] resolveParameters = parameters
                .GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Select(pi => new NamedParameter(pi.Name, pi.GetValue(parameters, new object[0])))
                .Cast<Parameter>()
                .ToArray();

            return ResolutionExtensions.Resolve<T>(ctx, resolveParameters);
        }
    }
}