﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MinimumFullHeap.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Collections
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Implements a minimum priority queue using an array-based binary heap.
    /// </summary>
    /// <typeparam name="T">The type of values stored in the heap.</typeparam>
    public class MinimumFullHeap<T> : FullHeap<T> where T : IComparable<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MinimumFullHeap{T}"/> class.
        /// </summary>
        public MinimumFullHeap()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MinimumFullHeap{T}"/> class.
        /// </summary>
        /// <param name="items">The items which will be add to the heap.</param>
        public MinimumFullHeap(IEnumerable<T> items)
            : base(items)
        {
        }

        /// <summary>
        /// Gets the minimum item in the heap. The heap must not be empty for this operation.
        /// </summary>
        /// <value>The minimum item in the heap.</value>
        public T Minimum
        {
            get
            {
                return this.Top();
            }
        }

        /// <summary>
        /// Deletes and returns the minimum item stored in the heap. The heap must not be empty for this operation.
        /// </summary>
        /// <returns>The value of the deleted item.</returns>
        public T ExtractMinimum()
        {
            return this.Extract();
        }

        /// <summary>
        /// Decreases the value of a specified item in the heap.
        /// </summary>
        /// <param name="index">The index of the node to decrease.</param>
        /// <param name="value">The node's new value.</param>
        /// <remarks>
        /// If <paramref name="value"/> is less then the current value of the node indexed <paramref name="index"/>, 
        /// then the method has no effect.
        /// </remarks>
        public void DecreaseItem(int index, T value)
        {
            this.Alter(index, value);
        }

        /// <summary>
        /// Compares two items of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="item">The first item.</param>
        /// <param name="other">The second item.</param>
        /// <returns><c>true</c> if the first item should go higher in the heap, otherwise <c>false</c>.</returns>
        protected override bool Compare(T item, T other)
        {
            return item.CompareTo(other) < 0;
        }
    }
}