﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullHeap.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    /// <summary>
    /// The base class for priority queues implemented as an array-based binary heap.
    /// </summary>
    /// <typeparam name="T">The type of elements stored in the heap.</typeparam>
    public abstract class FullHeap<T> where T : IComparable<T>
    {
        /// <summary>
        /// The data buffer which contains the heap's items.
        /// </summary>
        private T[] buffer;

        /// <summary>
        /// Initializes a new instance of the <see cref="FullHeap{T}"/> class.
        /// </summary>
        protected FullHeap()
        {
            this.buffer = new T[0];
            this.Count = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FullHeap{T}"/> class.
        /// </summary>
        /// <param name="items">The items which will be add to the heap.</param>
        protected FullHeap(IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            T[] arr = items as T[];
            if (arr == null)
            {
                arr = items.ToArray();
            }

            this.buffer = arr;
            this.Count = arr.Length;

            for (int i = (this.Count / 2) - 1; i >= 0; i--)
            {
                this.DownHeap(i);
            }
        }

        /// <summary>
        /// Gets the total number of items stored in the heap.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Adds the specified item to the heap.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void Add(T item)
        {
            if (ReferenceEquals(item, null))
            {
                throw new ArgumentNullException("item");
            }

            if (this.buffer.Length <= this.Count)
            {
                int baseLength = this.buffer.Length > 0 ? this.buffer.Length : 1;
                this.AllocateMoreMemory(2 * baseLength);
            }

            this.buffer[this.Count] = item;
            this.UpHeap(this.Count);
            this.Count++;
        }

        /// <summary>
        /// Gets the item on the top of the heap. The heap must not be empty for this operation.
        /// </summary>
        /// <returns>The item on the top of the heap.</returns>
        /// <exception cref="System.InvalidOperationException">The heap is empty.</exception>
        public T Top()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException("The heap is empty.");
            }

            return this.buffer[0];
        }

        /// <summary>
        /// Deletes the topmost item stored in the heap. The heap must not be empty for this operation.
        /// </summary>
        /// <returns>The deleted item.</returns>
        public T Extract()
        {
            if (this.Count == 0)
            {
                throw new InvalidOperationException("The heap is empty.");
            }

            T result = this.buffer[0];
            this.buffer[0] = this.buffer[--this.Count];
            this.DownHeap(0);

            return result;
        }

        /// <summary>
        /// Gets the index of the specified item.
        /// </summary>
        /// <param name="item">The item which index will be get.</param>
        /// <returns>The item's index or <c>-1</c> if the item is not stored in the heap.</returns>
        public int IndexOf(T item)
        {
            if (ReferenceEquals(item, null))
            {
                throw new ArgumentNullException("item");
            }

            for (int i = 0; i < this.Count; i++)
            {
                if (this.buffer[i].Equals(item))
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets all items stored in the heap.
        /// </summary>
        /// <returns>A sequence of items stored in the heap.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "By design.")]
        public IEnumerable<T> GetAllItems()
        {
            return this.buffer.Take(this.Count).ToArray();
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if ((obj == null) || (obj.GetType() != this.GetType()))
            {
                return false;
            }

            FullHeap<T> other = (FullHeap<T>)obj;
            
            if (other.Count != this.Count)
            {
                return false;
            }

            for (int i = 0; i < this.Count; i++)
            {
                if (!this.buffer[i].Equals(other.buffer[i]))
                {
                    return false;
                }
            }
            
            return true;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash 
        /// table. 
        /// </returns>
        public override int GetHashCode()
        {
            int result = 0;
            for (int i = 0; i < this.Count; i++)
            {
                result ^= this.buffer[i].GetHashCode();
            }

            return result;
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents this instance.
        /// </summary><returns>A <see cref="string"/> that represents this instance.</returns>
        public override string ToString()
        {
            if (this.Count == 0)
            {
                return "(empty)";
            }

            return this.Top().ToString();
        }

        /// <summary>
        /// Alters the value of the item with the specified index.
        /// </summary>
        /// <param name="index">The index of the item to alter.</param>
        /// <param name="value">The new item value.</param>
        protected void Alter(int index, T value)
        {
            int n = index;
            if (n >= this.buffer.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            if (this.Compare(this.buffer[n], value))
            {
                return;
            }

            this.buffer[n] = value;
            this.UpHeap(n);
        }

        /// <summary>
        /// Compares two items of type <typeparamref name="T"/>.
        /// </summary>
        /// <param name="item">The first item.</param>
        /// <param name="other">The second item.</param>
        /// <returns><c>true</c> if the first item should go higher in the heap, otherwise <c>false</c>.</returns>
        protected abstract bool Compare(T item, T other);

        /// <summary>
        /// Fixes the heap starting from a specified node, moving up.
        /// </summary>
        /// <param name="index">The index of the initial node.</param>
        [SuppressMessage("Microsoft.Usage", "CA2233:OperationsShouldNotOverflow", MessageId = "index-1",
            Justification = "It's ok here.")]
        private void UpHeap(int index)
        {
            T x = this.buffer[index];
            int fatherIndex = (index - 1) / 2;
            
            while ((fatherIndex >= 0) && this.Compare(x, this.buffer[fatherIndex]))
            {
                T temp = this.buffer[index];
                this.buffer[index] = this.buffer[fatherIndex];
                this.buffer[fatherIndex] = temp;

                index = fatherIndex;
                fatherIndex = (fatherIndex - 1) / 2;
            }
        }

        /// <summary>
        /// Fixes the heap starting from a specified node, moving down.
        /// </summary>
        /// <param name="index">The index of the initial node.</param>
        [SuppressMessage("Microsoft.Usage", "CA2233:OperationsShouldNotOverflow", MessageId = "2*index",
            Justification = "It's ok here.")]
        private void DownHeap(int index)
        {
            T x = this.buffer[index];
            int childIndex = (2 * index) + 1;

            while (childIndex <= (this.Count - 1))
            {
                if ((childIndex + 1) <= (this.Count - 1))
                {
                    if (this.Compare(this.buffer[childIndex + 1], this.buffer[childIndex]))
                    {
                        childIndex++;
                    }
                }

                if (this.Compare(x, this.buffer[childIndex]))
                {
                    break;
                }

                T temp = this.buffer[index];
                this.buffer[index] = this.buffer[childIndex];
                this.buffer[childIndex] = temp;

                index = childIndex;
                childIndex = (2 * childIndex) + 1;
            }
        }

        /// <summary>
        /// Allocates a larger data buffer and copies all items to it.
        /// </summary>
        /// <param name="newSize">The size of the new data buffer.</param>
        /// <remarks>
        /// <paramref name="newSize"/> must be larger then the current buffer size.
        /// </remarks>
        private void AllocateMoreMemory(int newSize)
        {
            if (newSize <= this.buffer.Length)
            {
                throw new InvalidOperationException(
                    "The new buffer size must be larger then the current buffer size.");
            }

            T[] newData = new T[newSize];
            for (int i = 0; i < this.buffer.Length; i++)
            {
                newData[i] = this.buffer[i];
            }

            this.buffer = newData;
        }
    }
}