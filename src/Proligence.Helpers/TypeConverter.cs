﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeConverter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Implements the type conversion utility.
    /// </summary>
    public class TypeConverter : ITypeConverter
    {
        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <typeparam name="TIn">The type of the input value.</typeparam>
        /// <typeparam name="TOut">The type of the output value.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public static TOut ChangeType<TIn, TOut>(TIn value)
        {
            return new TypeConverter().Convert<TIn, TOut>(value);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <typeparam name="TIn">The type of the input value.</typeparam>
        /// <typeparam name="TOut">The type of the output value.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The converted value.</returns>
        public static TOut ChangeType<TIn, TOut>(TIn value, IFormatProvider provider)
        {
            return new TypeConverter().Convert<TIn, TOut>(value, provider);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="conversionType">type of the output value.</param>
        /// <returns>The converted value.</returns>
        public static object ChangeType(object value, Type conversionType)
        {
            return new TypeConverter().Convert(value, conversionType);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="conversionType">type of the output value.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The converted value.</returns>
        public static object ChangeType(object value, Type conversionType, IFormatProvider provider)
        {
            return new TypeConverter().Convert(value, conversionType, provider);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <typeparam name="TIn">The type of the input value.</typeparam>
        /// <typeparam name="TOut">The type of the output value.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <returns>The converted value.</returns>
        public TOut Convert<TIn, TOut>(TIn value)
        {
            return this.Convert<TIn, TOut>(value, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <typeparam name="TIn">The type of the input value.</typeparam>
        /// <typeparam name="TOut">The type of the output value.</typeparam>
        /// <param name="value">The value to convert.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The converted value.</returns>
        public TOut Convert<TIn, TOut>(TIn value, IFormatProvider provider)
        {
            if (ReferenceEquals(value, null))
            {
                return default(TOut);
            }

            Type conversionType = typeof(TOut);
            if (conversionType.IsInstanceOfType(value))
            {
                return (TOut)(object)value;
            }

            if (conversionType.IsEnum)
            {
                string stringValue = value as string;
                if (stringValue != null)
                {
                    return (TOut)Enum.Parse(conversionType, stringValue);
                }

                return (TOut)Enum.ToObject(conversionType, value);
            }

            return (TOut)System.Convert.ChangeType(value, conversionType, provider);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="conversionType">type of the output value.</param>
        /// <returns>The converted value.</returns>
        public object Convert(object value, Type conversionType)
        {
            return this.Convert(value, conversionType, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts a value to the specified type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="conversionType">type of the output value.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>The converted value.</returns>
        public object Convert(object value, Type conversionType, IFormatProvider provider)
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            }

            if (value == null)
            {
                return conversionType.IsValueType ? Activator.CreateInstance(conversionType) : null;
            }

            if (conversionType.IsGenericType && conversionType.IsValueType)
            {
                Type nullableUnderlyingType = Nullable.GetUnderlyingType(conversionType);
                if (nullableUnderlyingType != null)
                {
                    conversionType = nullableUnderlyingType;
                }
            }

            if (conversionType.IsInstanceOfType(value))
            {
                return value;
            }

            if (conversionType.IsEnum)
            {
                string stringValue = value as string;
                if (stringValue != null)
                {
                    return Enum.Parse(conversionType, stringValue);
                }

                return Enum.ToObject(conversionType, value);
            }

            return System.Convert.ChangeType(value, conversionType, provider);
        }
    }
}