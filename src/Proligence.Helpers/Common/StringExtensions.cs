﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Web.UI;

    /// <summary>
    /// Implements extension methods for the <see cref="string"/> class.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Extends String.Format to use with any object and property named parameters.
        /// </summary>
        /// <param name="format">The template.</param>
        /// <param name="data">The data.</param>
        /// <param name="provider">The culture-specific settings which will be used to format the string.</param>
        /// <returns>Formatted string</returns>
        /// <exception cref="System.ArgumentNullException">When source string or passed data are null.</exception>
        /// <remarks>Based on James Newton-King blog post <c>http://james.newtonking.com</c></remarks>
        public static string FormatWithObject(this string format, object data, IFormatProvider provider = null)
        {
            if (string.IsNullOrEmpty(format))
            {
                throw new ArgumentNullException("format");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var regex = new Regex(
                @"(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",
                RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            var values = new List<object>();
            var rewrittenTemplate = regex.Replace(
                format,
                delegate(Match m)
                    {
                        var start = m.Groups["start"];
                        var property = m.Groups["property"];
                        var formatGroup = m.Groups["format"];
                        var end = m.Groups["end"];
                        values.Add((property.Value == "0") ? data : DataBinder.Eval(data, property.Value));
                        return new string('{', start.Captures.Count) + (values.Count - 1) + formatGroup.Value
                               + new string('}', end.Captures.Count);
                    });

            return string.Format(provider ?? CultureInfo.CurrentCulture, rewrittenTemplate, values.ToArray());
        }

        /// <summary>
        /// Computes Damerau-Levenshtein edit distance between two strings.
        /// </summary>
        /// <param name="source">
        /// Original string to compute distance for.
        /// </param>
        /// <param name="other">
        /// Other string to compute distance to.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// Edit distance between two provided strings.
        /// </returns>
        [SuppressMessage(
            "StyleCop.CSharp.DocumentationRules", 
            "SA1650:ElementDocumentationMustBeSpelledCorrectly", 
            Justification = "Reviewed. Suppression is OK here.")]
        public static int DistanceTo(this string source, string other)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "Source string cannot be null.");
            }

            if (other == null)
            {
                throw new ArgumentNullException("other", "Destination string cannot be null.");
            }

            if (source == other)
            {
                return 0;
            }

            var lengthSource = source.Length;
            var lengthOther = other.Length;

            if (lengthSource == 0)
            {
                return lengthOther;
            }

            if (lengthOther == 0)
            {
                return lengthSource;
            }

            var matrix = new int[lengthSource + 1][];
            matrix[0] = new int[lengthOther + 1];

            for (var i = 1; i <= lengthSource; i++)
            {
                matrix[i] = new int[lengthOther + 1];

                matrix[i][0] = i;
                for (var j = 1; j <= lengthOther; j++)
                {
                    var cost = other[j - 1] == source[i - 1] ? 0 : 1;
                    if (i == 1)
                    {
                        matrix[0][j] = j;
                    }

                    matrix[i][j] = 
                        Math.Min(matrix[i - 1][j] + 1, Math.Min(matrix[i][j - 1] + 1, matrix[i - 1][j - 1] + cost));

                    if (i > 1 
                        && j > 1 
                        && source[i - 1] == other[j - 2] 
                        && source[i - 2] == other[j - 1])
                    {
                        matrix[i][j] = Math.Min(matrix[i][j], matrix[i - 2][j - 2] + cost);
                    }
                }
            }

            return matrix[lengthSource][lengthOther];
        }
    }
}
