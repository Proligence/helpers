// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RandomExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Common
{
    using System;

    /// <summary>
    /// Extensions for the <see cref="Random"/> class.
    /// </summary>
    public static class RandomExtensions
    {
        /// <summary>
        /// Returns a non-negative random long integer.
        /// </summary>
        /// <param name="random">
        /// The random number generator.
        /// </param>
        /// <param name="minValue">
        /// The min value.
        /// </param>
        /// <param name="maxValue">
        /// The max value.
        /// </param>
        /// <returns>
        /// Long integer value.
        /// </returns>
        public static long NextInt64(this Random random, long minValue = 0, long maxValue = long.MaxValue)
        {
            if (random == null)
            {
                throw new ArgumentNullException("random");
            }

            if (minValue < 0)
            {
                throw new ArgumentException("Minimum value cannot be negative", "minValue");
            }

            if (maxValue < 0)
            {
                throw new ArgumentException("Maximum value cannot be negative", "maxValue");
            }

            if (maxValue < minValue)
            {
                throw new ArgumentException("Maximum value has to be greater or equal minimum value.", "maxValue");
            }

            var range = random.NextDouble();
            var max = maxValue - minValue;
            return (long)((range * max) + minValue);
        }
    }
}