﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeadlineTaskCell.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Scheduling
{
    using System;

    /// <summary>
    /// Wrapper class to make <see cref="IDeadlineTask"/> objects comparable by their deadlines.
    /// </summary>
    /// <typeparam name="TTask">The type of the wrapped task.</typeparam>
    internal class DeadlineTaskCell<TTask> : IComparable<DeadlineTaskCell<TTask>>
        where TTask : class, IDeadlineTask
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlineTaskCell&lt;TTask&gt;"/> class.
        /// </summary>
        /// <param name="task">The task object.</param>
        public DeadlineTaskCell(TTask task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            this.Task = task;
        }

        /// <summary>
        /// Gets the task object contained in this <see cref="DeadlineTaskCell{TTask}"/>.
        /// </summary>
        public TTask Task { get; private set; }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. 
        /// The return value has the following meanings: 
        /// Less than zero - This object is less than the other parameter
        /// Zero - This object is equal to other. 
        /// Greater than zero - This object is greater than other.
        /// </returns>
        public int CompareTo(DeadlineTaskCell<TTask> other)
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }

            return this.Task.Deadline.CompareTo(other.Task.Deadline);
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as DeadlineTaskCell<TTask>;
            if (other == null)
            {
                return false;
            }

            return other.Task.Equals(this.Task);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash
        /// table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.Task.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="string"/> that represents this instance.</returns>
        public override string ToString()
        {
            return "<" + this.Task + ">";
        }
    }
}