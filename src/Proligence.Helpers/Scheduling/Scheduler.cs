﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Scheduler.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Scheduling
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;

    /// <summary>
    /// The base class for classes which implement task scheduling.
    /// </summary>
    /// <typeparam name="TTask">The type of tasks which can be used in the scheduler.</typeparam>
    public abstract class Scheduler<TTask> : IDisposable
        where TTask : class, ITask
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Scheduler{TTask}"/> class.
        /// </summary>
        protected Scheduler()
        {
            this.PollFrequency = 100;
            this.TaskQueueLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        }

        /// <summary>
        /// Gets or sets the number of milliseconds for which the current thread will be suspended between polls to
        /// find a task which is ready for execution. Default value is <c>100 ms</c>.
        /// </summary>
        public int PollFrequency { get; set; }

        /// <summary>
        /// Gets the object used to synchronize access to the tasks queue.
        /// </summary>
        public ReaderWriterLockSlim TaskQueueLock { get; private set; }

        /// <summary>
        /// Adds the specified task to the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to schedule.</param>
        public void ScheduleTask(TTask task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            this.UsingWriteLock(() => this.ScheduleTaskInternal(task));
        }

        /// <summary>
        /// Removes the specified task from the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to remove.</param>
        public void RemoveTask(TTask task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("task");
            }

            this.UsingWriteLock(() => this.RemoveTaskInternal(task));
        }

        /// <summary>
        /// Executes the next task which is waiting in queue for execution. If there is no task to execute, the method
        /// blocks the current thread and waits for a task which is ready for execution.
        /// </summary>
        /// <returns>The executed task.</returns>
        public virtual TTask ExecuteTask()
        {
            TTask task = this.GetNextTask();
            while (task == null)
            {
                Thread.Sleep(this.PollFrequency);
                task = this.GetNextTask();
            }

            task.DoWork();
            return task;
        }

        /// <summary>
        /// Executes the next task which is waiting in queue for execution and does not block the current thread if
        /// there is not task ready for execution.
        /// </summary>
        /// <returns>The executed task or <c>null</c> if there is no task to execute.</returns>
        public virtual TTask TryExecuteTask()
        {
            TTask task = this.GetNextTask();
            if (task == null)
            {
                return null;
            }

            task.DoWork();
            return task;
        }

        /// <summary>
        /// Executes the specified action while locking the scheduler's task queue for reading.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void UsingReadLock(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            bool lockHeld = this.TaskQueueLock.IsReadLockHeld || this.TaskQueueLock.IsWriteLockHeld;

            if (!lockHeld)
            {
                this.TaskQueueLock.EnterReadLock();
            }

            try
            {
                action();
            }
            finally
            {
                if (!lockHeld)
                {
                    this.TaskQueueLock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Executes the specified action while locking the scheduler's task queue for writing.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void UsingWriteLock(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            bool lockHeld = this.TaskQueueLock.IsWriteLockHeld;

            if (!lockHeld)
            {
                this.TaskQueueLock.EnterWriteLock();
            }

            try
            {
                action();
            }
            finally
            {
                if (!lockHeld)
                {
                    this.TaskQueueLock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Adds the specified task to the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to schedule.</param>
        protected abstract void ScheduleTaskInternal(TTask task);

        /// <summary>
        /// Removes the specified task from the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to remove.</param>
        protected abstract void RemoveTaskInternal(TTask task);

        /// <summary>
        /// Gets the next task for execution from the tasks queue.
        /// </summary>
        /// <returns>The task or <c>null</c> if there is no task which is ready for execution.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "By design.")]
        protected abstract TTask GetNextTask();

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.TaskQueueLock.Dispose();
                this.TaskQueueLock = null;
            }
        }
    }
}