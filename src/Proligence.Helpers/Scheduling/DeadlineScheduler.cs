﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeadlineScheduler.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Scheduling
{
    using System;
    using System.Collections.Generic;
    using Proligence.Helpers.Collections;

    /// <summary>
    /// Implements a task scheduler which schedules tasks based on their deadlines.
    /// </summary>
    /// <typeparam name="TTask">The type of tasks which can be used in the scheduler.</typeparam>
    public class DeadlineScheduler<TTask> : Scheduler<TTask>
        where TTask : class, IDeadlineTask
    {
        /// <summary>
        /// The task queue.
        /// </summary>
        private readonly MinimumFullHeap<DeadlineTaskCell<TTask>> queue;

        /// <summary>
        /// Maps tasks to their cell containers which are used in the heap-based queue.
        /// </summary>
        private readonly Dictionary<TTask, DeadlineTaskCell<TTask>> taskCells;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadlineScheduler&lt;TTask&gt;"/> class.
        /// </summary>
        public DeadlineScheduler()
        {
            this.queue = new MinimumFullHeap<DeadlineTaskCell<TTask>>();
            this.taskCells = new Dictionary<TTask, DeadlineTaskCell<TTask>>();
        }

        /// <summary>
        /// Adds the specified task to the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to schedule.</param>
        protected override void ScheduleTaskInternal(TTask task)
        {
            if (this.taskCells.ContainsKey(task))
            {
                throw new InvalidOperationException("The specified task is already scheduled.");
            }

            var cell = new DeadlineTaskCell<TTask>(task);
            this.taskCells.Add(task, cell);
            this.queue.Add(cell);
        }

        /// <summary>
        /// Removes the specified task from the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to remove.</param>
        protected override void RemoveTaskInternal(TTask task)
        {
            DeadlineTaskCell<TTask> taskCell;
            if (!this.taskCells.TryGetValue(task, out taskCell))
            {
                throw new InvalidOperationException("The specified task is not scheduled.");
            }

            int index = this.queue.IndexOf(taskCell);
            if (index != -1)
            {
                var tmpCell = new DeadlineTaskCell<TTask>((TTask)task.Clone());
                tmpCell.Task.Deadline = long.MinValue;

                this.queue.DecreaseItem(index, tmpCell);
                this.queue.ExtractMinimum();
                
                this.taskCells.Remove(task);
            }
        }

        /// <summary>
        /// Gets the next task for execution from the tasks queue.
        /// </summary>
        /// <returns>The task or <c>null</c> if there is no task which is ready for execution.</returns>
        protected override TTask GetNextTask()
        {
            this.TaskQueueLock.EnterWriteLock();
            try 
            {
                if (this.queue.Count > 0)
                {
                    DeadlineTaskCell<TTask> taskCell = this.queue.Minimum;
                    if (taskCell.Task.Deadline < DateTime.Now.ToFileTimeUtc())
                    {
                        this.queue.ExtractMinimum();
                        this.taskCells.Remove(taskCell.Task);
                        return taskCell.Task;
                    }
                }
            }
            finally
            {
                this.TaskQueueLock.ExitWriteLock();
            }
            
            return null;
        }
    }
}