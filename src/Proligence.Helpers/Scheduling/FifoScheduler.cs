﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FifoScheduler.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Scheduling
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Implements a task scheduler which schedules tasks using first-in-first-out rule.
    /// </summary>
    /// <typeparam name="TTask">The type of tasks which can be used in the scheduler.</typeparam>
    public class FifoScheduler<TTask> : Scheduler<TTask>
        where TTask : class, ITask
    {
        /// <summary>
        /// The list of queued tasks.
        /// </summary>
        private readonly List<TTask> queue;

        /// <summary>
        /// Initializes a new instance of the <see cref="FifoScheduler&lt;TTask&gt;"/> class.
        /// </summary>
        public FifoScheduler()
        {
            this.queue = new List<TTask>();
        }

        /// <summary>
        /// Adds the specified task to the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to schedule.</param>
        protected override void ScheduleTaskInternal(TTask task)
        {
            if (this.queue.Contains(task))
            {
                throw new InvalidOperationException("The specified task is already scheduled.");
            }

            this.queue.Add(task);
        }

        /// <summary>
        /// Removes the specified task from the queue of scheduled tasks.
        /// </summary>
        /// <param name="task">The task to remove.</param>
        protected override void RemoveTaskInternal(TTask task)
        {
            if (!this.queue.Remove(task))
            {
                throw new InvalidOperationException("The specified task is not scheduled.");
            }
        }

        /// <summary>
        /// Gets the next task for execution from the tasks queue.
        /// </summary>
        /// <returns>The task or <c>null</c> if there is no task which is ready for execution.</returns>
        protected override TTask GetNextTask()
        {
            this.TaskQueueLock.EnterWriteLock();
            try 
            {
                if (this.queue.Count > 0)
                {
                    TTask task = this.queue[0];
                    this.queue.RemoveAt(0);
                    
                    return task;
                }
            }
            finally
            {
                this.TaskQueueLock.ExitWriteLock();
            }
            
            return null;
        }
    }
}