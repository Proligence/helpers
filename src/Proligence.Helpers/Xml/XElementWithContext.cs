﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XElementWithContext.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Xml
{
    using System;
    using System.Linq.Expressions;
    using System.Xml.Linq;

    /// <summary>
    /// XML element with context.
    /// </summary>
    /// <typeparam name="TContext">Type of the context object.</typeparam>
    public class XElementWithContext<TContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="XElementWithContext{TContext}"/> class.
        /// </summary>
        /// <param name="element">The XML element.</param>
        /// <param name="context">The context.</param>
        public XElementWithContext(XElement element, TContext context)
        {
            this.Element = element;
            this.Context = context;
        }

        /// <summary>
        /// Gets the XML element.
        /// </summary>
        public XElement Element
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        public TContext Context
        {
            get;
            private set;
        }

        public XElementWithContext<TNewContext> With<TNewContext>(TNewContext context)
        {
            return new XElementWithContext<TNewContext>(this.Element, context);
        }

        public XElementWithContext<TContext> ToAttr<TProperty>(
            Expression<Func<TContext, TProperty>> targetExpression)
        {
            this.Element.ToAttr(Context, targetExpression);
            return this;
        }

        public XElementWithContext<TContext> FromAttr<TProperty>(
            Expression<Func<TContext, TProperty>> targetExpression)
        {
            this.Element.FromAttr(Context, targetExpression);
            return this;
        }
    }
}