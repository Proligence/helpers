﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Xml
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Xml.Linq;

    /// <summary>
    /// Provides extension methods for XML manipulation.
    /// </summary>
    public static class XmlExtensions
    {
        /// <summary>
        /// Returns an attribute of a given name, or null if not found.
        /// </summary>
        /// <param name="el">Element to search.</param>
        /// <param name="name">Name to get.</param>
        /// <returns>Attribute string value.</returns>
        public static string Attr(this XElement el, string name) 
        {
            if (el == null)
            {
                throw new ArgumentNullException("el");
            }

            var attr = el.Attribute(name);
            return attr == null ? null : attr.Value;
        }
 
        /// <summary>
        /// Fluently sets the value of an attribute.
        /// </summary>
        /// <typeparam name="T">Type of the value.</typeparam>
        /// <param name="el">Element to set attribute on.</param>
        /// <param name="name">Name of the attribute.</param>
        /// <param name="value">Value to set.</param>
        /// <returns>The element.</returns>
        public static XElement Attr<T>(this XElement el, string name, T value) 
        {
            if (el == null)
            {
                throw new ArgumentNullException("el");
            }

            el.SetAttributeValue(name, value);
            return el;
        }
 
        /// <summary>
        /// Fills property with data from XML.
        /// </summary>
        /// <typeparam name="TTarget">Type of target object.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="el">XML Element.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetExpression">Member expression for choosing a property.</param>
        /// <returns>XML Element provided.</returns>
        public static XElement FromAttr<TTarget, TProperty>(
            this XElement el, 
            TTarget target,
            Expression<Func<TTarget, TProperty>> targetExpression) 
        {
            if (el == null)
            {
                throw new ArgumentNullException("el");
            }

            if (ReferenceEquals(target, null))
            {
                throw new ArgumentNullException("target");
            }

            if (targetExpression == null)
            {
                throw new ArgumentNullException("targetExpression");
            }

            var memberExpression = targetExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new InvalidOperationException("Expression is not a member expression.");
            }

            var propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new InvalidOperationException("Expression is not for a property.");
            }

            var name = propertyInfo.Name;
            var attr = el.Attribute(name);
            if (attr == null)
            {
                return el;
            }

            if (typeof(TProperty) == typeof (string)) 
            {
                propertyInfo.SetValue(target, (string) attr, null);
                return el;
            }

            if (attr.Value == "null") 
            {
                propertyInfo.SetValue(target, null, null);
            }
            else if (typeof(TProperty) == typeof (int)) 
            {
                propertyInfo.SetValue(target, (int) attr, null);
            }
            else if (typeof(TProperty) == typeof (bool)) 
            {
                propertyInfo.SetValue(target, (bool) attr, null);
            }
            else if (typeof(TProperty) == typeof (DateTime)) 
            {
                propertyInfo.SetValue(target, (DateTime) attr, null);
            }
            else if (typeof(TProperty) == typeof (double)) 
            {
                propertyInfo.SetValue(target, (double) attr, null);
            }
            else if (typeof(TProperty) == typeof (float)) 
            {
                propertyInfo.SetValue(target, (float) attr, null);
            }
            else if (typeof(TProperty) == typeof (decimal)) 
            {
                propertyInfo.SetValue(target, (decimal) attr, null);
            }
            else if (typeof(TProperty) == typeof (int?)) 
            {
                propertyInfo.SetValue(target, (int?) attr, null);
            }
            else if (typeof(TProperty) == typeof (bool?)) 
            {
                propertyInfo.SetValue(target, (bool?) attr, null);
            }
            else if (typeof(TProperty) == typeof (DateTime?)) 
            {
                propertyInfo.SetValue(target, (DateTime?) attr, null);
            }
            else if (typeof(TProperty) == typeof (double?)) 
            {
                propertyInfo.SetValue(target, (double?) attr, null);
            }
            else if (typeof(TProperty) == typeof (float?)) 
            {
                propertyInfo.SetValue(target, (float?) attr, null);
            }
            else if (typeof(TProperty) == typeof (decimal?)) 
            {
                propertyInfo.SetValue(target, (decimal?) attr, null);
            }

            return el;
        }
 
        /// <summary>
        /// Stores given property value in XML attribute.
        /// </summary>
        /// <typeparam name="TTarget">Type of target object.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="el">XML Element.</param>
        /// <param name="target">Target object.</param>
        /// <param name="targetExpression">Member expression for choosing a property.</param>
        /// <returns>XML Element provided.</returns>
        public static XElement ToAttr<TTarget, TProperty>(
            this XElement el, 
            TTarget target,
            Expression<Func<TTarget, TProperty>> targetExpression) 
        {
            if (el == null)
            {
                throw new ArgumentNullException("el");
            }

            if (ReferenceEquals(target, null))
            {
                throw new ArgumentNullException("target");
            }

            if (targetExpression == null)
            {
                throw new ArgumentNullException("targetExpression");
            }

            var memberExpression = targetExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new InvalidOperationException("Expression is not a member expression.");
            }

            var propertyInfo = memberExpression.Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new InvalidOperationException("Expression is not for a property.");
            }

            var name = propertyInfo.Name;
            var val = propertyInfo.GetValue(target, null);

            if (typeof (TProperty) == typeof (string)) 
            {
                el.Attr(name, (string) val);
                return el;
            }

            if (val == null) 
            {
                el.Attr(name, "null");
            }
            else if (typeof(TProperty) == typeof (int)) 
            {
                el.Attr(name, (int) val);
            }
            else if (typeof(TProperty) == typeof (bool)) 
            {
                el.Attr(name, (bool) val);
            }
            else if (typeof(TProperty) == typeof (DateTime)) 
            {
                el.Attr(name, (DateTime) val);
            }
            else if (typeof(TProperty) == typeof (double)) 
            {
                el.Attr(name, (double) val);
            }
            else if (typeof(TProperty) == typeof (float)) 
            {
                el.Attr(name, (float) val);
            }
            else if (typeof (TProperty) == typeof (decimal)) 
            {
                el.Attr(name, (decimal) val);
            }
            else if (typeof(TProperty) == typeof (int?)) 
            {
                el.Attr(name, (int?) val);
            }
            else if (typeof(TProperty) == typeof (bool?)) 
            {
                el.Attr(name, (bool?) val);
            }
            else if (typeof(TProperty) == typeof (DateTime?)) 
            {
                el.Attr(name, (DateTime?) val);
            }
            else if (typeof(TProperty) == typeof (double?)) 
            {
                el.Attr(name, (double?) val);
            }
            else if (typeof(TProperty) == typeof (float?)) 
            {
                el.Attr(name, (float?) val);
            }
            else if (typeof(TProperty) == typeof (decimal?)) 
            {
                el.Attr(name, (decimal?) val);
            }

            return el;
        }
 
        /// <summary>
        /// Sets the object in context of which the further operations will be based upon.
        /// </summary>
        /// <typeparam name="TContext">Object type.</typeparam>
        /// <param name="el">XML Element.</param>
        /// <param name="context">Object to use in subsequent operations.</param>
        /// <returns>XML Element with context information.</returns>
        public static XElementWithContext<TContext> With<TContext>(this XElement el, TContext context) 
        {
            return new XElementWithContext<TContext>(el, context);
        }
    }
}
