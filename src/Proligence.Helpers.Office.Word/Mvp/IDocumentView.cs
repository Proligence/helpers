﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentView.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Mvp
{
    using Proligence.Helpers.Office.Common.Mvp;

    /// <summary>
    /// Defines the interface for classes which implement Word MVP views based on documents.
    /// </summary>
    /// <typeparam name="TModel">The type which represents the view's model.</typeparam>
    public interface IDocumentView<in TModel> : IView<TModel>
    {
        /// <summary>
        /// Updates the displayed status.
        /// </summary>
        /// <param name="message">The message to display.</param>
        void UpdateStatus(string message);

        /// <summary>
        /// Clears and releases the status bar.
        /// </summary>
        void ClearStatus();
    }
}