﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordApplication.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    using Microsoft.Office.Interop.Word;
    using Proligence.Helpers.Office.Common;

    /// <summary>
    /// Wraps the <see cref="Microsoft.Office.Interop.Word.Application"/> COM class instance, so that it can be 
    /// injected using Autofac.
    /// </summary>
    public class WordApplication : OfficeApplication, IWordApplication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WordApplication"/> class.
        /// </summary>
        /// <param name="application">The Word application instance.</param>
        public WordApplication(Application application)
        {
            this.Instance = application;
        }

        /// <summary>
        /// Gets the Word application instance.
        /// </summary>
        public Application Instance { get; private set; }

        /// <summary>
        /// Gets the currently active Word document or <c>null</c> if there is no document open.
        /// </summary>
        public Document ActiveDocument
        {
            get
            {
                if (this.Instance != null)
                {
                    if (this.Instance.Documents.Count > 0)
                    {
                        return this.Instance.ActiveDocument;
                    }
                }

                return null;
            }
        }
    }
}