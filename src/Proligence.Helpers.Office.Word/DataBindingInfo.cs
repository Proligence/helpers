﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataBindingInfo.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    /// <summary>
    /// Represents the information required to set up data binding for a control in a Word document.
    /// </summary>
    public sealed class DataBindingInfo
    {
        /// <summary>
        /// Initializes static members of the <see cref="DataBindingInfo"/> class.
        /// </summary>
        static DataBindingInfo()
        {
            DefaultPrefixMappings = "xmlns:i='http://www.w3.org/2001/XMLSchema-instance' ";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataBindingInfo"/> class.
        /// </summary>
        public DataBindingInfo()
        {
            this.PrefixMappings = DefaultPrefixMappings;
        }

        /// <summary>
        /// Gets or sets the default value of the <see cref="PrefixMappings"/> property for new
        /// <see cref="DataBindingInfo"/> objects.
        /// </summary>
        public static string DefaultPrefixMappings { get; set; }

        /// <summary>
        /// Gets or sets the XPath expression of the XML node to which the control should be data bound to.
        /// </summary>
        public string XPath { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the custom XML part to which the control should be data bound to.
        /// </summary>
        public string CustomXmlPartId { get; set; }

        /// <summary>
        /// Gets or sets the XSD prefix mappings for the specified XPath expression.
        /// </summary>
        public string PrefixMappings { get; set; }
    }
}