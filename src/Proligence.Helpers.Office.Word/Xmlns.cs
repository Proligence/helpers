﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Xmlns.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    /// <summary>
    /// Contains definitions of well-known XML namespaces.
    /// </summary>
    public static class Xmlns
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        /// The XML namespace for the <c>xml</c> prefix.
        /// </summary>
        public const string Xml = "http://www.w3.org/XML/1998/namespace";

        /// <summary>
        /// The XML namespace for Open XML package relationships.
        /// </summary>
        public const string PackageRelationships =
            "http://schemas.openxmlformats.org/package/2006/relationships";

        /// <summary>
        /// The XML namespace for Open XML package content types definition.
        /// </summary>
        public const string PackageContentTypes =
            "http://schemas.openxmlformats.org/package/2006/content-types";

        /// <summary>
        /// The XML namespace for Open XML document relationships.
        /// </summary>
        public const string DocumentRelationships =
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

        /// <summary>
        /// The XML namespace for Open XML document custom XML parts.
        /// </summary>
        public const string DocumentCustomXml = 
            "http://schemas.openxmlformats.org/officeDocument/2006/customXml";

        /// <summary>
        /// The main XML namespace for <c>Wordprocessing</c> XML elements.
        /// </summary>
        public const string WordprocessingML =
            "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

        /// <summary>
        /// The XML namespace for Word 2010 <c>Wordprocessing</c> ML features.
        /// </summary>
        public const string Word14 = 
            "http://schemas.microsoft.com/office/word/2010/wordml";

        /// <summary>
        /// The XML namespace for <c>DrawingML</c> XML elements.
        /// </summary>
        public const string DrawingML =
            "http://schemas.openxmlformats.org/drawingml/2006/main";

        /// <summary>
        /// The XML namespace for wordprocessing drawing elements.
        /// </summary>
        public const string WordprocessingDrawing =
            "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";

        /// <summary>
        /// The XML namespace for <c>DrawingML</c> picture elements.
        /// </summary>
        public const string Picture =
            "http://schemas.openxmlformats.org/drawingml/2006/picture";

        /// <summary>
        /// The XML namespace for flat OPC XML packages.
        /// </summary>
        public const string XmlPackage = 
            "http://schemas.microsoft.com/office/2006/xmlPackage";

        /// <summary>
        /// The XML namespace for <c>MathML</c> XML elements.
        /// </summary>
        public const string MathML =
            "http://schemas.openxmlformats.org/officeDocument/2006/math";

        /// <summary>
        /// The XML namespace for legacy Office XML elements.
        /// </summary>
        public const string Office = 
            "urn:schemas-microsoft-com:office:office";

        /* ReSharper restore InconsistentNaming */
    }
}