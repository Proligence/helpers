﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpenXmlWriter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// Specialized XML writer for generating Open XML markup.
    /// </summary>
    public class OpenXmlWriter : XmlWriter
    {
        /// <summary>
        /// The underlying XML writer.
        /// </summary>
        private readonly XmlWriter writer;

        /// <summary>
        /// The generated unique identifiers.
        /// </summary>
        private readonly SortedSet<int> ids;

        /// <summary>
        /// The generated unique identifiers for each object type.
        /// </summary>
        private readonly Dictionary<string, IList<int>> mappedIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenXmlWriter"/> class.
        /// </summary>
        /// <param name="stream">The stream to which the generated XML will be written.</param>
        public OpenXmlWriter(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            var settings = new XmlWriterSettings
            {
                ConformanceLevel = ConformanceLevel.Auto, 
                Encoding = Encoding.UTF8
            };
            
            this.writer = XmlWriter.Create(stream, settings);
            this.ids = new SortedSet<int>();
            this.mappedIds = new Dictionary<string, IList<int>>();
        }

        /// <summary>
        /// When overridden in a derived class, gets the state of the writer.
        /// </summary>
        /// <returns>One of the <see cref="T:System.Xml.WriteState" /> values.</returns>
        public override WriteState WriteState
        {
            get
            {
                return this.writer.WriteState;
            }
        }

        /// <summary>
        /// When overridden in a derived class, writes the XML declaration with the version "1.0".
        /// </summary>
        public override void WriteStartDocument()
        {
            this.writer.WriteStartDocument();
        }

        /// <summary>
        /// When overridden in a derived class, writes the XML declaration with the version "1.0" and the standalone
        /// attribute.
        /// </summary>
        /// <param name="standalone">If true, it writes "standalone=yes"; if false, it writes "standalone=no".</param>
        public override void WriteStartDocument(bool standalone)
        {
            this.writer.WriteStartDocument(standalone);
        }

        /// <summary>
        /// When overridden in a derived class, closes any open elements or attributes and puts the writer back in the
        /// Start state.
        /// </summary>
        public override void WriteEndDocument()
        {
            this.writer.WriteEndDocument();
        }

        /// <summary>
        /// When overridden in a derived class, writes the DOCTYPE declaration with the specified name and optional
        /// attributes.
        /// </summary>
        /// <param name="name">The name of the DOCTYPE. This must be non-empty.</param>
        /// <param name="pubid">
        /// If non-null it also writes PUBLIC <c>pubid</c> <c>sysid</c> where <paramref name="pubid" /> and
        /// <paramref name="sysid" /> are replaced with the value of the given arguments.
        /// </param>
        /// <param name="sysid">
        /// If <paramref name="pubid" /> is null and <paramref name="sysid" /> is non-null it writes SYSTEM
        /// <c>sysid</c> where <paramref name="sysid" /> is replaced with the value of this argument.
        /// </param>
        /// <param name="subset">
        /// If non-null it writes [subset] where subset is replaced with the value of this argument.
        /// </param>
        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            this.writer.WriteDocType(name, pubid, sysid, subset);
        }

        /// <summary>
        /// When overridden in a derived class, writes the specified start tag and associates it with the given
        /// namespace and prefix.
        /// </summary>
        /// <param name="prefix">The namespace prefix of the element.</param>
        /// <param name="localName">The local name of the element.</param>
        /// <param name="ns">The namespace URI to associate with the element.</param>
        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            this.writer.WriteStartElement(prefix, localName, ns);
        }

        /// <summary>
        /// When overridden in a derived class, closes one element and pops the corresponding namespace scope.
        /// </summary>
        public override void WriteEndElement()
        {
            this.writer.WriteEndElement();
        }

        /// <summary>
        /// When overridden in a derived class, closes one element and pops the corresponding namespace scope.
        /// </summary>
        public override void WriteFullEndElement()
        {
            this.writer.WriteFullEndElement();
        }

        /// <summary>
        /// When overridden in a derived class, writes the start of an attribute with the specified prefix, local
        /// name, and namespace URI.
        /// </summary>
        /// <param name="prefix">The namespace prefix of the attribute.</param>
        /// <param name="localName">The local name of the attribute.</param>
        /// <param name="ns">The namespace URI for the attribute.</param>
        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            this.writer.WriteStartAttribute(prefix, localName, ns);
        }

        /// <summary>
        /// When overridden in a derived class, closes the previous
        /// <see cref="XmlWriter.WriteStartAttribute(System.String,System.String)" /> call.
        /// </summary>
        public override void WriteEndAttribute()
        {
            this.writer.WriteEndAttribute();
        }

        /// <summary>
        /// When overridden in a derived class, writes out a &lt;![CDATA[...]]&gt; block containing the specified text.
        /// </summary>
        /// <param name="text">The text to place inside the CDATA block.</param>
        public override void WriteCData(string text)
        {
            this.writer.WriteCData(text);
        }

        /// <summary>
        /// When overridden in a derived class, writes out a comment &lt;!--...--&gt; containing the specified text.
        /// </summary>
        /// <param name="text">Text to place inside the comment.</param>
        public override void WriteComment(string text)
        {
            this.writer.WriteComment(text);
        }

        /// <summary>
        /// When overridden in a derived class, writes out a processing instruction with a space between the name and
        /// text as follows: &lt;?name text?&gt;.
        /// </summary>
        /// <param name="name">The name of the processing instruction.</param>
        /// <param name="text">The text to include in the processing instruction.</param>
        public override void WriteProcessingInstruction(string name, string text)
        {
            this.writer.WriteProcessingInstruction(name, text);
        }

        /// <summary>
        /// When overridden in a derived class, writes out an entity reference as &amp;name;.
        /// </summary>
        /// <param name="name">The name of the entity reference.</param>
        public override void WriteEntityRef(string name)
        {
            this.writer.WriteEntityRef(name);
        }

        /// <summary>
        /// When overridden in a derived class, forces the generation of a character entity for the specified Unicode
        /// character value.
        /// </summary>
        /// <param name="ch">The Unicode character for which to generate a character entity.</param>
        public override void WriteCharEntity(char ch)
        {
            this.writer.WriteCharEntity(ch);
        }

        /// <summary>
        /// When overridden in a derived class, writes out the given white space.
        /// </summary>
        /// <param name="ws">The string of white space characters.</param>
        public override void WriteWhitespace(string ws)
        {
            this.writer.WriteWhitespace(ws);
        }

        /// <summary>
        /// When overridden in a derived class, writes the given text content.
        /// </summary>
        /// <param name="text">The text to write.</param>
        public override void WriteString(string text)
        {
            this.writer.WriteString(text);
        }

        /// <summary>
        /// When overridden in a derived class, generates and writes the surrogate character entity for the surrogate
        /// character pair.
        /// </summary>
        /// <param name="lowChar">The low surrogate. This must be a value between 0xDC00 and 0xDFFF.</param>
        /// <param name="highChar">The high surrogate. This must be a value between 0xD800 and 0xDBFF.</param>
        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            this.writer.WriteSurrogateCharEntity(lowChar, highChar);
        }

        /// <summary>
        /// When overridden in a derived class, writes text one buffer at a time.
        /// </summary>
        /// <param name="buffer">Character array containing the text to write.</param>
        /// <param name="index">The position in the buffer indicating the start of the text to write.</param>
        /// <param name="count">The number of characters to write.</param>
        public override void WriteChars(char[] buffer, int index, int count)
        {
            this.writer.WriteChars(buffer, index, count);
        }

        /// <summary>
        /// When overridden in a derived class, writes raw markup manually from a character buffer.
        /// </summary>
        /// <param name="buffer">Character array containing the text to write.</param>
        /// <param name="index">The position within the buffer indicating the start of the text to write.</param>
        /// <param name="count">The number of characters to write.</param>
        /// <exception cref="NotSupportedException">The method is not supported.</exception>
        public override void WriteRaw(char[] buffer, int index, int count)
        {
            this.writer.WriteRaw(buffer, index, count);
        }

        /// <summary>
        /// When overridden in a derived class, writes raw markup manually from a string.
        /// </summary>
        /// <param name="data">String containing the text to write.</param>
        /// <exception cref="NotSupportedException">The method is not supported.</exception>
        public override void WriteRaw(string data)
        {
            this.writer.WriteRaw(data);
        }

        /// <summary>
        /// When overridden in a derived class, encodes the specified binary bytes as Base64 and writes out the
        /// resulting text.
        /// </summary>
        /// <param name="buffer">Byte array to encode.</param>
        /// <param name="index">The position in the buffer indicating the start of the bytes to write.</param>
        /// <param name="count">The number of bytes to write.</param>
        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            this.writer.WriteBase64(buffer, index, count);
        }

        /// <summary>
        /// When overridden in a derived class, closes this stream and the underlying stream.
        /// </summary>
        public override void Close()
        {
            this.writer.Close();
        }

        /// <summary>
        /// When overridden in a derived class, flushes whatever is in the buffer to the underlying streams and also
        /// flushes the underlying stream.
        /// </summary>
        public override void Flush()
        {
            this.writer.Flush();
        }

        /// <summary>
        /// When overridden in a derived class, returns the closest prefix defined in the current namespace scope for
        /// the namespace URI.
        /// </summary>
        /// <param name="ns">The namespace URI whose prefix you want to find.</param>
        /// <returns>The matching prefix or null if no matching namespace URI is found in the current scope.</returns>
        public override string LookupPrefix(string ns)
        {
            return this.writer.LookupPrefix(ns);
        }

        /// <summary>
        /// Generates a unique numeric identifier.
        /// </summary>
        /// <returns>The generated identifier.</returns>
        public int GenerateUniqueId()
        {
            int id = Environment.TickCount;
            while (this.ids.Contains(id))
            {
                id = Environment.TickCount;
            }

            this.ids.Add(id);
            
            return id;
        }

        /// <summary>
        /// Generates a unique numeric identifier for the specified object type.
        /// </summary>
        /// <param name="objectType">The type of object for which identifier will be generated.</param>
        /// <returns>The generated identifier.</returns>
        public int GenerateUniqueId(string objectType)
        {
            IList<int> list;
            if (!this.mappedIds.TryGetValue(objectType, out list))
            {
                list = new List<int>();
                this.mappedIds.Add(objectType, list);
            }

            int id = Environment.TickCount;
            while (list.Contains(id))
            {
                id = Environment.TickCount;
            }

            list.Add(id);
            this.ids.Add(id);

            return id;
        }
    }
}