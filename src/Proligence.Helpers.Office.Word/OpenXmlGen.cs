﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpenXmlGen.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    using System;
    using DocumentFormat.OpenXml.Wordprocessing;

    /* ReSharper disable PossiblyMistakenUseOfParamsMethod */

    /// <summary>
    /// Implements helper methods for generating Open XML markup.
    /// </summary>
    public static class OpenXmlGen
    {
        /// <summary>
        /// Creates the Open XML markup for a new control.
        /// </summary>
        /// <returns>The <see cref="SdtBlock"/> object which represents the created control.</returns>
        public static SdtBlock GenerateSdtBlock()
        {
            var sdtProperties = new SdtProperties();

            var sdtId = new SdtId { Val = new Random(Environment.TickCount).Next() };
            sdtProperties.Append(sdtId);

            return new SdtBlock(sdtProperties);
        }
    }
}