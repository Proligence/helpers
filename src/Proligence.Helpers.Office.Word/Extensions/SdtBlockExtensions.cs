﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SdtBlockExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Wordprocessing;

    /* ReSharper disable PossiblyMistakenUseOfParamsMethod */

    /// <summary>
    /// Implements extension methods for the <see cref="SdtBlock"/> class.
    /// </summary>
    public static class SdtBlockExtensions
    {
        /// <summary>
        /// Sets the specified alias for a <see cref="SdtBlock"/>.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="alias">The alias to set.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock Alias(this SdtBlock sdt, string alias)
        {
            return sdt.AppendSdtProperty(new SdtAlias { Val = alias });
        }

        /// <summary>
        /// Sets the specified tag for a <see cref="SdtBlock"/>.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="tag">The tag to set.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock Tag(this SdtBlock sdt, string tag)
        {
            return sdt.AppendSdtProperty(new Tag { Val = tag });
        }

        /// <summary>
        /// Configures the properties of an <see cref="SdtBlock"/> to make the control read-only.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock ReadOnly(this SdtBlock sdt)
        {
            return sdt.AppendSdtProperty(new Lock { Val = LockingValues.SdtContentLocked });
        }

        /// <summary>
        /// Configures the properties of an <see cref="SdtBlock"/> to make the control a drop down list.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="items">The values for the drop down list.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock DropDownList(this SdtBlock sdt, params string[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            return sdt.DropDownList(items.ToDictionary(x => x, x => x));
        }

        /// <summary>
        /// Configures the properties of an <see cref="SdtBlock"/> to make the control a drop down list.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="items">
        /// The values for the drop down list specified as a dictionary mapping display texts to item values.
        /// </param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock DropDownList(this SdtBlock sdt, IDictionary<string, string> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            var dropDownList = new SdtContentDropDownList();
            foreach (KeyValuePair<string, string> item in items)
            {
                var listItem = new ListItem { DisplayText = item.Key, Value = item.Value };
                dropDownList.Append(listItem);
            }

            return sdt.AppendSdtProperty(dropDownList);
        }

        /// <summary>
        /// Binds the specified <see cref="SdtBlock"/> to a custom XML part.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="itemId">The identifier of the custom XML part to which the control will be bound.</param>
        /// <param name="xpath">The XPath expression of the XML node to which the control will be bound.</param>
        /// <param name="prefixMappings">The XML namespace prefixes declaration for the XPath or <c>null</c>.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock DataBind(this SdtBlock sdt, string itemId, string xpath, string prefixMappings = null)
        {
            var dataBinding = new DataBinding 
            { 
                PrefixMappings = prefixMappings,
                StoreItemId = itemId,
                XPath = xpath
            };
            
            return sdt.AppendSdtProperty(dataBinding);
        }

        /// <summary>
        /// Sets the specified text for a <see cref="SdtBlock"/>.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="text">The text to set.</param>
        /// <param name="richText"><c>true</c> to enable rich text for the control; otherwise, <c>false</c>.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock Text(this SdtBlock sdt, string text, bool richText = false)
        {
            if (sdt == null)
            {
                throw new ArgumentNullException("sdt");
            }

            if (sdt.SdtContentBlock == null)
            {
                sdt.SdtContentBlock = new SdtContentBlock();
            }

            var runText = new Text(text);

            if (richText)
            {
                runText.Space = SpaceProcessingModeValues.Preserve;
            }

            var run = new Run();
            run.Append(runText);

            var paragraph = new Paragraph();
            paragraph.Append(run);

            sdt.SdtContentBlock.Append(paragraph);

            return sdt;
        }

        /// <summary>
        /// Append the specified property to a <see cref="SdtBlock"/>.
        /// </summary>
        /// <param name="sdt">The <see cref="SdtBlock"/> to configure.</param>
        /// <param name="property">The property element to append to the <see cref="SdtBlock"/>.</param>
        /// <returns>The specified <see cref="SdtBlock"/>.</returns>
        public static SdtBlock AppendSdtProperty(this SdtBlock sdt, OpenXmlElement property)
        {
            if (sdt == null)
            {
                throw new ArgumentNullException("sdt");
            }

            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            if (sdt.SdtContentBlock == null)
            {
                sdt.SdtContentBlock = new SdtContentBlock();
            }

            sdt.Append(property);

            return sdt;
        }
    }
}