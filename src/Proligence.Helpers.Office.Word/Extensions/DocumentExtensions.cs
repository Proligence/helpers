﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Extensions
{
    using System;
    using Microsoft.Office.Interop.Word;

    /// <summary>
    /// Implements extension methods for the <c>Microsoft.Office.Interop.Word.Document</c> class.
    /// </summary>
    public static class DocumentExtensions
    {
        /// <summary>
        /// Gets the bookmarks with the specified name.
        /// </summary>
        /// <param name="document">The document which contains the bookmark.</param>
        /// <param name="bookmarkName">The name of the bookmark to get.</param>
        /// <returns>The specified bookmark or <c>null</c> if the bookmark was not found.</returns>
        public static Bookmark GetBookmark(this Document document, string bookmarkName)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            if (bookmarkName == null)
            {
                throw new ArgumentNullException("bookmarkName");
            }

            if (document.Bookmarks.Exists(bookmarkName))
            {
                return document.Bookmarks[bookmarkName];
            }

            document.Bookmarks.ShowHidden = true;

            if (document.Bookmarks.Exists("_" + bookmarkName))
            {
                return document.Bookmarks["_" + bookmarkName];
            }

            return null;
        }

        /// <summary>
        /// Gets the content contained in the bookmark with the specified name.
        /// </summary>
        /// <param name="document">The document from which content will be returned.</param>
        /// <param name="bookmarkName">The name of the bookmark.</param>
        /// <returns>The document range contained in the bookmark.</returns>
        public static Range GetBookmarkContent(this Document document, string bookmarkName)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            Bookmark bookmark = document.GetBookmark(bookmarkName);
            if (bookmark != null)
            {
                return bookmark.Range;
            }

            return null;
        }
    }
}