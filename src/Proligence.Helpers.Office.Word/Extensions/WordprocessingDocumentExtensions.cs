﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordprocessingDocumentExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Extensions
{
    using System;
    using DocumentFormat.OpenXml.CustomXmlDataProperties;
    using DocumentFormat.OpenXml.Packaging;

    /* ReSharper disable PossiblyMistakenUseOfParamsMethod */

    /// <summary>
    /// Implements extension methods for the <see cref="WordprocessingDocument"/> class.
    /// </summary>
    public static class WordprocessingDocumentExtensions
    {
        /// <summary>
        /// Creates an empty custom XML part in the specified document.
        /// </summary>
        /// <param name="document">The document in which the part will be created.</param>
        /// <returns>The created part.</returns>
        public static CustomXmlPart NewCustomXmlPart(this WordprocessingDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            CustomXmlPart templatePart = document.MainDocumentPart.AddCustomXmlPart(CustomXmlPartType.CustomXml);
            
            var dataStoreItem = new DataStoreItem
            {
                ItemId = Guid.NewGuid().ToString("B").ToUpperInvariant()
            };

            string customXmlNs = "http://schemas.openxmlformats.org/officeDocument/2006/customXml";
            dataStoreItem.AddNamespaceDeclaration("ds", customXmlNs);

            CustomXmlPropertiesPart xmlProperties = templatePart.AddNewPart<CustomXmlPropertiesPart>();
            xmlProperties.DataStoreItem = dataStoreItem;

            return templatePart;
        }
    }
}