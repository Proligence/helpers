﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomXmlPartExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Extensions
{
    using System;
    using DocumentFormat.OpenXml.Packaging;

    /// <summary>
    /// Implements extension methods for the <see cref="CustomXmlPart"/> class.
    /// </summary>
    public static class CustomXmlPartExtensions
    {
        /// <summary>
        /// Gets the identifier of the specified <see cref="CustomXmlPart"/>.
        /// </summary>
        /// <param name="part">The part which identifier will be returned.</param>
        /// <returns>The identifier of the specified part or <c>null</c>.</returns>
        public static string GetItemId(this CustomXmlPart part)
        {
            if (part == null)
            {
                throw new ArgumentNullException("part");
            }

            if (part.CustomXmlPropertiesPart != null)
            {
                if (part.CustomXmlPropertiesPart.DataStoreItem != null)
                {
                    return part.CustomXmlPropertiesPart.DataStoreItem.ItemId;
                }
            }

            return null;
        }
    }
}