﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OpenXmlWriterExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Implements extension methods for the <see cref="OpenXmlWriter"/> class.
    /// </summary>
    public static class OpenXmlWriterExtensions
    {
        /// <summary>
        /// Renders a paragraph element with the specified text.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the XML will be rendered.</param>
        /// <param name="text">The text to render.</param>
        /// <param name="preserveSpace"><c>true</c> to set <c>xml:space</c> to <c>preserve</c>.</param>
        public static void RenderParagraph(this OpenXmlWriter writer, string text, bool preserveSpace = false)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteStartElement("p", Xmlns.WordprocessingML);
            writer.RenderRun(text, preserveSpace);
            writer.WriteEndElement();   /* p */
        }

        /// <summary>
        /// Renders a run element with the specified text.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the XML will be rendered.</param>
        /// <param name="text">The text to render.</param>
        /// <param name="preserveSpace"><c>true</c> to set <c>xml:space</c> to <c>preserve</c>.</param>
        public static void RenderRun(this OpenXmlWriter writer, string text, bool preserveSpace = false)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteStartElement("r", Xmlns.WordprocessingML);
            writer.RenderText(text, preserveSpace);
            writer.WriteEndElement();   /* r */
        }

        /// <summary>
        /// Renders a text element with the specified text.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the XML will be rendered.</param>
        /// <param name="text">The text to render.</param>
        /// <param name="preserveSpace"><c>true</c> to set <c>xml:space</c> to <c>preserve</c>.</param>
        public static void RenderText(this OpenXmlWriter writer, string text, bool preserveSpace = false)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteStartElement("t", Xmlns.WordprocessingML);

            if (preserveSpace)
            {
                writer.WriteAttributeString("xml", "space", Xmlns.Xml, "preserve");
            }

            writer.WriteString(text ?? string.Empty);
            writer.WriteEndElement();   /* t */
        }

        /// <summary>
        /// Renders a label with a text value.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the XML will be rendered.</param>
        /// <param name="labeledFieldInfo">Details of the labeled field to render.</param>
        public static void RenderLabeledField(this OpenXmlWriter writer, LabeledFieldInfo labeledFieldInfo)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (labeledFieldInfo == null)
            {
                throw new ArgumentNullException("labeledFieldInfo");
            }

            writer.WriteStartElement("p", Xmlns.WordprocessingML);

            // Paragraph properties
            writer.WriteStartElement("pPr", Xmlns.WordprocessingML);
            writer.WriteStartElement("ind", Xmlns.WordprocessingML);
            writer.WriteAttributeString("left", Xmlns.WordprocessingML, "2552");
            writer.WriteAttributeString("hanging", Xmlns.WordprocessingML, "2552");
            writer.WriteEndElement();   /* ind */
            writer.WriteEndElement();   /* pPr */

            // Label run
            writer.WriteStartElement("r", Xmlns.WordprocessingML);
            writer.WriteStartElement("rPr", Xmlns.WordprocessingML);
            writer.WriteStartElement("b", Xmlns.WordprocessingML);
            writer.WriteEndElement();   /* b */
            writer.WriteEndElement();   /* rPr */
            writer.WriteElementString("t", Xmlns.WordprocessingML, labeledFieldInfo.LabelText ?? string.Empty);
            writer.WriteEndElement();   /* r */

            // Tab run
            writer.WriteStartElement("r", Xmlns.WordprocessingML);
            writer.WriteStartElement("tab", Xmlns.WordprocessingML);
            writer.WriteEndElement();   /* tab */
            writer.WriteEndElement();   /* r */

            if (labeledFieldInfo.Editor)
            {
                // Control
                writer.WriteStartElement("sdt", Xmlns.WordprocessingML);
                writer.WriteStartElement("sdtPr", Xmlns.WordprocessingML);
                writer.WriteStartElement("id", Xmlns.WordprocessingML);

                writer.WriteAttributeString(
                    "val",
                    Xmlns.WordprocessingML,
                    writer.GenerateUniqueId("sdt").ToString(CultureInfo.InvariantCulture));

                writer.WriteEndElement();   /* id */

                if (labeledFieldInfo.ReadOnly)
                {
                    writer.WriteStartElement("lock", Xmlns.WordprocessingML);
                    writer.WriteAttributeString("val", Xmlns.WordprocessingML, "sdtContentLocked");
                    writer.WriteEndElement();   /* lock */
                }

                if (labeledFieldInfo.DataBindingInfo != null)
                {
                    writer.WriteDataBinding(labeledFieldInfo.DataBindingInfo);
                }

                if (labeledFieldInfo.ValueList != null)
                {
                    writer.WriteStartElement("dropDownList", Xmlns.WordprocessingML);
                    
                    writer.WriteAttributeString(
                        "lastValue",
                        Xmlns.WordprocessingML,
                        labeledFieldInfo.ValueText ?? string.Empty);

                    foreach (KeyValuePair<string, string> listItem in labeledFieldInfo.ValueList)
                    {
                        writer.WriteStartElement("listItem", Xmlns.WordprocessingML);
                        writer.WriteAttributeString("displayText", Xmlns.WordprocessingML, listItem.Key);
                        writer.WriteAttributeString("value", Xmlns.WordprocessingML, listItem.Value);
                        writer.WriteEndElement();   /* listItem */
                    }
                    
                    writer.WriteEndElement();   /* dropDownList */
                }
                else if (labeledFieldInfo.CheckBox)
                {
                    writer.WriteStartElement("checkbox", Xmlns.Word14);
                    
                    writer.WriteStartElement("checked", Xmlns.Word14);
                    writer.WriteAttributeString("val", Xmlns.Word14, labeledFieldInfo.ValueText ?? "0");
                    writer.WriteEndElement();   /* checked */

                    writer.WriteStartElement("checkedState", Xmlns.Word14);
                    writer.WriteAttributeString("val", Xmlns.Word14, "2612");
                    writer.WriteAttributeString("font", Xmlns.Word14, "MS Gothic");
                    writer.WriteEndElement();   /* checkedState */

                    writer.WriteStartElement("uncheckedState", Xmlns.Word14);
                    writer.WriteAttributeString("val", Xmlns.Word14, "2610");
                    writer.WriteAttributeString("font", Xmlns.Word14, "MS Gothic");
                    writer.WriteEndElement();   /* uncheckedState */

                    writer.WriteEndElement();   /* checkbox */
                }

                writer.WriteStartElement("text", Xmlns.WordprocessingML);
                writer.WriteEndElement();   /* text */
                writer.WriteEndElement();   /* sdtPr */

                writer.WriteStartElement("sdtContent", Xmlns.WordprocessingML);
                writer.WriteStartElement("r", Xmlns.WordprocessingML);

                if (labeledFieldInfo.CheckBox)
                {
                    writer.WriteStartElement("rPr", Xmlns.WordprocessingML);
                    writer.WriteStartElement("rFonts", Xmlns.WordprocessingML);
                    writer.WriteAttributeString("ascii", Xmlns.WordprocessingML, "MS Gothic");
                    writer.WriteAttributeString("eastAsia", Xmlns.WordprocessingML, "MS Gothic");
                    writer.WriteAttributeString("hAnsi", Xmlns.WordprocessingML, "MS Gothic");
                    writer.WriteAttributeString("hint", Xmlns.WordprocessingML, "eastAsia");
                    writer.WriteEndElement();   /* rFonts */
                    writer.WriteEndElement();   /* rPr */

                    string value;
                    if (string.IsNullOrEmpty(labeledFieldInfo.ValueText) || (labeledFieldInfo.ValueText == "0"))
                    {
                        value = Encoding.UTF8.GetString(new byte[] { 0xe2, 0x98, 0x90 });
                    }
                    else
                    {
                        value = Encoding.UTF8.GetString(new byte[] { 0xe2, 0x98, 0x92 });
                    }

                    writer.WriteElementString("t", Xmlns.WordprocessingML, value);
                }
                else if (labeledFieldInfo.ValueList != null)
                {
                    string value = labeledFieldInfo.ValueText ?? string.Empty;

                    if (labeledFieldInfo.ValueText != null)
                    {
                        var listEntries = labeledFieldInfo.ValueList
                            .Where(x => x.Value == labeledFieldInfo.ValueText)
                            .ToList();

                        if (listEntries.Any())
                        {
                            value = listEntries.First().Key;
                        }
                    }
                    
                    writer.WriteElementString("t", Xmlns.WordprocessingML, value);
                }
                else
                {
                    string value = labeledFieldInfo.ValueText ?? string.Empty;
                    writer.WriteElementString("t", Xmlns.WordprocessingML, value);
                }

                writer.WriteEndElement();   /* r */
                writer.WriteEndElement();   /* sdtContent */
                writer.WriteEndElement();   /* sdt */
            }
            else
            {
                // Static text
                writer.WriteElementString("r", Xmlns.WordprocessingML, labeledFieldInfo.ValueText ?? string.Empty);
            }

            writer.WriteEndElement();   /* p */
        }

        /// <summary>
        /// Renders the beginning of a (<c>sdt</c>) control.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the bookmark will be rendered.</param>
        /// <param name="tag">The tag of the control.</param>
        /// <param name="showPlaceholder">
        /// Specifies whether the content of the control should be displayed as a placeholder.
        /// </param>
        /// <returns>The identifier of the generated control.</returns>
        public static string RenderStartControl(this OpenXmlWriter writer, string tag, bool showPlaceholder = false)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteStartElement("sdt", Xmlns.WordprocessingML);
            writer.WriteStartElement("sdtPr", Xmlns.WordprocessingML);

            writer.WriteStartElement("id", Xmlns.WordprocessingML);
            string id = writer.GenerateUniqueId("sdt").ToString(CultureInfo.InvariantCulture);
            writer.WriteAttributeString("val", Xmlns.WordprocessingML, id);
            writer.WriteEndElement();   /* id */

            if (!string.IsNullOrEmpty(tag))
            {
                writer.WriteStartElement("tag", Xmlns.WordprocessingML);
                writer.WriteAttributeString("val", Xmlns.WordprocessingML, tag);
                writer.WriteEndElement();   /* tag */
            }

            if (showPlaceholder)
            {
                writer.WriteElementString("showingPlcHdr", Xmlns.WordprocessingML, null);
            }

            writer.WriteEndElement();   /* sdtPr */

            writer.WriteStartElement("sdtEndPr", Xmlns.WordprocessingML);
            writer.WriteEndElement();   /* sdtEndPr */

            writer.WriteStartElement("sdtContent");

            return id;
        }

        /// <summary>
        /// Renders the ending of a (<c>sdt</c>) control.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the bookmark will be rendered.</param>
        public static void RenderEndControl(this OpenXmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteEndElement();   /* sdtContent */
            writer.WriteEndElement();   /* sdt */
        }

        /// <summary>
        /// Renders a checkbox control.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the XML will be rendered.</param>
        /// <param name="value"><c>true</c> to rendered a checked checkbox; otherwise, <c>false</c>.</param>
        /// <param name="tag">The tag of the control or <c>null</c>.</param>
        /// <param name="readOnly"><c>true</c> to render a read-only control; otherwise, <c>false</c>.</param>
        public static void RenderCheckBox(
            this OpenXmlWriter writer,
            bool value,
            string tag = null,
            bool readOnly = false)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            writer.WriteStartElement("sdt", Xmlns.WordprocessingML);
            writer.WriteStartElement("sdtPr", Xmlns.WordprocessingML);
            writer.WriteStartElement("id", Xmlns.WordprocessingML);

            writer.WriteAttributeString(
                "val",
                Xmlns.WordprocessingML,
                writer.GenerateUniqueId("sdt").ToString(CultureInfo.InvariantCulture));

            writer.WriteEndElement(); /* id */

            if (readOnly)
            {
                writer.WriteStartElement("lock", Xmlns.WordprocessingML);
                writer.WriteAttributeString("val", Xmlns.WordprocessingML, "sdtContentLocked");
                writer.WriteEndElement(); /* lock */
            }

            if (!string.IsNullOrEmpty(tag))
            {
                writer.WriteStartElement("tag", Xmlns.WordprocessingML);
                writer.WriteAttributeString("val", Xmlns.WordprocessingML, tag);
                writer.WriteEndElement();   /* tag */
            }

            writer.WriteStartElement("checkbox", Xmlns.Word14);

            writer.WriteStartElement("checked", Xmlns.Word14);
            writer.WriteAttributeString("val", Xmlns.Word14, value ? "1" : "0");
            writer.WriteEndElement(); /* checked */

            writer.WriteStartElement("checkedState", Xmlns.Word14);
            writer.WriteAttributeString("val", Xmlns.Word14, "2612");
            writer.WriteAttributeString("font", Xmlns.Word14, "MS Gothic");
            writer.WriteEndElement(); /* checkedState */

            writer.WriteStartElement("uncheckedState", Xmlns.Word14);
            writer.WriteAttributeString("val", Xmlns.Word14, "2610");
            writer.WriteAttributeString("font", Xmlns.Word14, "MS Gothic");
            writer.WriteEndElement(); /* uncheckedState */

            writer.WriteEndElement(); /* checkbox */
            writer.WriteEndElement(); /* sdtPr */

            writer.WriteStartElement("sdtContent", Xmlns.WordprocessingML);
            writer.WriteStartElement("r", Xmlns.WordprocessingML);

            writer.WriteStartElement("rPr", Xmlns.WordprocessingML);
            writer.WriteStartElement("rFonts", Xmlns.WordprocessingML);
            writer.WriteAttributeString("ascii", Xmlns.WordprocessingML, "MS Gothic");
            writer.WriteAttributeString("eastAsia", Xmlns.WordprocessingML, "MS Gothic");
            writer.WriteAttributeString("hAnsi", Xmlns.WordprocessingML, "MS Gothic");
            writer.WriteAttributeString("hint", Xmlns.WordprocessingML, "eastAsia");
            writer.WriteEndElement(); /* rFonts */
            writer.WriteEndElement(); /* rPr */

            string text;
            if (value)
            {
                text = Encoding.UTF8.GetString(new byte[] { 0xe2, 0x98, 0x92 });
            }
            else
            {
                text = Encoding.UTF8.GetString(new byte[] { 0xe2, 0x98, 0x90 });
            }

            writer.WriteElementString("t", Xmlns.WordprocessingML, text);

            writer.WriteEndElement(); /* r */
            writer.WriteEndElement(); /* sdtContent */
            writer.WriteEndElement(); /* sdt */
        }

        /// <summary>
        /// Renders the start element of the bookmark with the specified name.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the bookmark will be written to.</param>
        /// <param name="name">The name of the bookmark.</param>
        /// <returns>The ID of the generated bookmark.</returns>
        public static string WriteStartBookmark(this OpenXmlWriter writer, string name)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            int bookmarkId = writer.GenerateUniqueId("bookmark");
            string bookmarkIdStr = bookmarkId.ToString(CultureInfo.InvariantCulture);

            writer.WriteStartElement("bookmarkStart", Xmlns.WordprocessingML);
            writer.WriteAttributeString("name", Xmlns.WordprocessingML, name);
            writer.WriteAttributeString("id", Xmlns.WordprocessingML, bookmarkIdStr);
            writer.WriteEndElement();

            return bookmarkIdStr;
        }

        /// <summary>
        /// Writes the end element of the bookmark with the specified ID.
        /// </summary>
        /// <param name="writer">The Open XML writer to which the bookmark will be written to.</param>
        /// <param name="id">The unique identifier of the bookmark to close.</param>
        public static void WriteEndBookmark(this OpenXmlWriter writer, string id)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            writer.WriteStartElement("bookmarkEnd", Xmlns.WordprocessingML);
            writer.WriteAttributeString("id", Xmlns.WordprocessingML, id);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the <c>w:dataBinding</c> element for the specified <see cref="DataBindingInfo"/>.
        /// </summary>
        /// <param name="writer">The writer to which Open XML will be written.</param>
        /// <param name="dataBindingInfo">The data binding information.</param>
        public static void WriteDataBinding(this OpenXmlWriter writer, DataBindingInfo dataBindingInfo)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (dataBindingInfo == null)
            {
                throw new ArgumentNullException("dataBindingInfo");
            }

            writer.WriteStartElement("dataBinding", Xmlns.WordprocessingML);

            writer.WriteAttributeString("xpath", Xmlns.WordprocessingML, dataBindingInfo.XPath);

            if (!string.IsNullOrEmpty(dataBindingInfo.PrefixMappings))
            {
                writer.WriteAttributeString("prefixMappings", Xmlns.WordprocessingML, dataBindingInfo.PrefixMappings);
            }

            if (!string.IsNullOrEmpty(dataBindingInfo.CustomXmlPartId))
            {
                writer.WriteAttributeString("storeItemID", Xmlns.WordprocessingML, dataBindingInfo.CustomXmlPartId);
            }

            writer.WriteEndElement();   /* dataBinding */
        }
    }
}