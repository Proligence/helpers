﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LabeledFieldInfo.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Word
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents the information about a labeled control or text value in a Word document.
    /// </summary>
    public sealed class LabeledFieldInfo
    {
        /// <summary>
        /// Gets or sets the text of the label.
        /// </summary>
        public string LabelText { get; set; }

        /// <summary>
        /// Gets or sets the text of the field value.
        /// </summary>
        public string ValueText { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a control should be rendered instead of a static text.
        /// </summary>
        public bool Editor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the rendered control should be read-only.
        /// </summary>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a checkbox control should rendered.
        /// </summary>
        public bool CheckBox { get; set; }

        /// <summary>
        /// Gets or sets the list of available values for the control (display name -> value).
        /// </summary>
        public IDictionary<string, string> ValueList { get; set; }

        /// <summary>
        /// Gets or sets the data binding configuration for the control.
        /// </summary>
        public DataBindingInfo DataBindingInfo { get; set; }
    }
}