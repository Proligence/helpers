﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExcelAddInBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using System;
    using global::Autofac;
    using Microsoft.Office.Tools;

    /// <summary>
    /// The base class for Excel add-in classes.
    /// </summary>
    public abstract class ExcelAddInBase : Common.AddInBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelAddInBase"/> class.
        /// </summary>
        /// <param name="factory">The Office factory.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="primaryCookie">The primary cookie.</param>
        /// <param name="identifier">The identifier.</param>
        protected ExcelAddInBase(
            Factory factory, IServiceProvider serviceProvider, string primaryCookie, string identifier)
            : base(factory, serviceProvider, primaryCookie, identifier)
        {
        }

        /// <summary>
        /// Gets the <see cref="Microsoft.Office.Interop.Excel.Application"/> object for the Excel application.
        /// </summary>
        public abstract Microsoft.Office.Interop.Excel.Application ExcelApplication { get; }

        /// <summary>
        /// Registers additional components in the application's dependency injection container.
        /// </summary>
        /// <param name="builder">The dependency injection container builder.</param>
        protected override void BuildContainer(ContainerBuilder builder)
        {
            base.BuildContainer(builder);

            builder.RegisterInstance(new ExcelApplication(this.ExcelApplication))
                .As<IExcelApplication>()
                .SingleInstance();
        }
    }
}