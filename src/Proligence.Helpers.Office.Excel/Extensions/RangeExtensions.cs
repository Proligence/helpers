﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RangeExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Extensions
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Implements extension methods fro the <see cref="Range"/> interface.
    /// </summary>
    public static class RangeExtensions
    {
        /// <summary>
        /// Enables text wrapping for the specified range.
        /// </summary>
        /// <param name="range">The Excel range.</param>
        public static void EnableTextWrapping(this Range range)
        {
            if ((range != null) && !(bool)range.WrapText)
            {
                range.WrapText = true;
            }
        }

        /// <summary>
        /// Formats the specified range of cells to allow only values from the specified set.
        /// </summary>
        /// <param name="range">The range of cells to configure.</param>
        /// <param name="values">A list of possible values for the column's cells.</param>
        public static void RestrictCellValues(this Range range, IEnumerable<string> values)
        {
            if (range == null)
            {
                throw new ArgumentNullException("range");
            }

            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            range.Validation.Delete();
            
            range.Validation.Add(
                XlDVType.xlValidateList,
                XlDVAlertStyle.xlValidAlertStop,
                XlFormatConditionOperator.xlBetween,
                string.Join(";", values));

            range.Validation.InCellDropdown = true;
        }

        /// <summary>
        /// Formats the specified range of cells to allow only values from the specified set.
        /// </summary>
        /// <param name="range">The range of cells to configure.</param>
        /// <param name="values">A list of possible values for the column's cells.</param>
        public static void RestrictCellValues(this Range range, params string[] values)
        {
            RestrictCellValues(range, (IEnumerable<string>)values);
        }
    }
}