﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorksheetExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Extensions
{
    using System;
    using System.Linq;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Implements extension methods for the <see cref="Worksheet"/> class.
    /// </summary>
    public static class WorksheetExtensions
    {
        /// <summary>
        /// Gets the list object with the specified name.
        /// </summary>
        /// <param name="worksheet">The worksheet which contains the list object to get.</param>
        /// <param name="name">The name of the list object to get.</param>
        /// <returns>
        /// The retrieved list object or <c>null</c> if the specified worksheet does not contain any object with the
        /// specified name.
        /// </returns>
        public static ListObject GetListObject(this Worksheet worksheet, string name)
        {
            if (worksheet == null)
            {
                throw new ArgumentNullException("worksheet");
            }

            return worksheet.ListObjects.Cast<ListObject>().FirstOrDefault(obj => obj.Name == name);
        }
    }
}