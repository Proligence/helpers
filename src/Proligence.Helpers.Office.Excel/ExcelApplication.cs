﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExcelApplication.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using Microsoft.Office.Interop.Excel;
    using Proligence.Helpers.Office.Common;

    /// <summary>
    /// Wraps the <see cref="Microsoft.Office.Interop.Excel.Application"/> COM class instance, so that it can be 
    /// injected using Autofac.
    /// </summary>
    public class ExcelApplication : OfficeApplication, IExcelApplication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelApplication"/> class.
        /// </summary>
        /// <param name="application">The Excel application instance.</param>
        public ExcelApplication(Application application)
        {
            this.Instance = application;
        }

        /// <summary>
        /// Gets the Excel application instance.
        /// </summary>
        public Application Instance { get; private set; }

        /// <summary>
        /// Gets the current worksheet.
        /// </summary>
        public Worksheet CurrentWorksheet
        {
            get
            {
                if (this.Instance != null)
                {
                    Workbook activeWorkbook = this.Instance.ComInvoke(x => x.ActiveWorkbook);
                    if (activeWorkbook != null)
                    {
                        Worksheet activeSheet = (Worksheet)activeWorkbook.ComInvoke(x => x.ActiveSheet);
                        if (activeSheet != null)
                        {
                            return activeSheet;
                        }
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the current workbook.
        /// </summary>
        public Workbook CurrentWorkbook
        {
            get
            {
                if (this.Instance != null)
                {
                    return this.Instance.ComInvoke(x => x.ActiveWorkbook);
                }

                return null;
            }
        }
    }
}