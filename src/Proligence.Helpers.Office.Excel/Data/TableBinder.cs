﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TableBinder.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Office.Interop.Excel;
    using Proligence.Helpers.Common;

    /// <summary>
    /// Implements data binding for Excel data tables based on <see cref="ColumnDataSource"/> mappings.
    /// </summary>
    public class TableBinder : ITableBinder
    {
        /// <summary>
        /// The data table column mappings.
        /// </summary>
        private readonly IEnumerable<ColumnDataSource> mappings;

        /// <summary>
        /// The data source objects for the data table.
        /// </summary>
        private object[] dataSourceItems;

        /// <summary>
        /// The data table associated with the binder.
        /// </summary>
        private ExcelDataTable excelDataTable;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableBinder"/> class.
        /// </summary>
        /// <param name="mappings">The data table column mappings.</param>
        public TableBinder(IEnumerable<ColumnDataSource> mappings)
        {
            this.mappings = mappings;
            this.WriteChangesToDataSource = true;
        }

        /// <summary>
        /// Occurs after an item from the data source has been updated.
        /// </summary>
        public event EventHandler<EventArgs<object>> ItemUpdated;

        /// <summary>
        /// Occurs after a new item has been added to the data source.
        /// </summary>
        public event EventHandler<EventArgs<object>> ItemCreated;

        /// <summary>
        /// Occurs after an item has been deleted from the data source.
        /// </summary>
        public event EventHandler<EventArgs<object>> ItemDeleted;

        /// <summary>
        /// Gets the data source for the data table.
        /// </summary>
        public IEnumerable DataSource { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether changed cells will be automatically updated in the data source.
        /// </summary>
        public bool WriteChangesToDataSource { get; set; }

        /// <summary>
        /// Gets or sets the function used to create items for new rows.
        /// </summary>
        public Func<object> ItemFactory { get; set; }

        /// <summary>
        /// Renders the user from the specified model.
        /// </summary>
        /// <param name="listObject">The list object which contains the table.</param>
        /// <param name="data">The list of objects which contain data for each row.</param>
        public void Bind(ListObject listObject, IList data)
        {
            if (listObject == null)
            {
                throw new ArgumentNullException("listObject");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            Range range = listObject.Range.Offset[1].Resize[data.Count];
            object[,] arr = (object[,])range.Value2;

            int i = 1;
            foreach (object rowData in data)
            {
                int j = 1;
                foreach (var columnMapping in this.mappings)
                {
                    object value = columnMapping.GetValue(rowData);
                    arr[i, j++] = value != null ? value.ToString() : null;
                }

                i++;
            }

            range.Value2 = arr;
        }

        /// <summary>
        /// Binds the specified data to the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to bind.</param>
        public void Bind(ExcelDataTable dataTable, object data)
        {
            var enumerable = data as IEnumerable;
            if (enumerable != null)
            {
                this.BindData(dataTable, enumerable, false);
            }
            else
            {
                this.BindData(dataTable, new[] { data }, false);
            }
        }

        /// <summary>
        /// Binds the specified data to the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to bind.</param>
        public void Bind(ExcelDataTable dataTable, IEnumerable data)
        {
            this.BindData(dataTable, data, false);
        }

        /// <summary>
        /// Displays the specified data in the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to display.</param>
        public void Display(ExcelDataTable dataTable, object data)
        {
            var enumerable = data as IEnumerable;
            if (enumerable != null)
            {
                this.BindData(dataTable, enumerable, true);
            }
            else
            {
                this.BindData(dataTable, new[] { data }, true);
            }
        }

        /// <summary>
        /// Binds the specified data to the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to bind.</param>
        /// <param name="oneWay">
        /// <c>true</c> to only display data or <c>false</c> to display data and subscribe for change updates.
        /// </param>
        protected virtual void BindData(ExcelDataTable dataTable, IEnumerable data, bool oneWay)
        {
            if (dataTable == null)
            {
                throw new ArgumentNullException("dataTable");
            }

            dataTable.Updated -= this.OnDataTableUpdated;
            dataTable.Inserted -= this.OnDataTableInserted;
            dataTable.Deleted -= this.DataTableOnDeleted;

            Range columns = dataTable.GetHeaderRange();
            Range rowsRange = dataTable.GetRowsRange();

            if (data != null)
            {
                /* ReSharper disable once PossibleMultipleEnumeration */
                object[] dataArr = data.AsQueryable().Cast<object>().ToArray();

                // Resize table if new rows were added to the data source
                if (rowsRange.Rows.Count < dataArr.Length)
                {
                    rowsRange = rowsRange.Resize[dataArr.Length, rowsRange.Columns.Count];
                }

                object[,] arr = (object[,])rowsRange.Value2;

                int rowIndex = 0;
                foreach (object dataObject in dataArr)
                {
                    this.RenderDataRow(columns, dataObject, arr, rowIndex);
                    rowIndex++;
                }

                // Clear the rest of the table.
                int rowCount = arr.GetLength(0);
                for (; rowIndex < rowCount; rowIndex++)
                {
                    for (int i = 1; i <= columns.Count; i++)
                    {
                        arr[rowIndex + 1, i] = null;
                    }
                }

                rowsRange.Value2 = arr;

                /* ReSharper disable once PossibleMultipleEnumeration */
                this.DataSource = data;
                this.dataSourceItems = dataArr;
            }
            else
            {
                rowsRange.Clear();
                this.DataSource = null;
            }

            if (!oneWay)
            {
                dataTable.Updated += this.OnDataTableUpdated;
                dataTable.Inserted += this.OnDataTableInserted;
                dataTable.Deleted += this.DataTableOnDeleted;
            }

            this.excelDataTable = dataTable;
        }

        /// <summary>
        /// Updates the specified cell at the data source object.
        /// </summary>
        /// <param name="dataObject">The data source object to update.</param>
        /// <param name="mapping">The column mapping of the changed cell.</param>
        /// <param name="value">The new cell value.</param>
        protected virtual void UpdateDataSource(object dataObject, ColumnDataSource mapping, object value)
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }

            mapping.SetValue(dataObject, value);
        }

        /// <summary>
        /// Called after an item from the data source has been updated.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected virtual void OnItemUpdated(EventArgs<object> e)
        {
            var handler = this.ItemUpdated;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Called after a new item has been added to the data source.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected virtual void OnItemCreated(EventArgs<object> e)
        {
            var handler = this.ItemCreated;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Called after an item has been deleted from the data source.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected virtual void OnItemDeleted(EventArgs<object> e)
        {
            var handler = this.ItemDeleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Copies the values of a single data object to a row in the Excel worksheet.
        /// </summary>
        /// <param name="headerRange">The range which contains the table's column headers.</param>
        /// <param name="dataObject">The data object to bind.</param>
        /// <param name="arr">The array to which values will be copied.</param>
        /// <param name="rowIndex">The current row index.</param>
        private void RenderDataRow(Range headerRange, object dataObject, object[,] arr, int rowIndex)
        {
            int columnIndex = 0;
            foreach (Range column in headerRange)
            {
                string columnName = column.Offset[0, 0].Value.ToString();

                ColumnDataSource mapping = this.mappings.FirstOrDefault(m => m.Name == columnName);
                if (mapping == null)
                {
                    mapping = this.mappings.FirstOrDefault(m => m.Index == columnIndex);
                }

                string value = null;
                if (mapping != null)
                {
                    object objValue = mapping.GetValue(dataObject);
                    value = objValue != null ? objValue.ToString() : null;
                }

                arr[rowIndex + 1, columnIndex + 1] = value;
                columnIndex++;
            }
        }

        /// <summary>
        /// Copies the values of the specified data objects to a range of rows in the Excel worksheet.
        /// </summary>
        /// <param name="dataObjects">The data objects to bind.</param>
        /// <param name="range">The Excel range to update.</param>
        private void RenderDataRange(IEnumerable<object> dataObjects, Range range)
        {
            if (dataObjects == null)
            {
                throw new ArgumentNullException("dataObjects");
            }

            if (range == null)
            {
                throw new ArgumentNullException("range");
            }

            bool singleValue = range.Cells.Count < 2;

            object[,] arr;
            if (singleValue)
            {
                arr = new object[,] { { null, null }, { null, range.Value2 } };
            }
            else
            {
                arr = (object[,])range.Value2;
            }

            int rowIndex = 0;
            Range headerRange = this.excelDataTable.GetHeaderRange();
            foreach (object dataObject in dataObjects)
            {
                this.RenderDataRow(headerRange, dataObject, arr, rowIndex++);
            }

            object value2 = singleValue ? arr[1, 1] : arr;
            this.excelDataTable.SuppressEvents(() => { range.Value2 = value2; });
        }

        /// <summary>
        /// Called when the data table has changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        private void OnDataTableUpdated(object sender, RangeEventArgs e)
        {
            if (this.WriteChangesToDataSource)
            {
                int baseRowIndex = e.Range.Row;
                int baseColumnIndex = e.Range.Column;

                object[,] arr;
                if (e.Range.Cells.Count > 1)
                {
                    arr = (object[,])e.Range.Value2;
                }
                else
                {
                    arr = new object[,] { { null, null }, { null, e.Range.Value2 } };
                }

                var dataObjects = new List<object>();
                for (int i = 0; i < e.Range.Rows.Count; i++)
                {
                    bool newItemCreated = false;
                    int idx = baseRowIndex + i - 2;
                    if ((this.dataSourceItems.Length <= idx) || (idx < 0))
                    {
                        if (this.dataSourceItems.Length == idx)
                        {
                            // If we are trying to update the next row after the last one, then we need to create a new
                            // one (this is how you add new rows in Excel...)
                            this.OnDataTableInserted(sender, e);
                            newItemCreated = true;
                        }
                        else
                        {
                            break;
                        }
                    }

                    object dataObject = this.dataSourceItems[idx];
                    for (int j = 0; j < e.Range.Columns.Count; j++)
                    {
                        ColumnDataSource mapping = this.mappings.ElementAtOrDefault(baseColumnIndex + j - 1);
                        if (mapping != null)
                        {
                            object value = arr[i + 1, j + 1];
                            this.UpdateDataSource(dataObject, mapping, value);

                            if (newItemCreated)
                            {
                                this.OnItemCreated(new EventArgs<object>(dataObject));
                            }
                            else
                            {
                                this.OnItemUpdated(new EventArgs<object>(dataObject));                                
                            }
                        }
                    }

                    dataObjects.Add(dataObject);
                }

                // Re-render the updated rows, so the user sees what is actually in the underlying objects
                Range range = this.excelDataTable.GetRowsRange(e.Range.Row - 2, e.Range.Rows.Count);
                this.RenderDataRange(dataObjects, range);
            }
        }

        /// <summary>
        /// Called when new rows are inserted into the data table.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        private void OnDataTableInserted(object sender, RangeEventArgs e)
        {
            if (this.WriteChangesToDataSource)
            {
                var listDataSource = this.DataSource as IList;
                if (listDataSource == null)
                {
                    throw new InvalidOperationException("Row insertion is not supported for the current data source.");
                }

                if (this.ItemFactory == null)
                {
                    throw new InvalidOperationException("Cannot insert rows when no item factory is specified.");
                }

                int baseRowIndex = e.Range.Row;
                int baseColumnIndex = e.Range.Column;

                // Create new objects for inserted rows
                var itemsToAdd = new List<Tuple<int, object>>();
                for (int i = 0; i < e.Range.Rows.Count; i++)
                {
                    object dataObject = this.ItemFactory();
                    itemsToAdd.Add(new Tuple<int, object>(baseRowIndex + i - 2, dataObject));
                }

                // Add new items to the underlying collcetion
                foreach (Tuple<int, object> item in itemsToAdd)
                {
                    listDataSource.Insert(item.Item1, item.Item2);
                }

                this.dataSourceItems = listDataSource.Cast<object>().ToArray();

                // Update the properties of the newly created items
                object[,] arr;
                if (e.Range.Cells.Count > 1)
                {
                    arr = (object[,])e.Range.Value2;
                }
                else
                {
                    arr = new object[,] { { null, null }, { null, e.Range.Value2 } };
                }

                var dataObjects = new List<object>();
                for (int i = 0; i < e.Range.Rows.Count; i++)
                {
                    if (this.dataSourceItems.Length <= baseRowIndex + i - 2)
                    {
                        break;
                    }

                    object dataObject = this.dataSourceItems[baseRowIndex + i - 2];
                    for (int j = 0; j < e.Range.Columns.Count; j++)
                    {
                        ColumnDataSource mapping = this.mappings.ElementAtOrDefault(baseColumnIndex + j - 1);
                        if (mapping != null)
                        {
                            object value = arr[i + 1, j + 1];
                            this.UpdateDataSource(dataObject, mapping, value);
                            this.OnItemUpdated(new EventArgs<object>(dataObject));
                        }
                    }

                    dataObjects.Add(dataObject);
                }

                // Re-render the updated rows, so the user sees what is actually in the underlying objects
                Range range = this.excelDataTable.GetRowsRange(e.Range.Row - 2, e.Range.Rows.Count);
                this.RenderDataRange(dataObjects, range);
            }
        }

        /// <summary>
        /// Called when the data table's rows are deleted.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        private void DataTableOnDeleted(object sender, RangeEventArgs e)
        {
            if (this.WriteChangesToDataSource)
            {
                var listDataSource = this.DataSource as IList;
                if (listDataSource == null)
                {
                    throw new InvalidOperationException("Row deletion is not supported for the current data source.");
                }

                int baseRowIndex = e.Range.Row;
                
                var itemsToRemove = new List<object>();
                for (int i = 0; i < e.Range.Rows.Count; i++)
                {
                    if (this.dataSourceItems.Length <= baseRowIndex + i - 2)
                    {
                        break;
                    }

                    object dataObject = this.dataSourceItems[baseRowIndex + i - 2];
                    itemsToRemove.Add(dataObject);
                }

                foreach (object obj in itemsToRemove)
                {
                    listDataSource.Remove(obj);
                    this.OnItemUpdated(new EventArgs<object>(obj));
                }

                this.dataSourceItems = listDataSource.Cast<object>().ToArray();
            }
        }
    }
}