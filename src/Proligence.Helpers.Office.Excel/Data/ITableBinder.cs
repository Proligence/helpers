﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITableBinder.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Collections;
    using Microsoft.Office.Interop.Excel;
    using Proligence.Helpers.Common;

    /// <summary>
    /// Defines the API for classes which bind Excel data tables to data sources.
    /// </summary>
    public interface ITableBinder
    {
        /// <summary>
        /// Occurs after an item from the data source has been updated.
        /// </summary>
        event EventHandler<EventArgs<object>> ItemUpdated;

        /// <summary>
        /// Occurs after a new item has been added to the data source.
        /// </summary>
        event EventHandler<EventArgs<object>> ItemCreated;

        /// <summary>
        /// Occurs after an item has been deleted from the data source.
        /// </summary>
        event EventHandler<EventArgs<object>> ItemDeleted;

        /// <summary>
        /// Gets the data source for the data table.
        /// </summary>
        IEnumerable DataSource { get; }

        /// <summary>
        /// Gets or sets a value indicating whether changed cells will be automatically updated in the data source.
        /// </summary>
        bool WriteChangesToDataSource { get; set; }

        /// <summary>
        /// Gets or sets the function used to create items for new rows.
        /// </summary>
        Func<object> ItemFactory { get; set; }

        /// <summary>
        /// Renders the user from the specified model.
        /// </summary>
        /// <param name="listObject">The list object which contains the table.</param>
        /// <param name="data">The list of objects which contain data for each row.</param>
        void Bind(ListObject listObject, IList data);

        /// <summary>
        /// Binds the specified data to the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to bind.</param>
        void Bind(ExcelDataTable dataTable, object data);

        /// <summary>
        /// Displays the specified data in the specified data table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="data">The data to display.</param>
        void Display(ExcelDataTable dataTable, object data);
    }
}