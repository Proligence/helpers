﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITableFormatter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System.Collections.Generic;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Defines the API for classes which implement formatting data tables in Excel worksheets.
    /// </summary>
    public interface ITableFormatter
    {
        /// <summary>
        /// Formats the data table in an Excel worksheet.
        /// </summary>
        /// <param name="worksheet">The worksheet in which the data table will be formatted.</param>
        /// <param name="mappings">The column mappings to use to format the table.</param>
        void CreateTable(Worksheet worksheet, IEnumerable<ColumnDataSource> mappings);

        /// <summary>
        /// Creates an Excel list object containing the table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns>The list object.</returns>
        ListObject CreateListObject(ExcelDataTable dataTable);

        /// <summary>
        /// Creates and formats a new table in the specified range.
        /// </summary>
        /// <param name="tableRange">The worksheet range in which the table will be created.</param>
        /// <returns>The created table.</returns>
        TableRange CreateTableRange(Range tableRange);

        /// <summary>
        /// Updates the columns of the data table to match the specified mappings.
        /// </summary>
        /// <param name="dataTable">The data table which will be updated.</param>
        /// <param name="mappings">The new column mappings to use to format the table.</param>
        void UpdateListObject(ExcelDataTable dataTable, IEnumerable<ColumnDataSource> mappings);
    }
}