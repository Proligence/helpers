﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericColumnDataSource.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;

    /// <summary>
    /// Generic implementation of <see cref="ColumnDataSource"/>.
    /// </summary>
    /// <typeparam name="T">The type which contains the mapped property.</typeparam>
    public class GenericColumnDataSource<T> : ColumnDataSource
    {
        /// <summary>
        /// Gets or sets the cell value getter.
        /// </summary>
        public Func<T, object> Getter { get; set; }

        /// <summary>
        /// Gets or sets the cell value setter.
        /// </summary>
        public Action<T, object> Setter { get; set; }

        /// <summary>
        /// Gets the value from the mapped data source for the cell at the specified zero-based row.
        /// </summary>
        /// <param name="dataObject">The object which contains the row data.</param>
        /// <returns>The value which should be stored in the cell.</returns>
        public override object GetValue(object dataObject)
        {
            return this.Getter((T)dataObject);
        }

        /// <summary>
        /// Sets the value at the mapped data source from the cell at the specified zero-based row.
        /// </summary>
        /// <param name="dataObject">The object which contains the row data.</param>
        /// <param name="value">The value which should be set at the data object.</param>
        public override void SetValue(object dataObject, object value)
        {
            this.Setter((T)dataObject, value);
        }
    }
}