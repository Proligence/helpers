﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TableRange.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Represents an Excel worksheet range which contains a formatted table.
    /// </summary>
    public class TableRange
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableRange"/> class.
        /// </summary>
        /// <param name="worksheet">The worksheet which contains the range.</param>
        /// <param name="name">The name of the list object which contains the formatted table.</param>
        public TableRange(Worksheet worksheet, string name)
        {
            if (worksheet == null)
            {
                throw new ArgumentNullException("worksheet");
            }

            this.Worksheet = worksheet;

            this.ListObject = worksheet.ListObjects.Cast<ListObject>().FirstOrDefault(lo => lo.Name == name);
            if (this.ListObject == null)
            {
                throw new ArgumentException("Failed to find list object '" + name + "'.", "name");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableRange"/> class.
        /// </summary>
        /// <param name="worksheet">The worksheet which contains the range.</param>
        /// <param name="listObject">The list object which contains the formatted table.</param>
        public TableRange(Worksheet worksheet, ListObject listObject)
        {
            this.Worksheet = worksheet;
            this.ListObject = listObject;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableRange"/> class.
        /// </summary>
        protected TableRange()
        {
        }

        /// <summary>
        /// Gets or sets the worksheet which contains the data table.
        /// </summary>
        public Worksheet Worksheet { get; protected set; }

        /// <summary>
        /// Gets or sets the object which represents the Excel list containing the table.
        /// </summary>
        public ListObject ListObject { get; protected set; }

        /// <summary>
        /// Gets the range which contains the entire table.
        /// </summary>
        public Range Table
        {
            get
            {
                return this.ListObject.Range;
            }
        }

        /// <summary>
        /// Gets the range which contains the header row of the table.
        /// </summary>
        public Range Header
        {
            get
            {
                return this.ListObject.HeaderRowRange;
            }
        }

        /// <summary>
        /// Gets the range which contains all of the table's rows (without the header).
        /// </summary>
        public Range Rows
        {
            get
            {
                return this.ListObject.DataBodyRange;
            }
        }

        /// <summary>
        /// Gets the range which contains the table row with the specified index.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the table row to get.</param>
        /// <returns>The range which contains the specified table row.</returns>
        public Range GetRow(int rowIndex)
        {
            if (this.ListObject.ListRows.Count > rowIndex)
            {
                return this.ListObject.ListRows[rowIndex + 1].Range;
            }

            return null;
        }

        /// <summary>
        /// Gets the range which contains all data rows of the data table.
        /// </summary>
        /// <param name="startIndex">The zero-based index of the first row.</param>
        /// <param name="rowCount">The number of rows in the range.</param>
        /// <returns>The range which contains all data rows of the data table.</returns>
        public Range GetRowRange(int startIndex, int rowCount)
        {
            int columnCount = this.ListObject.ListColumns.Count;
            return this.ListObject.DataBodyRange.Offset[startIndex, 0].Resize[rowCount, columnCount];
        }

        /// <summary>
        /// Gets the indexes of the rows which are currently selected in the worksheet.
        /// </summary>
        /// <returns>A sequence containing the indexes of the selected rows.</returns>
        public IEnumerable<int> GetSelectedRowIndexes()
        {
            return from Range area in ((Range)this.Worksheet.Application.Selection).Areas
                   from Range row in area.Rows
                   where row.Row >= 2
                   select (row.Row - 2);
        }

        /// <summary>
        /// Gets the indexes of the rows which are currently selected in the worksheet.
        /// </summary>
        /// <returns>A sequence containing the indexes of the selected rows.</returns>
        public IEnumerable<Range> GetSelectedRows()
        {
            return this.GetSelectedRowIndexes().Select(this.GetRow);
        }

        /// <summary>
        /// Determines whether the data table row with the specified index is empty.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the data table row.</param>
        /// <returns><c>true</c> if the specified data table row is empty; otherwise, <c>false</c>.</returns>
        public bool IsEmptyRow(int rowIndex)
        {
            Range rowRange = this.GetRow(rowIndex);
            return (int)this.Worksheet.Application.WorksheetFunction.CountA(rowRange) == 0;
        }

        /// <summary>
        /// Gets the value of a cell in a data table row.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the data table row.</param>
        /// <param name="columnIndex">The zero-based index of the data table column.</param>
        /// <returns>The value of the specified cell.</returns>
        public dynamic GetCellValue(int rowIndex, int columnIndex)
        {
            return this.Rows.Offset[rowIndex, columnIndex].Value;
        }
    }
}