﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TableFormatter.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Formats tables in Excel worksheets.
    /// </summary>
    public class TableFormatter : ITableFormatter
    {
        /// <summary>
        /// Formats the data table in an Excel worksheet.
        /// </summary>
        /// <param name="worksheet">The worksheet in which the data table will be formatted.</param>
        /// <param name="mappings">The column mappings to use to format the table.</param>
        public void CreateTable(Worksheet worksheet, IEnumerable<ColumnDataSource> mappings)
        {
            if (worksheet == null)
            {
                throw new ArgumentNullException("worksheet");
            }

            if (mappings == null)
            {
                throw new ArgumentNullException("mappings");
            }

            worksheet.Cells.Clear();

            IEnumerable<ColumnDataSource> columnDataSources = SortColumnMappings(mappings).ToArray();

            int currentColumnIndex = 0;
            foreach (ColumnDataSource mapping in columnDataSources)
            {
                Range range = worksheet.Range["A1", "A1"].Offset[0, currentColumnIndex];
                range.Value = mapping.Name;
                currentColumnIndex++;
            }

            UpdateColumnFormatting(worksheet, columnDataSources);
        }

        /// <summary>
        /// Creates an Excel list object containing the table.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns>The list object.</returns>
        public ListObject CreateListObject(ExcelDataTable dataTable)
        {
            if (dataTable == null)
            {
                throw new ArgumentNullException("dataTable");
            }

            Range tableRange = dataTable.GetTableRange();
            tableRange.Font.Name = dataTable.FontName;
            tableRange.Font.Size = dataTable.FontSize;

            ListObject listObject = dataTable.Worksheet.ListObjects.Add(
                XlListObjectSourceType.xlSrcRange,
                tableRange,
                XlListObjectHasHeaders: XlYesNoGuess.xlYes);

            listObject.Name = dataTable.RangeName;
            listObject.TableStyle = dataTable.StyleName;

            UpdateColumnFormatting(dataTable.Worksheet, SortColumnMappings(dataTable.Mappings));

            return listObject;
        }

        /// <summary>
        /// Creates and formats a new table in the specified range.
        /// </summary>
        /// <param name="tableRange">The worksheet range in which the table will be created.</param>
        /// <returns>The created table.</returns>
        public TableRange CreateTableRange(Range tableRange)
        {
            if (tableRange == null)
            {
                throw new ArgumentNullException("tableRange");
            }

            ListObject listObject = tableRange.Worksheet.ListObjects.Add(
                XlListObjectSourceType.xlSrcRange,
                tableRange,
                XlListObjectHasHeaders: XlYesNoGuess.xlYes);

            return new TableRange(tableRange.Worksheet, listObject);
        }

        /// <summary>
        /// Updates the columns of the data table to match the specified mappings.
        /// </summary>
        /// <param name="dataTable">The data table which will be updated.</param>
        /// <param name="mappings">The new column mappings to use to format the table.</param>
        public void UpdateListObject(ExcelDataTable dataTable, IEnumerable<ColumnDataSource> mappings)
        {
            if (dataTable == null)
            {
                throw new ArgumentNullException("dataTable");
            }

            if (mappings == null)
            {
                throw new ArgumentNullException("mappings");
            }

            IEnumerable<ColumnDataSource> columnDataSources = SortColumnMappings(mappings).ToArray();
            ListObject listObject = dataTable.ListObject;

            int currentColumnIndex = 0;
            foreach (ColumnDataSource mapping in columnDataSources)
            {
                ListColumn currentColumn = null;
                if (currentColumnIndex + 1 <= listObject.ListColumns.Count)
                {
                    currentColumn = listObject.ListColumns[currentColumnIndex + 1];
                }

                if ((currentColumn != null) && (currentColumn.Name == mapping.Name))
                {
                    currentColumnIndex++;
                    continue;
                }

                ListColumn existingColumn = null;
                for (int i = currentColumnIndex + 1; i < listObject.ListColumns.Count; i++)
                {
                    ListColumn column = listObject.ListColumns[i];
                    if (column.Name == mapping.Name)
                    {
                        existingColumn = column;
                        break;
                    }
                }

                ListColumn newColumn = listObject.ListColumns.Add(currentColumnIndex + 1);
                if (existingColumn != null)
                {
                    existingColumn.Range.Copy(newColumn.Range);
                    existingColumn.Delete();
                }

                newColumn.Name = mapping.Name;

                currentColumnIndex++;
            }

            UpdateColumnFormatting(dataTable.Worksheet, columnDataSources);
        }

        /// <summary>
        /// Sorts the specified column mappings by column index.
        /// </summary>
        /// <param name="mappings">The mappings to sort.</param>
        /// <returns>The sorted mappings.</returns>
        private static IEnumerable<ColumnDataSource> SortColumnMappings(IEnumerable<ColumnDataSource> mappings)
        {
            IEnumerable<ColumnDataSource> result = mappings.ToArray();
            if (result.Any(m => m.Index != null))
            {
                result = result.OrderBy(m => m.Index);
            }

            return result;
        }

        /// <summary>
        /// Updates the column formatting of the specified worksheet to match the specified mappings.
        /// </summary>
        /// <param name="worksheet">The worksheet to update.</param>
        /// <param name="mappings">The column mappings.</param>
        private static void UpdateColumnFormatting(_Worksheet worksheet, IEnumerable<ColumnDataSource> mappings)
        {
            int currentColumnIndex = 0;
            foreach (ColumnDataSource mapping in mappings)
            {
                Range range = worksheet.Range["A1", "A1"].Offset[0, currentColumnIndex];

                if (mapping.Width != 0)
                {
                    range.ColumnWidth = (double)mapping.Width;
                }

                Range entireColumn = range.EntireColumn;

                if (mapping.NumberFormat != null)
                {
                    entireColumn.NumberFormat = mapping.NumberFormat;
                }

                if (mapping.HorizontalAlignment != null)
                {
                    entireColumn.HorizontalAlignment = (XlHAlign)Enum.Parse(
                        typeof(XlHAlign),
                        mapping.HorizontalAlignment);
                }

                if ((mapping.Values != null) && (mapping.Values.Count > 0))
                {
                    string flatlist = string.Join(";", mapping.Values);
                    entireColumn.Validation.Delete();
                    
                    entireColumn.Validation.Add(
                        XlDVType.xlValidateList,
                        XlDVAlertStyle.xlValidAlertStop,
                        XlFormatConditionOperator.xlBetween,
                        flatlist);

                    entireColumn.Validation.IgnoreBlank = true;
                    entireColumn.Validation.InCellDropdown = true;
                }

                currentColumnIndex++;
            }
        }
    }
}