﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExcelDataTable.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Represents a data table in an Excel worksheet.
    /// </summary>
    public class ExcelDataTable
    {
        /// <summary>
        /// The number of data rows which are currently in the table (used to detect row addition/deletion).
        /// </summary>
        private int dataRowCount;

        /// <summary>
        /// Determines whether the events of this object are temporarily suppressed.
        /// </summary>
        private bool eventsSuppressed;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelDataTable"/> class.
        /// </summary>
        /// <param name="worksheet">The worksheet which contains the data table.</param>
        /// <param name="mappings">The column data sources mapped to the data table's columns.</param>
        public ExcelDataTable(Worksheet worksheet, IEnumerable<ColumnDataSource> mappings)
        {
            this.Worksheet = worksheet;
            this.Mappings = mappings.ToList();
            this.RangeName = Guid.NewGuid().ToString().Replace("-", string.Empty);
            this.TableFormatter = new TableFormatter();
            this.TableBinder = new TableBinder(this.Mappings);
            this.StyleName = "TableStyleLight9";
            this.FontName = "Tahoma";
            this.FontSize = 8;

            this.Worksheet.Change += this.OnWorksheetChange;
        }

        /// <summary>
        /// Occurs when the cells of the data table are changed.
        /// </summary>
        public event EventHandler<RangeEventArgs> Updated;

        /// <summary>
        /// Occurs when rows are inserted into the data table.
        /// </summary>
        public event EventHandler<RangeEventArgs> Inserted;

        /// <summary>
        /// Occurs when rows of the data table are deleted.
        /// </summary>
        public event EventHandler<RangeEventArgs> Deleted;

        /// <summary>
        /// Gets the worksheet which contains the data table.
        /// </summary>
        public Worksheet Worksheet { get; private set; }

        /// <summary>
        /// Gets the object which represents the Excel list containing the table.
        /// </summary>
        public ListObject ListObject { get; private set; }

        /// <summary>
        /// Gets the column data sources mapped to the data table's columns.
        /// </summary>
        public IList<ColumnDataSource> Mappings { get; private set; }

        /// <summary>
        /// Gets or sets the <see cref="ITableFormatter"/> used to format the table.
        /// </summary>
        public ITableFormatter TableFormatter { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ITableBinder"/> used to bind the data source to the Excel table.
        /// </summary>
        public ITableBinder TableBinder { get; set; }

        /// <summary>
        /// Gets or sets the name of the Excel range which contains the data table.
        /// </summary>
        public string RangeName { get; set; }

        /// <summary>
        /// Gets or sets the name of the style which will be used to format the table.
        /// </summary>
        public string StyleName { get; set; }

        /// <summary>
        /// Gets or sets the name of the font which will be used to format the table.
        /// </summary>
        public string FontName { get; set; }

        /// <summary>
        /// Gets or sets the size of the font which will be used to format the table.
        /// </summary>
        public int FontSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether row updating is disabled.
        /// </summary>
        public bool DisableUpdate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether row inserting is disabled.
        /// </summary>
        public bool DisableInsert { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether row deletion is disabled.
        /// </summary>
        public bool DisableDelete { get; set; }

        /// <summary>
        /// Formats the table range which contains non-empty cells.
        /// </summary>
        public virtual void FormatTable()
        {
            this.SuppressEvents(
                () =>
                {
                    this.TableFormatter.CreateTable(this.Worksheet, this.Mappings);
                    this.ListObject = this.TableFormatter.CreateListObject(this);
                    this.dataRowCount = this.ListObject.DataBodyRange.Rows.Count;
                });
        }

        /// <summary>
        /// Updates the column mappings of the data table.
        /// </summary>
        /// <param name="mappings">The new column mappings.</param>
        public virtual void UpdateMappings(ColumnDataSource[] mappings)
        {
            this.SuppressEvents(
                () =>
                {
                    this.TableFormatter.UpdateListObject(this, mappings);
                    
                    this.Mappings.Clear();
                    foreach (ColumnDataSource mapping in mappings)
                    {
                        this.Mappings.Add(mapping);
                    }
                    
                    this.dataRowCount = this.ListObject.DataBodyRange.Rows.Count;
                });
        }

        /// <summary>
        /// Binds the specified data to the data table.
        /// </summary>
        /// <param name="data">The data to bind to the table.</param>
        public virtual void BindData(object data)
        {
            this.SuppressEvents(
                () =>
                {
                    this.TableBinder.Bind(this, data);
                    this.dataRowCount = this.ListObject.DataBodyRange.Rows.Count;
                });
        }

        /// <summary>
        /// Displays the specified data in the data table.
        /// </summary>
        /// <param name="data">The data to display to the table.</param>
        public virtual void DisplayData(object data)
        {
            this.SuppressEvents(
                () =>
                {
                    this.TableBinder.Display(this, data);
                    this.dataRowCount = this.ListObject.DataBodyRange.Rows.Count;
                });
        }

        /// <summary>
        /// Gets the range which contains the entire data table.
        /// </summary>
        /// <param name="rowCount">
        /// The number of rows which will be included in the range or <c>-1</c> to get the range with currently filled 
        /// rows.
        /// </param>
        /// <returns>The range which contains the entire data table.</returns>
        public Range GetTableRange(int rowCount = -1)
        {
            if (this.ListObject != null)
            {
                return this.ListObject.Range;
            }

            int columnCount = this.Mappings.Count() - 1;
            if (columnCount < 0)
            {
                columnCount = 0;
            }

            if (rowCount == -1)
            {
                rowCount = 0;

                int index = 0;
                Range rowRange = this.GetRowRange(index);
                while ((int)this.Worksheet.Application.WorksheetFunction.CountA(rowRange) != 0)
                {
                    index++;
                    rowCount++;
                    rowRange = rowRange.Offset[1, 0];
                }
            }

            Range topLeft = this.Worksheet.Range["A1", "A1"];
            Range bottomRight = topLeft.Offset[rowCount + 1, columnCount];
            return this.Worksheet.Range[topLeft, bottomRight];
        }

        /// <summary>
        /// Gets the range which contains the header of the data table.
        /// </summary>
        /// <returns>The range.</returns>
        public Range GetHeaderRange()
        {
            if (this.ListObject != null)
            {
                return this.ListObject.HeaderRowRange;
            }

            Range left = this.Worksheet.Range["A1", "A1"];
            Range right = left.Offset[0, this.Mappings.Count() - 1];

            return this.Worksheet.Range[left, right];
        }

        /// <summary>
        /// Gets the columns which mappings specify certain criteria.
        /// </summary>
        /// <param name="selector">Delegate which selects columns to return.</param>
        /// <returns>Ranges for columns for which <paramref name="selector"/> returned <c>true</c>.</returns>
        public IEnumerable<Range> GetColumns(Func<ColumnDataSource, bool> selector = null)
        {
            if (selector == null)
            {
                selector = c => true;
            }

            var results = new List<Range>();

            int index = 0;
            foreach (ColumnDataSource mapping in this.Mappings)
            {
                if (selector(mapping))
                {
                    if (this.ListObject != null)
                    {
                        results.Add(this.ListObject.ListColumns[index + 1].Range);
                    }
                    else
                    {
                        results.Add(this.Worksheet.Range["A1", "A1"].Offset[0, index].EntireColumn);
                    }
                }

                index++;
            }

            return results;
        }

        /// <summary>
        /// Gets the index of the first column which satisfies the specified selector.
        /// </summary>
        /// <param name="selector">The selector delegate.</param>
        /// <returns>The zero-base index of the first column which satisfies the selector or <c>-1</c>.</returns>
        public int GetColumnIndex(Func<ColumnDataSource, bool> selector)
        {
            if (selector == null)
            {
                throw new ArgumentNullException("selector");
            }

            int index = 0;
            foreach (ColumnDataSource mapping in this.Mappings)
            {
                if (selector(mapping))
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        /// <summary>
        /// Gets the range which contains the specified data table row.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the data table row.</param>
        /// <returns>The range which contains the specified data table row.</returns>
        public Range GetRowRange(int rowIndex)
        {
            if (this.ListObject != null)
            {
                if (this.ListObject.ListRows.Count > rowIndex)
                {
                    return this.ListObject.ListRows[rowIndex + 1].Range;
                }

                return null;
            }

            Range left = this.Worksheet.Range["A2", "A2"].Offset[rowIndex, 0];
            Range right = left.Offset[0, this.Mappings.Count() - 1];

            return this.Worksheet.Range[left, right];
        }

        /// <summary>
        /// Gets the range which contains all data rows of the data table.
        /// </summary>
        /// <returns>The range which contains all data rows of the data table.</returns>
        public Range GetRowsRange()
        {
            if (this.ListObject != null)
            {
                return this.ListObject.DataBodyRange;
            }

            Range tableRange = this.GetTableRange();
            return tableRange.Offset[1, 0].Resize[tableRange.Rows.Count - 1, tableRange.Columns.Count];
        }

        /// <summary>
        /// Gets the range which contains all data rows of the data table.
        /// </summary>
        /// <param name="startIndex">The zero-based index of the first row.</param>
        /// <param name="rowCount">The number of rows in the range.</param>
        /// <returns>The range which contains all data rows of the data table.</returns>
        public Range GetRowsRange(int startIndex, int rowCount)
        {
            if (this.ListObject != null)
            {
                int columnCount = this.ListObject.ListColumns.Count;
                return this.ListObject.DataBodyRange.Offset[startIndex, 0].Resize[rowCount, columnCount];
            }

            Range tableRange = this.GetTableRange();
            return tableRange.Offset[startIndex + 2, 0].Resize[rowCount, tableRange.Columns.Count];
        }

        /// <summary>
        /// Gets the indexes of the rows which are currently selected in the worksheet.
        /// </summary>
        /// <returns>A sequence containing the indexes of the selected rows.</returns>
        public IEnumerable<int> GetSelectedRows()
        {
            Range selection = (Range)this.Worksheet.Application.Selection;
            return from Range area in selection.Areas
                   from Range row in area.Rows
                   where row.Row >= 2
                   select (row.Row - 2);
        }

        /// <summary>
        /// Determines whether the data table row with the specified index is empty.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the data table row.</param>
        /// <returns><c>true</c> if the specified data table row is empty; otherwise, <c>false</c>.</returns>
        public bool IsEmptyRow(int rowIndex)
        {
            Range rowRange = this.GetRowRange(rowIndex);
            return (int)this.Worksheet.Application.WorksheetFunction.CountA(rowRange) == 0;
        }

        /// <summary>
        /// Gets the value of a cell in a data table row.
        /// </summary>
        /// <param name="rowIndex">The zero-based index of the data table row.</param>
        /// <param name="columnIndex">The zero-based index of the data table column.</param>
        /// <returns>The value of the specified cell.</returns>
        public dynamic GetCellValue(int rowIndex, int columnIndex)
        {
            Range baseRange = this.Worksheet.Range["A1", "A1"];
            return baseRange.Offset[rowIndex + 1, columnIndex].Value;
        }

        /// <summary>
        /// Enables text wrapping for the specified column.
        /// </summary>
        /// <param name="columnIndex">The zero-based index of the column.</param>
        public void EnableTextWrapping(int columnIndex)
        {
            Range range = this.Worksheet.Range["A1", "A1"].Offset[0, columnIndex].EntireColumn;
            this.EnableTextWrapping(range);
        }

        /// <summary>
        /// Enables text wrapping for the specified range.
        /// </summary>
        /// <param name="range">The Excel range.</param>
        public void EnableTextWrapping(Range range)
        {
            if ((range != null) && !(bool)range.WrapText)
            {
                range.WrapText = true;
            }
        }

        /// <summary>
        /// Executes the specified action with the <see cref="Updated"/> event suppressed.
        /// </summary>
        /// <param name="action">The action to invoke.</param>
        public void SuppressEvents(System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            this.eventsSuppressed = true;
            try
            {
                action();
            }
            finally
            {
                this.eventsSuppressed = false;
            }
        }

        /// <summary>
        /// Raises the <see cref="Updated" /> event.
        /// </summary>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        protected virtual void OnUpdated(RangeEventArgs e)
        {
            if (!this.eventsSuppressed)
            {
                EventHandler<RangeEventArgs> handler = this.Updated;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="Inserted" /> event.
        /// </summary>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        protected virtual void OnInserted(RangeEventArgs e)
        {
            if (!this.eventsSuppressed)
            {
                EventHandler<RangeEventArgs> handler = this.Inserted;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="Deleted" /> event.
        /// </summary>
        /// <param name="e">The <see cref="RangeEventArgs"/> instance containing the event data.</param>
        protected virtual void OnDeleted(RangeEventArgs e)
        {
            if (!this.eventsSuppressed)
            {
                EventHandler<RangeEventArgs> handler = this.Deleted;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        /// <summary>
        /// Rolls back the last change made by the user without triggering any events.
        /// </summary>
        protected void SilentUndo()
        {
            if (!this.eventsSuppressed)
            {
                this.SuppressEvents(() => this.Worksheet.Application.Undo());
            }
            else
            {
                try
                {
                    this.Worksheet.Application.Undo();
                }
                catch (COMException ex)
                {
                    System.Diagnostics.Trace.WriteLine("SilentUndo: " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Called when the Excel worksheet is changed.
        /// </summary>
        /// <param name="target">The target.</param>
        private void OnWorksheetChange(Range target)
        {
            if (this.DisableUpdate && !this.eventsSuppressed)
            {
                this.SilentUndo();
                return;
            }

            try
            {
                Range range = this.Worksheet.Application.Intersect(this.GetTableRange(), target);
                if ((range != null) && (range.Rows.Count > 0))
                {
                    // Handle row deletion and insertion
                    if (this.ListObject != null)
                    {
                        int count = 0;
                        if (this.ListObject.DataBodyRange != null)
                        {
                            count = this.ListObject.DataBodyRange.Rows.Count;
                        }

                        if (count < this.dataRowCount)
                        {
                            if (this.DisableDelete && !this.eventsSuppressed)
                            {
                                this.SilentUndo();
                                return;
                            }

                            this.OnDeleted(new RangeEventArgs(range));
                            this.dataRowCount = count;
                            return;
                        }
                        
                        if (count > this.dataRowCount)
                        {
                            if (this.DisableInsert && !this.eventsSuppressed)
                            {
                                this.SilentUndo();
                                return;
                            }

                            this.OnInserted(new RangeEventArgs(range));
                            this.dataRowCount = count;
                            return;
                        }
                    }

                    this.OnUpdated(new RangeEventArgs(range));

                    if (this.ListObject != null)
                    {
                        this.dataRowCount = this.ListObject.DataBodyRange.Rows.Count;
                    }
                }
            }
            catch
            {
                // Rollback the change
                this.SilentUndo();
                throw;
            }
        }
    }
}