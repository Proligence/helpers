﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PropertyColumnDataSource.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Reflection;

    /// <summary>
    /// Binds an excel column to the specified property of the <typeparamref name="T"/> type.
    /// </summary>
    /// <typeparam name="T">The type which contains the mapped property.</typeparam>
    public class PropertyColumnDataSource<T> : ColumnDataSource
    {
        /// <summary>
        /// The descriptor of the property which holds the value for the column's cells.
        /// </summary>
        private readonly PropertyInfo propertyInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyColumnDataSource{T}"/> class.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        /// <param name="columnName">The name of the column.</param>
        public PropertyColumnDataSource(string propertyName, string columnName = null)
        {
            var bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            this.propertyInfo = typeof(T).GetProperty(propertyName, bindingFlags);
            if (this.propertyInfo == null)
            {
                throw new InvalidOperationException(
                    "Failed to resolve property '" + typeof(T).FullName + "." + propertyName + "'.");
            }

            this.Name = columnName;
            this.TypeConverter = new TypeConverter();

            this.InitializeValues();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyColumnDataSource{T}"/> class.
        /// </summary>
        /// <param name="selector">An expression which returns the property to map.</param>
        /// <param name="columnName">The name of the column.</param>
        public PropertyColumnDataSource(Expression<Func<T, object>> selector, string columnName = null)
        {
            Expression body = selector;

            var lambdaBody = body as LambdaExpression;
            if (lambdaBody != null)
            {
                body = lambdaBody.Body;
            }

            if (body.NodeType == ExpressionType.Convert)
            {
                body = ((UnaryExpression)body).Operand;
            }

            if (body.NodeType != ExpressionType.MemberAccess)
            {
                throw new InvalidOperationException("The specified expression does not select a valid property.");
            }

            this.propertyInfo = (PropertyInfo)((MemberExpression)body).Member;
            this.Name = columnName;
            this.TypeConverter = new TypeConverter();

            this.InitializeValues();
        }

        /// <summary>
        /// Gets or sets the <see cref="ITypeConverter"/> which will be used to convert values between Excel cells
        /// and the mapped object's property.
        /// </summary>
        public ITypeConverter TypeConverter { get; set; }

        /// <summary>
        /// Gets or sets the function which converts the cell's value to the property's type.
        /// </summary>
        public Func<string, object> ValueConverter { get; set; }

        /// <summary>
        /// Gets or sets the function which converts the property's value to the cell's text.
        /// </summary>
        public Func<object, string> ValueFormatter { get; set; }

        /// <summary>
        /// Converts cell values to <see cref="Guid"/> type.
        /// </summary>
        /// <param name="obj">The value to convert.</param>
        /// <returns>The conversion result.</returns>
        public static object GuidValueConverter(object obj)
        {
            if (obj != null)
            {
                return Guid.Parse(obj.ToString());
            }

            return Guid.Empty;
        }
        
        /// <summary>
        /// Gets the value from the mapped data source for the cell at the specified zero-based row.
        /// </summary>
        /// <param name="dataObject">The object which contains the row data.</param>
        /// <returns>The value which should be stored in the cell.</returns>
        public override object GetValue(object dataObject)
        {
            object value = this.propertyInfo.GetValue(dataObject, new object[0]);
            if (this.ValueFormatter != null)
            {
                value = this.ValueFormatter(value);
            }

            return value;
        }

        /// <summary>
        /// Sets the value at the mapped data source from the cell at the specified zero-based row.
        /// </summary>
        /// <param name="dataObject">The object which contains the row data.</param>
        /// <param name="value">The value which should be set at the data object.</param>
        public override void SetValue(object dataObject, object value)
        {
            if (this.ValueConverter != null)
            {
                value = this.ValueConverter(value != null ? value.ToString() : null);
            }
            else if (value == null)
            {
                if (this.propertyInfo.PropertyType.IsValueType)
                {
                    value = Activator.CreateInstance(this.propertyInfo.PropertyType);
                }
            }
            else if (value.GetType() != this.propertyInfo.PropertyType)
            {
                value = this.TypeConverter.Convert(value, this.propertyInfo.PropertyType, CultureInfo.CurrentCulture);
            }

            this.propertyInfo.SetValue(dataObject, value, new object[0]);
        }

        /// <summary>
        /// Initializes the <see cref="ColumnDataSource.Values"/> property for enumeration types.
        /// </summary>
        private void InitializeValues()
        {
            if (this.propertyInfo.PropertyType.IsEnum)
            {
                this.Values = Enum.GetNames(this.propertyInfo.PropertyType);
            }
            else if (this.propertyInfo.PropertyType.IsGenericType && this.propertyInfo.PropertyType.IsValueType)
            {
                Type nullableUnderlyingType = Nullable.GetUnderlyingType(this.propertyInfo.PropertyType);
                if ((nullableUnderlyingType != null) && nullableUnderlyingType.IsEnum)
                {
                    this.Values = Enum.GetNames(nullableUnderlyingType);
                }
            }
        }
    }
}