﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TableRangeView.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Proligence.Helpers.Office.Common;
    using Proligence.Helpers.Office.Excel.Mvp;

    /// <summary>
    /// The base class for classes which implement Excel MVP views based on table ranges.
    /// </summary>
    /// <typeparam name="TModel">The type which represents the view's model.</typeparam>
    public abstract class TableRangeView<TModel> : WorksheetView<TModel>
        where TModel : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableRangeView{TModel}" /> class.
        /// </summary>
        /// <param name="application">The Excel application instance.</param>
        /// <param name="progressNotifier">The internal progress notification implementation.</param>
        /// <param name="tableFormatter">The table formatter service.</param>
        protected TableRangeView(
            IExcelApplication application,
            IProgressNotifier progressNotifier,
            ITableFormatter tableFormatter)
            : base(application, progressNotifier)
        {
            this.TableFormatter = tableFormatter;
        }

        /// <summary>
        /// Gets the table formatter.
        /// </summary>
        protected ITableFormatter TableFormatter { get; private set; }

        /// <summary>
        /// Gets or sets the data table which contains the user profiles list.
        /// </summary>
        protected TableRange DataTable { get; set; }

        /// <summary>
        /// Displays the specified model in the current view.
        /// </summary>
        /// <param name="model">The model to bind.</param>
        public override void DisplayModel(TModel model)
        {
            if (model == null)
            {
                return;
            }

            this.SetupDataTable(model);

            if (this.DataTable != null)
            {
                ColumnDataSource[] mappings = this.GenerateMappings(model);
                this.UpdateDataTable(mappings);

                var tableBinder = new TableBinder(mappings);
                IList rows = this.GetRowModels(model).Cast<object>().ToList();
                tableBinder.Bind(this.DataTable.ListObject, rows);

                if (this.DataTable.Worksheet != null)
                {
                    this.DataTable.Worksheet.Columns.AutoFit();
                }
            }
        }

        /// <summary>
        /// Generates data table mappings based on the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>The generated column mappings.</returns>
        protected abstract ColumnDataSource[] GenerateMappings(TModel model);

        /// <summary>
        /// Gets the model for each row of the table from the view's model.
        /// </summary>
        /// <param name="model">The view model.</param>
        /// <returns>Sequence of row data objects.</returns>
        protected abstract IEnumerable GetRowModels(TModel model);

        /// <summary>
        /// Sets up the current worksheet for displaying and storing a list of user profiles.
        /// </summary>
        /// <param name="model">The model.</param>
        protected virtual void SetupDataTable(TModel model)
        {
            if (this.DataTable != null)
            {
                return;
            }

            if (this.Application.CurrentWorksheet == null)
            {
                return;
            }

            if (!this.Application.IsActiveWorksheetEmpty())
            {
                this.Application.CurrentWorksheet.Rows.Clear();
            }

            ColumnDataSource[] mappings = this.GenerateMappings(model);
            this.TableFormatter.CreateTable(this.Application.CurrentWorksheet, mappings);

            var range = this.Application.CurrentWorksheet.Range["A1", "A1"].Resize[1, mappings.Length];
            this.DataTable = this.TableFormatter.CreateTableRange(range);
        }

        /// <summary>
        /// Updates columns of existing data table range.
        /// </summary>
        /// <param name="mappings">The new column mappings.</param>
        protected virtual void UpdateDataTable(IEnumerable<ColumnDataSource> mappings)
        {
            IList<string> columnNames = this.GetColumnNames();

            List<ColumnDataSource> newColumnMappings = mappings
                .Where(m => !columnNames.Contains(m.Name))
                .ToList();

            if (newColumnMappings.Any())
            {
                var cell = this.Application.CurrentWorksheet.Range["A1", "A1"].Offset[0, columnNames.Count];
                foreach (ColumnDataSource mapping in newColumnMappings)
                {
                    cell.Value = mapping.Name;
                    cell = cell.Offset[0, 1];
                }
            }
        }

        /// <summary>
        /// Gets the names of the columns in the current worksheet.
        /// </summary>
        /// <returns>Sequence of column names.</returns>
        protected virtual IList<string> GetColumnNames()
        {
            var columnNames = new List<string>();

            var cell = this.Application.CurrentWorksheet.Range["A1", "A1"];
            while (cell.Value != null)
            {
                columnNames.Add(cell.Value.ToString());
                cell = cell.Offset[0, 1];
            }

            return columnNames;
        }
    }
}