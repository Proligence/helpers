﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressNotifier.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using Proligence.Helpers.Autofac;
    using Proligence.Helpers.Office.Common;

    /// <summary>
    /// Notifies the user of the progress of a long task.
    /// </summary>
    [Dependency(DependencyType.SingleInstance, typeof(IProgressNotifier))]
    public class ProgressNotifier : IProgressNotifier
    {
        /// <summary>
        /// The Excel application instance.
        /// </summary>
        private readonly IExcelApplication application;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressNotifier"/> class.
        /// </summary>
        /// <param name="application">The Excel application instance.</param>
        public ProgressNotifier(IExcelApplication application)
        {
            this.application = application;
        }

        /// <summary>
        /// Updates the displayed status.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void UpdateStatus(string message)
        {
            this.application.Instance.StatusBar = message;
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// Clears and releases the status bar.
        /// </summary>
        public void ClearStatus()
        {
            this.application.Instance.StatusBar = false;
        }
    }
}