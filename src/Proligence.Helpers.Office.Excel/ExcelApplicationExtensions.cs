﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExcelApplicationExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Microsoft.Office.Interop.Excel;
    using Proligence.Helpers.Office.Common;

    /// <summary>
    /// Implements extension methods for the <see cref="IExcelApplication"/> interface.
    /// </summary>
    public static class ExcelApplicationExtensions
    {
        /// <summary>
        /// The <see cref="System.Reflection.Missing.Value"/> object.
        /// </summary>
        private static readonly Missing Missing = Missing.Value;

        /// <summary>
        /// Creates and activates a new worksheet in the current workbook.
        /// </summary>
        /// <param name="application">The Excel application instance wrapper.</param>
        /// <returns>The created worksheet.</returns>
        public static Worksheet NewWorksheet(this IExcelApplication application)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            if (application.Instance == null)
            {
                throw new InvalidOperationException("There is no Excel instance.");
            }

            Workbook activeWorkbook = application.CurrentWorkbook;
            if (activeWorkbook == null)
            {
                activeWorkbook = application.Instance.ComInvoke(x => x.Application.Workbooks.Add());

                // ReSharper disable once RedundantCast - avoid ambiguity between method and event
                ((_Workbook)activeWorkbook).ComInvoke(x => x.Activate());
            }

            var worksheet = (Worksheet)activeWorkbook.ComInvoke(x => x.Worksheets.Add());

            // ReSharper disable once RedundantCast - avoid ambiguity between method and event
            ((_Worksheet)worksheet).ComInvoke(x => x.Activate());

            return worksheet;
        }

        /// <summary>
        /// Gets or creates a worksheet with the specified name.
        /// </summary>
        /// <param name="application">The Excel application instance wrapper.</param>
        /// <param name="name">The name of the worksheet to get or create.</param>
        /// <returns>The worksheet instance.</returns>
        public static Worksheet GetOrCreateWorksheet(this IExcelApplication application, string name)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            IEnumerable<Worksheet> worksheets = application.Instance.ComInvoke(
                x => x.Worksheets.Cast<Worksheet>().ToArray());

            Worksheet worksheet = worksheets.ComInvoke(x => x.FirstOrDefault(w => w.Name == name));

            if (worksheet == null)
            {
                Worksheet last = worksheets.Last();
                worksheet = (Worksheet)application.Instance.ComInvoke(x => x.Worksheets.Add(Missing, last, 1, Missing));
                worksheet.ComInvoke(x => x.Name = name);
            }

            return worksheet;
        }

        /// <summary>
        /// Determines whether a worksheet with the specified name exists in the current workbook.
        /// </summary>
        /// <param name="application">The Excel application instance wrapper.</param>
        /// <param name="name">The name of the worksheet to check.</param>
        /// <returns>
        /// <c>true</c> if the current workbook contains a worksheet with the specified name; otherwise, <c>false</c>.
        /// </returns>
        public static bool WorksheetExists(this IExcelApplication application, string name)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            IEnumerable<Worksheet> worksheets = application.Instance.ComInvoke(
                x => x.Worksheets.Cast<Worksheet>().ToArray());

            Worksheet worksheet = worksheets.ComInvoke(x => x.FirstOrDefault(w => w.Name == name));

            return worksheet != null;
        }

        /// <summary>
        /// Determines whether the currently active worksheet is empty (does not contain any data).
        /// </summary>
        /// <param name="application">The Excel application instance wrapper.</param>
        /// <returns><c>true</c> if the currently active worksheet is empty; otherwise, <c>false</c>.</returns>
        public static bool IsActiveWorksheetEmpty(this IExcelApplication application)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            Worksheet worksheet = application.CurrentWorksheet;
            int dataCount = (int)application.Instance.ComInvoke(x => x.WorksheetFunction.CountA(worksheet.Cells));

            return dataCount == 0;
        }

        /// <summary>
        /// Gets the list object with the specified name from the current worksheet.
        /// </summary>
        /// <param name="application">The Excel application instance wrapper.</param>
        /// <param name="name">The name of the list object to get.</param>
        /// <returns>The list object or <c>null</c> if a list object with the specified name does not exist.</returns>
        public static ListObject GetListObject(this IExcelApplication application, string name)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            return application.CurrentWorksheet.ComInvoke(
                x => x.ListObjects
                    .Cast<ListObject>()
                    .FirstOrDefault(obj => obj.Name == name));
        }
    }
}