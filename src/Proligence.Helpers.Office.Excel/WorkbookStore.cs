﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkbookStore.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Xml;
    using Microsoft.Office.Core;
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// Provides a general object storage in Excel workbooks.
    /// </summary>
    public class WorkbookStore : IWorkbookStore
    {
        /// <summary>
        /// The Excel application.
        /// </summary>
        private readonly IExcelApplication application;

        /// <summary>
        /// Contains workbook store XML parts for active Excel workbooks.
        /// </summary>
        private readonly Dictionary<string, WorkbookStoreXmlPart> parts;

        /// <summary>
        /// The underlying data contract serializer.
        /// </summary>
        private readonly DataContractSerializer serializer;

        /// <summary>
        /// XML writer settings used for serialization.
        /// </summary>
        private readonly XmlWriterSettings xmlWriterSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkbookStore" /> class.
        /// </summary>
        /// <param name="application">The Excel application.</param>
        public WorkbookStore(IExcelApplication application)
        {
            if (application == null)
            {
                throw new ArgumentNullException("application");
            }

            this.application = application;
            this.parts = new Dictionary<string, WorkbookStoreXmlPart>();
            this.serializer = new DataContractSerializer(typeof(WorkbookStoreXmlPart));
            this.xmlWriterSettings = new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = false };

            this.application.Instance.WorkbookOpen += this.OnWorkbookOpen;
            this.application.Instance.WorkbookBeforeSave += this.OnWorkbookBeforeSave;
            this.application.Instance.WorkbookAfterSave += this.OnWorkbookAfterSave;
        }

        /// <summary>
        /// Gets the active Excel workbook.
        /// </summary>
        private Workbook ActiveWorkbook
        {
            get
            {
                Workbook workbook = this.application.Instance.ActiveWorkbook;
                if (workbook == null)
                {
                    throw new InvalidOperationException("There is no active workbook.");
                }

                return workbook;
            }
        }

        /// <summary>
        /// Gets an object with the specified name.
        /// </summary>
        /// <typeparam name="T">The type of the object to get.</typeparam>
        /// <param name="name">The name of the object to get.</param>
        /// <returns>
        /// The object deserialized from the workbook or <c>default(T)</c> if the object was not found.
        /// </returns>
        public T Retrieve<T>(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            lock (this.parts)
            {
                WorkbookStoreXmlPart part;
                if (this.parts.TryGetValue(this.ActiveWorkbook.FullName, out part))
                {
                    string xml;
                    if (part.TryGetValue(name, out xml))
                    {
                        using (var xmlReader = XmlReader.Create(new StringReader(xml)))
                        {
                            var valueSerializer = new DataContractSerializer(typeof(T));
                            return (T)valueSerializer.ReadObject(xmlReader);
                        }
                    }
                }

                return default(T);
            }
        }

        /// <summary>
        /// Stores the specified object.
        /// </summary>
        /// <typeparam name="T">The type of the object to store.</typeparam>
        /// <param name="name">The name under which the object will be stored.</param>
        /// <param name="value">The object to store.</param>
        public void Store<T>(string name, T value)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (object.ReferenceEquals(value, null))
            {
                throw new ArgumentNullException("value");
            }

            lock (this.parts)
            {
                WorkbookStoreXmlPart part;
                if (!this.parts.TryGetValue(this.ActiveWorkbook.FullName, out part))
                {
                    part = new WorkbookStoreXmlPart();
                    this.parts.Add(this.ActiveWorkbook.FullName, part);
                }

                using (var textWriter = new StringWriter(CultureInfo.InvariantCulture))
                {
                    using (var xmlWriter = XmlWriter.Create(textWriter, this.xmlWriterSettings))
                    {
                        var valueSerializer = new DataContractSerializer(typeof(T));
                        valueSerializer.WriteObject(xmlWriter, value);
                        xmlWriter.Flush();
                        textWriter.Flush();
                    }

                    part[name] = textWriter.ToString();
                }
            }
        }

        /// <summary>
        /// Removes the specified object from the store.
        /// </summary>
        /// <param name="name">The name of the object to remove.</param>
        public void Remove(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            lock (this.parts)
            {
                WorkbookStoreXmlPart part;
                if (this.parts.TryGetValue(this.ActiveWorkbook.FullName, out part))
                {
                    part.Remove(name);
                }
            }
        }

        /// <summary>
        /// Saves all objects to the workbook.
        /// </summary>
        public void Flush()
        {
            Workbook workbook = this.ActiveWorkbook;
            
            var xmlBuilder = new StringBuilder();
            lock (this.parts)
            {
                WorkbookStoreXmlPart part;
                if (this.parts.TryGetValue(workbook.FullName, out part))
                {
                    using (var xmlWriter = XmlWriter.Create(xmlBuilder, this.xmlWriterSettings))
                    {
                        this.serializer.WriteObject(xmlWriter, part);
                        xmlWriter.Flush();
                    }
                }
            }

            CustomXMLPart xmlPart = GetWorkbookStoreXmlPart(workbook);
            if (xmlPart != null)
            {
                xmlPart.Delete();
            }

            workbook.CustomXMLParts.Add(xmlBuilder.ToString());
        }

        /// <summary>
        /// Clears all unsaved objects and reloads the store from the workbook.
        /// </summary>
        public void Clear()
        {
            Workbook workbook = this.application.Instance.ActiveWorkbook;
            if (workbook == null)
            {
                return;
            }

            CustomXMLPart xmlPart = GetWorkbookStoreXmlPart(workbook);
            if (xmlPart != null)
            {
                using (var stream = new MemoryStream())
                {
                    string xml = xmlPart.XML;

                    // HACK: Fix encoding, should be UTF-8!
                    xml = xml.Replace("encoding=\"utf-16\"", "encoding=\"utf-8\"");

                    WorkbookStoreXmlPart part;
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(xml);
                        writer.Flush();
                        stream.Flush();
                        stream.Seek(0L, SeekOrigin.Begin);

                        part = (WorkbookStoreXmlPart)this.serializer.ReadObject(stream);
                    }

                    lock (this.parts)
                    {
                        this.parts[workbook.FullName] = part;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the custom XML part which contains the serialized workbook store.
        /// </summary>
        /// <param name="workbook">The Excel workbook from which XML part will be returned.</param>
        /// <returns>The custom XML part which contains the serialized workbook store or <c>null</c>.</returns>
        private static CustomXMLPart GetWorkbookStoreXmlPart(_Workbook workbook)
        {
            int length = 0;
            CustomXMLPart result = null;
            foreach (CustomXMLPart part in workbook.CustomXMLParts)
            {
                string xml = part.XML;

                // HACK:
                // The root name is pretty ugly, but there doesn't seem to be any way to change, because it is being
                // serialized using ISerializable interface.
                if (xml.Contains("ArrayOfKeyValueOfstringstring") && (xml.Length > length))
                {
                    length = xml.Length;
                    result = part;
                }
            }

            return result;
        }

        /// <summary>
        /// Called when a workbook is opened.
        /// </summary>
        /// <param name="workbook">The opened workbook.</param>
        private void OnWorkbookOpen(Workbook workbook)
        {
            this.Clear();
        }

        /// <summary>
        /// Called before a workbook is saved.
        /// </summary>
        /// <param name="workbook">The saved workbook.</param>
        /// <param name="saveAsUi">if set to <c>true</c> the Save As dialog box will be displayed.</param>
        /// <param name="cancel">if set to <c>true</c> the save procedure will be canceled.</param>
        private void OnWorkbookBeforeSave(Workbook workbook, bool saveAsUi, ref bool cancel)
        {
            this.Flush();
        }

        /// <summary>
        /// Called after a workbook is saved.
        /// </summary>
        /// <param name="workbook">The saved workbook..</param>
        /// <param name="success"><c>true</c> if the saved succeeded; otherwise, <c>false</c>.</param>
        private void OnWorkbookAfterSave(Workbook workbook, bool success)
        {
            this.Clear();
        }
    }
}