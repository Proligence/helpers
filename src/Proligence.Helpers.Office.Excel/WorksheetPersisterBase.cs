﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorksheetPersisterBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using Microsoft.Office.Interop.Excel;

    /// <summary>
    /// The base class for classes which implement persisting data from worksheets.
    /// </summary>
    /// <typeparam name="TModel">The type which represents the model of the worksheet.</typeparam>
    public abstract class WorksheetPersisterBase<TModel> : IWorksheetPersister<TModel>
    {
        /// <summary>
        /// Updates the specified model with data from the specified worksheet.
        /// </summary>
        /// <param name="worksheet">The worksheet which contains the updated data.</param>
        /// <param name="model">The model which will be updated.</param>
        public abstract void UpdateModel(Worksheet worksheet, TModel model);
    }
}