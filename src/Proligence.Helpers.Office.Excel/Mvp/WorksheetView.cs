﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorksheetView.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel.Mvp
{
    using Proligence.Helpers.Office.Common;
    using Proligence.Helpers.Office.Common.Mvp;

    /// <summary>
    /// The base class for classes which implement Excel MVP views based on worksheets.
    /// </summary>
    /// <typeparam name="TModel">The type which represents the view's model.</typeparam>
    public abstract class WorksheetView<TModel> : View<TModel>, IWorksheetView<TModel>, IProgressNotifier
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorksheetView{TModel}"/> class.
        /// </summary>
        /// <param name="application">The Excel application instance.</param>
        /// <param name="progressNotifier">The internal progress notification implementation.</param>
        protected WorksheetView(IExcelApplication application, IProgressNotifier progressNotifier)
        {
            this.Application = application;
            this.ProgressNotifier = progressNotifier;
        }

        /// <summary>
        /// Gets the Excel application instance.
        /// </summary>
        protected IExcelApplication Application { get; private set; }

        /// <summary>
        /// Gets the internal progress notification implementation.
        /// </summary>
        protected IProgressNotifier ProgressNotifier { get; private set; }

        /// <summary>
        /// Updates the displayed status.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void UpdateStatus(string message)
        {
            this.ProgressNotifier.UpdateStatus(message);
        }

        /// <summary>
        /// Clears and releases the status bar.
        /// </summary>
        public void ClearStatus()
        {
            this.ProgressNotifier.ClearStatus();
        }
    }
}