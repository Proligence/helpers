﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWorkbookStore.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    /// <summary>
    /// Provides a general object storage in Excel workbooks.
    /// </summary>
    public interface IWorkbookStore
    {
        /// <summary>
        /// Gets an object with the specified name.
        /// </summary>
        /// <typeparam name="T">The type of the object to get.</typeparam>
        /// <param name="name">The name of the object to get.</param>
        /// <returns>
        /// The object deserialized from the workbook or <c>default(T)</c> if the object was not found.
        /// </returns>
        T Retrieve<T>(string name);

        /// <summary>
        /// Stores the specified object.
        /// </summary>
        /// <typeparam name="T">The type of the object to store.</typeparam>
        /// <param name="name">The name under which the object will be stored.</param>
        /// <param name="value">The object to store.</param>
        void Store<T>(string name, T value);

        /// <summary>
        /// Removes the specified object from the store.
        /// </summary>
        /// <param name="name">The name of the object to remove.</param>
        void Remove(string name);

        /// <summary>
        /// Saves all objects to the workbook.
        /// </summary>
        void Flush();

        /// <summary>
        /// Clears all unsaved objects and reloads the store from the workbook.
        /// </summary>
        void Clear();
    }
}