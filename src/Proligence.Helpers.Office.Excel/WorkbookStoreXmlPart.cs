﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkbookStoreXmlPart.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Office.Excel
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a general object storage which can be serialized into an Excel workbook.
    /// </summary>
    [Serializable]
    public class WorkbookStoreXmlPart : Dictionary<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkbookStoreXmlPart"/> class.
        /// </summary>
        public WorkbookStoreXmlPart()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkbookStoreXmlPart"/> class.
        /// </summary>
        /// <param name="info">
        /// A <see cref="SerializationInfo" /> object containing the information required to serialize the
        /// <see cref="WorkbookStoreXmlPart" />.
        /// </param>
        /// <param name="context">
        /// A <see cref="StreamingContext" /> structure containing the source and destination of the serialized
        /// stream associated with the <see cref="WorkbookStoreXmlPart" />.
        /// </param>
        protected WorkbookStoreXmlPart(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}