﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultCacheTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.UnitTests.Common
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Cache;
    using NUnit.Framework;

    [TestFixture]
    public class DefaultCacheTests
    {
        [Test]
        public void ShouldSetRegexIfNotExists()
        {
            var cache = new DefaultPatternCache();
            var entry = new Regex("foo");
            cache.GetOrAdd("test", () => entry);

            var returned = cache.GetOrAdd("test", () => null);
            Assert.AreEqual(entry, returned);
        }

        [Test]
        public void ShouldSetRegexIfNull()
        {
            var cache = new DefaultPatternCache();
            var entry = new Regex("foo");

            var cached1 = cache.GetOrAdd("test", () => null);
            Assert.IsNull(cached1);

            var returned = cache.GetOrAdd("test", () => entry);
            Assert.AreEqual(entry, returned);
        }

        [Test]
        public void ShouldNotSetRegexIfExists()
        {
            var cache = new DefaultPatternCache();
            var entry1 = new Regex("foo");
            var entry2 = new Regex("bar");

            var returned = cache.GetOrAdd("test", () => entry1);
            Assert.AreEqual(entry1, returned);

            returned = cache.GetOrAdd("test", () => entry2);
            Assert.AreEqual(entry1, returned);
        }

        [Test]
        public void ShouldSetGroupsIfNotExists()
        {
            var cache = new DefaultPatternCache();
            var entry = new List<string> { "foo" };
            cache.GetOrAddGroups("test", () => entry);

            var returned = cache.GetOrAddGroups("test", () => null);
            Assert.AreEqual(entry, returned);
        }

        [Test]
        public void ShouldSetGroupsIfNull()
        {
            var cache = new DefaultPatternCache();
            var entry = new List<string> { "foo" };

            var cached1 = cache.GetOrAddGroups("test", () => null);
            Assert.IsNull(cached1);

            var returned = cache.GetOrAddGroups("test", () => entry);
            Assert.AreEqual(entry, returned);
        }

        [Test]
        public void ShouldNotSetGroupsIfExists()
        {
            var cache = new DefaultPatternCache();
            var entry1 = new List<string> { "foo" };
            var entry2 = new List<string> { "bar" };

            var returned = cache.GetOrAddGroups("test", () => entry1);
            Assert.AreEqual(entry1, returned);

            returned = cache.GetOrAddGroups("test", () => entry2);
            Assert.AreEqual(entry1, returned);
        }
    }
}