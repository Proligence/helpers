﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PatternsTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Patterns.UnitTests.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Cache;
    using Core;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PatternsTests
    {
        private IPatternService service;

        [SetUp]
        public void Setup()
        {
            var cache = new Mock<IPatternCache>();

            cache.Setup(c => c.GetOrAdd(It.IsAny<string>(), It.IsAny<Func<Regex>>()))
                 .Returns<string, Func<Regex>>((pattern, setter) => setter());

            cache.Setup(c => c.GetOrAddGroups(It.IsAny<string>(), It.IsAny<Func<IEnumerable<string>>>()))
                 .Returns<string, Func<IEnumerable<string>>>((pattern, setter) => setter());

            this.service = new PatternService(cache.Object);    
        }

        [Test]
        public void ShouldMatchCorrectString()
        {
            var text = "http://www.someurl.com/path1/path2/path3";
            var pattern = "http://{host}/path1/{secondPath}/path3";

            Assert.IsTrue(this.service.TryMatch(text, pattern));
        }

        [Test]
        public void ShouldNotMatchIncorrectString()
        {
            var text = "http://www.someurl.com/invalidPath/path2/path3";
            var pattern = "http://{host}/path1/{secondPath}/path3";

            Assert.IsFalse(this.service.TryMatch(text, pattern));
        }

        [Test]
        public void ShouldMatchOneOrMoreCharacters()
        {
            var text1 = "foo/bar/baz";
            var text2 = "foo//baz";

            var pattern = "foo/{first}/baz";

            Assert.IsTrue(this.service.TryMatch(text1, pattern));
            Assert.IsFalse(this.service.TryMatch(text2, pattern));
        }

        [Test]
        public void ShouldMatchZeroOrMoreCharacters()
        {
            var text1 = "foo/bar/baz";
            var text2 = "foo//baz";

            var pattern = "foo/{first*}/baz";

            Assert.IsTrue(this.service.TryMatch(text1, pattern));
            Assert.IsTrue(this.service.TryMatch(text2, pattern));
        }

        [Test]
        public void ShouldMatchSingleCharacter()
        {
            var text1 = "foo/b/baz";
            var text2 = "foo/bar/baz";
            var text3 = "foo//baz";

            var pattern = "foo/{first?}/baz";

            Assert.IsTrue(this.service.TryMatch(text1, pattern));
            Assert.IsFalse(this.service.TryMatch(text2, pattern));
            Assert.IsFalse(this.service.TryMatch(text3, pattern));
        }

        [Test]
        public void ShouldCatchNamedGroupsOnly()
        {
            var text = "foo/bar/baz/baz/baz";
            var pattern = "foo/{first}/{second}/{*}/baz";

            PatternMatch details;
            Assert.IsTrue(this.service.TryMatch(text, pattern, out details));

            var groups = details.Groups.ToArray();
            Assert.AreEqual(groups.Length, 2);
            Assert.AreEqual(groups[0].Key, "first");
            Assert.AreEqual(groups[1].Key, "second");
        }

        [Test]
        public void ExplicitWildcardQuantifiersShouldBeGreedy()
        {
            var text = "foo/bar/baz/baz/baz";
            var pattern = "foo/{first*}/{second}/baz";

            PatternMatch details;

            Assert.IsTrue(this.service.TryMatch(text, pattern, out details));

            var groups = details.Groups.ToArray();
            Assert.AreEqual(groups.Length, 2);
            Assert.AreEqual(groups[0].Key, "first");
            Assert.AreEqual(groups[0].Value, "bar/baz");
            Assert.AreEqual(groups[1].Key, "second");
            Assert.AreEqual(groups[1].Value, "baz");
        }

        [Test]
        public void DefaultQuantifierShouldNotBeGreedy()
        {
            var text = "foo/bar/baz/baz/baz";
            var pattern = "foo/{first}/{second}/{third*}";

            PatternMatch details;

            Assert.IsTrue(this.service.TryMatch(text, pattern, out details));

            var groups = details.Groups.ToArray();
            Assert.AreEqual(3, groups.Length);
            Assert.AreEqual("first", groups[0].Key);
            Assert.AreEqual("bar", groups[0].Value);
            Assert.AreEqual("second", groups[1].Key);
            Assert.AreEqual("baz", groups[1].Value);
            Assert.AreEqual("third", groups[2].Key);
            Assert.AreEqual("baz/baz", groups[2].Value);
        }
    }
}