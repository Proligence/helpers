﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppDomainExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Autofac
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;
    using global::Autofac;
    using NUnit.Framework;
    using Proligence.Helpers.Autofac;
    using Module = global::Autofac.Module;

    /// <summary>
    /// Implements unit tests for the <see cref="AppDomainExtensions"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder",
        Justification = "Unit test fixture.")]
    public class AppDomainExtensionsUnitTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void BuildIocContainerFromNullAppDomain()
        {
            AppDomainExtensions.BuildIocContainer(null);
        }

        [Test]
        public void BuildIocContainerResolveType()
        {
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer();
            StringBuilder obj = container.Resolve<StringBuilder>();

            Assert.That(obj, Is.Not.Null);
        }

        [Test]
        public void BuildIocContainerResolveTypeWhenRegisterAssemblyTypesFalse()
        {
            var options = new ContainerBuildOptions { RegisterAssemblyTypes = false };
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer(options);

            StringBuilder instance;
            bool result = container.TryResolve(out instance);

            Assert.That(result, Is.False);
        }

        [Test]
        public void BuildIocContainerResolveTypeFromModule()
        {
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer();
            Stack obj = container.Resolve<Stack>();

            Assert.That(obj, Is.SameAs(TestModule.Object));
        }

        [Test]
        public void BuildIocContainerResolveTypeFromModuleWhenRegisterAssemblyModulesFalse()
        {
            var options = new ContainerBuildOptions { RegisterAssemblyModules = false };
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer(options);
            Stack obj = container.Resolve<Stack>();

            Assert.That(obj, Is.Not.SameAs(TestModule.Object));
        }

        [Test]
        public void BuildIocContainerWithAdditionalConfiguration()
        {
            var instance = new StringBuilder();
            IContainer container =
                AppDomain.CurrentDomain.BuildIocContainer(
                    builder => builder.RegisterInstance(instance).As<StringBuilder>().SingleInstance());
            StringBuilder obj = container.Resolve<StringBuilder>();

            Assert.That(obj, Is.SameAs(instance));
        }

        [Test]
        public void BuildIocContainerResolvePerDependencyServiceRegisteredByAttribute()
        {
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer();
            IPerDependencyService instance1 = container.Resolve<IPerDependencyService>();
            IPerDependencyService instance2 = container.Resolve<IPerDependencyService>();

            Assert.That(instance1, Is.Not.Null);
            Assert.That(instance2, Is.Not.Null);
            Assert.That(instance1, Is.Not.SameAs(instance2));
        }

        [Test]
        public void BuildIocContainerResolveSingleInstanceServiceRegisteredByAttribute()
        {
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer();
            ISingleInstanceService instance1 = container.Resolve<ISingleInstanceService>();
            ISingleInstanceService instance2 = container.Resolve<ISingleInstanceService>();

            Assert.That(instance1, Is.Not.Null);
            Assert.That(instance2, Is.Not.Null);
            Assert.That(instance1, Is.SameAs(instance2));
        }

        [Test]
        public void BuildIocContainerResolvePerLifetimeScopeServiceRegisteredByAttribute()
        {
            IContainer container = AppDomain.CurrentDomain.BuildIocContainer();
            IPerLifetimeScopeService instance1 = container.Resolve<IPerLifetimeScopeService>();
            
            ILifetimeScope scope = container.BeginLifetimeScope();
            IPerLifetimeScopeService instance2 = scope.Resolve<IPerLifetimeScopeService>();
            IPerLifetimeScopeService instance3 = scope.Resolve<IPerLifetimeScopeService>();

            Assert.That(instance1, Is.Not.Null);
            Assert.That(instance2, Is.Not.Null);
            Assert.That(instance3, Is.Not.Null);
            Assert.That(instance1, Is.Not.SameAs(instance2));
            Assert.That(instance2, Is.SameAs(instance3));
        }

        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "Test code")]
        public class TestModule : Module
        {
            private static readonly Stack Obj = new Stack();

            public static object Object
            {
                get
                {
                    return Obj;
                }
            }

            protected override void Load(ContainerBuilder builder)
            {
                builder.RegisterInstance(Obj).As<Stack>();
            }
        }

        internal interface IPerDependencyService
        {
        }

        internal interface ISingleInstanceService
        {
        }

        internal interface IPerLifetimeScopeService
        {
        }

        [Dependency(DependencyType.PerDependency, typeof(IPerDependencyService))]
        internal class PerDependencyService : IPerDependencyService
        {
        }

        [Dependency(DependencyType.SingleInstance, typeof(ISingleInstanceService))]
        internal class SingleInstanceService : ISingleInstanceService
        {
        }

        [Dependency(DependencyType.PerLifetimeScope, typeof(IPerLifetimeScopeService))]
        internal class PerLifetimeScopeService : IPerLifetimeScopeService
        {
        }
    }
}