﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentContextExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Autofac
{
    using System;
    using global::Autofac;
    using NUnit.Framework;
    using Proligence.Helpers.Autofac;

    [TestFixture]
    public class ComponentContextExtensionsUnitTests
    {
        [Test]
        public void ResolveShouldThrowExceptionWhenAnonymousObjectNull()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Test>().As<ITest>();
            var container = builder.Build();

            var exception = Assert.Throws<ArgumentNullException>(
                () => container.Resolve<ITest>((object)null));

            Assert.That(exception.ParamName, Is.EqualTo("parameters"));
        }

        [Test]
        public void ResolveUsingAnonymousObject()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Test>().As<ITest>();
            var container = builder.Build();

            var obj = container.Resolve<ITest>(new { a = 7, b = "Test", c = true });

            Assert.That(obj, Is.InstanceOf<Test>());
            Assert.That(obj.A, Is.EqualTo(7));
            Assert.That(obj.B, Is.EqualTo("Test"));
            Assert.That(obj.C, Is.EqualTo(true));
        }

        private class Test : ITest
        {
            public Test(int a, string b, bool c)
            {
                this.A = a;
                this.B = b;
                this.C = c;
            }

            public int A { get; set; }
            public string B { get; set; }
            public bool C { get; set; }
        }

        private interface ITest
        {
            int A { get; set; }
            string B { get; set; }
            bool C { get; set; }
        }
    }
}