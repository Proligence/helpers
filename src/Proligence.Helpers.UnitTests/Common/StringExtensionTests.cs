﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensionTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Common
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    using NUnit.Framework;
    using Proligence.Helpers.Common;

    /// <summary>
    /// String edit distance computation unit tests.
    /// </summary>
    [TestFixture]
    public class StringExtensionTests
    {
        [Test]
        public void ZeroDistanceWhenStringsAreEqual()
        {
            Assert.AreEqual(0, "test".DistanceTo("test"));
        }

        [Test]
        public void DistanceWhenCharactersArePrepended()
        {
            Assert.AreEqual(1, "test".DistanceTo("tests"));
            Assert.AreEqual(1, "test".DistanceTo("stest"));
            Assert.AreEqual(2, "test".DistanceTo("mytest"));
            Assert.AreEqual(7, "test".DistanceTo("mycrazytest"));
        }

        [Test]
        public void DistanceWhenCharactersArePrependedAndAppended()
        {
            Assert.AreEqual(9, "test".DistanceTo("mytestiscrazy"));
        }

        [Test]
        public void DistanceWhenCharactersAreDuplicated()
        {
            Assert.AreEqual(1, "test".DistanceTo("teest"));
        }

        [Test]
        public void DistanceWhenCharactersAreDeleted()
        {
            Assert.AreEqual(1, "test".DistanceTo("tst"));
            Assert.AreEqual(2, "test".DistanceTo("ts"));
        }

        [Test]
        public void DistanceWhenCharactersAreTransposed()
        {
            Assert.AreEqual(1, "test".DistanceTo("tset"));
        }

        [Test]
        public void DistanceWhenCharactersAreTransposedAndAppended()
        {
            Assert.AreEqual(2, "test".DistanceTo("tsets"));
        }

        [Test]
        public void DistanceWhenCharactersAreDuplicatedAndTransposed()
        {
            Assert.AreEqual(1, "banana".DistanceTo("banaan"));
            Assert.AreEqual(1, "banana".DistanceTo("abnana"));
            Assert.AreEqual(2, "banana".DistanceTo("baanaa"));
        }

        [Test]
        [SuppressMessage(
            "StyleCop.CSharp.ReadabilityRules", 
            "SA1122:UseStringEmptyForEmptyStrings", 
            Justification = "Reviewed. Suppression is OK here.")]
        public void ZeroDistanceWhenBothStringsAreEmpty()
        {
            Assert.AreEqual(0, "".DistanceTo(""));
        }

        [Test]
        [Explicit]
        public void CheckPerformanceOf100KChecks()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            for (var i = 0; i < 100000; i++)
            {
                "test".DistanceTo("mytseet123");
            }

            watch.Stop();
            Assert.Pass("100k checks took: {0} milliseconds.", watch.ElapsedMilliseconds);
        }
    }
}