﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RandomExtensionTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Common
{
    using System;
    using NUnit.Framework;
    using Proligence.Helpers.Common;

    /// <summary>
    /// Long random number generator unit tests.
    /// </summary>
    [TestFixture]
    public class RandomExtensionTests
    {
        /// <summary>
        /// Should generate some value.
        /// </summary>
        [Test]
        public void ShouldGenerateValue()
        {
            Random rng = new Random();
            long value = rng.NextInt64();
            Assert.Greater(value, 0);
        }

        /// <summary>
        /// Should generate the same value, if bounds allow only a single value.
        /// </summary>
        [Test]
        public void ShouldGenerateSameValueIfMinAndMaxAreEqual()
        {
            Random rng = new Random();
            for (int i = 0; i < 10000; i++)
            {
                long value = rng.NextInt64(i, i);
                Assert.AreEqual(value, i);
            }
        }

        /// <summary>
        /// Tests if the <see cref="RandomExtensions.NextInt64"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified <see cref="Random"/> object is null.
        /// </summary>
        [Test]
        public void NextInt64WhenRandomNull()
        {
            Random rng = null;
            var exception = Assert.Throws<ArgumentNullException>(() => rng.NextInt64());
            
            Assert.That(exception, Is.Not.Null);
            Assert.That(exception.ParamName, Is.EqualTo("random"));
        }
    }
}