﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultObjectMapperTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using global::Autofac;
    using Moq;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="DefaultObjectMapper{TSource,TDestination}"/> class.
    /// </summary>
    [TestFixture]
    public class DefaultObjectMapperTests
    {
        /// <summary>
        /// Test enum.
        /// </summary>
        private enum TestEnum
        {
            /// <summary>
            /// Default value
            /// </summary>
            None,

            /// <summary>
            /// Foo member
            /// </summary>
            Foo,

            /// <summary>
            /// Bar member
            /// </summary>
            Bar,

            /// <summary>
            /// Baz member
            /// </summary>
            Baz
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method correctly maps
        /// property values when the source and destination objects have the same set of properties.
        /// </summary>
        [Test]
        public void MapPropertiesShouldMapPropertiesWhenObjectsHaveSameProperties()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithSameProperties>(componentContext.Object);
            var source = new TestClass { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithSameProperties();
            
            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo("Test"));
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method correctly maps
        /// property values when the destination objects has a subset of the properties of the source object.
        /// </summary>
        [Test]
        public void MapPropertiesShouldMapPropertiesWhenDestinationHasSubsetOfProperties()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithSubset>(componentContext.Object);
            var source = new TestClass { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithSubset();

            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo("Test"));
            Assert.That(result.Bar, Is.EqualTo(7));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method correctly maps
        /// property values when the destination objects has a superset of the properties of the source object.
        /// </summary>
        [Test]
        public void MapPropertiesShouldMapPropertiesWhenDestinationHasSupersetOfProperties()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithSuperset>(componentContext.Object);
            var source = new TestClass { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithSuperset();

            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo("Test"));
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(true));
            Assert.That(result.Qux, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method correctly maps
        /// properties which have a private setter at the destination object.
        /// </summary>
        [Test]
        public void MapPropertiesShouldMapPropertiesWithNonpublicSetterAtDestinationObject()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithPropertyWithPrivateSetter>(
                componentContext.Object);
            var source = new TestClass { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithPropertyWithPrivateSetter();
            
            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo("Test"));
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method does not map
        /// properties which do not have a setter at the destination object.
        /// </summary>
        [Test]
        public void MapPropertiesShouldSkipPropertiesWithoutSetterAtDestinationObject()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithPropertyWithWithoutSetter>(
                componentContext.Object);
            var source = new TestClass { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithPropertyWithWithoutSetter();

            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.Null);
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method does not map
        /// properties which do not have a getter at the source object.
        /// </summary>
        [Test]
        public void MapPropertiesShouldSkipPropertiesWithoutGetterAtSourceObject()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClassWithPropertyWithWithoutGetter, TestClass>(
                componentContext.Object);
            var source = new TestClassWithPropertyWithWithoutGetter { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClass();

            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.Null);
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method does not map
        /// properties which are decorated with the <see cref="ExcludeFromMappingAttribute"/> attribute.
        /// </summary>
        [Test]
        public void MapPropertiesShouldSkipPropertiesMarkedWithExcludeFromMappingAttribute()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClassWithExcludedProperty2, TestClassWithExcludedProperty>(
                componentContext.Object);
            var source = new TestClassWithExcludedProperty2 { Foo = "Test", Bar = 7, Baz = true };
            var result = new TestClassWithExcludedProperty();
            
            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo(0));
            Assert.That(result.Bar, Is.EqualTo(7));
            Assert.That(result.Baz, Is.EqualTo(false));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultObjectMapper{TSource,TDestination}.MapProperties"/> method correctly maps
        /// enum values.
        /// </summary>
        [Test]
        public void MapPropertiesShouldMapEnumPropertiesWhenSourceValueConvertible()
        {
            var componentContext = new Mock<IComponentContext>();
            var mapper = new DefaultObjectMapper<TestClass, TestClassWithPropertyWithEnumType>(
                componentContext.Object);
            var source = new TestClass { Foo = "Bar", Bar = 2, Baz = true };
            var result = new TestClassWithPropertyWithEnumType();

            mapper.MapProperties(source, result);

            Assert.That(result.Foo, Is.EqualTo(TestEnum.Bar));
            Assert.That(result.Bar, Is.EqualTo(TestEnum.Bar));
        }

        /// <summary>
        /// Test if properties marked as <see cref="MapAsNestedAttribute"/> are mapped correctly.
        /// </summary>
        [Test]
        public void MapPropertyWithMapAsNestedAttribute()
        {
            var builder = new ContainerBuilder();
            builder.RegisterGeneric(typeof(CtorObjectFactory<>)).As(typeof(IObjectFactory<>)).SingleInstance();
            builder.RegisterGeneric(typeof(DefaultObjectMapper<,>)).As(typeof(IObjectMapper<,>)).SingleInstance();
            IComponentContext componentContext = builder.Build();

            var mapper = new DefaultObjectMapper<TestClassWithNested1, TestClassWithNested2>(componentContext);
            var source = new TestClassWithNested1 { Nested = new TestNestedClass1 { Foo = "Test" } };
            var result = new TestClassWithNested2();

            mapper.MapProperties(source, result);

            Assert.That(result.Nested.Foo, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Test class with three properties.
        /// </summary>
        private class TestClass
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public string Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Test class with same properties as <see cref="TestClass"/>.
        /// </summary>
        private class TestClassWithSameProperties
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public string Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClassWithSameProperties"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Test class with a subset of properties from <see cref="TestClass"/>
        /// </summary>
        private class TestClassWithSubset
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public string Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }
        }

        /// <summary>
        /// Test class with a superset of properties from <see cref="TestClass"/>
        /// </summary>
        private class TestClassWithSuperset
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public string Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClassWithSuperset"/> is baz.
            /// </summary>
            public bool Baz { get; set; }

            /// <summary>
            /// Gets or sets the qux.
            /// </summary>
            public object Qux { get; set; }
        }

        /// <summary>
        /// Test class with a property with a private setter.
        /// </summary>
        private class TestClassWithPropertyWithPrivateSetter
        {
            /// <summary>
            /// Gets the foo.
            /// </summary>
            public string Foo { get; private set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

#pragma warning disable 649

        /// <summary>
        /// Test class with a property without a setter.
        /// </summary>
        private class TestClassWithPropertyWithWithoutSetter
        {
            /// <summary>
            /// The foo.
            /// </summary>
            private string foo;

            /// <summary>
            /// Gets the foo.
            /// </summary>
            public string Foo
            {
                get { return this.foo; }
            }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Test class with a property without a getter.
        /// </summary>
        private class TestClassWithPropertyWithWithoutGetter
        {
            /// <summary>
            /// The foo.
            /// </summary>
            private string foo;

            /// <summary>
            /// Sets the foo.
            /// </summary>
            public string Foo
            {
                set { this.foo = value; }
            }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Test class with property with different type than in <see cref="TestClass"/>.
        /// </summary>
        private class TestClassWithExcludedProperty
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            [ExcludeFromMappingAttribute]
            public long Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Another test class with property with different type than in <see cref="TestClass"/>.
        /// </summary>
        private class TestClassWithExcludedProperty2
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public string Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public int Bar { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="TestClass"/> is baz.
            /// </summary>
            [ExcludeFromMappingAttribute]
            public bool Baz { get; set; }
        }

        /// <summary>
        /// Test class with properties of enum type.
        /// </summary>
        private class TestClassWithPropertyWithEnumType
        {
            /// <summary>
            /// Gets or sets the foo.
            /// </summary>
            public TestEnum Foo { get; set; }

            /// <summary>
            /// Gets or sets the bar.
            /// </summary>
            public TestEnum Bar { get; set; }
        }

        /// <summary>
        /// Test class with properties marked with <see cref="MapAsNestedAttribute"/>.
        /// </summary>
        private class TestClassWithNested1
        {
            [MapAsNested]
            public TestNestedClass1 Nested { get; set; }
        }

        /// <summary>
        /// Nested class.
        /// </summary>
        private class TestNestedClass1
        {
            /// <summary>
            /// Gets or sets the foo string.
            /// </summary>
            public string Foo { get; set; }
        }

        /// <summary>
        /// Another test class with properties marked with <see cref="MapAsNestedAttribute"/>.
        /// </summary>
        private class TestClassWithNested2
        {
            [MapAsNested]
            public TestNestedClass2 Nested { get; set; }
        }

        /// <summary>
        /// Another nested class.
        /// </summary>
        private class TestNestedClass2
        {
            /// <summary>
            /// Gets or sets the foo string.
            /// </summary>
            public string Foo { get; set; }
        }
    }
}