﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataAnnotationValidatorTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection.DataAnnotations;

    /// <summary>
    /// Implements unit tests for the <see cref="DataAnnotationValidator"/> class.
    /// </summary>
    [TestFixture]
    public class DataAnnotationValidatorTests
    {
        /// <summary>
        /// Tests if new <see cref="DataAnnotationValidator"/> objects are initialized in recursive mode.
        /// </summary>
        [Test]
        public void ValidatorShouldBeInRecursiveModeByDefault()
        {
            var validator = new DataAnnotationValidator();

            Assert.That(validator.RecursiveMode, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void ValidateObjectWhenObjectNull()
        {
            var validator = new DataAnnotationValidator();

            var exception = Assert.Throws<ArgumentNullException>(() => validator.ValidateObject(null));

            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method does not throw any exception
        /// when the specified object is valid.
        /// </summary>
        [Test]
        public void ValidateObjectWhenObjectValid()
        {
            var validator = new DataAnnotationValidator();
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };

            Assert.DoesNotThrow(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method throws a
        /// <see cref="ValidationException"/> when the specified object is invalid.
        /// </summary>
        [Test]
        public void ValidateObjectWhenObjectInvalid()
        {
            var validator = new DataAnnotationValidator();
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = null, MyProperty3 = "bar" };

            Assert.Throws<ValidationException>(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method does not throw any exception when
        /// the subobjects of the specified object are valid and <see cref="DataAnnotationValidator.RecursiveMode"/>
        /// is set to <c>true</c>.
        /// </summary>
        [Test]
        public void ValidateObjectRecursiveWhenSubobjectValid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var subobj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            Assert.DoesNotThrow(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method throws a
        /// <see cref="ValidationException"/> when a subobject of the specified object is invalid
        /// <see cref="DataAnnotationValidator.RecursiveMode"/> is set to <c>true</c>.
        /// </summary>
        [Test]
        public void ValidateObjectRecursiveWhenSubobjectInvalid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var subobj = new TestObject { MyProperty1 = "baz", MyProperty2 = null };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            Assert.Throws<ValidationException>(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method does not throw any exception when
        /// a subobject of the specified object is invalid and <see cref="DataAnnotationValidator.RecursiveMode"/> is
        /// set to <c>false</c>.
        /// </summary>
        [Test]
        public void ValidateObjectNonRecursiveWhenSubobjectInvalid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = false };
            var subobj = new TestObject { MyProperty1 = "foo", MyProperty2 = null };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            Assert.DoesNotThrow(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.ValidateObject"/> method works correctly when the validated
        /// object has cycles in its references graph.
        /// </summary>
        [Test]
        public void ValidateObjectRecursiveWhenObjectWithCycles()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };
            obj.MyObject1 = obj;    // recursive reference!

            Assert.DoesNotThrow(() => validator.ValidateObject(obj));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void TryValidateObjectWhenObjectNull()
        {
            var validator = new DataAnnotationValidator();

            var exception = Assert.Throws<ArgumentNullException>(() => validator.TryValidateObject(null));

            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method does not return any validation
        /// errors when the specified object is valid.
        /// </summary>
        [Test]
        public void TryValidateObjectWhenObjectValid()
        {
            var validator = new DataAnnotationValidator();
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method returns a validation when the
        /// specified object is invalid.
        /// </summary>
        [Test]
        public void TryValidateObjectWhenObjectInvalid()
        {
            var validator = new DataAnnotationValidator();
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = null, MyProperty3 = "bar" };

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            var validationResult = result.Single();
            Assert.That(validationResult.ErrorMessage, Is.EqualTo("The MyProperty2 field is required."));
            Assert.That(validationResult.MemberNames.ToArray(), Is.EqualTo(new[] { "MyProperty2" }));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method does not return any validation
        /// errors when the subobjects of the specified object are valid and 
        /// <see cref="DataAnnotationValidator.RecursiveMode"/> is set to <c>true</c>.
        /// </summary>
        [Test]
        public void TryValidateObjectRecursiveWhenSubobjectValid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var subobj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method returns a validation error
        /// when a subobject of the specified object is invalid and <see cref="DataAnnotationValidator.RecursiveMode"/>
        /// is set to <c>true</c>.
        /// </summary>
        [Test]
        public void TryValidateObjectRecursiveWhenSubobjectInvalid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var subobj = new TestObject { MyProperty1 = "baz", MyProperty2 = null };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            var validationResult = result.Single();
            Assert.That(validationResult.ErrorMessage, Is.EqualTo("The MyProperty2 field is required."));
            Assert.That(validationResult.MemberNames.ToArray(), Is.EqualTo(new[] { "MyProperty2" }));
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method does not return any validation
        /// errors when a subobject of the specified object is invalid and
        /// <see cref="DataAnnotationValidator.RecursiveMode"/> is set to <c>false</c>.
        /// </summary>
        [Test]
        public void TryValidateObjectNonRecursiveWhenSubobjectInvalid()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = false };
            var subobj = new TestObject { MyProperty1 = "foo", MyProperty2 = null };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar", MyObject1 = subobj };

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.TryValidateObject"/> method works correctly when the
        /// validated object has cycles in its references graph.
        /// </summary>
        [Test]
        public void TryValidateObjectRecursiveWhenObjectWithCycles()
        {
            var validator = new DataAnnotationValidator { RecursiveMode = true };
            var obj = new TestObject { MyProperty1 = "foo", MyProperty2 = "bar" };
            obj.MyObject1 = obj;    // recursive reference!

            IEnumerable<ValidationResult> result = validator.TryValidateObject(obj);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DataAnnotationValidator.IgnoreAssemblies"/> method works correctly.
        /// </summary>
        [Test]
        public void TestIgnoreAssemblies()
        {
            var validator = new DataAnnotationValidator();
            
            validator.IgnoreAssemblies("Autofac");

            Assert.That(validator.IgnoredAssemblies.Contains(
                Assembly.GetAssembly(typeof(global::Autofac.ContainerBuilder))));
        }

        /* ReSharper disable UnusedAutoPropertyAccessor.Local */

        /// <summary>
        /// Test class.
        /// </summary>
        private class TestObject
        {
            /// <summary>
            /// Gets or sets the first test property.
            /// </summary>
            [Required]
            public string MyProperty1 { get; set; }

            /// <summary>
            /// Gets or sets the second test property.
            /// </summary>
            [Required]
            public string MyProperty2 { get; set; }

            /// <summary>
            /// Gets or sets the third test property.
            /// </summary>
            public string MyProperty3 { get; set; }

            /// <summary>
            /// Gets or sets the first sample subobject.
            /// </summary>
            public object MyObject1 { get; set; }

            /// <summary>
            /// Gets or sets the second sample subobject.
            /// </summary>
            public object MyObject2 { get; set; }
        }

        /* ReSharper restore UnusedAutoPropertyAccessor.Local */
    }
}