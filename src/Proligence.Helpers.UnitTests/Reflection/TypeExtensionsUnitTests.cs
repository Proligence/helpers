﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="TypeExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class TypeExtensionsUnitTests
    {
        /// <summary>
        /// Creates a new object using a public constructor without parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPublicConstructorWithoutParameters()
        {
            TestClass instance = typeof(TestClass).CreateInstance<TestClass>();

            Assert.That(instance, Is.InstanceOf<TestClass>());
        }

        /// <summary>
        /// Creates a new object using a public constructor with parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPublicConstructorWithParameters()
        {
            TestClass instance = typeof(TestClass).CreateInstance<TestClass>("test");

            Assert.That(instance, Is.InstanceOf<TestClass>());
            Assert.That(instance.Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using a public constructor without parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPublicConstructorWithoutParametersNonGeneric()
        {
            object instance = typeof(TestClass).CreateInstance();

            Assert.That(instance, Is.InstanceOf<TestClass>());
        }

        /// <summary>
        /// Creates a new object using a public constructor with parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPublicConstructorWithParametersNonGeneric()
        {
            object instance = typeof(TestClass).CreateInstance("test");

            Assert.That(instance, Is.InstanceOf<TestClass>());
            Assert.That(((TestClass)instance).Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using an internal constructor without parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingInternalConstructorWithoutParameters()
        {
            InternalTestClass instance = typeof(InternalTestClass).CreateInstance<InternalTestClass>();

            Assert.That(instance, Is.InstanceOf<InternalTestClass>());
        }

        /// <summary>
        /// Creates a new object using an internal constructor with parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingInternalConstructorWithParameters()
        {
            InternalTestClass instance = typeof(InternalTestClass).CreateInstance<InternalTestClass>("test");

            Assert.That(instance, Is.InstanceOf<InternalTestClass>());
            Assert.That(instance.Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using an internal constructor without parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingInternalConstructorWithoutParametersNonGeneric()
        {
            object instance = typeof(InternalTestClass).CreateInstance();

            Assert.That(instance, Is.InstanceOf<InternalTestClass>());
        }

        /// <summary>
        /// Creates a new object using an internal constructor with parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingInternalConstructorWithParametersNonGeneric()
        {
            object instance = typeof(InternalTestClass).CreateInstance("test");

            Assert.That(instance, Is.InstanceOf<InternalTestClass>());
            Assert.That(((InternalTestClass)instance).Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using a protected constructor without parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingProtectedConstructorWithoutParameters()
        {
            ProtectedTestClass instance = typeof(ProtectedTestClass).CreateInstance<ProtectedTestClass>();

            Assert.That(instance, Is.InstanceOf<ProtectedTestClass>());
        }

        /// <summary>
        /// Creates a new object using a protected constructor with parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingProtectedConstructorWithParameters()
        {
            ProtectedTestClass instance = typeof(ProtectedTestClass).CreateInstance<ProtectedTestClass>("test");

            Assert.That(instance, Is.InstanceOf<ProtectedTestClass>());
            Assert.That(instance.Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using a protected constructor without parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingProtectedConstructorWithoutParametersNonGeneric()
        {
            object instance = typeof(ProtectedTestClass).CreateInstance();

            Assert.That(instance, Is.InstanceOf<ProtectedTestClass>());
        }

        /// <summary>
        /// Creates a new object using a protected constructor with parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingProtectedConstructorWithParametersNonGeneric()
        {
            object instance = typeof(ProtectedTestClass).CreateInstance("test");

            Assert.That(instance, Is.InstanceOf<ProtectedTestClass>());
            Assert.That(((ProtectedTestClass)instance).Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using a private constructor without parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPrivateConstructorWithoutParameters()
        {
            PrivateTestClass instance = typeof(PrivateTestClass).CreateInstance<PrivateTestClass>();

            Assert.That(instance, Is.InstanceOf<PrivateTestClass>());
        }

        /// <summary>
        /// Creates a new object using a private constructor with parameters.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPrivateConstructorWithParameters()
        {
            PrivateTestClass instance = typeof(PrivateTestClass).CreateInstance<PrivateTestClass>("test");

            Assert.That(instance, Is.InstanceOf<PrivateTestClass>());
            Assert.That(instance.Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Creates a new object using a private constructor without parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPrivateConstructorWithoutParametersNonGeneric()
        {
            object instance = typeof(PrivateTestClass).CreateInstance();

            Assert.That(instance, Is.InstanceOf<PrivateTestClass>());
        }

        /// <summary>
        /// Creates a new object using a private constructor with parameters, using the non-generic API.
        /// </summary>
        [Test]
        public void CreateInstanceUsingPrivateConstructorWithParametersNonGeneric()
        {
            object instance = typeof(PrivateTestClass).CreateInstance("test");

            Assert.That(instance, Is.InstanceOf<PrivateTestClass>());
            Assert.That(((PrivateTestClass)instance).Parameter, Is.EqualTo("test"));
        }

        /// <summary>
        /// Tests if the <see cref="TypeExtensions.CreateInstance{T}"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified type is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateInstanceWhenTypeNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => TypeExtensions.CreateInstance<object>(null, new object[0]));

            Assert.That(exception.ParamName, Is.EqualTo("type"));
        }

        /// <summary>
        /// Tests if the <see cref="TypeExtensions.CreateInstance"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified type is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateInstanceNonGenericWhenTypeNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => TypeExtensions.CreateInstance(null, new object[0]));

            Assert.That(exception.ParamName, Is.EqualTo("type"));
        }

        /// <summary>
        /// Tests if the <see cref="TypeExtensions.CreateInstance{T}"/> method works correctly when the specified
        /// parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateInstanceWhenParametersNull()
        {
            TestClass instance = typeof(TestClass).CreateInstance<TestClass>(null);

            Assert.That(instance, Is.InstanceOf<TestClass>());
        }

        /// <summary>
        /// Tests if the <see cref="TypeExtensions.CreateInstance"/> method works correctly when the specified
        /// parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateInstanceNonGenericWhenParametersNull()
        {
            object instance = typeof(TestClass).CreateInstance(null);

            Assert.That(instance, Is.InstanceOf<TestClass>());
        }

        /// <summary>
        /// Tests if the <see cref="TypeExtensions.CreateInstance"/> method throws an
        /// <see cref="ArgumentNullException"/> when there is no constructor with the specified parameter types.
        /// </summary>
        [Test]
        public void CreateInstanceWhenConstructorNotAvailable()
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => typeof(TestClass).CreateInstance<TestClass>(0));

            Assert.That(exception.ParamName, Is.EqualTo("parameters"));
            Assert.That(
                exception.Message, 
                Is.StringContaining("Failed to find constructor with specified parameter types."));
        }

        /// <summary>
        /// Tests the performance of the <see cref="TypeExtensions.CreateInstance"/> method.
        /// </summary>
        [Test, Explicit]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Test")]
        public void TestCreateInstancePerformance()
        {
            int count = 10000;

            var newCall = new Stopwatch();
            newCall.Start();
            
            for (int i = 0; i < count; i++)
            {
                new TestClass();
            }
            
            newCall.Stop();

            var activator = new Stopwatch();
            activator.Start();

            for (int i = 0; i < count; i++)
            {
                Activator.CreateInstance(typeof(TestClass));
            }
            
            activator.Stop();

            var createInstance = new Stopwatch();
            createInstance.Start();
            
            for (int i = 0; i < count; i++)
            {
                typeof(TestClass).CreateInstance();
            }
            
            createInstance.Stop();

            Console.WriteLine("New           : " + newCall.ElapsedTicks);
            Console.WriteLine("Activator:    : " + activator.ElapsedTicks);
            Console.WriteLine("CreateInstance: " + createInstance.ElapsedTicks);
        }

        /// <summary>
        /// Test class.
        /// </summary>
        internal class TestClass
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestClass"/> class.
            /// </summary>
            public TestClass()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestClass"/> class.
            /// </summary>
            /// <param name="parameter">The parameter.</param>
            public TestClass(string parameter)
            {
                this.Parameter = parameter;
            }

            /// <summary>
            /// Gets or sets the parameter's value.
            /// </summary>
            public string Parameter { get; protected set; }
        }

        /// <summary>
        /// Test class.
        /// </summary>
        internal class InternalTestClass
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="InternalTestClass"/> class.
            /// </summary>
            internal InternalTestClass()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="InternalTestClass"/> class.
            /// </summary>
            /// <param name="parameter">The parameter value.</param>
            internal InternalTestClass(string parameter)
            {
                this.Parameter = parameter;
            }

            /// <summary>
            /// Gets or sets the parameter's value.
            /// </summary>
            public string Parameter { get; protected set; }
        }

        /// <summary>
        /// Test class.
        /// </summary>
        internal class ProtectedTestClass
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ProtectedTestClass"/> class.
            /// </summary>
            protected ProtectedTestClass()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ProtectedTestClass"/> class.
            /// </summary>
            /// <param name="parameter">The parameter.</param>
            protected ProtectedTestClass(string parameter)
            {
                this.Parameter = parameter;
            }

            /// <summary>
            /// Gets or sets the parameter's value.
            /// </summary>
            public string Parameter { get; protected set; }
        }

        /// <summary>
        /// Test class.
        /// </summary>
        internal class PrivateTestClass
        {
            // ReSharper disable UnusedMember.Local
            
            /// <summary>
            /// Prevents a default instance of the <see cref="PrivateTestClass"/> class from being created.
            /// </summary>
            private PrivateTestClass()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PrivateTestClass"/> class.
            /// </summary>
            /// <param name="parameter">The parameter.</param>
            private PrivateTestClass(string parameter)
            {
                this.Parameter = parameter;
            }

            // ReSharper restore UnusedMember.Local

            /// <summary>
            /// Gets or sets the parameter's value.
            /// </summary>
            public string Parameter { get; protected set; }
        }
    }
}