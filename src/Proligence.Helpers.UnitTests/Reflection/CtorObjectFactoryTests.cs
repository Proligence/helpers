﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CtorObjectFactoryTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using System;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="CtorObjectFactory{T}"/> class.
    /// </summary>
    [TestFixture]
    public class CtorObjectFactoryTests
    {
        /// <summary>
        /// Tests if the <see cref="CtorObjectFactory{T}.Create"/> method correctly creates new instances of types
        /// with a public, parameterless constructor.
        /// </summary>
        [Test]
        public void CreateObjectOfClassWithParameterlessConstructor()
        {
            var factory = new CtorObjectFactory<ClassWithParameterlessCtor>();

            ClassWithParameterlessCtor result = factory.Create();

            Assert.That(result, Is.InstanceOf<ClassWithParameterlessCtor>());
        }

        /// <summary>
        /// Tests if the <see cref="CtorObjectFactory{T}.Create"/> method
        /// </summary>
        [Test]
        public void CreateObjectOfClassWithoutParameterlessConstructor()
        {
            var exception = Assert.Throws<InvalidOperationException>(
                () => new CtorObjectFactory<ClassWithoutParameterlessCtor>());

            string expectedMessage = 
                "Failed to find parameterless constructor for 'Proligence.Helpers.UnitTests.Reflection." +
                "CtorObjectFactoryTests+ClassWithoutParameterlessCtor' type.";
            Assert.That(exception.Message, Is.EqualTo(expectedMessage));
        }

        /// <summary>
        /// Test class with parameterless constructor.
        /// </summary>
        private class ClassWithParameterlessCtor
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ClassWithParameterlessCtor"/> class.
            /// </summary>
            public ClassWithParameterlessCtor()
            {
            }
        }

        /// <summary>
        /// Test class without parameterless constructor.
        /// </summary>
        private class ClassWithoutParameterlessCtor
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ClassWithoutParameterlessCtor"/> class.
            /// </summary>
            /// <param name="foo">Test parameter.</param>
            public ClassWithoutParameterlessCtor(int foo)
            {
            }
        }
    }
}