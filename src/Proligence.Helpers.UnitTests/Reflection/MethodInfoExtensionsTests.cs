﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MethodInfoExtensionsTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    using NUnit.Framework;

    using Proligence.Helpers.Reflection;

    /// <summary>
    /// MethodInfo ToDelegate helper unit tests.
    /// </summary>
    [TestFixture]
    public class MethodInfoExtensionsTests
    {
        /// <summary>
        /// Delegate with the same signature as the method should be called without problems.
        /// </summary>
        [Test]
        public void ShouldCreateAndCallDelegateWithSameSignature()
        {
            var del =
                (Func<ITestInterface, TestClass, ITestInterface>)
                this.GetType().GetMethod("TestMethodFunc").ToDelegate(this, typeof(ITestInterface), typeof(TestClass));

            var testClass = new TestClass();
            var newTestClass = new TestClass();
            ITestInterface retVal = null;

            Assert.DoesNotThrow(() => retVal = del(testClass, newTestClass));
            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal.GetType() == typeof(TestClass));
            Assert.IsTrue(testClass.Equals(retVal));
        }

        /// <summary>
        /// Should be able to cast using inherited types in delegate signature.
        /// </summary>
        [Test]
        public void ShouldCreateAndCallDelegateWithInheritedTypes()
        {
            var del =
                (Func<NewTestClass, NewTestClass, NewTestClass>)
                this.GetType()
                    .GetMethod("TestMethodFunc")
                    .ToDelegate(this, typeof(NewTestClass), typeof(NewTestClass));

            var testClass = new NewTestClass();
            NewTestClass retVal = null;

            Assert.DoesNotThrow(() => retVal = del(testClass, testClass));
            Assert.IsNotNull(retVal);
            Assert.IsTrue(retVal.GetType() == typeof(NewTestClass));
            Assert.IsTrue(testClass.Equals(retVal));
        }

        /// <summary>
        /// Incorrect type arguments should throw ArgumentException.
        /// </summary>
        [Test]
        public void ShouldFailOnIncorrectTypeArguments()
        {
            Assert.Throws<ArgumentException>(
                () => this.GetType().GetMethod("TestMethodFunc").ToDelegate(this, typeof(object), typeof(object)));
        }

        /// <summary>
        /// Incorrect delegate signature should throw InvalidCastException.
        /// </summary>
        [Test]
        public void ShouldFailOnIncorrectSignature()
        {
            var del = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(ITestInterface), typeof(TestClass));

            Assert.Throws<InvalidCastException>(() => ((Func<string, string, object>)del)("Test1", "Test2"));
        }

        /// <summary>
        /// Tests delegate caching.
        /// </summary>
        [Test]
        public void SubsequentCreationsShouldReturnTheSameDelegate()
        {
            var del1 = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(NewTestClass), typeof(NewTestClass));
            var del2 = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(NewTestClass), typeof(NewTestClass));
            Assert.AreEqual(del1, del2);
        }

        /// <summary>
        /// Tests delegate creation.
        /// </summary>
        [Test]
        public void ShouldCreateNewDelegateForDifferentTypeParameters()
        {
            var del1 = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(ITestInterface), typeof(NewTestClass));
            var del2 = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(NewTestClass), typeof(NewTestClass));
            Assert.AreNotEqual(del1, del2);
        }

        /// <summary>
        /// Tests if Action delegate gets created.
        /// </summary>
        [Test]
        public void ReturnTypeVoidShouldResolveToAction()
        {
            var del = this.GetType().GetMethod("TestMethodAction").ToDelegate(this, typeof(NewTestClass));
            Assert.AreEqual(del.GetType(), typeof(Action<NewTestClass>));
        }

        /// <summary>
        /// Tests if <c>Func</c> delegate gets created.
        /// </summary>
        [Test]
        public void ReturnTypeNonVoidShouldResolveToFunc()
        {
            var del = this.GetType().GetMethod("TestMethodFunc").ToDelegate(
                this, typeof(TestClass), typeof(NewTestClass));
            Assert.AreEqual(del.GetType(), typeof(Func<TestClass, NewTestClass, TestClass>));
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters")]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented")]
        public T1 TestMethodFunc<T1, T2>(T1 firstObject, T2 secondObject) 
            where T1 : ITestInterface where T2 : TestClass
        {
            Debug.WriteLine(secondObject.GetType().ToString());
            return firstObject;
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters")]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented")]
        public void TestMethodAction<T1>(T1 firstObject) where T1 : ITestInterface
        {
            Debug.WriteLine(firstObject.GetType().ToString());
        }
    }

    #region Test objects

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass",
        Justification = "Reviewed. Suppression is OK here.")]
    public class TestClass : ITestInterface
    {
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass",
        Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented",
        Justification = "Reviewed. Suppression is OK here.")]
    public class NewTestClass : TestClass
    {
    }

    #endregion
}