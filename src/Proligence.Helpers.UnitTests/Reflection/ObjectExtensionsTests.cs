﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectExtensionsTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Reflection
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using Helpers.Reflection;
    using NUnit.Framework;

    // ReSharper disable InvokeAsExtensionMethod

    /// <summary>
    /// Implements unit tests for the <see cref="ObjectExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class ObjectExtensionsTests
    {
        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.GetField{T}"/> method throws a <see cref="ArgumentNullException"/>
        /// if the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void GetFieldWhenObjNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => ObjectExtensions.GetField<object>(null, "x"));
            
            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.GetField{T}"/> method properly sets a value of an instance field
        /// in the object's type.
        /// </summary>
        [Test]
        public void GetFieldCurrentInstance()
        {
            var obj = new TestObject();
            obj.CurrentInstance = "str";
            string value = ObjectExtensions.GetField<string>(obj, "currentInstance");

            Assert.That(value, Is.EqualTo("str"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.GetField{T}"/> method properly sets a value of an instance field
        /// in the object's base type.
        /// </summary>
        [Test]
        public void GetFieldBaseInstance()
        {
            var obj = new TestObject();
            obj.BaseInstance = "str";
            string value = ObjectExtensions.GetField<string>(obj, "baseInstance");

            Assert.That(value, Is.EqualTo("str"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.GetField{T}"/> method throws a <see cref="ArgumentException"/> if
        /// the specified object does not have a field with the specified name.
        /// </summary>
        [Test]
        public void GetFieldWithInvalidName()
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => ObjectExtensions.GetField<object>(new TestObject(), "invalid"));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
            StringAssert.Contains("Failed to find a field with the name 'invalid'.", exception.Message);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.SetField"/> method throws a <see cref="ArgumentNullException"/>
        /// if the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void SetFieldWhenObjNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => ObjectExtensions.SetField(null, "x", "y"));
            
            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.SetField"/> method properly sets a value of an instance field in 
        /// the object's type.
        /// </summary>
        [Test]
        public void SetFieldCurrentInstance()
        {
            var obj = new TestObject();
            ObjectExtensions.SetField(obj, "currentInstance", "value");

            Assert.That(obj.CurrentInstance, Is.EqualTo("value"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.SetField"/> method properly sets a value of an instance field in 
        /// the object's base type.
        /// </summary>
        [Test]
        public void SetFieldBaseInstance()
        {
            var obj = new TestObject();
            ObjectExtensions.SetField(obj, "baseInstance", "value");

            Assert.That(obj.BaseInstance, Is.EqualTo("value"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.SetField"/> method throws a <see cref="ArgumentException"/> if the
        /// specified object does not have a field with the specified name.
        /// </summary>
        [Test]
        public void SetFieldWithInvalidName()
        {
            var exception = Assert.Throws<ArgumentException>(
                () => ObjectExtensions.SetField(new TestObject(), "invalid", "value"));

            Assert.That(exception.ParamName, Is.EqualTo("name"));
            StringAssert.Contains("Failed to find a field with the name 'invalid'.", exception.Message);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method correctly calls private methods.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodIsPrivate()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("PrivateMethod", 2, 3);

            Assert.That(value, Is.EqualTo("5"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method correctly calls protected methods.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodIsProtected()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("ProtectedMethod", 2, 3);

            Assert.That(value, Is.EqualTo("-1"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method correctly calls internal methods.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodIsInternal()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("InternalMethod", 2, 3);

            Assert.That(value, Is.EqualTo("6"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method correctly calls public methods.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodIsPublic()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("PublicMethod", 4, 2);

            Assert.That(value, Is.EqualTo("2"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method throws a <see cref="ArgumentException"/>
        /// when the specified method does not exist.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodDoesNotExist()
        {
            var obj = new TestObject();
            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => obj.CallMethod("InvalidMethod", 4, 2));

            string expectedMessage =
                "Failed to find method 'InvalidMethod' on object of type '" + obj.GetType().FullName + "'.";
            Assert.That(exception.Message, Is.StringContaining(expectedMessage));
            Assert.That(exception.ParamName, Is.EqualTo("methodName"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method works correctly when the specified object
        /// contains two methods with the same name.
        /// </summary>
        [Test]
        public void CallMethodWhenNameAmbiguous()
        {
            var obj = new TestObject();
            object value1 = obj.CallMethod("AmbiguousMethod", "str");
            object value2 = obj.CallMethod("AmbiguousMethod", 0);

            Assert.That(value1, Is.EqualTo("Str"));
            Assert.That(value2, Is.EqualTo("Int"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method works correctly when the specified parameters
        /// collection is empty.
        /// </summary>
        [Test]
        public void CallMethodWhenParametersEmpty()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("ParameterlessMethod");

            Assert.That(value, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method works correctly when the specified parameters
        /// collection is <c>null</c>.
        /// </summary>
        [Test]
        public void CallMethodWhenParametersNull()
        {
            var obj = new TestObject();
            object value = obj.CallMethod("ParameterlessMethod", null);

            Assert.That(value, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified target object is <c>null</c>.
        /// </summary>
        [Test]
        public void CallMethodWhenTargetNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ObjectExtensions.CallMethod(null, "ToString"));

            Assert.That(exception.ParamName, Is.EqualTo("target"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallMethod"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified method is <c>null</c>.
        /// </summary>
        [Test]
        public void CallMethodWhenMethodNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.CallMethod(null));

            Assert.That(exception.ParamName, Is.EqualTo("methodName"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallGeneric{TReturn}"/> method works correctly.
        /// </summary>
        [Test]
        public void CallMethodGeneric()
        {
            var obj = new TestObject();
            string value = obj.CallMethod<string>("PublicMethod", 4, 2);

            Assert.That(value, Is.EqualTo("2"));
        }

        /// <summary>
        /// Should call generic action (function with no return value).
        /// </summary>
        [Test]
        public void ShouldCallGenericAction()
        {
            var testObj = new TestObject();
            testObj.CallGeneric("GenericAction", new[] { typeof(string) }, "Test");
            
            Assert.That(testObj.GenericActionParameter, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallGeneric"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void CallGenericActionWhenTargetNull()
        {
            TestObject testObj = null;
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => testObj.CallGeneric("GenericAction", new[] { typeof(string) }, "Test"));
            
            Assert.That(exception.ParamName, Is.EqualTo("target"));
        }

        /// <summary>
        /// Should call generic function with return value.
        /// </summary>
        [Test]
        public void ShouldCallGenericFunction()
        {
            var testObj = new TestObject();
            var retVal = testObj.CallGeneric<string>("GenericFunction", new[] { typeof(string) }, "Test");
            Assert.That(retVal, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CallGeneric{TReturn}"/> method throws a 
        /// <see cref="ArgumentNullException"/> when the specified object is <c>null</c>.
        /// </summary>
        [Test]
        public void CallGenericFunctionWhenTargetNull()
        {
            TestObject testObj = null;
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => testObj.CallGeneric<string>("GenericFunction", new[] { typeof(string) }, "Test"));
            
            Assert.That(exception.ParamName, Is.EqualTo("target"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectExtensions.CreateDelegate"/> method throws a 
        /// <see cref="ArgumentNullException"/> when the specified object is null.
        /// </summary>
        [Test]
        public void CreateDelegateWhenTargetNull()
        {
            TestObject testObj = null;
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => testObj.CreateDelegate("Test", new[] { typeof(string) }));
            
            Assert.That(exception.ParamName, Is.EqualTo("target"));
        }

        [Test]
        public void MemberwiseCloneWhenObjectNull()
        {
            CloneTestObject testObj = null;

            // ReSharper disable once ExpressionIsAlwaysNull
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => testObj.MemberwiseClone());

            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }

        [Test]
        public void MemberwiseCloneWhenObjectNotNull()
        {
            CloneTestObject testObj = new CloneTestObject { Name = "foo", Number = 7 };
            
            CloneTestObject cloned = testObj.MemberwiseClone();

            Assert.That(cloned, Is.Not.SameAs(testObj));
            Assert.That(cloned.Name, Is.EqualTo("foo"));
            Assert.That(cloned.Number, Is.EqualTo(7));
        }

        /// <summary>
        /// The class of the test type.
        /// </summary>
        private class TestObject : TestObjectBase
        {
            /// <summary>
            /// Instance test field.
            /// </summary>
            private string currentInstance;

            /// <summary>
            /// Gets or sets the value of the instance field.
            /// </summary>
            public string CurrentInstance
            {
                get { return this.currentInstance; }
                set { this.currentInstance = value; }
            }

            /// <summary>
            /// Gets the last parameter passed to the <see cref="GenericAction{T}"/> method.
            /// </summary>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", 
                Justification = "Used through reflection.")]
            public string GenericActionParameter { get; private set; }

            /// <summary>
            /// Some generic action.
            /// </summary>
            /// <param name="parameter">
            /// The parameter.
            /// </param>
            /// <typeparam name="T">
            /// Type parameter.
            /// </typeparam>
            public void GenericAction<T>(T parameter)
            {
                this.GenericActionParameter = parameter.ToString();
            }

            /// <summary>
            /// Some generic function.
            /// </summary>
            /// <typeparam name="T">Type parameter.</typeparam>
            /// <param name="parameter">The parameter.</param>
            /// <returns>Object passed as parameter.</returns>
            public T GenericFunction<T>(T parameter)
            {
                return parameter;
            }

            /// <summary>
            /// Parameterless test method.
            /// </summary>
            /// <returns>Test string.</returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            public string ParameterlessMethod()
            {
                return "Test";
            }

            /// <summary>
            /// Public test method.
            /// </summary>
            /// <param name="a">First parameter</param>
            /// <param name="b">Second parameter</param>
            /// <returns><paramref name="a"/> / <paramref name="b"/></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            public string PublicMethod(int a, int b)
            {
                return (a / b).ToString(CultureInfo.InvariantCulture);
            }

            /// <summary>
            /// Ambiguous test method.
            /// </summary>
            /// <param name="str">String parameter.</param>
            /// <returns>String value - <c>Str</c></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            public string AmbiguousMethod(string str)
            {
                return "Str";
            }

            /// <summary>
            /// Ambiguous test method.
            /// </summary>
            /// <param name="n">Integer parameter.</param>
            /// <returns>String value - <c>Int</c></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            public string AmbiguousMethod(int n)
            {
                return "Int";
            }

            /// <summary>
            /// Internal test method.
            /// </summary>
            /// <param name="a">First parameter</param>
            /// <param name="b">Second parameter</param>
            /// <returns><paramref name="a"/> * <paramref name="b"/></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            internal string InternalMethod(int a, int b)
            {
                return (a * b).ToString(CultureInfo.InvariantCulture);
            }

            /// <summary>
            /// Protected test method.
            /// </summary>
            /// <param name="a">First parameter</param>
            /// <param name="b">Second parameter</param>
            /// <returns><paramref name="a"/> - <paramref name="b"/></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            protected string ProtectedMethod(int a, int b)
            {
                return (a - b).ToString(CultureInfo.InvariantCulture);
            }

            /// <summary>
            /// Private test method.
            /// </summary>
            /// <param name="a">First parameter</param>
            /// <param name="b">Second parameter</param>
            /// <returns><paramref name="a"/> / <paramref name="b"/></returns>
            [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode",
                Justification = "Used via reflection.")]
            private string PrivateMethod(int a, int b)
            {
                return (a + b).ToString(CultureInfo.InvariantCulture);
            }
        }
     
        /// <summary>
        /// The base class of the test type.
        /// </summary>
        private class TestObjectBase
        {
            /// <summary>
            /// Instance test field.
            /// </summary>
            private string baseInstance;

            /// <summary>
            /// Gets or sets the value of the instance field.
            /// </summary>
            public string BaseInstance
            {
                get { return this.baseInstance; }
                set { this.baseInstance = value; }
            }
        }

        /// <summary>
        /// Class for testing <see cref="ObjectExtensions.MemberwiseClone"/>.
        /// </summary>
        private class CloneTestObject
        {
            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            public int Number { get; set; }
        }
    }

    // ReSharper restore InvokeAsExtensionMethod
}