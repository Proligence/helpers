﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeadlineTaskCellUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Scheduling
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;
    using Proligence.Helpers.Scheduling;

    /// <summary>
    /// Implements unit tests for the <see cref="DeadlineTaskCell{TTask}"/> class.
    /// </summary>
    [TestFixture]
    public class DeadlineTaskCellUnitTests
    {
        /// <summary>
        /// Tests if new instance of the <see cref="DeadlineTaskCell{TTask}"/> class are created correctly.
        /// </summary>
        [Test]
        public void CreateTaskCell()
        {
            var task = new TaskMock();
            var taskCell = new DeadlineTaskCell<TaskMock>(task);
            
            Assert.That(taskCell.Task, Is.SameAs(task));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="DeadlineTaskCell{TTask}"/> class throws a 
        /// <see cref="ArgumentNullException"/> when the specified task object is <c>null</c>.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Unit test.")]
        public void CreateTaskCellWhenTaskNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new DeadlineTaskCell<TaskMock>(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("task"));
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.CompareTo"/> method correctly compares task cells when
        /// the other task cell has a earlier deadline than the current task cell.
        /// </summary>
        [Test]
        public void CompareToWhenEarlierDeadline()
        {
            var cell1 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = DateTime.Now.ToFileTimeUtc() });
            var cell2 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = DateTime.Now.AddMinutes(1).ToFileTimeUtc() });

            Assert.That(cell1.CompareTo(cell2), Is.LessThan(0));
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.CompareTo"/> method correctly compares task cells when
        /// the other task cell has the same deadline as the current task cell.
        /// </summary>
        [Test]
        public void CompareToWhenSameDeadline()
        {
            var cell1 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = DateTime.Now.ToFileTimeUtc() });
            var cell2 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = cell1.Task.Deadline });

            Assert.That(cell1.CompareTo(cell2), Is.EqualTo(0));
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.CompareTo"/> method correctly compares task cells when
        /// the other task cell has a later deadline than the current task cell.
        /// </summary>
        [Test]
        public void CompareToWhenLaterDeadline()
        {
            var cell1 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = DateTime.Now.AddMinutes(1).ToFileTimeUtc() });
            var cell2 = new DeadlineTaskCell<TaskMock>(
                new TaskMock { Deadline = DateTime.Now.ToFileTimeUtc() });

            Assert.That(cell1.CompareTo(cell2), Is.GreaterThan(0));
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.CompareTo"/> method correctly compares task cells when
        /// the other task cell is <c>null</c>.
        /// </summary>
        [Test]
        public void CompareToWhenOtherNull()
        {
            var cell1 = new DeadlineTaskCell<TaskMock>(new TaskMock());
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => cell1.CompareTo(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("other"));
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.Equals(object)"/> method correctly compares task cells
        /// when the task cells are equal.
        /// </summary>
        [Test]
        public void EqualsWhenEqual()
        {
            var task = new TaskMock();
            var cell1 = new DeadlineTaskCell<TaskMock>(task);
            var cell2 = new DeadlineTaskCell<TaskMock>(task);

            Assert.That(cell1.Equals(cell2), Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.Equals(object)"/> method correctly compares task cells
        /// when the other cell has a different task.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentTask()
        {
            var cell1 = new DeadlineTaskCell<TaskMock>(new TaskMock());
            var cell2 = new DeadlineTaskCell<TaskMock>(new TaskMock());

            Assert.That(cell1.Equals(cell2), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="DeadlineTaskCell{TTask}.Equals(object)"/> method correctly compares task cells
        /// when the other object is not a task cell.
        /// </summary>
        [Test]
        public void EqualsWhenOtherType()
        {
            var cell = new DeadlineTaskCell<TaskMock>(new TaskMock());
            bool result = cell.Equals("str");

            Assert.That(result, Is.False);
        }

        /// <summary>
        /// Tests the <see cref="DeadlineTaskCell{TTask}.GetHashCode"/> method.
        /// </summary>
        [Test]
        public void TestGetHashCode()
        {
            var cell = new DeadlineTaskCell<TaskMock>(new TaskMock());
            int hashCode = cell.GetHashCode();

            Assert.That(hashCode, Is.EqualTo(cell.Task.GetHashCode()));
        }

        /// <summary>
        /// Tests the <see cref="DeadlineTaskCell{TTask}.ToString"/> method.
        /// </summary>
        [Test]
        public void TestToString()
        {
            var cell = new DeadlineTaskCell<TaskMock>(new TaskMock());
            string str = cell.ToString();

            Assert.That(str, Is.EqualTo("<" + cell.Task + ">"));
        }
    }
}