﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchedulerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Scheduling
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using NUnit.Framework;
    using Proligence.Helpers.Scheduling;

    /// <summary>
    /// Implements generic unit tests for <see cref="Scheduler{TTask}"/> subclasses.
    /// </summary>
    /// <typeparam name="TScheduler">The type of the tested scheduler.</typeparam>
    /// <typeparam name="TTask">The type of tasks managed by the scheduler.</typeparam>
    public abstract class SchedulerUnitTests<TScheduler, TTask>
        where TScheduler : Scheduler<TTask> 
        where TTask : TaskMock
    {
        /// <summary>
        /// Gets the tested <see cref="DeadlineScheduler{TTask}"/> instance.
        /// </summary>
        protected TScheduler Scheduler { get; private set; }

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.Scheduler = this.CreateScheduler();
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this.Scheduler.Dispose();
        }

        /// <summary>
        /// Tests the default value of the <see cref="DeadlineScheduler{TTask}.PollFrequency"/> property.
        /// </summary>
        [Test]
        public void TestDefaultPollFrequency()
        {
            Assert.That(this.Scheduler.PollFrequency, Is.EqualTo(100));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.ScheduleTask"/> method correctly schedules a single task.
        /// </summary>
        [Test]
        public void ScheduleTask()
        {
            var task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);

            Assert.That(
                this.GetScheduledTasks(this.Scheduler).ToArray(),
                Is.EquivalentTo(new[] { task }));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.ScheduleTask"/> method correctly schedules multiple tasks.
        /// </summary>
        [Test]
        public void ScheduleManyTasks()
        {
            TTask[] tasks = new TTask[3];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = this.CreateTask();
            }

            foreach (TTask task in tasks)
            {
                this.Scheduler.ScheduleTask(task);
            }

            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.EquivalentTo(tasks));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.ScheduleTask"/> method throws a 
        /// <see cref="ArgumentNullException"/> when the specified task is <c>null</c>.
        /// </summary>
        [Test]
        public void ScheduleTaskWhenTaskNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.Scheduler.ScheduleTask(null));

            Assert.That(exception.ParamName, Is.EqualTo("task"));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.ScheduleTask"/> method throws a
        /// <see cref="InvalidOperationException"/> when the specified task is already scheduled.
        /// </summary>
        [Test]
        public void ScheduleDuplicateTask()
        {
            var task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);
            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(
                () => this.Scheduler.ScheduleTask(task));

            Assert.That(exception.Message, Is.EqualTo("The specified task is already scheduled."));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.RemoveTask"/> method correctly removes scheduled tasks from the
        /// queue.
        /// </summary>
        [Test]
        public void RemoveTask()
        {
            TTask[] tasks = new TTask[3];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = this.CreateTask();
            }

            foreach (TTask task in tasks)
            {
                this.Scheduler.ScheduleTask(task);
            }

            this.Scheduler.RemoveTask(tasks[1]);

            Assert.That(
                this.GetScheduledTasks(this.Scheduler).ToArray(), 
                Is.EquivalentTo(new[] { tasks[0], tasks[2] }));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.RemoveTask"/> method correctly removes the last scheduled task
        /// from the queue.
        /// </summary>
        [Test]
        public void RemoveLastTask()
        {
            TTask task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);
            this.Scheduler.RemoveTask(task);

            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.EquivalentTo(new TTask[0]));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.RemoveTask"/> method throws a <see cref="ArgumentNullException"/>
        /// when the specified task is <c>null</c>.
        /// </summary>
        [Test]
        public void RemoveTaskWhenTaskNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.Scheduler.RemoveTask(null));

            Assert.That(exception.ParamName, Is.EqualTo("task"));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.RemoveTask"/> method throws a 
        /// <see cref="InvalidOperationException"/> when the specified task is not scheduled.
        /// </summary>
        [Test]
        public void RemoveUnscheduledTask()
        {
            var task = this.CreateTask();
            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(
                () => this.Scheduler.RemoveTask(task));

            Assert.That(exception.Message, Is.EqualTo("The specified task is not scheduled."));
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.ExecuteTask"/> method properly executes a task when one
        /// task is available.
        /// </summary>
        [Test]
        public void ExecuteTaskWhenTaskAvailable()
        {
            TTask task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);

            TaskMock executed = this.Scheduler.ExecuteTask();

            Assert.That(task.WorkDone, Is.True);
            Assert.That(executed, Is.SameAs(task));
            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.ExecuteTask"/> method properly executes a task when
        /// multiple tasks are available.
        /// </summary>
        [Test]
        public void ExecuteTaskWhenMultipleTasksAvailable()
        {
            TTask task1 = this.CreateTask();
            TTask task2 = this.CreateTask();
            TTask task3 = this.CreateTask();
            this.Scheduler.ScheduleTask(task1);
            this.Scheduler.ScheduleTask(task2);
            this.Scheduler.ScheduleTask(task3);

            TaskMock executed = this.Scheduler.ExecuteTask();

            Assert.That(task1.WorkDone, Is.True);
            Assert.That(executed, Is.SameAs(task1));
            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.EquivalentTo(new[] { task2, task3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.ExecuteTask"/> method properly executes a task when
        /// no tasks are available.
        /// </summary>
        [Test, Timeout(1000)]
        public void ExecuteTaskWhenNoTasksAvailable()
        {
            bool running = false;
            int elapsed = 0;
            TaskMock executed = null;
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    running = true;
                    DateTime dt = DateTime.Now;
                    executed = this.Scheduler.ExecuteTask();
                    elapsed = (DateTime.Now - dt).Milliseconds;
                });

            while (!running)
            {
                Thread.Sleep(10);
            }

            Thread.Sleep(100);
            Assert.That(executed, Is.Null);

            TTask task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);

            while (executed == null)
            {
                Thread.Sleep(10);
            }

            Assert.That(task.WorkDone, Is.True);
            Assert.That(executed, Is.SameAs(task));
            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.Empty);
            Assert.That(elapsed, Is.GreaterThanOrEqualTo(100));
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.TryExecuteTask"/> method properly executes a task when
        /// one task is available.
        /// </summary>
        [Test]
        public void TryExecuteTaskWhenTaskAvailable()
        {
            TTask task = this.CreateTask();
            this.Scheduler.ScheduleTask(task);

            TaskMock executed = this.Scheduler.TryExecuteTask();

            Assert.That(task.WorkDone, Is.True);
            Assert.That(executed, Is.SameAs(task));
            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.TryExecuteTask"/> method properly executes a task when
        /// multiple tasks are available.
        /// </summary>
        [Test]
        public void TryExecuteTaskWhenMultipleTasksAvailable()
        {
            TTask task1 = this.CreateTask();
            TTask task2 = this.CreateTask();
            TTask task3 = this.CreateTask();
            this.Scheduler.ScheduleTask(task1);
            this.Scheduler.ScheduleTask(task2);
            this.Scheduler.ScheduleTask(task3);

            TaskMock executed = this.Scheduler.TryExecuteTask();

            Assert.That(task1.WorkDone, Is.True);
            Assert.That(executed, Is.SameAs(task1));
            Assert.That(this.GetScheduledTasks(this.Scheduler).ToArray(), Is.EquivalentTo(new[] { task2, task3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FifoScheduler{TTask}.TryExecuteTask"/> method properly executes a task when
        /// no tasks are available.
        /// </summary>
        [Test, Timeout(1000)]
        public void TryExecuteTaskWhenNoTasksAvailable()
        {
            TaskMock executed = this.Scheduler.TryExecuteTask();

            Assert.That(executed, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingReadLock"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="Action"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void UsingReadLockWhenActionNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.Scheduler.UsingReadLock(null));

            Assert.That(exception.ParamName, Is.EqualTo("action"));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingReadLock"/> method works correctly when the specified
        /// <see cref="Action"/> is not <c>null</c>.
        /// </summary>
        [Test]
        public void UsingReadLockWhenActionNotNull()
        {
            bool actionCalled = false;
            
            this.Scheduler.UsingReadLock(() =>
            {
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.True);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.False);
                actionCalled = true;
            });

            Assert.IsTrue(actionCalled);
            Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
            Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingReadLock"/> method works correctly when the current thread
        /// has already acquired a read lock for the scheduler's task queue.
        /// </summary>
        [Test]
        public void UsingReadLockWhenReadLockAlreadyAcquired()
        {
            bool actionCalled = false;
            this.Scheduler.TaskQueueLock.EnterReadLock();

            this.Scheduler.UsingReadLock(() =>
            {
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.True);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.False);
                actionCalled = true;
            });
           
            Assert.IsTrue(actionCalled);
            Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.True);
            Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.False);

            this.Scheduler.TaskQueueLock.ExitReadLock();
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingReadLock"/> method works correctly when the current thread
        /// has already acquired a write lock for the scheduler's task queue.
        /// </summary>
        [Test]
        public void UsingReadLockWhenWriteLockAlreadyAcquired()
        {
            bool actionCalled = false;
            this.Scheduler.TaskQueueLock.EnterWriteLock();

            this.Scheduler.UsingReadLock(() =>
            {
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);
                actionCalled = true;
            });

            Assert.IsTrue(actionCalled);
            Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
            Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);

            this.Scheduler.TaskQueueLock.ExitWriteLock();
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingWriteLock"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="Action"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void UsingWriteLockWhenActionNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.Scheduler.UsingWriteLock(null));

            Assert.That(exception.ParamName, Is.EqualTo("action"));
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingWriteLock"/> method works correctly when the specified
        /// <see cref="Action"/> is not <c>null</c>.
        /// </summary>
        [Test]
        public void UsingWriteLockWhenActionNotNull()
        {
            bool actionCalled = false;

            this.Scheduler.UsingWriteLock(() =>
            {
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);
                actionCalled = true;
            });

            Assert.IsTrue(actionCalled);
            Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
            Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingWriteLock"/> method works correctly when the current thread
        /// has already acquired a read lock for the scheduler's task queue.
        /// </summary>
        [Test]
        [ExpectedException(typeof(LockRecursionException))]
        public void UsingWriteLockWhenReadLockAlreadyAcquired()
        {
            bool actionCalled = false;
            this.Scheduler.TaskQueueLock.EnterReadLock();

            try
            {
                this.Scheduler.UsingWriteLock(() =>
                {
                    Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.True);
                    Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);
                    actionCalled = true;
                });

                Assert.IsTrue(actionCalled);
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.True);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);
            }
            finally
            {
                this.Scheduler.TaskQueueLock.ExitReadLock();
            }
        }

        /// <summary>
        /// Tests if the <see cref="Scheduler{TTask}.UsingWriteLock"/> method works correctly when the current thread
        /// has already acquired a write lock for the scheduler's task queue.
        /// </summary>
        [Test]
        public void UsingWriteLockWhenWriteLockAlreadyAcquired()
        {
            bool actionCalled = false;
            this.Scheduler.TaskQueueLock.EnterWriteLock();

            this.Scheduler.UsingWriteLock(() =>
            {
                Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
                Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);
                actionCalled = true;
            });

            Assert.IsTrue(actionCalled);
            Assert.That(this.Scheduler.TaskQueueLock.IsReadLockHeld, Is.False);
            Assert.That(this.Scheduler.TaskQueueLock.IsWriteLockHeld, Is.True);

            this.Scheduler.TaskQueueLock.ExitWriteLock();
        }

        /// <summary>
        /// Schedules the specified tasks and executes them using <c>Parallel.For</c>.
        /// </summary>
        /// <param name="tasks">The tasks to execute.</param>
        protected void ScheduleTasksInParallel(TTask[] tasks)
        {
            if (tasks == null)
            {
                throw new ArgumentNullException("tasks");
            }

            foreach (TTask task in tasks)
            {
                this.Scheduler.ScheduleTask(task);
            }

            Parallel.For(
                0,
                10,
                (i, state) =>
                {
                    for (int j = 0; j < 10; j++)
                    {
                        this.Scheduler.ExecuteTask();
                    }
                });
        }

        /// <summary>
        /// Creates a scheduler instance for testing purposes.
        /// </summary>
        /// <returns>The created scheduler instance.</returns>
        protected abstract TScheduler CreateScheduler();

        /// <summary>
        /// Creates a task instance for testing purposes.
        /// </summary>
        /// <returns>The created task instance.</returns>
        protected abstract TTask CreateTask();

        /// <summary>
        /// Gets all tasks scheduled in the specified scheduler.
        /// </summary>
        /// <param name="scheduler">The scheduler instance.</param>
        /// <returns>Sequence of tasks which are scheduled in the specified scheduler.</returns>
        protected abstract IEnumerable<TTask> GetScheduledTasks(TScheduler scheduler);
    }
}