﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TaskMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Scheduling
{
    using System;
    using Proligence.Helpers.Scheduling;

    /// <summary>
    /// Implements the <see cref="ITask"/> interface for testing purposes.
    /// </summary>
    public class TaskMock : IDeadlineTask
    {
        /// <summary>
        /// Gets or sets the numeric identifier associated with the task.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the UTC timestamp of the task's deadline.
        /// </summary>
        public long Deadline { get; set; }

        /// <summary>
        /// Gets or sets the delegate which is invoked by the <see cref="DoWork"/> method.
        /// </summary>
        public Action<TaskMock> DoWorkFunc { get; set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="DoWork"/> method was called.
        /// </summary>
        public bool WorkDone { get; private set; }

        /// <summary>
        /// Performs the task's work.
        /// </summary>
        public void DoWork()
        {
            if (this.DoWorkFunc != null)
            {
                this.DoWorkFunc(this);
            }

            this.WorkDone = true;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return new TaskMock { Deadline = this.Deadline };
        }
    }
}