﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeadlineSchedulerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Scheduling
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Helpers.Scheduling;

    /// <summary>
    /// Implements unit tests for the <see cref="DeadlineScheduler{TTask}"/> class.
    /// </summary>
    [TestFixture]
    public class DeadlineSchedulerUnitTests : SchedulerUnitTests<DeadlineSchedulerMock, TaskMock>
    {
        /// <summary>
        /// Tests if the <see cref="DeadlineScheduler{TTask}.ExecuteTask"/> method properly executes multiple tasks
        /// with different deadlines.
        /// </summary>
        [Test, Timeout(1000)]
        public void ExecuteTasksWithDifferentDeadlines()
        {
            var tasks = new TaskMock[3];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new TaskMock { Deadline = DateTime.Now.AddMilliseconds(100 * (i + 1)).ToFileTimeUtc() };
            }

            this.Scheduler.ScheduleTask(tasks[2]);
            this.Scheduler.ScheduleTask(tasks[0]);
            this.Scheduler.ScheduleTask(tasks[1]);
            
             DateTime time = DateTime.Now;
            var executed = new TaskMock[3];
            for (int i = 0; i < executed.Length; i++)
            {
                executed[i] = this.Scheduler.ExecuteTask();
            }
            
            int elapsed = (DateTime.Now - time).Milliseconds;
            Assert.That(elapsed, Is.GreaterThanOrEqualTo(300));
            Assert.That(elapsed, Is.LessThan(500));
            
            for (int i = 0; i < executed.Length; i++)
            {
                Assert.That(executed[i].WorkDone, Is.True);
                Assert.That(executed[i], Is.SameAs(tasks[i]));
            }

            Assert.That(this.Scheduler.GetTasks().Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Tests executing tasks from multiple threads.
        /// </summary>
        [Test]
        public void ExecuteTasksFromMultipleThreads()
        {
            List<int> list = new List<int>();
            
            var tasks = new TaskMock[100];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new TaskMock
                {
                    Id = i,
                    Deadline = DateTime.Now.ToFileTimeUtc(),
                    DoWorkFunc = t =>
                    {
                        lock (list)
                        {
                            list.Add(t.Id);
                        }
                    }
                };
            }

            this.ScheduleTasksInParallel(tasks);

            Assert.That(list.Count, Is.EqualTo(tasks.Length));
            Assert.That(
                list.OrderBy(i => i).ToArray(), 
                Is.EqualTo(Enumerable.Range(0, tasks.Length).ToArray()));
        }

        /// <summary>
        /// Creates a scheduler instance for testing purposes.
        /// </summary>
        /// <returns>The created scheduler instance.</returns>
        protected override DeadlineSchedulerMock CreateScheduler()
        {
            return new DeadlineSchedulerMock();
        }

        /// <summary>
        /// Creates a task instance for testing purposes.
        /// </summary>
        /// <returns>The created task instance.</returns>
        protected override TaskMock CreateTask()
        {
            return new TaskMock { Deadline = DateTime.Now.AddSeconds(-1).ToFileTimeUtc() };
        }

        /// <summary>
        /// Gets all tasks scheduled in the specified scheduler.
        /// </summary>
        /// <param name="scheduler">The scheduler instance.</param>
        /// <returns>Sequence of tasks which are scheduled in the specified scheduler.</returns>
        protected override IEnumerable<TaskMock> GetScheduledTasks(DeadlineSchedulerMock scheduler)
        {
            if (scheduler == null)
            {
                throw new ArgumentNullException("scheduler");
            }

            return scheduler.GetTasks();
        }
    }
}