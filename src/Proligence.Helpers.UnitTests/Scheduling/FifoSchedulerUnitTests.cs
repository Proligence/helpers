﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FifoSchedulerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Scheduling
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Helpers.Scheduling;

    /// <summary>
    /// Implements unit tests for the <see cref="FifoScheduler{TTask}"/> class.
    /// </summary>
    [TestFixture]
    public class FifoSchedulerUnitTests : SchedulerUnitTests<FifoSchedulerMock, TaskMock>
    {
        /// <summary>
        /// Checks if tasks are executed in the correct order by the <see cref="Scheduler{TTask}.ExecuteTask"/> 
        /// method.
        /// </summary>
        [Test]
        public void TestTaskExecutionOrder()
        {
            this.TaskExecutionOrderTest(scheduler => scheduler.ExecuteTask());
        }

        /// <summary>
        /// Checks if tasks are executed in the correct order by the <see cref="Scheduler{TTask}.TryExecuteTask"/> 
        /// method.
        /// </summary>
        [Test]
        public void TestTaskExecutionOrderNonBlocking()
        {
            this.TaskExecutionOrderTest(scheduler => scheduler.TryExecuteTask());
        }

        /// <summary>
        /// Tests executing tasks from multiple threads.
        /// </summary>
        [Test]
        public void ExecuteTasksFromMultipleThreads()
        {
            List<int> list = new List<int>();
            
            var tasks = new TaskMock[100];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new TaskMock
                {
                    Id = i,
                    DoWorkFunc = t =>
                    {
                        lock (list)
                        {
                            list.Add(t.Id);
                        }
                    }
                };
            }

            this.ScheduleTasksInParallel(tasks);

            Assert.That(list.Count, Is.EqualTo(tasks.Length));
            Assert.That(
                list.OrderBy(i => i).ToArray(), 
                Is.EqualTo(Enumerable.Range(0, tasks.Length).ToArray()));
        }

        /// <summary>
        /// Creates a scheduler instance for testing purposes.
        /// </summary>
        /// <returns>The created scheduler instance.</returns>
        protected override FifoSchedulerMock CreateScheduler()
        {
            return new FifoSchedulerMock();
        }

        /// <summary>
        /// Creates a task instance for testing purposes.
        /// </summary>
        /// <returns>The created task instance.</returns>
        protected override TaskMock CreateTask()
        {
            return new TaskMock();
        }

        /// <summary>
        /// Gets all tasks scheduled in the specified scheduler.
        /// </summary>
        /// <param name="scheduler">The scheduler instance.</param>
        /// <returns>Sequence of tasks which are scheduled in the specified scheduler.</returns>
        protected override IEnumerable<TaskMock> GetScheduledTasks(FifoSchedulerMock scheduler)
        {
            if (scheduler == null)
            {
                throw new ArgumentNullException("scheduler");
            }

            return scheduler.GetTasks();
        }

        /// <summary>
        /// Generic test method for task execution order.
        /// </summary>
        /// <param name="executeFunc">The delegate which executes the task from the specified scheduler.</param>
        private void TaskExecutionOrderTest(Func<FifoScheduler<TaskMock>, TaskMock> executeFunc)
        {
            var tasks = new TaskMock[10];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = new TaskMock { Id = i };
            }

            int[] order = new[] { 5, 3, 4, 0, 1, 6, 2, 9, 7, 8 };
            foreach (int i in order)
            {
                this.Scheduler.ScheduleTask(tasks[i]);
            }

            foreach (int i in order)
            {
                TaskMock executed = executeFunc(this.Scheduler);
                Assert.That(executed.WorkDone, Is.True);
                Assert.That(executed, Is.SameAs(tasks[i]));
            }
        }
    }
}