﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NetworkExtensionsTest.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Network
{
    using System;
    using System.IO;
    using System.Net;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading;
    using Helpers.Network;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="NetworkExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class NetworkExtensionsTest
    {
        /// <summary>
        /// Should send binary serialized object.
        /// </summary>
        [Test]
        public void ShouldSendSerializedObject()
        {
            var testObj = new TestObjectToSend();
            var bytesSent = testObj.Send(new IPEndPoint(IPAddress.Loopback, 9999));
            Assert.That(bytesSent, Is.GreaterThan(0));
        }

        /// <summary>
        /// Should send string representation of an object.
        /// </summary>
        [Test]
        public void ShouldSendObjectAsString()
        {
            var testObj = new TestObjectToSend();
            var bytesSent = testObj.SendAsString(new IPEndPoint(IPAddress.Loopback, 9999));
            Assert.That(bytesSent, Is.GreaterThan(0));
        }

        /// <summary>
        /// Should send and receive the same object.
        /// </summary>
        [Test]
        public void ShouldSendAndReceiveObject()
        {
            var endpoint = new IPEndPoint(IPAddress.Any, 9990);
            TestObjectToSend receivedObj = null;
            endpoint.ListenStart(bytes =>
                                     {
                                         using (var memorystream = new MemoryStream(bytes, 0, bytes.Length))
                                         {
                                             var bf = new BinaryFormatter();
                                             receivedObj = (TestObjectToSend)bf.Deserialize(memorystream);
                                         }
                                     });

            var testObj = new TestObjectToSend
                              {
                                  DateValue = DateTime.Now, 
                                  IntegerValue = 99, 
                                  StringValue = "Test"
                              };
            testObj.Send(new IPEndPoint(IPAddress.Loopback, 9990));

            Thread.Sleep(3000);

            endpoint.ListenStop();

            Assert.That(receivedObj, Is.Not.Null);
            Assert.That(receivedObj.DateValue, Is.EqualTo(testObj.DateValue));
            Assert.That(receivedObj.StringValue, Is.EqualTo(testObj.StringValue));
            Assert.That(receivedObj.IntegerValue, Is.EqualTo(testObj.IntegerValue));
        }

        /// <summary>
        /// Should correctly replace existing listener.
        /// </summary>
        public void ShouldReplaceExistingListener()
        {
        }

        /// <summary>
        /// Test sending object.
        /// </summary>
        [Serializable]
        internal class TestObjectToSend
        {
            /// <summary>
            /// Gets or sets StringValue.
            /// </summary>
            public string StringValue { get; set; }

            /// <summary>
            /// Gets or sets IntegerValue.
            /// </summary>
            public int IntegerValue { get; set; }

            /// <summary>
            /// Gets or sets DateValue.
            /// </summary>
            public DateTime DateValue { get; set; }

            /// <summary>
            /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
            /// </summary>
            /// <returns>
            /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
            /// </returns>
            /// <filterpriority>2</filterpriority>
            public override string ToString()
            {
                return "TestObject";
            }
        }
    }
}
