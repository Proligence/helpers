﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MinimumFullHeapUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;
    using Proligence.Helpers.Collections;

    /// <summary>
    /// Implements unit tests for the <see cref="MinimumFullHeap{T}"/> class.
    /// </summary>
    [TestFixture]
    public class MinimumFullHeapUnitTests : FullHeapUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="MinimumFullHeap{T}"/> class throws a 
        /// <see cref="ArgumentNullException"/> when the specified items sequence is <c>null</c>.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Unit test.")]
        public void CreateHeapWhenItemsNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new MinimumFullHeap<int>(null));
            Assert.That(exception.ParamName, Is.EqualTo("items"));
        }

        /// <summary>
        /// Tests if a heap is correctly created from a list of items.
        /// </summary>
        [Test]
        public void CreateHeapFromList()
        {
            var heap = new MinimumFullHeap<int>(new List<int>(new[] { 1, 0, 2, 9, 3, 8, 4, 7, 5, 6 }));
            
            this.AssertItems(heap, 1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the minimum item can be read from a <see cref="MinimumFullHeap{T}"/>.
        /// </summary>
        [Test]
        public void GetMinimumItem()
        {
            var heap = new MinimumFullHeap<int>(new[] { 10, 7, 9, 5, 6, 8, 4, 0, 1, 3, -1, 2 });
            
            Assert.That(heap.Minimum, Is.EqualTo(-1));
            this.AssertItems(heap, 10, 7, 9, 5, 6, 8, 4, 0, 1, 3, -1, 2);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the minimum item can be extracted from a <see cref="MinimumFullHeap{T}"/>.
        /// </summary>
        [Test]
        public void ExtractMinimumItem()
        {
            var heap = new MinimumFullHeap<int>(new[] { -1, 0, 2, 5, 1, 8, 4, 7, 9, 6, 3, 10 });
            int min = heap.ExtractMinimum();

            Assert.That(min, Is.EqualTo(-1));
            this.AssertItems(heap, 0, 2, 5, 1, 8, 4, 7, 9, 6, 3, 10);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.DecreaseItem"/> method properly decreases the item if the
        /// specified value is less than the specified item.
        /// </summary>
        [Test]
        public void DecreaseItemWhenLessThanItem()
        {
            var heap = new MinimumFullHeap<int>(new[] { 0, 1, 2, 5, 3, 8, 4, 7, 9, 6, 10 });
            heap.DecreaseItem(5, -1);

            this.AssertItems(heap, 0, 1, 2, 5, 3, -1, 4, 7, 9, 6, 10);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.DecreaseItem"/> method has no effect if the specified value is
        /// equal to the specified item.
        /// </summary>
        [Test]
        public void DecreaseItemWhenEqualToItem()
        {
            var heap = new MinimumFullHeap<int>(new[] { 0, 1, 2, 5, 3, 8, 4, 7, 9, 6, 10 });
            heap.DecreaseItem(5, 5);

            this.AssertItems(heap, 0, 1, 2, 5, 3, 5, 4, 7, 9, 6, 10);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.DecreaseItem"/> method has no effect if the specified value is
        /// greater then the specified item.
        /// </summary>
        [Test]
        public void DecreaseItemWhenGreaterThanItem()
        {
            var heap = new MinimumFullHeap<int>(new[] { 0, 1, 2, 5, 3, 8, 4, 7, 9, 6, 10 });
            heap.DecreaseItem(5, 100);

            this.AssertItems(heap, 0, 1, 2, 5, 3, 8, 4, 7, 9, 6, 10);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.IndexOf"/> method correctly returns an item from the middle of
        /// the heap.
        /// </summary>
        [Test]
        public void IndexOf()
        {
            FullHeap<int> heap = this.CreateHeap(0, 2, 5, 3);
            int index = heap.IndexOf(2);

            Assert.That(index, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.IndexOf"/> method correctly returns the first item from the
        /// heap.
        /// </summary>
        [Test]
        public void IndexOfFirst()
        {
            FullHeap<int> heap = this.CreateHeap(3, 1, 2, 9);
            int index = heap.IndexOf(1);

            Assert.That(index, Is.EqualTo(0));
        }

        /// <summary>
        /// Tests if the <see cref="MinimumFullHeap{T}.IndexOf"/> method correctly returns the last item from the
        /// heap.
        /// </summary>
        [Test]
        public void IndexOfLast()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 9, 2);
            int index = heap.IndexOf(9);

            Assert.That(index, Is.EqualTo(2));
        }

        /// <summary>
        /// Creates an empty heap.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <returns>The created heap.</returns>
        protected override FullHeap<T> CreateEmptyHeap<T>()
        {
            return new MinimumFullHeap<T>();
        }

        /// <summary>
        /// Creates a heap which contains the specified items.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="items">The items to add to the created heap.</param>
        /// <returns>The created heap.</returns>
        protected override FullHeap<T> CreateHeap<T>(params T[] items)
        {
            return new MinimumFullHeap<T>(items);
        }

        /// <summary>
        /// Asserts that the heap's internal state is valid.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="heap">The heap instance.</param>
        protected override void AssertValidHeap<T>(FullHeap<T> heap)
        {
            if (heap == null)
            {
                throw new ArgumentNullException("heap");
            }

            T[] buffer = this.GetBuffer(heap);

            for (int i = 0; i < heap.Count; i++)
            {
                int childIndex1 = (i * 2) + 1;
                if (childIndex1 < heap.Count)
                {
                    Assert.That(buffer[i], Is.LessThanOrEqualTo(buffer[childIndex1]));
                }

                int childIndex2 = childIndex1 + 1;
                if (childIndex2 < heap.Count)
                {
                    Assert.That(buffer[i], Is.LessThanOrEqualTo(buffer[childIndex2]));
                }
            }
        }
    }
}