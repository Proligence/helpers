﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FullHeapUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Helpers.Collections;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements generic unit tests for <see cref="FullHeap{T}"/> subclasses.
    /// </summary>
    [TestFixture]
    public abstract class FullHeapUnitTests
    {
        /// <summary>
        /// Tests if an empty heap is created correctly.
        /// </summary>
        [Test]
        public void CreateEmptyHeap()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            
            this.AssertItems(heap, new int[0]);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if a heap is correctly created from an array of items.
        /// </summary>
        [Test]
        public void CreateHeapFromArray()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
            
            this.AssertItems(heap, 1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if items are correctly added to an empty heap.
        /// </summary>
        [Test]
        public void AddItemToEmptyHeap()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            heap.Add(5);

            this.AssertItems(heap, 5);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if items are correctly added to the heap.
        /// </summary>
        [Test]
        public void AddItemToHeap()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 10, 6);
            heap.Add(5);

            this.AssertItems(heap, 1, 0, 2, 9, 3, 8, 4, 7, 10, 6, 5);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if minimum items are correctly added to the heap.
        /// </summary>
        [Test]
        public void AddMinimumItemToHeap()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
            heap.Add(-1);

            this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 5, 6, -1);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if maximum items are correctly added to the heap.
        /// </summary>
        [Test]
        public void AddMaximumItemToHeap()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
            heap.Add(10);

            this.AssertItems(heap, 1, 0, 2, 9, 3, 8, 4, 7, 5, 6, 10);
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests if multiple items are correctly added to the heap.
        /// </summary>
        [Test]
        public void AddMultipleItemsToHeap()
        {
            var testData = new[]
            {
                new Tuple<int, int, int>(-1, 5, 10), 
                new Tuple<int, int, int>(10, 5, -1),
                new Tuple<int, int, int>(5, 10, -1),
                new Tuple<int, int, int>(-1, 10, 5), 
                new Tuple<int, int, int>(10, -1, 5), 
                new Tuple<int, int, int>(5, -1, 10), 
            };

            foreach (Tuple<int, int, int> tuple in testData)
            {
                FullHeap<int> heap = this.CreateHeap(1, 0, 2, 9, 3, 8, 4, 7, 5, 6);
                heap.Add(tuple.Item1);
                heap.Add(tuple.Item2);
                heap.Add(tuple.Item3);

                this.AssertItems(heap, 1, 0, 2, 9, 3, 8, 4, 7, 5, 6, tuple.Item1, tuple.Item2, tuple.Item3);
                this.AssertValidHeap(heap);
            }
        }

        /// <summary>
        /// Tests if duplicate items are correctly added to the heap.
        /// </summary>
        [Test]
        public void AddDuplicateItemsToHeap()
        {
            FullHeap<string> heap = this.CreateEmptyHeap<string>();
            heap.Add("foo");
            heap.Add("bar");
            heap.Add("foo");
            heap.Add("bar");
            heap.Add("bar");
            heap.Add("baz");

            this.AssertItems(heap, "bar", "bar", "bar", "baz", "foo", "foo");
            this.AssertValidHeap(heap);
        }

        /// <summary>
        /// Tests a <see cref="ArgumentNullException"/> is thrown if a <c>null</c> item is added to the heap.
        /// </summary>
        [Test]
        public void AddNullItemToHeap()
        {
            FullHeap<string> heap = this.CreateHeap("str");
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => heap.Add(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("item"));
        }

        /// <summary>
        /// Tests a <see cref="ArgumentNullException"/> is the <see cref="FullHeap{T}.Top"/> method is called on an
        /// empty heap.
        /// </summary>
        [Test]
        public void TopWhenHeapEmpty()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(() => heap.Top());
            
            Assert.That(exception.Message, Is.EqualTo("The heap is empty."));
        }

        /// <summary>
        /// Tests a <see cref="ArgumentNullException"/> is the <see cref="FullHeap{T}.Extract"/> method is called on
        /// an empty heap.
        /// </summary>
        [Test]
        public void ExtractWhenHeapEmpty()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            InvalidOperationException exception = Assert.Throws<InvalidOperationException>(() => heap.Extract());
            
            Assert.That(exception.Message, Is.EqualTo("The heap is empty."));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.IndexOf"/> method returns <c>-1</c> if the specified item is not in
        /// the heap.
        /// </summary>
        [Test]
        public void IndexOfNotExistingItem()
        {
            FullHeap<int> heap = this.CreateHeap(1, 0, 3);
            int index = heap.IndexOf(2);

            Assert.That(index, Is.EqualTo(-1));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.IndexOf"/> method returns <c>-1</c> if the heap is empty.
        /// </summary>
        [Test]
        public void IndexOfWhenEmptyHeap()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            int index = heap.IndexOf(1);

            Assert.That(index, Is.EqualTo(-1));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.IndexOf"/> method throws a <see cref="ArgumentNullException"/> if the
        /// specified item is <c>null</c>.
        /// </summary>
        [Test]
        public void IndexOfNull()
        {
            FullHeap<string> heap = this.CreateHeap("str");
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => heap.IndexOf(null));

            Assert.That(exception.ParamName, Is.EqualTo("item"));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.GetAllItems"/> method works correctly when the heap is empty.
        /// </summary>
        [Test]
        public void GetAllItemsWhenHeapEmpty()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            IEnumerable<int> result = heap.GetAllItems();

            Assert.That(result.Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.GetAllItems"/> method works correctly when the heap contains a single
        /// item.
        /// </summary>
        [Test]
        public void GetAllItemsWhenSingleItem()
        {
            FullHeap<int> heap = this.CreateHeap(1);
            IEnumerable<int> result = heap.GetAllItems();

            Assert.That(result.ToArray(), Is.EquivalentTo(new[] { 1 }));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.GetAllItems"/> method works correctly when the heap contains multiple
        /// items.
        /// </summary>
        [Test]
        public void GetAllItemsWhenMultipleItems()
        {
            FullHeap<int> heap = this.CreateHeap(1, 2, 3);
            IEnumerable<int> result = heap.GetAllItems();

            Assert.That(result.ToArray(), Is.EquivalentTo(new[] { 1, 2, 3 }));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>false</c> if the specified 
        /// argument is <c>null</c>.
        /// </summary>
        [Test]
        public void EqualsWhenNull()
        {
            FullHeap<int> heap = this.CreateHeap(1, 2, 3);
            
            Assert.That(heap.Equals(null), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>false</c> if the specified 
        /// argument's type is different than the heap's type.
        /// </summary>
        [Test]
        public void EqualsWhenOtherType()
        {
            FullHeap<int> heap = this.CreateHeap(1, 2, 3);
            
            Assert.That(heap.Equals("string"), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>false</c> if the specified heap
        /// has a different number of items.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentCount()
        {
            FullHeap<int> heap1 = this.CreateHeap(1, 2, 3);
            FullHeap<int> heap2 = this.CreateHeap(1, 2, 3, 4);
            
            Assert.That(heap1.Equals(heap2), Is.False);
            Assert.That(heap2.Equals(heap1), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>false</c> if the specified heap 
        /// has different items.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentItems()
        {
            FullHeap<int> heap1 = this.CreateHeap(1, 2, 3);
            FullHeap<int> heap2 = this.CreateHeap(1, 4, 3);
            
            Assert.That(heap1.Equals(heap2), Is.False);
            Assert.That(heap2.Equals(heap1), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>true</c> if the specified heap 
        /// is equal to the current heap.
        /// </summary>
        [Test]
        public void EqualsWhenEqual()
        {
            FullHeap<int> heap1 = this.CreateHeap(1, 2, 3);
            FullHeap<int> heap2 = this.CreateHeap(1, 2, 3);
            
            Assert.That(heap1.Equals(heap2), Is.True);
            Assert.That(heap2.Equals(heap1), Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.Equals(object)"/> method returns <c>true</c> if the specified heap 
        /// is has the same elements as the current heap, but in different order.
        /// </summary>
        [Test]
        public void EqualsWhenDifferentItemOrder()
        {
            FullHeap<int> heap1 = this.CreateHeap(1, 2, 3);
            FullHeap<int> heap2 = this.CreateHeap(3, 2, 1);
            
            Assert.That(heap1.Equals(heap2), Is.True);
            Assert.That(heap2.Equals(heap1), Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.GetHashCode"/> method works correctly when the heap is empty.
        /// </summary>
        [Test]
        public void GetHashCodeWhenEmpty()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();
            
            Assert.That(heap.GetHashCode(), Is.EqualTo(0));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.GetHashCode"/> method works correctly when the heap is not empty.
        /// </summary>
        [Test]
        public void GetHashCodeWhenNotEmpty()
        {
            FullHeap<int> heap = this.CreateHeap(1, 2, 3);
            
            Assert.That(heap.GetHashCode(), Is.EqualTo(1 ^ 2 ^ 3));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.ToString"/> method works correctly when the heap is empty.
        /// </summary>
        [Test]
        public void ToStringWhenEmpty()
        {
            FullHeap<int> heap = this.CreateEmptyHeap<int>();

            Assert.That(heap.ToString(), Is.EqualTo("(empty)"));
        }

        /// <summary>
        /// Tests if the <see cref="FullHeap{T}.ToString"/> method works correctly when the heap is not empty.
        /// </summary>
        [Test]
        public void ToStringWhenNotEmpty()
        {
            FullHeap<int> heap = this.CreateHeap(1, 2, 3);

            Assert.That(heap.ToString(), Is.EqualTo(heap.Top().ToString(CultureInfo.InvariantCulture)));
        }

        /// <summary>
        /// Creates an empty heap.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <returns>The created heap.</returns>
        protected abstract FullHeap<T> CreateEmptyHeap<T>() 
            where T : IComparable<T>;

        /// <summary>
        /// Creates a heap which contains the specified items.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="items">The items to add to the created heap.</param>
        /// <returns>The created heap.</returns>
        protected abstract FullHeap<T> CreateHeap<T>(params T[] items) 
            where T : IComparable<T>;

        /// <summary>
        /// Asserts that the heap's internal state is valid.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="heap">The heap instance.</param>
        protected abstract void AssertValidHeap<T>(FullHeap<T> heap) 
            where T : IComparable<T>;

        /// <summary>
        /// Asserts that the heap contains the specified items.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="heap">The heap instance.</param>
        /// <param name="expectedItems">Array of expected items.</param>
        protected void AssertItems<T>(FullHeap<T> heap, params T[] expectedItems)
            where T : IComparable<T>
        {
            if (heap == null)
            {
                throw new ArgumentNullException("heap");
            }

            if (expectedItems == null)
            {
                throw new ArgumentNullException("expectedItems");
            }

            Assert.That(heap.Count, Is.EqualTo(expectedItems.Length));
                
            Assert.That(
                this.GetBuffer(heap).Take(expectedItems.Length).ToArray(), 
                Is.EquivalentTo(expectedItems));
        }

        /// <summary>
        /// Gets the internal data buffer of the heap.
        /// </summary>
        /// <typeparam name="T">The type of items stored in the heap.</typeparam>
        /// <param name="heap">The heap instance.</param>
        /// <returns>The heap's buffer array.</returns>
        protected T[] GetBuffer<T>(FullHeap<T> heap) 
            where T : IComparable<T>
        {
            if (heap == null)
            {
                throw new ArgumentNullException("heap");
            }

            return heap.GetField<T[]>("buffer");
        }
    }
}