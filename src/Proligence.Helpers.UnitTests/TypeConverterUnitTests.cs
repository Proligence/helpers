﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeConverterUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.UnitTests
{
    using System;
    using System.Globalization;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="TypeConverter"/> class.
    /// </summary>
    [TestFixture]
    public class TypeConverterUnitTests
    {
        /// <summary>
        /// Test enumerated type.
        /// </summary>
        private enum TestEnum
        {
            /// <summary>
            /// Test value.
            /// </summary>
            MyValue = 2
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="string"/> to a
        /// <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertStringToString()
        {
            TestConversion("My string", "My string");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="string"/> to an
        /// <see cref="object"/>.
        /// </summary>
        [Test]
        public void ConvertStringToObject()
        {
            TestConversion("My string", (object)"My string");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="string"/> to an <see cref="int"/>.
        /// </summary>
        [Test]
        public void ConvertStringToInt()
        {
            TestConversion("5", 5);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="object"/> to an
        /// <see cref="object"/>.
        /// </summary>
        [Test]
        public void ConvertObjectToObject()
        {
            var obj = new object();
            
            TestConversion(obj, obj);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="object"/> to a
        /// <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertObjectToString()
        {
            TestConversion((object)"My string", "My string");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="object"/> to an
        /// <see cref="int"/>.
        /// </summary>
        [Test]
        public void ConvertObjectToInt()
        {
            TestConversion((object)5, 5);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="int"/> to an
        /// <see cref="object"/>.
        /// </summary>
        [Test]
        public void ConvertIntToObject()
        {
            TestConversion(5, (object)5);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="int"/> to a <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertIntToString()
        {
            TestConversion(5, "5");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="int"/> to a <see cref="long"/>.
        /// </summary>
        [Test]
        public void ConvertIntToLong()
        {
            TestConversion(5, 5L);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="int"/> to a
        /// <see cref="Nullable{Int32}"/>.
        /// </summary>
        [Test]
        public void ConvertIntToNullableInt()
        {
            TestConversion(5, (int?)5);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="Nullable{Int32}"/> to an
        /// <see cref="int"/>.
        /// </summary>
        [Test]
        public void ConvertNullableIntToInt()
        {
            TestConversion((int?)5, 5);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="bool"/> to an
        /// <see cref="object"/>.
        /// </summary>
        [Test]
        public void ConvertBoolToObject()
        {
            TestConversion(true, (object)true);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="bool"/> to a <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertBoolToString()
        {
            TestConversion(true, "True");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="string"/> to a <see cref="bool"/>.
        /// </summary>
        /// <param name="str">The tested boolean string.</param>
        [TestCase("TRUE")]
        [TestCase("True")]
        [TestCase("true")]
        public void ConvertStringToBool(string str)
        {
            TestConversion(str, true);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <c>null</c> value to an <see cref="object"/>.
        /// </summary>
        [Test]
        public void ConvertNullToObject()
        {
            TestConversion((string)null, (object)null);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <c>null</c> value to a <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertNullToString()
        {
            TestConversion((object)null, (string)null);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <c>null</c> value to an <see cref="int"/>.
        /// </summary>
        [Test]
        public void ConvertNullToInt()
        {
            TestConversion((object)null, 0);   
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts a <see cref="string"/> to an enumerated type.
        /// </summary>
        [Test]
        public void ConvertStringToEnum()
        {
            TestConversion("MyValue", TestEnum.MyValue);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an enumerated type to a <see cref="string"/>.
        /// </summary>
        [Test]
        public void ConvertEnumToString()
        {
            TestConversion(TestEnum.MyValue, "MyValue");
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an <see cref="int"/> to an enumerated type.
        /// </summary>
        [Test]
        public void ConvertIntToEnum()
        {
            TestConversion((int)TestEnum.MyValue, TestEnum.MyValue);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter"/> correctly converts an enumerated type to an <see cref="int"/>.
        /// </summary>
        [Test]
        public void ConvertEnumToInt()
        {
            TestConversion(TestEnum.MyValue, (int)TestEnum.MyValue);
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter.Convert(object,System.Type)"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified conversion type is <c>null</c>.
        /// </summary>
        [Test]
        public void ConvertWhenConversionTypeNull()
        {
            var converter = new TypeConverter();
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => converter.Convert("test", null));

            Assert.That(exception.ParamName, Is.EqualTo("conversionType"));
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter.ChangeType(object,System.Type)"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified conversion type is <c>null</c>.
        /// </summary>
        [Test]
        public void ChangeTypeWhenConversionTypeNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => TypeConverter.ChangeType("test", null));

            Assert.That(exception.ParamName, Is.EqualTo("conversionType"));
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter.ChangeType{TIn,TOut}(TIn, System.IFormatProvider)"/> method
        /// works correctly.
        /// </summary>
        [Test]
        public void ChangeTypeWhenFormatProviderSpecified()
        {
            int result = TypeConverter.ChangeType<string, int>("5", CultureInfo.InvariantCulture);

            Assert.That(result, Is.EqualTo(5));
        }

        /// <summary>
        /// Tests if the <see cref="TypeConverter.ChangeType(object, System.Type, System.IFormatProvider)"/> method
        /// works correctly.
        /// </summary>
        [Test]
        public void ChangeTypeNonGenericWhenFormatProviderSpecified()
        {
            object result = TypeConverter.ChangeType("5", typeof(int), CultureInfo.InvariantCulture);

            Assert.That(result, Is.EqualTo(5));
        }

        /// <summary>
        /// Tests if the specified value is properly converted by the <see cref="TypeConverter"/>.
        /// </summary>
        /// <typeparam name="TIn">The type of the input value.</typeparam>
        /// <typeparam name="TOut">The type of the output value.</typeparam>
        /// <param name="input">The input value.</param>
        /// <param name="expectedOutput">The expected output value.</param>
        private static void TestConversion<TIn, TOut>(TIn input, TOut expectedOutput)
        {
            var converter = new TypeConverter();

            Assert.That(converter.Convert<TIn, TOut>(input), Is.EqualTo(expectedOutput));
            Assert.That(converter.Convert(input, typeof(TOut)), Is.EqualTo(expectedOutput));
            Assert.That(TypeConverter.ChangeType<TIn, TOut>(input), Is.EqualTo(expectedOutput));
            Assert.That(TypeConverter.ChangeType(input, typeof(TOut)), Is.EqualTo(expectedOutput));
        }
    }
}