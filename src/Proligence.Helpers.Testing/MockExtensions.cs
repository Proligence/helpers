﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Testing
{
    using System;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements extension methods for the <see cref="Mock"/> class.
    /// </summary>
    public static class MockExtensions
    {
        /// <summary>
        /// Asserts that the <see cref="Mock.VerifyAll"/> method of the specified mock throws a
        /// <see cref="MockException"/>.
        /// </summary>
        /// <param name="mock">The mock to verify.</param>
        public static void AssertVerificationFailure(this Mock mock)
        {
            if (mock == null)
            {
                throw new ArgumentNullException("mock");
            }

            try
            {
                mock.VerifyAll();
                Assert.Fail("VerifyAll did not throw MockException.");
            }
            catch (MockException)
            {
            }
        }
    }
}