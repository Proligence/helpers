﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Arg.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Testing
{
    using System;
    using Moq;

    /// <summary>
    /// Implements helper methods for creating parameters used in <see cref="Moq"/> mocks.
    /// </summary>
    /// <typeparam name="T">The type of the parameter.</typeparam>
    public static class Arg<T>
    {
        /// <summary>
        /// Creates a parameter which can have any value.
        /// </summary>
        /// <returns>The mocked parameter value.</returns>
        public static T IsAny()
        {
            return Match.Create<T>(x => true);
        }

        /// <summary>
        /// Creates a parameter which can have any value of the specified subtype of <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T2">The type of allowed parameter values.</typeparam>
        /// <returns>The mocked parameter value.</returns>
        public static T IsAny<T2>()
            where T2 : T
        {
            return Match.Create<T>(x => x is T2);
        }

        /// <summary>
        /// Creates a parameter which can have any non-null value.
        /// </summary>
        /// <returns>The mocked parameter value.</returns>
        public static T IsNotNull()
        {
            return Match.Create<T>(x => !ReferenceEquals(x, null));
        }

        /// <summary>
        /// Creates a parameter which value has to specify the specified predicate.
        /// </summary>
        /// <param name="predicate">The delegate which calculates the predicate.</param>
        /// <returns>The mocked parameter value.</returns>
        public static T Is(Func<T, bool> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            return Match.Create<T>(x => predicate(x));
        }

        /// <summary>
        /// Creates a parameter which value has to be the same object as the specified instance.
        /// </summary>
        /// <param name="instance">The object of the allowed instance.</param>
        /// <returns>The mocked parameter value.</returns>
        public static T Is(T instance)
        {
            if (ReferenceEquals(instance, null))
            {
                throw new ArgumentNullException("instance");
            }

            return Match.Create<T>(x => ReferenceEquals(x, instance));
        }

        /// <summary>
        /// Creates a parameter which value has to be equal to the specified instance.
        /// </summary>
        /// <param name="instance">The object of the allowed instance.</param>
        /// <returns>The mocked parameter value.</returns>
        public static T IsEqual(T instance)
        {
            if (ReferenceEquals(instance, null))
            {
                throw new ArgumentNullException("instance");
            }

            return Match.Create<T>(x => !ReferenceEquals(x, null) && x.Equals(instance));
        }
    }
}