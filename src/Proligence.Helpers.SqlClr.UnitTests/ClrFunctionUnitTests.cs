﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrFunctionUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ClrFunction{T}"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Unit test fixture.")]
    public class ClrFunctionUnitTests
    {
        /// <summary>
        /// The tested <see cref="ClrFunction{T}"/> instance.
        /// </summary>
        private Mock<ClrFunction<string>> function;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.function = new Mock<ClrFunction<string>> { CallBase = true };

            this.function.Protected()
                .Setup<IDbConnection>("CreateContextConnection")
                .Returns(new Mock<IDbConnection>().Object);
        }

        /// <summary>
        /// Tests if the <see cref="ClrFunction{T}.Initialize"/> method is called before calculating the function's
        /// result.
        /// </summary>
        [Test]
        public void TestInitializeBeforeCalculatingResult()
        {
            bool initializeCalled = false;
            bool resultCalculated = false;
            
            this.function.Protected().Setup("Initialize").Callback(
                () =>
                {
                    Assert.That(initializeCalled, Is.False);
                    Assert.That(resultCalculated, Is.False);
                    initializeCalled = true;
                });

            this.function.Protected().Setup<string>("CalculateResult").Returns(
                () =>
                {
                    Assert.That(initializeCalled, Is.True);
                    Assert.That(resultCalculated, Is.False);
                    resultCalculated = true;
                    return "Test";
                });

            string result = this.function.Object.Execute();
            
            Assert.That(result, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ClrFunction{T}.Cleanup"/> method is called after calculating the function's result.
        /// </summary>
        [Test]
        public void TestCleanupAfterCalculatingResult()
        {
            bool resultCalculated = false;
            bool cleanupCalled = false;

            this.function.Protected().Setup<string>("CalculateResult").Returns(
                () =>
                {
                    Assert.That(resultCalculated, Is.False);
                    Assert.That(cleanupCalled, Is.False);
                    resultCalculated = true;
                    return "Test";
                });

            this.function.Protected().Setup("Cleanup").Callback(
                () =>
                {
                    Assert.That(resultCalculated, Is.True);
                    Assert.That(cleanupCalled, Is.False);
                    cleanupCalled = true;
                });

            string result = this.function.Object.Execute();
            
            Assert.That(result, Is.EqualTo("Test"));
            Assert.That(cleanupCalled, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ClrFunction{T}.Cleanup"/> method is called when an exception occurs while
        /// calculating the function's result.
        /// </summary>
        [Test]
        public void TestCleanupWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Sample exception message.");
            
            this.function.Protected().Setup<string>("CalculateResult").Throws(exception);

            bool cleanupCalled = false;
            this.function.Protected().Setup("Cleanup").Callback(
                () =>
                {
                    Assert.That(cleanupCalled, Is.False);
                    cleanupCalled = true;
                });

            InvalidOperationException thrownException = Assert.Throws<InvalidOperationException>(
                () => this.function.Object.Execute());
            
            Assert.That(thrownException, Is.SameAs(exception));
            Assert.That(cleanupCalled, Is.True);
        }
    }
}