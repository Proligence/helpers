﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlPipeProxyUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using NUnit.Framework;
    
    /// <summary>
    /// Implements unit tests for the <see cref="SqlPipeProxy"/> class.
    /// </summary>
    [TestFixture]
    public class SqlPipeProxyUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="SqlPipeProxy"/> class throws an
        /// <see cref="ArgumentNullException"/> when the specified pipe is <c>null</c>.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Unit test.")]
        public void CreateSqlPipeProxyWhenPipeNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new SqlPipeProxy(null));
            
            Assert.That(exception.ParamName, Is.EqualTo("pipe"));
        }
    }
}