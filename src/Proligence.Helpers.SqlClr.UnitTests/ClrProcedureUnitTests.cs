﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrProcedureUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ClrProcedure"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Unit test fixture.")]
    public class ClrProcedureUnitTests
    {
        /// <summary>
        /// The tested <see cref="ClrProcedure"/> instance.
        /// </summary>
        private Mock<ClrProcedure> procedure;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.procedure = new Mock<ClrProcedure>();

            this.procedure.Protected()
                .Setup<IDbConnection>("CreateContextConnection")
                .Returns(new Mock<IDbConnection>().Object);
        }

        /// <summary>
        /// Tests if the <see cref="ClrProcedure.Initialize"/> method is called before executing the procedure.
        /// </summary>
        [Test]
        public void TestInitializeBeforeExecutingProcedure()
        {
            bool initializeCalled = false;
            bool procedureExecuted = false;
            
            this.procedure.Protected().Setup("Initialize").Callback(
                () =>
                {
                    Assert.That(initializeCalled, Is.False);
                    Assert.That(procedureExecuted, Is.False);
                    initializeCalled = true;
                });

            this.procedure.Protected().Setup("ExecuteInternal").Callback(
                () =>
                {
                    Assert.That(initializeCalled, Is.True);
                    Assert.That(procedureExecuted, Is.False);
                    procedureExecuted = true;
                });

            this.procedure.Object.Execute();
            
            Assert.That(initializeCalled, Is.True);
            Assert.That(procedureExecuted, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ClrProcedure.Cleanup"/> method is called after executing the procedure.
        /// </summary>
        [Test]
        public void TestCleanupAfterExecutingProcedure()
        {
            bool procedureExecuted = false;
            bool cleanupCalled = false;

            this.procedure.Protected().Setup("ExecuteInternal").Callback(
                () =>
                {
                    Assert.That(procedureExecuted, Is.False);
                    Assert.That(cleanupCalled, Is.False);
                    procedureExecuted = true;
                });

            this.procedure.Protected().Setup("Cleanup").Callback(
                () =>
                {
                    Assert.That(procedureExecuted, Is.True);
                    Assert.That(cleanupCalled, Is.False);
                    cleanupCalled = true;
                });

            this.procedure.Object.Execute();
            
            Assert.That(procedureExecuted, Is.True);
            Assert.That(cleanupCalled, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ClrProcedure.Cleanup"/> method is called when an exception occurs while executing
        /// the procedure.
        /// </summary>
        [Test]
        public void TestCleanupWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Sample exception message.");
            
            this.procedure.Protected().Setup("ExecuteInternal").Throws(exception);

            bool cleanupCalled = false;
            this.procedure.Protected().Setup("Cleanup").Callback(
                () =>
                {
                    Assert.That(cleanupCalled, Is.False);
                    cleanupCalled = true;
                });

            InvalidOperationException thrownException = Assert.Throws<InvalidOperationException>(
                () => this.procedure.Object.Execute());
            
            Assert.That(thrownException, Is.SameAs(exception));
            Assert.That(cleanupCalled, Is.True);
        }
    }
}