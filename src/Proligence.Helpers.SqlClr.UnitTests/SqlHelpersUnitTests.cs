﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlHelpersUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="SqlHelpers"/> class.
    /// </summary>
    [TestFixture]
    public class SqlHelpersUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="SqlHelpers.SetupCommand"/> method works correctly.
        /// </summary>
        [Test]
        public void SetupCommand()
        {
            using (var command = new SqlCommand())
            {
                var parameters = new[]
                {
                    new SqlParameter("p1", "v1"), 
                    new SqlParameter("p2", "v2"), 
                    new SqlParameter("p3", "v3")
                };

                SqlHelpers.SetupCommand(command, "select * from dummy", CommandType.StoredProcedure, parameters);

                Assert.That(command.CommandText, Is.EqualTo("select * from dummy"));
                Assert.That(command.CommandType, Is.EqualTo(CommandType.StoredProcedure));
                Assert.That(command.Parameters.Cast<SqlParameter>().ToArray(), Is.EquivalentTo(parameters));
            }
        }

        /// <summary>
        /// Tests if the <see cref="SqlHelpers.SetupCommand"/> method works correctly when the specified parameters
        /// collection is <c>null</c> or empty.
        /// </summary>
        [Test]
        public void SetupCommandWithoutParameters()
        {
            var inputData = new[]
            {
                new SqlParameter[0],
                null
            };

            foreach (SqlParameter[] parameters in inputData)
            {
                using (var command = new SqlCommand())
                {
                    SqlHelpers.SetupCommand(command, "select * from dummy", CommandType.StoredProcedure, parameters);

                    Assert.That(command.CommandText, Is.EqualTo("select * from dummy"));
                    Assert.That(command.CommandType, Is.EqualTo(CommandType.StoredProcedure));
                    Assert.That(command.Parameters, Is.Empty);
                }
            }
        }

        /// <summary>
        /// Tests if the <see cref="SqlHelpers.SetupCommand"/> method throws an <see cref="ArgumentNullException"/>
        /// when the specified command is <c>null</c>.
        /// </summary>
        [Test]
        public void SetupCommandWhenCommandNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => SqlHelpers.SetupCommand(
                    null, "select * from dummy", CommandType.StoredProcedure, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("command"));
        }
    }
}