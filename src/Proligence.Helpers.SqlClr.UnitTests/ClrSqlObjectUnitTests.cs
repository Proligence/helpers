﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrSqlObjectUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="ClrSqlObject"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Unit test fixture.")]
    public class ClrSqlObjectUnitTests
    {
        /// <summary>
        /// The tested <see cref="ClrSqlObject"/> instance.
        /// </summary>
        private Mock<ClrSqlObject> clrSqlObject;

        /// <summary>
        /// The mock of the SQL data pipe associated with the tested <see cref="ClrSqlObject"/>.
        /// </summary>
        private Mock<ISqlPipe> pipeMock;

        /// <summary>
        /// The mock of the connection associated with the tested <see cref="ClrSqlObject"/>.
        /// </summary>
        private ConnectionMock connectionMock;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.clrSqlObject = new Mock<ClrSqlObject> { CallBase = true };
            this.connectionMock = new ConnectionMock();
            this.pipeMock = new Mock<ISqlPipe>();

            this.clrSqlObject.Protected()
                .Setup<IDbConnection>("CreateContextConnection")
                .Returns(this.connectionMock);

            this.clrSqlObject.Protected()
                .Setup<ISqlPipe>("CreateSqlPipe")
                .Returns(this.pipeMock.Object);
        }

        /// <summary>
        /// Tests the <see cref="ClrSqlObject.ContextConnection"/> property.
        /// </summary>
        [Test]
        public void TestContextConnection()
        {
            int callCount = 0;
            this.clrSqlObject.Protected()
                .Setup<IDbConnection>("CreateContextConnection")
                .Returns(() =>
                {
                    callCount++;
                    return this.connectionMock;
                });

            IDbConnection conn1 = this.clrSqlObject.Object.ContextConnection;
            IDbConnection conn2 = this.clrSqlObject.Object.ContextConnection;
            
            Assert.That(conn1, Is.SameAs(this.connectionMock));
            Assert.That(conn2, Is.SameAs(this.connectionMock));
            Assert.That(callCount, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests the <see cref="ClrSqlObject.Pipe"/> property.
        /// </summary>
        [Test]
        public void TestPipe()
        {
            ISqlPipe pipe = new Mock<ISqlPipe>().Object;

            int callCount = 0;
            this.clrSqlObject.Protected()
                .Setup<ISqlPipe>("CreateSqlPipe")
                .Returns(() =>
                {
                    callCount++;
                    return pipe;
                });

            ISqlPipe pipe1 = this.clrSqlObject.Object.Pipe;
            ISqlPipe pipe2 = this.clrSqlObject.Object.Pipe;
            
            Assert.That(pipe1, Is.SameAs(pipe));
            Assert.That(pipe2, Is.SameAs(pipe));
            Assert.That(callCount, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests the <see cref="ClrSqlObject.Dispose"/> method.
        /// </summary>
        [Test]
        public void TestDispose()
        {
            var obj = new ClrSqlObjectMock();            
            obj.Dispose();

            Assert.That(obj.IsDisposed, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="ClrSqlObject.Dispose(bool)"/> method.
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
        /// resources.
        /// </param>
        [TestCase(true)]
        [TestCase(false)]
        public void TestDisposeProtected(bool disposing)
        {
            using (var obj = new ClrSqlObjectMock(this.connectionMock))
            {
                obj.CallMethod("Dispose", disposing);

                Assert.That(obj.IsDisposed, Is.True);
                Assert.That(this.connectionMock.IsDisposed, Is.EqualTo(disposing));
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.Initialize"/> method opens the context connection with SQL server.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initially open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void TestInitializeOpensContextConnection(bool connOpen)
        {
            var f = new Mock<ClrSqlObject> { CallBase = true };
            f.Protected().Setup<IDbConnection>("CreateContextConnection").Returns(this.connectionMock);

            if (connOpen)
            {
                this.connectionMock.Open();
            }
            
            f.Object.CallMethod("Initialize");
            
            Assert.That(this.connectionMock.State, Is.EqualTo(ConnectionState.Open));
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.Cleanup"/> method closes the context connection with SQL server.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initially open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void TestCleanupClosesContextConnection(bool connOpen)
        {
            var f = new Mock<ClrSqlObject> { CallBase = true };
            f.Protected().Setup<IDbConnection>("CreateContextConnection").Returns(this.connectionMock);

            if (connOpen)
            {
                this.connectionMock.Open();
            }

            f.Object.CallMethod("Cleanup");
            
            Assert.That(this.connectionMock.State, Is.EqualTo(ConnectionState.Closed));
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteQueryToPipe"/> method works correctly when query parameters
        /// are not specified.
        /// </summary>
        [Test]
        public void ExecuteQueryToPipeWhenNoParametersSpecified()
        {
            SqlParameter[][] inputData = new[] { new SqlParameter[0], null };

            foreach (SqlParameter[] parameters in inputData)
            {
                using (var obj = new ClrSqlObjectMock())
                {
                    IDbCommand executedCommand = null;
                    obj.PipeMock.Setup(p => p.ExecuteAndSend(It.IsAny<IDbCommand>())).Callback<IDbCommand>(
                        cmd => executedCommand = cmd);

                    obj.InvokeExecuteQueryToPipe("dummy", parameters);

                    Assert.That(executedCommand, Is.Not.Null);
                    Assert.That(executedCommand.CommandText, Is.EqualTo("dummy"));
                    Assert.That(executedCommand.CommandType, Is.EqualTo(CommandType.Text));
                    Assert.That(executedCommand.Parameters, Is.Empty);
                }
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteQueryToPipe"/> method works correctly when query parameters
        /// are specified.
        /// </summary>
        [Test]
        public void ExecuteQueryToPipeWhenParametersSpecified()
        {
            var parameters = new[] { new SqlParameter("@p1", 1), new SqlParameter("@p2", 2) };
            
            using (var obj = new ClrSqlObjectMock())
            {
                IDbCommand executedCommand = null;
                obj.PipeMock.Setup(p => p.ExecuteAndSend(It.IsAny<IDbCommand>())).Callback<IDbCommand>(
                    cmd => executedCommand = cmd);

                obj.InvokeExecuteQueryToPipe("dummy", parameters);

                Assert.That(executedCommand, Is.Not.Null);
                Assert.That(executedCommand.CommandText, Is.EqualTo("dummy"));
                Assert.That(executedCommand.CommandType, Is.EqualTo(CommandType.Text));
                Assert.That(executedCommand.Parameters.Cast<SqlParameter>().ToArray(), Is.EquivalentTo(parameters));
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteQueryToPipe"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryToPipeWhenQueryNull()
        {
            using (var obj = new ClrSqlObjectMock())
            {
                ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                    () => obj.InvokeExecuteQueryToPipe(null));

                Assert.That(exception.ParamName, Is.EqualTo("query"));
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteProcedureToPipe"/> method works correctly when query parameters
        /// are not specified.
        /// </summary>
        [Test]
        public void ExecuteProcedureToPipeWhenNoParametersSpecified()
        {
            SqlParameter[][] inputData = new[] { new SqlParameter[0], null };

            foreach (SqlParameter[] parameters in inputData)
            {
                using (var obj = new ClrSqlObjectMock())
                {
                    IDbCommand executedCommand = null;
                    obj.PipeMock.Setup(p => p.ExecuteAndSend(It.IsAny<IDbCommand>())).Callback<IDbCommand>(
                        cmd => executedCommand = cmd);

                    obj.InvokeExecuteProcedureToPipe("dummy", parameters);

                    Assert.That(executedCommand, Is.Not.Null);
                    Assert.That(executedCommand.CommandText, Is.EqualTo("dummy"));
                    Assert.That(executedCommand.CommandType, Is.EqualTo(CommandType.StoredProcedure));
                    Assert.That(executedCommand.Parameters, Is.Empty);
                }
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteProcedureToPipe"/> method works correctly when query parameters
        /// are specified.
        /// </summary>
        [Test]
        public void ExecuteProcedureToPipeWhenParametersSpecified()
        {
            var parameters = new[] { new SqlParameter("@p1", 1), new SqlParameter("@p2", 2) };
            
            using (var obj = new ClrSqlObjectMock())
            {
                IDbCommand executedCommand = null;
                obj.PipeMock.Setup(p => p.ExecuteAndSend(It.IsAny<IDbCommand>())).Callback<IDbCommand>(
                    cmd => executedCommand = cmd);

                obj.InvokeExecuteProcedureToPipe("dummy", parameters);

                Assert.That(executedCommand, Is.Not.Null);
                Assert.That(executedCommand.CommandText, Is.EqualTo("dummy"));
                Assert.That(executedCommand.CommandType, Is.EqualTo(CommandType.StoredProcedure));
                Assert.That(executedCommand.Parameters.Cast<SqlParameter>().ToArray(), Is.EquivalentTo(parameters));
            }
        }

        /// <summary>
        /// Tests if the <see cref="ClrSqlObject.ExecuteProcedureToPipe"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified procedure name is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteProcedureToPipeWhenProcedureNameNull()
        {
            using (var obj = new ClrSqlObjectMock())
            {
                ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                    () => obj.InvokeExecuteProcedureToPipe(null));

                Assert.That(exception.ParamName, Is.EqualTo("procedureName"));
            }
        }

        /// <summary>
        /// Tests the <see cref="ClrSqlObject.CreateCommand"/> method.
        /// </summary>
        [Test]
        public void TestCreateCommand()
        {
            using (var connection = new SqlConnection("Data Source=dummy"))
            {
                IDbCommand command = this.clrSqlObject.Object.CallMethod<IDbCommand>("CreateCommand", connection);   

                Assert.That(command.CommandText, Is.EqualTo(string.Empty));
                Assert.That(command.Connection, Is.SameAs(connection));
            }
        }

        /// <summary>
        /// Implements a subclass of <see cref="ClrSqlObject"/> for testing purposes.
        /// </summary>
        private class ClrSqlObjectMock : ClrSqlObject
        {
            /// <summary>
            /// The context connection associated with the object.
            /// </summary>
            private readonly IDbConnection connection;

            /// <summary>
            /// Initializes a new instance of the <see cref="ClrSqlObjectMock"/> class.
            /// </summary>
            public ClrSqlObjectMock()
            {
                this.connection = new Mock<IDbConnection>().Object;
                this.PipeMock = new Mock<ISqlPipe>();
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="ClrSqlObjectMock"/> class.
            /// </summary>
            /// <param name="connection">The context connection associated with the object.</param>
            public ClrSqlObjectMock(IDbConnection connection)
            {
                this.connection = connection;
            }

            /// <summary>
            /// Gets the mock of the SQL data pipe assocaited with the object.
            /// </summary>
            public Mock<ISqlPipe> PipeMock { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the object is disposed.
            /// </summary>
            public bool IsDisposed { get; private set; }

            /// <summary>
            /// Executes an SQL query and sends its results to the SQL context pipe.
            /// </summary>
            /// <param name="query">The SQL query to execute.</param>
            /// <param name="parameters">The parameters for the specified SQL query.</param>
            public void InvokeExecuteQueryToPipe(string query, params SqlParameter[] parameters)
            {
                this.ExecuteQueryToPipe(query, parameters);
            }

            /// <summary>
            /// Executes an SQL procedure and sends its results to the SQL context pipe.
            /// </summary>
            /// <param name="procedureName">The name of the procedure to execute.</param>
            /// <param name="parameters">The parameters for the specified procedure.</param>
            public void InvokeExecuteProcedureToPipe(string procedureName, params SqlParameter[] parameters)
            {
                this.ExecuteProcedureToPipe(procedureName, parameters);
            }
            
            /// <summary>
            /// Creates a context SQL connection.
            /// </summary>
            /// <returns>The created connection.</returns>
            protected override IDbConnection CreateContextConnection()
            {
                return this.connection;
            }

            /// <summary>
            /// Creates an instance of the SQL data pipe.
            /// </summary>
            /// <returns>The created instance.</returns>
            protected override ISqlPipe CreateSqlPipe()
            {
                return this.PipeMock.Object;
            }

            /// <summary>
            /// Creates a new SQL command.
            /// </summary>
            /// <param name="conn">The connection to associate with the command.</param>
            /// <returns>The created command.</returns>
            protected override IDbCommand CreateCommand(IDbConnection conn)
            {
                return new CommandMock(new object[0][]);
            }

            /// <summary>
            /// Releases unmanaged and - optionally - managed resources
            /// </summary>
            /// <param name="disposing">
            /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
            /// resources.
            /// </param>
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                this.IsDisposed = true;
            }
        }
    }
}