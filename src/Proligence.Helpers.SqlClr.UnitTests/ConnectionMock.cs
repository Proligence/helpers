﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectionMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Mocks the <see cref="IDbConnection"/> interface.
    /// </summary>
    public class ConnectionMock : IDbConnection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionMock"/> class.
        /// </summary>
        public ConnectionMock()
        {
            this.State = ConnectionState.Closed;
            this.AllowClose = true;
        }

        /// <summary>
        /// Gets or sets the string used to open a database.
        /// </summary>
        /// <returns>A string containing connection settings.</returns>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the time to wait while trying to establish a connection before terminating the attempt and
        /// generating an error.
        /// </summary>
        /// <returns>The time (in seconds) to wait for a connection to open. The default value is 15 seconds.</returns>
        public int ConnectionTimeout { get; set; }

        /// <summary>
        /// Gets or sets the name of the current database or the database to be used after a connection is opened.
        /// </summary>
        /// <returns>
        /// The name of the current database or the name of the database to be used once a connection is open. The
        /// default value is an empty string.
        /// </returns>
        public string Database { get; set; }

        /// <summary>
        /// Gets or sets the current state of the connection.
        /// </summary>
        /// <returns>One of the <see cref="ConnectionState"/> values.</returns>
        public ConnectionState State { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the connection is open.
        /// </summary>
        /// <value><c>true</c> if the connection is open; otherwise, <c>false</c>.</value>
        public bool IsOpen
        {
            get
            {
                return this.State == ConnectionState.Open;
            }

            set
            {
                this.State = value ? ConnectionState.Open : ConnectionState.Closed;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the connection is allowed to be closed.
        /// </summary>
        /// <value><c>true</c> if the connection is allowed to be closed; otherwise, <c>false</c>.</value>
        public bool AllowClose { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the connection is disposed.
        /// </summary>
        public bool IsDisposed { get; set; }

        /// <summary>
        /// Opens a database connection with the settings specified by the ConnectionString property of the 
        /// provider-specific Connection object.
        /// </summary>
        public void Open()
        {
            if (this.State == ConnectionState.Open)
            {
                Assert.Fail("Attempt to open a connection which is already open.");
            }

            this.State = ConnectionState.Open;
        }

        /// <summary>
        /// Closes the connection to the database.
        /// </summary>
        public void Close()
        {
            if (this.State != ConnectionState.Open)
            {
                Assert.Fail("Attempt to close a connection which is not open.");
            }

            if (!this.AllowClose)
            {
                Assert.Fail("Connection is not allowed to be closed.");
            }

            this.State = ConnectionState.Closed;
        }

        /// <summary>
        /// Begins a database transaction.
        /// </summary>
        /// <returns>An object representing the new transaction.</returns>
        public IDbTransaction BeginTransaction()
        {
            return new Mock<IDbTransaction>().Object;
        }

        /// <summary>
        /// Begins a database transaction with the specified <see cref="T:System.Data.IsolationLevel"/> value.
        /// </summary>
        /// <param name="il">One of the <see cref="T:System.Data.IsolationLevel"/> values.</param>
        /// <returns>An object representing the new transaction.</returns>
        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return new Mock<IDbTransaction>().Object;
        }

        /// <summary>
        /// Changes the current database for an open Connection object.
        /// </summary>
        /// <param name="databaseName">The name of the database to use in place of the current database.</param>
        public void ChangeDatabase(string databaseName)
        {
        }

        /// <summary>
        /// Creates and returns a Command object associated with the connection.
        /// </summary>
        /// <returns>A Command object associated with the connection.</returns>
        public IDbCommand CreateCommand()
        {
            return new CommandMock(new object[0][]);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            this.IsDisposed = true;
        }
    }
}