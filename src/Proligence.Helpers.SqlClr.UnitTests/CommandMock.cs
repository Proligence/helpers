// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Moq;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Mocks the <see cref="IDbCommand"/> interface.
    /// </summary>
    internal class CommandMock : IDbCommand
    {
        /// <summary>
        /// Mocked results.
        /// </summary>
        private readonly object[][] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandMock"/> class.
        /// </summary>
        /// <param name="results">The mocked results which will be returned by the command.</param>
        public CommandMock(object[][] results)
        {
            this.results = results;
            this.Parameters = typeof(SqlParameterCollection).CreateInstance<SqlParameterCollection>();
        }

        /// <summary>
        /// Gets or sets the handler which is called when the command is executed.
        /// </summary>
        public Action<CommandMock> ExecuteHandler { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="IDbConnection"/> used by this instance of the <see cref="IDbCommand"/>.
        /// </summary>
        public IDbConnection Connection { get; set; }

        /// <summary>
        /// Gets or sets the transaction within which the Command object of a .NET Framework data provider executes.
        /// </summary>
        public IDbTransaction Transaction { get; set; }

        /// <summary>
        /// Gets or sets the text command to run against the data source.
        /// </summary>
        public string CommandText { get; set; }

        /// <summary>
        /// Gets or sets the wait time before terminating the attempt to execute a command and generating an error.
        /// </summary>
        public int CommandTimeout { get; set; }

        /// <summary>
        /// Gets or sets a value which indicates or specifies how the <see cref="IDbCommand.CommandText"/> property is
        /// interpreted.
        /// </summary>
        public CommandType CommandType { get; set; }

        /// <summary>
        /// Gets the <see cref="IDataParameterCollection"/>.
        /// </summary>
        public IDataParameterCollection Parameters { get; private set; }

        /// <summary>
        /// Gets or sets how command results are applied to the <see cref="DataRow"/> when used by the
        /// <see cref="IDataAdapter.Update(DataSet)"/> method of a <see cref="DbDataAdapter"/>.
        /// </summary>
        public UpdateRowSource UpdatedRowSource { get; set; }

        /// <summary>
        /// Creates a prepared (or compiled) version of the command on the data source.
        /// </summary>
        public void Prepare()
        {
        }

        /// <summary>
        /// Attempts to cancels the execution of an <see cref="IDbCommand"/>.
        /// </summary>
        public void Cancel()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new instance of an <see cref="IDbDataParameter"/> object.
        /// </summary>
        /// <returns>An <see cref="IDbDataParameter"/> object.</returns>
        public IDbDataParameter CreateParameter()
        {
            return new Mock<IDbDataParameter>().Object;
        }

        /// <summary>
        /// Executes an SQL statement against the Connection object of a .NET Framework data provider, and returns
        /// the number of rows affected.
        /// </summary>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery()
        {
            this.AssertConnectionOpen();
            
            if (this.ExecuteHandler != null)
            {
                this.ExecuteHandler(this);
            }

            if (this.results != null)
            {
                return this.results.Length;
            }

            return 0;
        }

        /// <summary>
        /// Executes the <see cref="IDbCommand.CommandText"/> against the <see cref="IDbCommand.Connection"/> and
        /// builds an <see cref="IDataReader"/>.
        /// </summary>
        /// <returns>An <see cref="T:System.Data.IDataReader"/> object.</returns>
        public IDataReader ExecuteReader()
        {
            this.AssertConnectionOpen();
            
            if (this.ExecuteHandler != null)
            {
                this.ExecuteHandler(this);
            }

            return this.CreateDataReader();
        }

        /// <summary>
        /// Executes the <see cref="IDbCommand.CommandText"/> against the <see cref="IDbCommand.Connection"/>, and
        /// builds an <see cref="IDataReader"/> using one of the <see cref="CommandBehavior"/> values.
        /// </summary>
        /// <param name="behavior">One of the <see cref="T:System.Data.CommandBehavior"/> values.</param>
        /// <returns>An <see cref="T:System.Data.IDataReader"/> object.</returns>
        public IDataReader ExecuteReader(CommandBehavior behavior)
        {
            return this.ExecuteReader();
        }

        /// <summary>
        /// Executes the query, and returns the first column of the first row in the resultset returned by the query.
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <returns>The first column of the first row in the resultset.</returns>
        public object ExecuteScalar()
        {
            this.AssertConnectionOpen();

            if (this.ExecuteHandler != null)
            {
                this.ExecuteHandler(this);
            }

            if (this.results.Length > 0)
            {
                if (this.results[0].Length > 0)
                {
                    return this.results[0][0];
                }
            }

            return null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Calls <see cref="Assert.Fail(string,object[])"/> if the connection is not open.
        /// </summary>
        private void AssertConnectionOpen()
        {
            if (this.Connection.State != ConnectionState.Open)
            {
                Assert.Fail("Connection is closed.");
            }
        }

        /// <summary>
        /// Creates a mocked <see cref="IDataReader"/> object which returns the data from the <see cref="results"/>
        /// variable.
        /// </summary>
        /// <returns>The created <see cref="IDataReader"/> object.</returns>
        private IDataReader CreateDataReader()
        {
            return new DataReaderMock(this.results);
        }
    }
}