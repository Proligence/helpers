﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextConnectionExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ContextConnectionExtensions"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable",
        Justification = "Test fixture.")]
    public class ContextConnectionExtensionsUnitTests
    {
        /// <summary>
        /// The <see cref="IContextConnection"/> mock.
        /// </summary>
        private Mock<IContextConnection> contextMock;

        /// <summary>
        /// The <see cref="IDbConnection"/> mock.
        /// </summary>
        private ConnectionMock connectionMock;

        /// <summary>
        /// The <see cref="IDbCommand"/> mock.
        /// </summary>
        private CommandMock commandMock;

        /// <summary>
        /// The sample results returned by <see cref="commandMock"/>.
        /// </summary>
        private object[][] sampleResults;

        /// <summary>
        /// Sample query/procedure parameters.
        /// </summary>
        private SqlParameter[] sampleParameters;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.connectionMock = new ConnectionMock();
            
            this.contextMock = new Mock<IContextConnection>();
            this.contextMock.SetupGet(c => c.ContextConnection).Returns(this.connectionMock);

            this.sampleResults = new[]
            {
                new object[] { 1, 2, 3 }, 
                new object[] { 4, 5, 6 }, 
                new object[] { 7, 8, 9 }
            };

            this.commandMock = new CommandMock(this.sampleResults);

            ContextConnectionExtensions.CreateCommand = connection =>
            {
                this.commandMock.Connection = connection;
                return this.commandMock;
            };

            this.sampleParameters = new[] { new SqlParameter("@p1", 1), new SqlParameter("@p2", 2) };
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteNonQuery"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteNonQuery(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteNonQuery("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteNonQuery"/> method works correctly when
        /// query parameters are specified.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteNonQueryWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler = 
                cmd => AssertCommand(cmd, "dummy", CommandType.Text, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteNonQuery("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteNonQuery"/> method works correctly when the
        /// specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteNonQueryWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);

            int result = this.contextMock.Object.ExecuteNonQuery("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteNonQuery"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified context is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteNonQueryWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteNonQuery(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteNonQuery"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteNonQueryWhenQueryNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteNonQuery(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalar(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            object result = this.contextMock.Object.ExecuteScalar("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar"/> method works correctly when query
        /// parameters are specified.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler = 
                cmd => AssertCommand(cmd, "dummy", CommandType.Text, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            object result = this.contextMock.Object.ExecuteScalar("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar"/> method works correctly when the
        /// specifified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);

            object result = this.contextMock.Object.ExecuteScalar("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteScalar(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarWhenQueryNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteScalar(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar{T}"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarGeneric(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteScalar<int>("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar{T}"/> method works correctly when query
        /// parameters are specified.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarGenericWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler =
                cmd => AssertCommand(cmd, "dummy", CommandType.Text, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteScalar<int>("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar{T}"/> method works correctly when the
        /// specifified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarGenericWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);

            int result = this.contextMock.Object.ExecuteScalar<int>("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar{T}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarGenericWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteScalar<int>(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalar{T}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarGenericWhenQueryNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteScalar<int>(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQuery"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteQuery(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            IEnumerable<object[]> results = this.contextMock.Object.ExecuteQuery("dummy");
            
            this.AssertQueryResult(results, this.sampleResults);
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQuery"/> method works correctly when query
        /// parameters are specified.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteQueryWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler =
                cmd => AssertCommand(cmd, "dummy", CommandType.Text, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            IEnumerable<object[]> results = this.contextMock.Object.ExecuteQuery("dummy", this.sampleParameters);
            
            this.AssertQueryResult(results, this.sampleResults);
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQuery"/> method works correctly when the
        /// specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.Text);

            IEnumerable<object[]> results = this.contextMock.Object.ExecuteQuery("dummy", null);
            
            this.AssertQueryResult(results, this.sampleResults);
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQuery"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteQuery(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQuery"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified query is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryWhenQueryNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteQuery(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("query"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteProcedure"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteProcedure(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteProcedure("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteProcedure"/> method works correctly when
        /// parameters are specified for the procedure.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteProcedureWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler =
                cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteProcedure("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteProcedure"/> method works correctly when
        /// the specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteProcedureWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);

            int result = this.contextMock.Object.ExecuteProcedure("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults.Length));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteProcedureWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteProcedure(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified procedure name is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteProcedureWhenProcedureNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteProcedure(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("procedureName"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarProcedure(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            object result = this.contextMock.Object.ExecuteScalarProcedure("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure"/> method works correctly when
        /// parameters are specified for the procedure.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarProcedureWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler = 
                cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            object result = this.contextMock.Object.ExecuteScalarProcedure("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure"/> method works correctly when
        /// the specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);

            object result = this.contextMock.Object.ExecuteScalarProcedure("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteScalarProcedure(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified procedure name is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureWhenProcedureNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteScalarProcedure(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("procedureName"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure{T}"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarProcedureGeneric(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteScalarProcedure<int>("dummy");

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure{T}"/> method works correctly
        /// when parameters are specified for the procedure.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteScalarProcedureGenericWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler =
                cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            int result = this.contextMock.Object.ExecuteScalarProcedure<int>("dummy", this.sampleParameters);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure{T}"/> method works correctly
        /// when the specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureGenericWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);
            
            int result = this.contextMock.Object.ExecuteScalarProcedure<int>("dummy", null);

            Assert.That(result, Is.EqualTo(this.sampleResults[0][0]));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure{T}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureGenericWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteScalarProcedure<int>(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteScalarProcedure{T}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified procedure name is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteScalarProcedureGenericWhenProcedureNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteScalarProcedure<int>(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("procedureName"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQueryProcedure"/> method works correctly.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteQueryProcedure(bool connOpen)
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            IEnumerable<object[]> results = this.contextMock.Object.ExecuteQueryProcedure("dummy");
            
            this.AssertQueryResult(results, this.sampleResults);
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQueryProcedure"/> method works correctly when
        /// parameters are specified for the procedure.
        /// </summary>
        /// <param name="connOpen">Indicates whether the connection is initally open.</param>
        [TestCase(true)]
        [TestCase(false)]
        public void ExecuteQueryProcedureWhenParametersSpecified(bool connOpen)
        {
            this.commandMock.ExecuteHandler =
                cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure, this.sampleParameters);
            this.connectionMock.IsOpen = connOpen;
            this.connectionMock.AllowClose = !connOpen;

            IEnumerable<object[]> results = 
                this.contextMock.Object.ExecuteQueryProcedure("dummy", this.sampleParameters);
            
            this.AssertQueryResult(results, this.sampleResults);
            Assert.That(this.connectionMock.IsOpen, Is.EqualTo(connOpen));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQueryProcedure"/> method works correctly when
        /// the specified parameters collection is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryProcedureWhenParametersNull()
        {
            this.commandMock.ExecuteHandler = cmd => AssertCommand(cmd, "dummy", CommandType.StoredProcedure);

            IEnumerable<object[]> results = this.contextMock.Object.ExecuteQueryProcedure("dummy", null);
            
            this.AssertQueryResult(results, this.sampleResults);
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQueryProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryProcedureWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.ExecuteQueryProcedure(null, "dummy", new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.ExecuteQueryProcedure"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified procedure name is <c>null</c>.
        /// </summary>
        [Test]
        public void ExecuteQueryProcedureWhenProcedureNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.ExecuteQueryProcedure(null, new SqlParameter[0]));

            Assert.That(exception.ParamName, Is.EqualTo("procedureName"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.UsingConnection{TResult}"/> method works correctly
        /// when the specified connection is closed.
        /// </summary>
        [Test]
        public void UsingConnectionWhenConnectionClosed()
        {
            this.connectionMock.IsOpen = false;

            string result = this.contextMock.Object.UsingConnection(
                connection =>
                {
                    Assert.That(connection, Is.SameAs(this.connectionMock));
                    Assert.That(this.connectionMock.IsOpen, Is.True);
                    return "Test";
                });

            Assert.That(result, Is.EqualTo("Test"));
            Assert.That(this.connectionMock.IsOpen, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.UsingConnection{TResult}"/> method works correctly
        /// when the specified connection is open.
        /// </summary>
        [Test]
        public void UsingConnectionWhenConnectionOpen()
        {
            this.connectionMock.IsOpen = true;
            this.connectionMock.AllowClose = false;
            
            string result = this.contextMock.Object.UsingConnection(
                connection =>
                {
                    Assert.That(connection, Is.SameAs(connectionMock));
                    return "Test";
                });

            Assert.That(result, Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.UsingConnection{TResult}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IContextConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void UsingConnectionWhenContextNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => ContextConnectionExtensions.UsingConnection(null, c => 0));

            Assert.That(exception.ParamName, Is.EqualTo("context"));
        }

        /// <summary>
        /// Tests if the <see cref="ContextConnectionExtensions.UsingConnection{TResult}"/> method throws a
        /// <see cref="ArgumentNullException"/> when the specified delegate is <c>null</c>.
        /// </summary>
        [Test]
        public void UsingConnectionWhenFuncNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.contextMock.Object.UsingConnection<int>(null));

            Assert.That(exception.ParamName, Is.EqualTo("func"));
        }

        /// <summary>
        /// Asserts the specified <see cref="IDbCommand"/>.
        /// </summary>
        /// <param name="command">The command to assert.</param>
        /// <param name="commandText">The expected command text.</param>
        /// <param name="commandType">The expected command type.</param>
        /// <param name="parameters">The expected parameters.</param>
        private static void AssertCommand(
            IDbCommand command, 
            string commandText, 
            CommandType commandType, 
            IEnumerable<SqlParameter> parameters = null)
        {
            if (parameters == null)
            {
                parameters = new SqlParameter[0];
            }

            Assert.That(command.CommandText, Is.EqualTo(commandText));
            Assert.That(command.CommandType, Is.EqualTo(commandType));
            Assert.That(command.Parameters.Cast<SqlParameter>().ToArray(), Is.EquivalentTo(parameters));
        }

        /// <summary>
        /// Asserts the data returned by the specified iterator.
        /// </summary>
        /// <param name="results">The iterator to assert.</param>
        /// <param name="expected">The expected data.</param>
        private void AssertQueryResult(IEnumerable<object[]> results, object[][] expected)
        {
            // ReSharper disable PossibleMultipleEnumeration
            IEnumerator<object[]> enumerator = results.GetEnumerator();
            // ReSharper restore PossibleMultipleEnumeration            
            for (int i = 0; i < expected.Length; i++)
            {
                object[] row = expected[i];
                Assert.That(enumerator.MoveNext(), Is.True);
                Assert.That(enumerator.Current.Length, Is.EqualTo(row.Length));
                
                // Connection should be open while results are enumerated.
                Assert.That(this.connectionMock.IsOpen, Is.True);

                for (int j = 0; j < row.Length; j++)
                {
                    Assert.That(enumerator.Current[j], Is.EqualTo(row[j]));
                }
            }

            Assert.That(enumerator.MoveNext(), Is.False);
        }
    }
}