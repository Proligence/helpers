﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SessionFactoryCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using FluentNHibernate.Cfg;
    using global::NHibernate;

    /// <summary>
    /// Builds and caches the <see cref="ISessionFactory"/> instance used for accessing a single database using 
    /// NHibernate.
    /// </summary>
    public abstract class SessionFactoryCache : IDisposable
    {
        /// <summary>
        /// The cached <see cref="ISessionFactory"/> object.
        /// </summary>
        private ISessionFactory sessionFactory;

        /// <summary>
        /// Gets the NHibernate session factory.
        /// </summary>
        public ISessionFactory SessionFactory
        {
            get
            {
                if (this.sessionFactory == null)
                {
                    this.sessionFactory = this.Configuration.BuildSessionFactory();
                }

                return this.sessionFactory;
            }
        }

        /// <summary>
        /// Gets the configuration of the NHibernate session factory.
        /// </summary>
        protected abstract FluentConfiguration Configuration { get; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.sessionFactory != null)
                {
                    this.sessionFactory.Dispose();
                }
            }
        }
    }
}