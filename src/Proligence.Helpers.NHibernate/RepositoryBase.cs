﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Security.Principal;
    using System.Threading;
    using global::NHibernate;
    using global::NHibernate.Criterion;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;
    using Proligence.Helpers.Reflection.DataAnnotations;
    using Expression = System.Linq.Expressions.Expression;

    /// <summary>
    /// The base class for classes which implement NHibernate data repositories.
    /// </summary>
    public abstract class RepositoryBase : NHibernateSessionHost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        protected RepositoryBase(SessionFactoryCache factoryCache, IDbConnection connection)
            : base(factoryCache, connection)
        {
        }

        /// <summary>
        /// Gets the object which caches the NHibernate session factory.
        /// </summary>
        public new SessionFactoryCache FactoryCache
        {
            get { return base.FactoryCache; }
        }

        /// <summary>
        /// Gets the connection to the SQL database.
        /// </summary>
        public new IDbConnection Connection
        {
            get { return base.Connection; }
        }
    }

    /// <summary>
    /// The base class for classes which implement NHibernate data repositories which support versioning.
    /// </summary>
    /// <typeparam name="TObject">The type of the objects stored in the repository.</typeparam>
    /// <typeparam name="TData">The type which represents the object's data in the database.</typeparam>
    /// <typeparam name="TId">The type of the identifier of the objects stored in the repository.</typeparam>
    [SuppressMessage("Microsoft.StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass",
        Justification = "Generic and non-generic class with the same name.")]
    public abstract class RepositoryBase<TObject, TData, TId> : RepositoryBase, IRepository<TObject, TId>
        where TObject : class, IIdentifiable<TId>
        where TData : class
    {
        /// <summary>
        /// The <see cref="PropertyInfo"/> objects of the properties of the <see cref="TData"/> which will be
        /// compared by the <see cref="CompareObjects"/> method.
        /// </summary>
        private readonly IEnumerable<PropertyInfo> dataObjectProperties;

        /// <summary>
        /// The <see cref="PropertyInfo"/> for the <see cref="IAuditable.ModifiedBy"/> property of the
        /// <typeparamref name="TData"/> type or <c>null</c>.
        /// </summary>
        private readonly PropertyInfo modifiedByProperty;

        /// <summary>
        /// The <see cref="PropertyInfo"/> for the <see cref="IAuditable.ModifiedAt"/> property of the
        /// <typeparamref name="TData"/> type or <c>null</c>.
        /// </summary>
        private readonly PropertyInfo modifiedAtProperty;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{TObject,TData,TId}" /> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        /// <param name="dataObjectFactory">An object used to create instances of <see cref="TData"/> type.</param>
        /// <param name="domainObjectFactory">An object used to create instances of <see cref="TObject"/> type.</param>
        /// <param name="dataObjectMapper">The data object mapper.</param>
        /// <param name="domainObjectMapper">The domain object mapper.</param>
        protected RepositoryBase(
            SessionFactoryCache factoryCache,
            IDbConnection connection,
            IObjectFactory<TData> dataObjectFactory,
            IObjectFactory<TObject> domainObjectFactory,
            IObjectMapper<TObject, TData> dataObjectMapper,
            IObjectMapper<TData, TObject> domainObjectMapper)
            : base(factoryCache, connection)
        {
            this.DataObjectFactory = dataObjectFactory;
            this.DomainObjectFactory = domainObjectFactory;
            this.DataObjectMapper = dataObjectMapper;
            this.DomainObjectMapper = domainObjectMapper;

            this.DataAnnotationValidator = new DataAnnotationValidator();

            // We will ignore NHibernate objects during validation, because they cause trouble and they will never
            // have data annotations.
            this.DataAnnotationValidator.IgnoredAssemblies.Add(typeof(ISession).Assembly);

            // Get and cache the TData properties which will be compared by CompareObjects method.
            this.dataObjectProperties = typeof(TData).GetProperties().Where(
                p => p.CanRead && p.GetIndexParameters().Length == 0);

            if (typeof(TData).GetInterfaces().Any(i => i == typeof(ICloneable)))
            {
                this.DataObjectCloner = x => (TData)((ICloneable)x).Clone();
            }
            else
            {
                // Dynamically generate DataObjectCloner delegate to call object.MemberwiseClone
                var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
                MethodInfo memberwiseCloneMethod = typeof(object).GetMethod("MemberwiseClone", bindingFlags);
                ParameterExpression obj = Expression.Parameter(typeof(TData));
                var callExpression = Expression.TypeAs(Expression.Call(obj, memberwiseCloneMethod), typeof(TData));
                var lambdaExpression = Expression.Lambda<Func<TData, TData>>(callExpression, obj);
                this.DataObjectCloner = lambdaExpression.Compile();
            }

            // Get properties of the IAuditable interface, so we can set them dynamically. We cannot use the interface
            // statically, because the interface provides only property getters!
            if (typeof(TData).GetInterface(typeof(IAuditable).Name) != null)
            {
                this.modifiedByProperty = typeof(TData).GetProperty("ModifiedBy");
                if (this.modifiedByProperty != null && !this.modifiedByProperty.CanWrite)
                {
                    this.modifiedByProperty = null;
                }

                this.modifiedAtProperty = typeof(TData).GetProperty("ModifiedAt");
                if (this.modifiedAtProperty != null && !this.modifiedAtProperty.CanWrite)
                {
                    this.modifiedAtProperty = null;
                }
            }
        }

        /// <summary>
        /// Gets the object used to create new instances of the <see cref="TData"/> type.
        /// </summary>
        protected IObjectFactory<TData> DataObjectFactory { get; private set; }

        /// <summary>
        /// Gets the domain object factory.
        /// </summary>
        protected IObjectFactory<TObject> DomainObjectFactory { get; private set; }

        /// <summary>
        /// Gets the data object mapper.
        /// </summary>
        protected IObjectMapper<TObject, TData> DataObjectMapper { get; private set; }

        /// <summary>
        /// Gets the domain object mapper.
        /// </summary>
        protected IObjectMapper<TData, TObject> DomainObjectMapper { get; private set; }

        /// <summary>
        /// Gets the object for validating data annotation restrictions on data objects.
        /// </summary>
        protected DataAnnotationValidator DataAnnotationValidator { get; private set; }

        /// <summary>
        /// Gets or sets the delegate which will be used to shallow-clone data objects.
        /// </summary>
        protected Func<TData, TData> DataObjectCloner { get; set; }

        /// <summary>
        /// Saves the specified object in the repository.
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        public virtual void Save(TObject obj, ISession session, ITransaction transaction)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            this.UsingSession(
                (s, t) =>
                {
                    TData dataObject;
                    if (!this.CompareIds(obj.Id, default(TId)))
                    {
                        dataObject = this.GetCurrentDataObject(obj.Id, s, t);
                        if (dataObject == null)
                        {
                            throw new InvalidOperationException("The specified identifier is invalid.");
                        }

                        TData currentData = this.DataObjectCloner(dataObject);
                        this.MapDataToDataObject(obj, dataObject);
                        
                        // HACK: Clear the session after cloning objects to avoid clashes with objects in NH cache.
                        s.Clear();

                        if (this.CompareObjects(dataObject, currentData, obj))
                        {
                            // The data of the specified object is up-to-date, so we don't need to do any update.
                            s.Evict(dataObject);
                            return obj;
                        }
                    }
                    else
                    {
                        dataObject = this.ConvertDomainObjectToDataObject(obj);
                    }

                    this.OnSaving(obj, dataObject, s);
                    this.SaveAuditData(dataObject);
                    this.ValidateObjectForSave(dataObject);
                    this.SaveOrUpdate(s, dataObject);
                    this.OnSaved(obj, dataObject, s);
                    s.Flush();

                    this.MapDataToDomainObject(dataObject, obj);
                    
                    return obj;
                },
                session,
                transaction);
        }

        /// <summary>
        /// Saves the specified object in the repository.
        /// </summary>
        /// <param name="obj">The object to save.</param>
        public virtual void Save(TObject obj)
        {
            this.Save(obj, null, null);
        }

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        public virtual void Delete(TObject obj, ISession session, ITransaction transaction)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            this.UsingSession(
                (s, t) =>
                {
                    TData dataObject = this.GetCurrentDataObject(obj.Id, s, t);
                    if (dataObject != null)
                    {
                        s.Delete(dataObject);
                    }
                },
                session,
                transaction);
        }

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        public virtual void Delete(TObject obj)
        {
            this.Delete(obj, null, null);
        }

        /// <summary>
        /// Gets an object from the repository with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object to get from the repository.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier.
        /// </returns>
        public virtual TObject GetById(TId objId, ISession session, ITransaction transaction)
        {
            TData data = this.UsingSession(
                (s, t) =>
                {
                    TData dataObject = this.GetCurrentDataObject(objId, s, t);
                    if (dataObject == null)
                    {
                        return null;
                    }
                    
                    return dataObject;
                },
                session,
                transaction);

            if (data == null)
            {
                return null;
            }

            TObject result = this.DomainObjectFactory.Create();
            this.DomainObjectMapper.MapProperties(data, result);
            this.OnRetrieved(result, data);

            return result;
        }

        /// <summary>
        /// Gets an object from the repository with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object to get from the repository.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier.
        /// </returns>
        public virtual TObject GetById(TId objId)
        {
            return this.GetById(objId, null, null);
        }

        /// <summary>
        /// Gets the objects from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The identifiers of the objects to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>The domain objects with the specified identifiers.</returns>
        public virtual IEnumerable<TObject> GetMany(
            IEnumerable<TId> ids,
            ISession session,
            ITransaction transaction)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }

            return this.UsingSession(
                (s, t) =>
                {
                    IList<TData> dataResults = s.CreateCriteria<TData>()
                        .Add(Restrictions.In("Id", ids.ToArray()))
                        .List<TData>();

                    return dataResults.Select(data => this.ToDomainObject(data, s)).ToArray();
                }, 
                session, 
                transaction);
        }

        /// <summary>
        /// Gets the objects from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The identifiers of the objects to get.</param>
        /// <returns>The domain objects with the specified identifiers.</returns>
        public virtual IEnumerable<TObject> GetMany(IEnumerable<TId> ids)
        {
            return this.GetMany(ids, null, null);
        }

        /// <summary>
        /// Gets all object from the repository.
        /// </summary>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>A sequence of domain objects.</returns>
        public virtual IEnumerable<TObject> GetAll(ISession session, ITransaction transaction)
        {
            return this.UsingSession(
                (s, t) => s.CreateCriteria<TData>()
                    .List<TData>()
                    .Select(data => this.ToDomainObject(data, s))
                    .ToArray(),
                session,
                transaction);
        }

        /// <summary>
        /// Gets all object from the repository.
        /// </summary>
        /// <returns>A sequence of domain objects.</returns>
        public virtual IEnumerable<TObject> GetAll()
        {
            return this.GetAll(null, null);
        }

        /// <summary>
        /// Compares two identifiers for equality.
        /// </summary>
        /// <param name="id">The first identifier to compare.</param>
        /// <param name="other">The other identifier to compare.</param>
        /// <returns><c>true</c> if the identifiers are equal; otherwise, <c>false</c>.</returns>
        protected virtual bool CompareIds(TId id, TId other)
        {
            if (object.ReferenceEquals(id, null))
            {
                return object.ReferenceEquals(other, null);
            }

            if (object.ReferenceEquals(other, null))
            {
                return false;
            }

            return id.Equals(other);
        }

        /// <summary>
        /// Gets the current data of the object with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object to get.</param>
        /// <param name="session">The NHibernate session which will be used to access the database.</param>
        /// <param name="transaction">The transaction to use.</param>
        /// <returns>
        /// The data of the specified object or <c>null</c> if there is no object with the specified identifier.
        /// </returns>
        protected virtual TData GetCurrentDataObject(TId objId, ISession session, ITransaction transaction)
        {
            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            TData dataObject = session.Get<TData>(objId);

            if (dataObject != null)
            {
                this.OnRetrieved(dataObject, session);
            }

            return dataObject;
        }

        /// <summary>
        /// Maps the property values from a <see cref="TObject"/> instance to a <see cref="TData"/> instance.
        /// </summary>
        /// <param name="domainObject">The domain (source) object.</param>
        /// <param name="dataObject">The data (destination) object.</param>
        protected virtual void MapDataToDataObject(TObject domainObject, TData dataObject)
        {
            this.DataObjectMapper.MapProperties(domainObject, dataObject);
        }

        /// <summary>
        /// Maps the property values from a <see cref="TData"/> instance to a <see cref="TObject"/> instance.
        /// </summary>
        /// <param name="dataObject">The domain (destination) object.</param>
        /// <param name="domainObject">The data (source) object.</param>
        protected virtual void MapDataToDomainObject(TData dataObject, TObject domainObject)
        {
            this.DomainObjectMapper.MapProperties(dataObject, domainObject);
        }

        /// <summary>
        /// Converts a <see cref="TObject"/> object to a <see cref="TData"/> object.
        /// </summary>
        /// <param name="domainObject">The object to convert.</param>
        /// <returns>The newly created (converted) object.</returns>
        protected virtual TData ConvertDomainObjectToDataObject(TObject domainObject)
        {
            var dataObject = this.DataObjectFactory.Create();
            this.DataObjectMapper.MapProperties(domainObject, dataObject);

            return dataObject;
        }

        /// <summary>
        /// Called before an object is saved to the database.
        /// </summary>
        /// <param name="obj">The domain object which will be save.</param>
        /// <param name="dataObject">The data object which represents the object which will be saved.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void OnSaving(TObject obj, TData dataObject, ISession session)
        {
        }

        /// <summary>
        /// Sets the values for the properties of the <see cref="IAuditable"/> interface for the specified data object.
        /// </summary>
        /// <param name="dataObject">The data object which properties will be set.</param>
        protected virtual void SaveAuditData(TData dataObject)
        {
            var auditableDataObject = dataObject as IAuditable;
            if (auditableDataObject != null)
            {
                if ((auditableDataObject.ModifiedBy == null) && (this.modifiedByProperty != null))
                {
                    this.modifiedByProperty.SetValue(auditableDataObject, this.GetCurrentUserName(), new object[0]);
                }

                if ((auditableDataObject.ModifiedAt == default(DateTimeOffset)) && (this.modifiedAtProperty != null))
                {
                    this.modifiedAtProperty.SetValue(auditableDataObject, this.GetCurrentDateTime(), new object[0]);
                }
            }
        }

        /// <summary>
        /// Gets the name of the current user that will be used to populate the <see cref="IAuditable.ModifiedBy"/>
        /// property.
        /// </summary>
        /// <returns>The name of the current user.</returns>
        protected virtual string GetCurrentUserName()
        {
            IPrincipal principal = Thread.CurrentPrincipal;
            
            IIdentity identity = null;
            if (principal != null)
            {
                identity = principal.Identity;
            }

            if ((identity != null) && !string.IsNullOrEmpty(identity.Name))
            {
                return identity.Name;
            }

            return Environment.UserName;
        }

        /// <summary>
        /// Gets the current date and time that will be used to populate the <see cref="IAuditable.ModifiedAt"/>
        /// property.
        /// </summary>
        /// <returns>The current date and time.</returns>
        protected virtual DateTimeOffset GetCurrentDateTime()
        {
            return DateTimeOffset.Now;
        }

        /// <summary>
        /// Throws an exception if the specified object is invalid and cannot be saved to the database.
        /// </summary>
        /// <param name="dataObject">The object to validate.</param>
        protected virtual void ValidateObjectForSave(TData dataObject)
        {
            this.DataAnnotationValidator.ValidateObject(dataObject);
        }

        /// <summary>
        /// Compares two objects for equality.
        /// </summary>
        /// <param name="obj">The first object to compare.</param>
        /// <param name="other">The other object to compare.</param>
        /// <param name="domainObject">The domain object from which <paramref name="obj"/> was created.</param>
        /// <returns><c>true</c> if the objects are equal; otherwise, <c>false</c>.</returns>
        protected virtual bool CompareObjects(TData obj, TData other, TObject domainObject)
        {
            if ((obj != null) && obj.Equals(other))
            {
                return true;
            }

            foreach (PropertyInfo propertyInfo in this.dataObjectProperties)
            {
                object objValue = propertyInfo.GetValue(obj, new object[0]);
                object otherValue = propertyInfo.GetValue(other, new object[0]);

                if (!this.AreEqual(objValue, otherValue))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the two specified objects are equal.
        /// </summary>
        /// <param name="obj">The first object to check.</param>
        /// <param name="other">The other object to check.</param>
        /// <returns><c>true</c> if the specified objects are equal; otherwise, <c>false</c>.</returns>
        protected virtual bool AreEqual(object obj, object other)
        {
            if (obj == null)
            {
                if (other != null)
                {
                    return false;
                }
            }
            else
            {
                if (other == null)
                {
                    return false;
                }

                var objEnumerable = obj as IEnumerable;
                var otherEnumerable = other as IEnumerable;
                if ((objEnumerable != null) && (otherEnumerable != null))
                {
                    object[] objArr = objEnumerable.Cast<object>().ToArray();
                    object[] otherArr = otherEnumerable.Cast<object>().ToArray();

                    if (objArr.Length != otherArr.Length)
                    {
                        return false;
                    }

                    for (int i = 0; i < objArr.Length; i++)
                    {
                        if (!this.AreEqual(objArr[i], otherArr[i]))
                        {
                            return false;
                        }
                    }
                }
                else if (!object.ReferenceEquals(obj, other) && !obj.Equals(other))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Updates the database to create a new object or update an existing one.
        /// </summary>
        /// <param name="session">The NHibernate session in which the object should be saved.</param>
        /// <param name="dataObject">The data object to save.</param>
        protected virtual void SaveOrUpdate(ISession session, TData dataObject)
        {
            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            if (dataObject == null)
            {
                throw new ArgumentNullException("dataObject");
            }

            session.SaveOrUpdate(dataObject);
        }

        /// <summary>
        /// Called after an object has been saved to the database.
        /// </summary>
        /// <param name="obj">The domain object which was saved.</param>
        /// <param name="dataObject">The data object which represents the object which was saved.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void OnSaved(TObject obj, TData dataObject, ISession session)
        {
        }

        /// <summary>
        /// Called when an object is retrieved from the database.
        /// </summary>
        /// <param name="dataObject">The data object which represents the retrieved object.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void OnRetrieved(TData dataObject, ISession session)
        {
        }

        /// <summary>
        /// Called when an object is retrieved from the database.
        /// </summary>
        /// <param name="obj">The retrieved domain object.</param>
        /// <param name="data">The data object which represents the retrieved object.</param>
        protected virtual void OnRetrieved(TObject obj, TData data)
        {
        }

        /// <summary>
        /// Converts the specified data object to a domain object.
        /// </summary>
        /// <param name="data">The data object.</param>
        /// <param name="session">The context NHibernate session.</param>
        /// <returns>The created domain object.</returns>
        protected TObject ToDomainObject(TData data, ISession session)
        {
            this.OnRetrieved(data, session);

            TObject obj = this.DomainObjectFactory.Create();
            this.MapDataToDomainObject(data, obj);
            this.OnRetrieved(obj, data);

            return obj;
        }
    }
}