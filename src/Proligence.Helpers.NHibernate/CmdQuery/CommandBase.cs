﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommandBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.CmdQuery
{
    using System;
    using global::NHibernate;

    /// <summary>
    /// The base class for classes which implement data access commands.
    /// </summary>
    public abstract class CommandBase : ICommand
    {
        /// <summary>
        /// Gets or sets the NHibernate session which will be used to execute the command.
        /// </summary>
        public ISession Session { get; set; }

        /// <summary>
        /// Gets or sets the NHibernate transaction which will be used to execute the command.
        /// </summary>
        public ITransaction Transaction { get; set; }

        /// <summary>
        /// Executes the command.
        /// </summary>
        public abstract void Execute();

        /// <summary>
        /// Executes the specified command in the context if this command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the executed command.</typeparam>
        /// <param name="command">The command to execute.</param>
        /// <returns>The executed command.</returns>
        protected TCommand Execute<TCommand>(TCommand command)
            where TCommand : ICommand
        {
            if (ReferenceEquals(command, null))
            {
                throw new ArgumentNullException("command");
            }

            command.Session = this.Session;
            command.Transaction = this.Transaction;
            command.Execute();

            return command;
        }

        /// <summary>
        /// Executes the specified query in the context if this command.
        /// </summary>
        /// <typeparam name="TQuery">The type of the executed query.</typeparam>
        /// <typeparam name="TResult">The type of the query's result.</typeparam>
        /// <param name="query">The query to execute.</param>
        /// <returns>The executed query.</returns>
        protected TQuery Execute<TQuery, TResult>(TQuery query)
            where TQuery : IQuery<TResult>
        {
            if (ReferenceEquals(query, null))
            {
                throw new ArgumentNullException("query");
            }

            query.Session = this.Session;
            query.Transaction = this.Transaction;
            query.Execute();

            return query;
        }
    }
}