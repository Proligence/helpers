﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICmdQueryExecutor.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.CmdQuery
{
    using System;

    /// <summary>
    /// Defines the API for command query executors.
    /// </summary>
    public interface ICmdQueryExecutor
    {
        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <param name="command">The command to execute.</param>
        /// <returns>The executed command.</returns>
        ICommand ExecuteCommand(ICommand command);

        /// <summary>
        /// Executes the command of the specified type (or interface).
        /// </summary>
        /// <typeparam name="TCommand">The type of the command to execute.</typeparam>
        /// <param name="parameters">Object creation parameters (as anonymous object).</param>
        /// <param name="setup">Optional command setup.</param>
        /// <returns>The executed command.</returns>
        TCommand ExecuteCommand<TCommand>(object parameters = null, Action<TCommand> setup = null)
            where TCommand : ICommand;

        /// <summary>
        /// Executes the specified query.
        /// </summary>
        /// <typeparam name="TResult">The type of the query's result.</typeparam>
        /// <param name="query">The query to execute.</param>
        /// <returns>The executed query.</returns>
        IQuery<TResult> ExecuteQuery<TResult>(IQuery<TResult> query);

        /// <summary>
        /// Executes the query of the specified type (or interface).
        /// </summary>
        /// <typeparam name="TQuery">The type of the query to execute.</typeparam>
        /// <typeparam name="TResult">The type of the query's result.</typeparam>
        /// <param name="parameters">Object creation parameters (as anonymous object).</param>
        /// <param name="setup">Optional query setup.</param>
        /// <returns>The executed query.</returns>
        TQuery ExecuteQuery<TQuery, TResult>(object parameters = null, Action<TQuery> setup = null)
            where TQuery : IQuery<TResult>;
    }
}