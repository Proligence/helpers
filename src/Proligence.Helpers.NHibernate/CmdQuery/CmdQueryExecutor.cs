﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdQueryExecutor.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.CmdQuery
{
    using System;
    using System.Data;
    using global::Autofac;
    using Proligence.Helpers.Autofac;

    /// <summary>
    /// Implements the default command/query executor.
    /// </summary>
    public class CmdQueryExecutor : NHibernateSessionHost, ICmdQueryExecutor
    {
        /// <summary>
        /// The dependency injection container.
        /// </summary>
        private readonly IComponentContext container;

        /// <summary>
        /// Initializes a new instance of the <see cref="CmdQueryExecutor" /> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        /// <param name="container">The dependency injection container.</param>
        public CmdQueryExecutor(
            SessionFactoryCache factoryCache,
            IDbConnection connection,
            IComponentContext container)
            : base(factoryCache, connection)
        {
            this.container = container;
        }

        /// <summary>
        /// Executes the specified command.
        /// </summary>
        /// <param name="command">The command to execute.</param>
        /// <returns>The executed command.</returns>
        public ICommand ExecuteCommand(ICommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            return this.UsingSession(
                (s, t) =>
                    {
                        command.Session = s;
                        command.Transaction = t;
                        command.Execute();
                        return command;
                    });
        }

        /// <summary>
        /// Executes the command of the specified type (or interface).
        /// </summary>
        /// <typeparam name="TCommand">The type of the command to execute.</typeparam>
        /// <param name="parameters">Object creation parameters (as anonymous object).</param>
        /// <param name="setup">Optional command setup.</param>
        /// <returns>The executed command.</returns>
        public TCommand ExecuteCommand<TCommand>(object parameters, Action<TCommand> setup)
            where TCommand : ICommand
        {
            TCommand command = parameters != null
                ? this.container.Resolve<TCommand>(parameters)
                : this.container.Resolve<TCommand>();

            if (setup != null)
            {
                setup(command);
            }

            return (TCommand)this.ExecuteCommand(command);
        }

        /// <summary>
        /// Executes the specified query.
        /// </summary>
        /// <typeparam name="TResult">The type of the query's result.</typeparam>
        /// <param name="query">The query to execute.</param>
        /// <returns>The executed query.</returns>
        public IQuery<TResult> ExecuteQuery<TResult>(IQuery<TResult> query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return this.UsingSession(
                (s, t) =>
                {
                    query.Session = s;
                    query.Transaction = t;
                    query.Execute();
                    return query;
                });
        }

        /// <summary>
        /// Executes the query of the specified type (or interface).
        /// </summary>
        /// <typeparam name="TQuery">The type of the query to execute.</typeparam>
        /// <typeparam name="TResult">The type of the query's result.</typeparam>
        /// <param name="parameters">Object creation parameters (as anonymous object).</param>
        /// <param name="setup">Optional query setup.</param>
        /// <returns>The executed query.</returns>
        public TQuery ExecuteQuery<TQuery, TResult>(object parameters, Action<TQuery> setup)
            where TQuery : IQuery<TResult>
        {
            TQuery query = parameters != null 
                ? this.container.Resolve<TQuery>(parameters) 
                : this.container.Resolve<TQuery>();
                
            if (setup != null)
            {
                setup(query);
            }

            return (TQuery)this.ExecuteQuery(query);
        }
    }
}