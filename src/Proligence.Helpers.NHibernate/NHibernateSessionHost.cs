﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NHibernateSessionHost.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using global::NHibernate;

    /// <summary>
    /// The base class for classes which host and use NHibernate sessions.
    /// </summary>
    public abstract class NHibernateSessionHost : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NHibernateSessionHost"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        protected NHibernateSessionHost(SessionFactoryCache factoryCache, IDbConnection connection)
        {
            if (factoryCache == null)
            {
                throw new ArgumentNullException("factoryCache");
            }

            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            this.FactoryCache = factoryCache;
            this.Connection = connection;
        }

        /// <summary>
        /// Gets the object which caches the NHibernate session factory.
        /// </summary>
        protected SessionFactoryCache FactoryCache { get; private set; }

        /// <summary>
        /// Gets the connection to the SQL database.
        /// </summary>
        protected IDbConnection Connection { get; private set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Executes the specified action using a specific or new NHibernate session.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <param name="session">The NHibernate session to use or <c>null</c> to create a new session.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingSession<TResult>(Func<ISession, TResult> action, ISession session)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (session != null)
            {
                TResult result = action.Invoke(session);
                session.Flush();
                return result;
            }

            using (IDbConnection connection = this.CreateConnection(this.Connection.ConnectionString))
            {
                using (session = this.FactoryCache.SessionFactory.OpenSession(connection))
                {
                    connection.Open();
                    TResult result = action.Invoke(session);
                    session.Flush();
                    return result;
                }
            }
        }

        /// <summary>
        /// Executes the specified action using a specific or new NHibernate session.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="session">The NHibernate session to use or <c>null</c> to create a new session.</param>
        protected void UsingSession(Action<ISession> action, ISession session)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (session != null)
            {
                action.Invoke(session);
                session.Flush();
            }
            else
            {
                using (IDbConnection connection = this.CreateConnection(this.Connection.ConnectionString))
                {
                    using (session = this.FactoryCache.SessionFactory.OpenSession(connection))
                    {
                        connection.Open();
                        action.Invoke(session);
                        session.Flush();
                    }
                }
            }
        }

        /// <summary>
        /// Executes the specified action using a specific or new NHibernate session and explicit transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <param name="session">The NHibernate session to use or <c>null</c> to create a new session.</param>
        /// <param name="transaction">The transaction to use or <c>null</c> to create a new transaction.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingSession<TResult>(
            Func<ISession, ITransaction, TResult> action,
            ISession session,
            ITransaction transaction)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            bool newSession = false;
            IDbConnection connection = null;
            if (session == null)
            {
                connection = this.CreateConnection(this.Connection.ConnectionString);
                connection.Open();

                session = this.FactoryCache.SessionFactory.OpenSession(connection);
                newSession = true;
            }

            try
            {
                bool newTransaction = false;
                if ((transaction == null) && (session.Transaction == null || newSession))
                {
                    transaction = session.BeginTransaction();
                    newTransaction = true;
                }

                try
                {
                    TResult result = action.Invoke(session, transaction);

                    if (newTransaction)
                    {
                        transaction.Commit();                        
                    }

                    session.Flush();
                    
                    return result;
                }
                catch (Exception)
                {
                    try
                    {
                        if ((transaction != null) && transaction.IsActive)
                        {
                            transaction.Rollback();                            
                        }
                    }
                    catch (TransactionException)
                    {
                        // An exception might occur in Rollback if the transaction was rolled back by the database!
                    }

                    throw;
                }
            }
            finally
            {
                if (connection != null)
                {
                    session.Dispose();
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes the specified action using a specific or new NHibernate session and explicit transaction.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        /// <param name="session">The NHibernate session to use or <c>null</c> to create a new session.</param>
        /// <param name="transaction">The transaction to use or <c>null</c> to create a new transaction.</param>
        protected void UsingSession(Action<ISession, ITransaction> action, ISession session, ITransaction transaction)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            IDbConnection connection = null;
            if (session == null)
            {
                connection = this.CreateConnection(this.Connection.ConnectionString);
                connection.Open();

                session = this.FactoryCache.SessionFactory.OpenSession(connection);
            }

            try
            {
                bool newTransaction = false;
                if (transaction == null)
                {
                    transaction = session.BeginTransaction();
                    newTransaction = true;
                }

                try
                {
                    action.Invoke(session, transaction);

                    if (newTransaction)
                    {
                        transaction.Commit();                        
                    }

                    session.Flush();
                }
                catch (Exception)
                {
                    try
                    {
                        if (transaction != null)
                        {
                            transaction.Rollback();                            
                        }
                    }
                    catch (TransactionException)
                    {
                        // An exception might occur in Rollback if the transaction was rolled back by the database!
                    }

                    throw;
                }
            }
            finally
            {
                if (connection != null)
                {
                    session.Dispose();
                    connection.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingSession<TResult>(Func<ISession, TResult> action)
        {
            return this.UsingSession(action, null);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        protected void UsingSession(Action<ISession> action)
        {
            this.UsingSession(action, null);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session and explicit transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingSession<TResult>(Func<ISession, ITransaction, TResult> action)
        {
            return this.UsingSession(action, null, null);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session and explicit transaction.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        protected void UsingSession(Action<ISession, ITransaction> action)
        {
            this.UsingSession(action, null, null);
        }

        /// <summary>
        /// Executes the specified action using the specified or new transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        /// <param name="transaction">The transaction to use or <c>null</c> to create a new transaction.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingTransaction<TResult>(
            ISession session,
            Func<ISession, TResult> action,
            ITransaction transaction)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            bool newTransaction = false;
            if (transaction == null)
            {
                transaction = session.BeginTransaction();
                newTransaction = true;
            }

            try
            {
                TResult result = action(session);

                if (newTransaction)
                {
                    transaction.Commit();                    
                }

                return result;
            }
            catch (Exception)
            {
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();                        
                    }
                }
                catch (TransactionException)
                {
                    // An exception might occur in Rollback if the transaction was rolled back by the database!
                }

                throw;
            }
        }

        /// <summary>
        /// Executes the specified action using the specified or new transaction.
        /// </summary>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        /// <param name="transaction">The transaction to use or <c>null</c> to create a new transaction.</param>
        protected void UsingTransaction(ISession session, Action<ISession> action, ITransaction transaction)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            bool newTransaction = false;
            if (transaction == null)
            {
                transaction = session.BeginTransaction();
                newTransaction = true;
            }

            try
            {
                action(session);

                if (newTransaction)
                {
                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (TransactionException)
                {
                    // An exception might occur in Rollback if the transaction was rolled back by the database!
                }

                throw;
            }
        }

        /// <summary>
        /// Executes the specified action using an explicit transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        protected TResult UsingTransaction<TResult>(ISession session, Func<ISession, TResult> action)
        {
            return this.UsingTransaction(session, action, null);
        }

        /// <summary>
        /// Executes the specified action using an explicit transaction.
        /// </summary>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        protected void UsingTransaction(ISession session, Action<ISession> action)
        {
            this.UsingTransaction(session, action, null);
        }

        /// <summary>
        /// Creates a new SQL connection with the specified connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>The created connection object.</returns>
        protected virtual IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.FactoryCache != null)
                {
                    this.FactoryCache.Dispose();
                }

                if (this.Connection != null)
                {
                    this.Connection.Dispose();
                }
            }
        }
    }
}