﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVersionManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System.Collections.Generic;
    using global::NHibernate;
    using Proligence.Helpers.Data;

    /// <summary>
    /// Defines the API for classes which implement NHibernate entity version management.
    /// </summary>
    /// <typeparam name="TId">The type which represents the unique identifier of entity objects.</typeparam>
    /// <typeparam name="TData">The type which represents the entities.</typeparam>
    /// <typeparam name="TDataVersion">The type which represents the entity versions.</typeparam>
    public interface IVersionManager<in TId, in TData, out TDataVersion>
        where TData : class, IIdentifiable<TId>, IVersioned<TId>
        where TDataVersion : class, IIdentifiable<TId>, IVersioned<TId>
    {
        /// <summary>
        /// Gets the specified entity version.
        /// </summary>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The specified version of the data object or <c>null</c> if there is no version with the specified
        /// identifier.
        /// </returns>
        TDataVersion GetVersion(TId versionId, ISession session = null, ITransaction transaction = null);

        /// <summary>
        /// Gets the specified version of the entity.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="versionNumber">The version number of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The specified version of the data object or <c>null</c> if there is no object with the specified
        /// identifier and version number.
        /// </returns>
        TDataVersion GetVersion(
            TId objectId,
            int versionNumber,
            ISession session = null,
            ITransaction transaction = null);

        /// <summary>
        /// Gets all entity versions with the specified version identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the entities to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>A sequence of entity version objects.</returns>
        IEnumerable<TDataVersion> GetManyVersions(
            IEnumerable<TId> ids,
            ISession session = null,
            ITransaction transaction = null);

        /// <summary>
        /// Gets the latest version of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The latest version of the specified identity or <c>null</c> if there is no object with the specified
        /// identifier.
        /// </returns>
        TDataVersion GetLatestVersion(TId objectId, ISession session = null, ITransaction transaction = null);

        /// <summary>
        /// Gets all versions of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// All versions of the specified entity or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        IEnumerable<TDataVersion> GetAllVersions(
            TId objectId,
            ISession session = null,
            ITransaction transaction = null);

        /// <summary>
        /// Creates a new version using the current data of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the object for which a new version will be created.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>The created version or <c>null</c> if there is no object with the specified identifier.</returns>
        TDataVersion CreateVersion(TId objectId, ISession session = null, ITransaction transaction = null);

        /// <summary>
        /// Creates or updates the specified object and creates a new version for it.
        /// </summary>
        /// <param name="dataObject">The data object to save.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>The created version.</returns>
        TDataVersion SaveAsNewVersion(TData dataObject, ISession session = null, ITransaction transaction = null);
    }
}