﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VersionedRepositoryBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::NHibernate;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// The base class for classes which implement NHibernate data repositories which support versioning.
    /// </summary>
    /// <typeparam name="TObject">The type of the objects stored in the repository.</typeparam>
    /// <typeparam name="TData">The type which represents the object's data in the database.</typeparam>
    /// <typeparam name="TVersionData">The type which represents the object version's data in the database.</typeparam>
    /// <typeparam name="TId">The type of the identifier of the objects stored in the repository.</typeparam>
    public abstract class VersionedRepositoryBase<TObject, TData, TVersionData, TId> 
        : RepositoryBase<TObject, TData, TId>, IVersionedRepository<TObject, TId>
        where TObject : class, IIdentifiable<TId>, IVersioned<TId>
        where TData : class, IIdentifiable<TId>, IVersioned<TId>
        where TVersionData : class, IIdentifiable<TId>, IVersioned<TId>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VersionedRepositoryBase{TObject,TData,TVersionData,TId}" />
        /// class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        /// <param name="versionManager">The version manager for the repository.</param>
        /// <param name="dataObjectFactory">The object used to create instances of <see cref="TData"/> type.</param>
        /// <param name="domainObjectFactory">An object used to create instances of <see cref="TObject"/> type.</param>
        /// <param name="dataObjectMapper">The data object mapper.</param>
        /// <param name="domainObjectMapper">The domain object mapper.</param>
        /// <param name="versionDataObjectMapper">The version data object mapper.</param>
        protected VersionedRepositoryBase(
            SessionFactoryCache factoryCache,
            IDbConnection connection,
            IVersionManager<TId, TData, TVersionData> versionManager,
            IObjectFactory<TData> dataObjectFactory,
            IObjectFactory<TObject> domainObjectFactory,
            IObjectMapper<TObject, TData> dataObjectMapper,
            IObjectMapper<TData, TObject> domainObjectMapper,
            IObjectMapper<TVersionData, TObject> versionDataObjectMapper)
            : base(
                factoryCache,
                connection,
                dataObjectFactory,
                domainObjectFactory, 
                dataObjectMapper,
                domainObjectMapper)  
        {
            this.VersionManager = versionManager;
            this.VersionDataObjectMapper = versionDataObjectMapper;
        }

        /// <summary>
        /// Gets the version manager for the repository.
        /// </summary>
        public IVersionManager<TId, TData, TVersionData> VersionManager { get; private set; }

        /// <summary>
        /// Gets the version data object mapper.
        /// </summary>
        protected IObjectMapper<TVersionData, TObject> VersionDataObjectMapper { get; private set; }

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="versionId">The identifier of the object version to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// version identifier.
        /// </returns>
        public TObject GetByVersionId(TId versionId, ISession session, ITransaction transaction)
        {
            return this.UsingSession(
                (s, t) =>
                {
                    TVersionData versionData = this.VersionManager.GetVersion(versionId, s, t);
                    if (versionData == null)
                    {
                        return null;
                    }

                    this.OnRetrieved(versionData, s);

                    TObject result = this.DomainObjectFactory.Create();
                    this.VersionDataObjectMapper.MapProperties(versionData, result);

                    this.OnRetrieved(versionData, result, s);

                    return result;
                }, 
                session,
                transaction);
        }

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="versionId">The identifier of the object version to get.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// version identifier.
        /// </returns>
        public TObject GetByVersionId(TId versionId)
        {
            return this.GetByVersionId(versionId, null, null);
        }

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="objId">The identifier of the object to get.</param>
        /// <param name="versionNumber">The number of the object version to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier and version number.
        /// </returns>
        public TObject GetByIdAndVersionNumber(
            TId objId,
            int versionNumber,
            ISession session,
            ITransaction transaction)
        {
            return this.UsingSession(
                (s, t) =>
                {
                    TVersionData versionData = this.VersionManager.GetVersion(objId, versionNumber, s, t);
                    if (versionData == null)
                    {
                        return null;
                    }

                    this.OnRetrieved(versionData, s);

                    TObject result = this.DomainObjectFactory.Create();
                    this.VersionDataObjectMapper.MapProperties(versionData, result);

                    this.OnRetrieved(versionData, result, s);

                    return result;
                },
                session,
                transaction);
        }

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="objId">The identifier of the object to get.</param>
        /// <param name="versionNumber">The number of the object version to get.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier and version number.
        /// </returns>
        public TObject GetByIdAndVersionNumber(TId objId, int versionNumber)
        {
            return this.GetByIdAndVersionNumber(objId, versionNumber, null, null);
        }

        /// <summary>
        /// Gets the object versions from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the objects to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>The domain object versions with the specified identifiers.</returns>
        public virtual IEnumerable<TObject> GetManyVersions(
            IEnumerable<TId> ids,
            ISession session,
            ITransaction transaction)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }

            return this.UsingSession(
                (s, t) =>
                    {
                        IEnumerable<TVersionData> dataResults = this.VersionManager.GetManyVersions(
                            ids,
                            session,
                            transaction);
                        
                        return dataResults.Select(data => this.ToDomainObject(data, s)).ToArray();
                    },
                session,
                transaction);
        }

        /// <summary>
        /// Gets the object versions from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the objects to get.</param>
        /// <returns>The domain object versions with the specified identifiers.</returns>
        public virtual IEnumerable<TObject> GetManyVersions(IEnumerable<TId> ids)
        {
            return this.GetManyVersions(ids, null, null);
        }

        /// <summary>
        /// Gets all versions of the object with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object which versions to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// All versions of the specified object or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        public IEnumerable<TObject> GetAllVersions(TId objId, ISession session, ITransaction transaction)
        {
            return this.UsingSession(
                (s, t) => this.VersionManager.GetAllVersions(objId, s, t).Select(
                    v =>
                    {
                        this.OnRetrieved(v, s);

                        TObject obj = this.DomainObjectFactory.Create();
                        this.VersionDataObjectMapper.MapProperties(v, obj);

                        this.OnRetrieved(v, obj, s);

                        return obj;
                    }).ToArray(),
                session,
                transaction);
        }

        /// <summary>
        /// Gets all versions of the object with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object which versions to get.</param>
        /// <returns>
        /// All versions of the specified object or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        public IEnumerable<TObject> GetAllVersions(TId objId)
        {
            return this.GetAllVersions(objId, null, null);
        }

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        public override void Delete(TObject obj, ISession session, ITransaction transaction)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            this.UsingSession(
                (s, t) =>
                {
                    TData dataObject = s.Get<TData>(obj.Id);
                    if (dataObject != null)
                    {
                        this.VersionManager.CreateVersion(obj.Id, s, t);
                        s.Delete(dataObject);
                    }
                },
                session,
                transaction);
        }

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        public override void Delete(TObject obj)
        {
            this.Delete(obj, null, null);
        }

        /// <summary>
        /// Restores the specified object version.
        /// </summary>
        /// <param name="versionId">The object version to restore.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        public virtual void RestoreVersion(TId versionId, ISession session, ITransaction transaction)
        {
            this.UsingSession(
                (s, t) =>
                {
                    TObject version = this.GetByVersionId(versionId, s, t);
                    if (version == null)
                    {
                        throw new ArgumentException("Failed to find version '" + versionId + "'.", "versionId");
                    }

                    TObject currentVersion = this.GetById(version.Id, s, t);
                    if (currentVersion == null)
                    {
                        throw new InvalidOperationException(
                            "Failed to find object with identifier '" + version.Id + "'.");
                    }

                    if (!currentVersion.VersionId.Equals(version.VersionId))
                    {
                        version.VersionId = currentVersion.VersionId;
                        version.Version = currentVersion.Version;
                        this.Save(version, s, t);
                    }
                }, 
                session,
                transaction);
        }

        /// <summary>
        /// Restores the specified object version.
        /// </summary>
        /// <param name="versionId">The object version to restore.</param>
        public virtual void RestoreVersion(TId versionId)
        {
            this.RestoreVersion(versionId, null, null);
        }

        /// <summary>
        /// Updates the database to create a new object or update an existing one.
        /// </summary>
        /// <param name="session">The NHibernate session in which the object should be saved.</param>
        /// <param name="dataObject">The data object to save.</param>
        protected override void SaveOrUpdate(ISession session, TData dataObject)
        {
            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            this.VersionManager.SaveAsNewVersion(dataObject, session, session.Transaction);
        }

        /// <summary>
        /// Called when an object version is retrieved from the database.
        /// </summary>
        /// <param name="versionDataObject">The data object which represents the retrieved object version.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void OnRetrieved(TVersionData versionDataObject, ISession session)
        {
        }

        /// <summary>
        /// Called when an object version is retrieved from the database.
        /// </summary>
        /// <param name="versionDataObject">The data object which represents the retrieved object version.</param>
        /// <param name="domainObject">The mapped domain object.</param>
        /// <param name="session">The NHibernate session.</param>
        protected virtual void OnRetrieved(TVersionData versionDataObject, TObject domainObject, ISession session)
        {
        }

        /// <summary>
        /// Converts the specified data object to a domain object.
        /// </summary>
        /// <param name="data">The data object.</param>
        /// <param name="session">The context NHibernate session.</param>
        /// <returns>The created domain object.</returns>
        protected TObject ToDomainObject(TVersionData data, ISession session)
        {
            this.OnRetrieved(data, session);

            TObject obj = this.DomainObjectFactory.Create();
            this.VersionDataObjectMapper.MapProperties(data, obj);
            this.OnRetrieved(data, session);
            this.OnRetrieved(data, obj, session);

            return obj;
        }
    }
}