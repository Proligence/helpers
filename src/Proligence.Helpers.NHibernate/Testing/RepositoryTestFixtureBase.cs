﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryTestFixtureBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.Testing
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using global::Autofac;
    using NUnit.Framework;
    using Proligence.Helpers.NHibernate;

    /// <summary>
    /// The base class for test fixtures which test data repositories.
    /// </summary>
    /// <typeparam name="TRepository">The type of the tested repository.</typeparam>
    [ExcludeFromCodeCoverage]
    public abstract class RepositoryTestFixtureBase<TRepository> 
        where TRepository : RepositoryBase
    {
        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        protected IContainer Container { get; private set; }

        /// <summary>
        /// Gets the repository instance.
        /// </summary>
        protected TRepository Repository { get; private set; }

        /// <summary>
        /// Gets the SQL server connection used by the test fixture.
        /// </summary>
        protected SqlConnection Connection 
        { 
            get
            {
                return (SqlConnection)this.Repository.Connection;
            }
        }

        /// <summary>
        /// Sets up the test fixture before all tests.
        /// </summary>
        [TestFixtureSetUp]
        public virtual void FixtureSetup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            this.BuildContainer(builder);
            this.Container = builder.Build();

            this.Repository = this.Container.Resolve<TRepository>();
        }

        /// <summary>
        /// Cleans up the test fixture after all tests.
        /// </summary>
        [TestFixtureTearDown]
        public virtual void TestFixtureTeardown()
        {
            this.Container.Dispose();
        }

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public virtual void Teardown()
        {
        }

        /// <summary>
        /// Called when the dependency injection container is built.
        /// </summary>
        /// <param name="builder">The container builder.</param>
        protected virtual void BuildContainer(ContainerBuilder builder)
        {
        }

        /// <summary>
        /// Executes the specified SQL query and returns the single value returned by the SQL server.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The value returned by SQL server or <c>null</c> if no results were returned.</returns>
        protected object ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            return SqlTestHelpers.ExecuteQuery(this.Connection, query, parameters);
        }

        /// <summary>
        /// Executes the specified SQL query and returns the values from the first column of the data set returned by 
        /// the SQL server.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>
        /// The values from the first column of the data set returned by the SQL server or <c>null</c> if no results 
        /// were returned.
        /// </returns>
        protected IEnumerable<object> ExecuteListQuery(string query, params SqlParameter[] parameters)
        {
            return SqlTestHelpers.ExecuteListQuery(this.Connection, query, parameters);
        }
    }
}