﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CmdQueryTestFixtureBase.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.Testing
{
    using global::Autofac;
    using NUnit.Framework;
    using Proligence.Helpers.NHibernate.CmdQuery;

    /// <summary>
    /// Base class for test fixtures which contain tests for <see cref="ICommand"/> and <see cref="IQuery{T}"/>
    /// implementations.
    /// </summary>
    public class CmdQueryTestFixtureBase
    {
        /// <summary>
        /// Gets the dependency injection container.
        /// </summary>
        protected IContainer Container { get; private set; }

        /// <summary>
        /// Gets the command/query executor instance.
        /// </summary>
        protected ICmdQueryExecutor Executor { get; private set; }

        /// <summary>
        /// Sets up the test fixture before all tests.
        /// </summary>
        [TestFixtureSetUp]
        public virtual void FixtureSetup()
        {
            var builder = new ContainerBuilder();
            this.BuildContainer(builder);
            this.Container = builder.Build();
            this.Executor = this.Container.Resolve<ICmdQueryExecutor>();
        }

        /// <summary>
        /// Cleans up the test fixture after all tests.
        /// </summary>
        [TestFixtureTearDown]
        public virtual void TestFixtureTeardown()
        {
            this.Container.Dispose();
        }

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public virtual void Teardown()
        {
        }

        /// <summary>
        /// Called when the dependency injection container is built.
        /// </summary>
        /// <param name="builder">The container builder.</param>
        protected virtual void BuildContainer(ContainerBuilder builder)
        {
        }
    }
}