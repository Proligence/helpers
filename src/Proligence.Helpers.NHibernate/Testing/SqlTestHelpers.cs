﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlTestHelpers.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.Testing
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// Implements helper methods for testing SQL-based data tiers.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class SqlTestHelpers
    {
        /// <summary>
        /// Executes the specified SQL query and returns the single value returned by the SQL server.
        /// </summary>
        /// <param name="connection">The connection to use.</param>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The value returned by SQL server or <c>null</c> if no results were returned.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "Unit test.")]
        public static object ExecuteQuery(SqlConnection connection, string query, params SqlParameter[] parameters)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddRange(parameters);

            bool closeConnection = false;
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
                closeConnection = true;
            }

            object result = command.ExecuteScalar();

            if (closeConnection)
            {
                connection.Close();
            }

            return result;
        }

        /// <summary>
        /// Executes the specified SQL query and returns the values from the first column of the data set returned by 
        /// the SQL server.
        /// </summary>
        /// <param name="connection">The SQL connection to use.</param>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>
        /// The values from the first column of the data set returned by the SQL server or <c>null</c> if no results 
        /// were returned.
        /// </returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "Unit test.")]
        public static IEnumerable<object> ExecuteListQuery(
            SqlConnection connection, 
            string query, 
            params SqlParameter[] parameters)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddRange(parameters);

            bool closeConnection = false;
            if (connection.State != ConnectionState.Open) 
            {
                connection.Open();
                closeConnection = true;
            }

            List<object> results = new List<object>();

            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader.FieldCount == 0)
                    {
                        return null;
                    }

                    results.Add(reader.GetValue(0));
                }
            }

            if (closeConnection) 
            {
                connection.Close();
            }

            return results;
        }

        /// <summary>
        /// Gets the specified table field from the database.
        /// </summary>
        /// <param name="connection">The SQL connection to use.</param>
        /// <param name="fieldName">The name of the data field to get.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="where">The 'where' clause which filters the row to get.</param>
        /// <param name="parameters">The parameter for the 'where' clause.</param>
        /// <returns>The value of the specified field convert to a <see cref="string"/>.</returns>
        public static string GetTableField(
            SqlConnection connection,
            string fieldName, 
            string tableName, 
            string where, 
            params SqlParameter[] parameters)
        {
            object result = ExecuteQuery(
                connection, 
                "SELECT " + fieldName + " FROM " + tableName + " WHERE " + where, 
                parameters);

            if ((result == null) || (result == DBNull.Value))
            {
                return null;
            }

            return Convert.ToString(result, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets the specified table field from the database.
        /// </summary>
        /// <typeparam name="T">The type of the result.</typeparam>
        /// <param name="connection">The SQL connection to use.</param>
        /// <param name="fieldName">The name of the data field to get.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="where">The 'where' clause which filters the row to get.</param>
        /// <param name="parameters">The parameter for the 'where' clause.</param>
        /// <returns>The value of the specified field convert to a <see cref="string"/>.</returns>
        public static T GetTableField<T>(
            SqlConnection connection,
            string fieldName, 
            string tableName, 
            string where, 
            params SqlParameter[] parameters)
        {
            object result = ExecuteQuery(
                connection, 
                "SELECT " + fieldName + " FROM " + tableName + " WHERE " + where, 
                parameters);

            if ((result == null) || (result == DBNull.Value))
            {
                return default(T);
            }

            return (T)result;
        }

        /// <summary>
        /// Gets the specified number of identifiers from the database.
        /// </summary>
        /// <param name="connection">The SQL connection to use.</param>
        /// <param name="count">The number of identifiers to get.</param>
        /// <param name="tableName">The name of the table to get identifiers from.</param>
        /// <param name="identifierColumnName">The name of the identifier column.</param>
        /// <param name="createQuery">The query which creates a new row in the table.</param>
        /// <param name="parameters">
        /// The function which returns the parameters for <paramref name="createQuery"/>.
        /// </param>
        /// <returns>Sequence of identifier.</returns>
        public static IEnumerable<Guid> GetIds(
            SqlConnection connection,
            int count, 
            string tableName,
            string identifierColumnName,
            string createQuery, 
            Func<Guid, SqlParameter[]> parameters)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }

            if (parameters == null)
            {
                throw new ArgumentNullException("parameters");
            }

            object currentCountObj = ExecuteQuery(connection, "select count(*) from " + tableName);
            int currentCount = Convert.ToInt32(currentCountObj, CultureInfo.InvariantCulture);
            for (int i = currentCount; i < count; i++) 
            {
                ExecuteQuery(connection, createQuery, parameters(Guid.NewGuid()));
            }

            IEnumerable<object> ids = ExecuteListQuery(
                connection,
                "select top " + count + " " + identifierColumnName + " from " + tableName);

            return ids.Select(id => Guid.Parse(id.ToString()));
        }
    }
}