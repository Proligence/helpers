﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultSessionFactoryCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using global::NHibernate;

    /// <summary>
    /// Default implementation for the object which builds and caches the <see cref="ISessionFactory"/> instance used
    /// for accessing the application's SQL database using NHibernate.
    /// </summary>
    public class DefaultSessionFactoryCache : SessionFactoryCache
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultSessionFactoryCache"/> class.
        /// </summary>
        /// <param name="sqlServer">The name of the SQL server.</param>
        /// <param name="sqlDatabase">The name of the SQL database.</param>
        public DefaultSessionFactoryCache(string sqlServer, string sqlDatabase)
            : this()
        {
            this.SqlServer = sqlServer;
            this.SqlDatabase = sqlDatabase;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultSessionFactoryCache"/> class.
        /// </summary>
        protected DefaultSessionFactoryCache()
        {
            this.MappingAssemblies = new List<Assembly>();
            this.Mappings = new List<Type>();
        }

        /// <summary>
        /// Gets the list of assemblies which contain NHibernate mappings.
        /// </summary>
        public IList<Assembly> MappingAssemblies { get; private set; }

        /// <summary>
        /// Gets the additional NHibernate mappings.
        /// </summary>
        public IList<Type> Mappings { get; private set; }

        /// <summary>
        /// Gets or sets the name of the SQL Server host.
        /// </summary>
        protected string SqlServer { get; set; }

        /// <summary>
        /// Gets or sets the name of the SQL database.
        /// </summary>
        protected string SqlDatabase { get; set; }

        /// <summary>
        /// Gets the configuration of the NHibernate session factory.
        /// </summary>
        protected override FluentConfiguration Configuration
        {
            get
            {
                var config = Fluently.Configure();

#if (DEBUG)
                config = config.Database(MsSqlConfiguration.MsSql2008.ConnectionString(builder => builder
                    .Server(this.SqlServer)
                    .Database(this.SqlDatabase)
                    .TrustedConnection())
                    .AdoNetBatchSize(0)
                    .ShowSql());
#else
                config = config.Database(MsSqlConfiguration.MsSql2008.ConnectionString(builder => builder
                    .Server(this.SqlServer)
                    .Database(this.SqlDatabase)
                    .TrustedConnection()));
#endif

                return config.Mappings(this.ProvideMappings);
            }
        }

        /// <summary>
        /// Provides the mapping configuration for the NHibernate session factory.
        /// </summary>
        /// <param name="mappingConfiguration">The mapping configuration object.</param>
        protected virtual void ProvideMappings(MappingConfiguration mappingConfiguration)
        {
            if (mappingConfiguration != null)
            {
                foreach (Assembly assembly in this.MappingAssemblies)
                {
                    mappingConfiguration.FluentMappings.AddFromAssembly(assembly);
                    mappingConfiguration.HbmMappings.AddFromAssembly(assembly);
                }

                foreach (Type mapping in this.Mappings)
                {
                    mappingConfiguration.FluentMappings.Add(mapping);
                }
            }
        }
    }
}