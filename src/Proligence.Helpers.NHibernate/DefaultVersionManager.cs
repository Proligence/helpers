﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultVersionManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using global::NHibernate;
    using global::NHibernate.Criterion;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Base implementation for the <see cref="IVersionManager{TId,TEntity,TVersion}"/> interface.
    /// </summary>
    /// <typeparam name="TId">The type which represents the unique identifier of entity objects.</typeparam>
    /// <typeparam name="TData">The type which represents the entities.</typeparam>
    /// <typeparam name="TDataVersion">The type which represents the entity versions.</typeparam>
    public class DefaultVersionManager<TId, TData, TDataVersion> : NHibernateSessionHost,
        IVersionManager<TId, TData, TDataVersion>
        where TData : class, IIdentifiable<TId>, IVersioned<TId>
        where TDataVersion : class, IIdentifiable<TId>, IVersioned<TId>
    {
        /// <summary>
        /// The object used to create new <typeparamref name="TDataVersion"/> objects.
        /// </summary>
        private readonly IObjectFactory<TDataVersion> versionFactory;

        /// <summary>
        /// The object used to map property values from data object to data version objects.
        /// </summary>
        private readonly IObjectMapper<TData, TDataVersion> versionMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultVersionManager{TId,TData,TDataVersion}"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        /// <param name="versionFactory">
        /// The object used to create new <typeparamref name="TDataVersion"/> objects.
        /// </param>
        /// <param name="versionMapper">
        /// The object used to map property values from data object to data version objects.
        /// </param>
        public DefaultVersionManager(
            SessionFactoryCache factoryCache,
            IDbConnection connection,
            IObjectFactory<TDataVersion> versionFactory,
            IObjectMapper<TData, TDataVersion> versionMapper)
            : base(factoryCache, connection)
        {
            this.versionFactory = versionFactory;
            this.versionMapper = versionMapper;
        }

        /// <summary>
        /// Gets the specified entity version.
        /// </summary>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The specified version of the data object or <c>null</c> if there is no version with the specified
        /// identifier.
        /// </returns>
        public virtual TDataVersion GetVersion(TId versionId, ISession session = null, ITransaction transaction = null)
        {
            return this.UsingSession((s, t) => s.Get<TDataVersion>(versionId), session, transaction);
        }

        /// <summary>
        /// Gets the specified version of the entity.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="versionNumber">The version number of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The specified version of the data object or <c>null</c> if there is no object with the specified
        /// identifier and version number.
        /// </returns>
        public virtual TDataVersion GetVersion(
            TId objectId,
            int versionNumber,
            ISession session = null,
            ITransaction transaction = null)
        {
            return this.UsingSession(
                (s, t) => s.CreateCriteria<TDataVersion>()
                    .Add(Restrictions.Eq("Id", objectId))
                    .Add(Restrictions.Eq("Version", versionNumber))
                    .List<TDataVersion>()
                    .SingleOrDefault(),
                session,
                transaction);
        }

        /// <summary>
        /// Gets all entity versions with the specified version identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the entities to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>A sequence of entity version objects.</returns>
        public virtual IEnumerable<TDataVersion> GetManyVersions(
            IEnumerable<TId> ids,
            ISession session = null,
            ITransaction transaction = null)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }

            return this.UsingSession(
                (s, t) => s.CreateCriteria<TDataVersion>()
                    .Add(Restrictions.Eq("VersionId", ids.ToArray()))
                    .List<TDataVersion>(),
                session,
                transaction);
        }

        /// <summary>
        /// Gets the latest version of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// The latest version of the specified identity or <c>null</c> if there is no object with the specified
        /// identifier.
        /// </returns>
        public virtual TDataVersion GetLatestVersion(
            TId objectId,
            ISession session = null,
            ITransaction transaction = null)
        {
            return this.UsingSession(
                (s, t) => 
                {
                    TData dataObject = s.Get<TData>(objectId);
                    if (dataObject == null)
                    {
                        return null;
                    }

                    return s.Get<TDataVersion>(dataObject.VersionId);
                },
                session,
                transaction);
        }

        /// <summary>
        /// Gets all versions of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the entity to get.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>
        /// All versions of the specified entity or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        public IEnumerable<TDataVersion> GetAllVersions(
            TId objectId,
            ISession session = null,
            ITransaction transaction = null)
        {
            return this.UsingSession(
                (s, t) => s
                    .CreateCriteria<TDataVersion>()
                    .Add(Restrictions.Eq("Id", objectId))
                    .AddOrder(Order.Desc("Version"))
                    .List<TDataVersion>(),
                session,
                transaction);
        }

        /// <summary>
        /// Creates a new version using the current data of the entity with the specified identifier.
        /// </summary>
        /// <param name="objectId">The identifier of the object for which a new version will be created.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>The created version or <c>null</c> if there is no object with the specified identifier.</returns>
        public virtual TDataVersion CreateVersion(
            TId objectId,
            ISession session = null,
            ITransaction transaction = null)
        {
            return this.UsingSession(
                (s, t) =>
                {
                    TData currentDataObject = s.Get<TData>(objectId);
                    if (currentDataObject == null)
                    {
                        return null;
                    }

                    TDataVersion version = this.MapToDataObjectToVersion(currentDataObject);
                    version.Version = currentDataObject.Version + 1;
                    s.Save(version);

                    currentDataObject.VersionId = version.VersionId;
                    currentDataObject.Version = version.Version;
                    s.Update(currentDataObject);

                    return version;
                },
                session,
                transaction);
        }

        /// <summary>
        /// Creates or updates the specified object and creates a new version for it.
        /// </summary>
        /// <param name="dataObject">The data object to save.</param>
        /// <param name="session">
        /// The specific NHibernate session to use to access the database or <c>null</c> to use separate (new)
        /// session.
        /// </param>
        /// <param name="transaction">
        /// The specific NHibernate transaction to use to access the database or <c>null</c> to use separate (new)
        /// transaction.
        /// </param>
        /// <returns>The created version.</returns>
        public virtual TDataVersion SaveAsNewVersion(
            TData dataObject,
            ISession session = null,
            ITransaction transaction = null)
        {
            return this.UsingSession(
                (s, t) =>
                {
                    TData currentDataObject = s.Get<TData>(dataObject.Id);
                    if (currentDataObject != null)
                    {
                        if (!dataObject.VersionId.Equals(currentDataObject.VersionId))
                        {
                            string messageFormat =
                                "The update was performed on a version of the data which is no longer up-to-date."
                                + Environment.NewLine
                                + "Object Type: {0}, Object ID: {1}, "
                                + "Expected Version: {2} ({3}), Actual Version: {4} ({5})";

                            throw new DBConcurrencyException(string.Format(
                                CultureInfo.InvariantCulture,
                                messageFormat,
                                dataObject.GetType().Name,
                                dataObject.Id,
                                currentDataObject.Version,
                                currentDataObject.VersionId,
                                dataObject.Version,
                                dataObject.VersionId));
                        }

                        // Save new version
                        TDataVersion version = this.MapToDataObjectToVersion(dataObject);
                        version.Version = currentDataObject.Version + 1;
                        s.Save(version);
                        s.Flush();

                        dataObject.VersionId = version.VersionId;
                        dataObject.Version = version.Version;
                        s.Evict(currentDataObject);
                        s.Update(dataObject);

                        return version;
                    }
                    else
                    {
                        // Save new entity object

                        // NOTE: For GUID identifiers make sure to create a unique version ID instead of using an
                        // empty GUID. This will be updated in the next step, however, should the update fail and
                        // this transaction was not rolled back, an empty GUID will block all future object creation.
                        // This fixes #TS-1234.
                        if (dataObject.VersionId is Guid)
                        {
                            dataObject.VersionId = (TId)(object)Guid.NewGuid();
                        }

                        s.Save(dataObject);
                        s.Flush();

                        // Create first version for entity object
                        TDataVersion version = this.MapToDataObjectToVersion(dataObject);
                        version.VersionId = default(TId);
                        version.Version = 1;
                        s.Save(version);
                        s.Flush();

                        // Update version identifier and number of entity object
                        dataObject.VersionId = version.VersionId;
                        dataObject.Version = 1;
                        s.Update(dataObject);

                        return version;
                    }
                },
                session,
                transaction);
        }

        /// <summary>
        /// Creates a new version object with the same data as the specified data object.
        /// </summary>
        /// <param name="dataObject">The data object.</param>
        /// <returns>The created version.</returns>
        protected virtual TDataVersion MapToDataObjectToVersion(TData dataObject)
        {
            TDataVersion version = this.versionFactory.Create();
            this.versionMapper.MapProperties(dataObject, version);

            return version;
        }
    }
}