﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System.Collections.Generic;
    using global::NHibernate;

    /// <summary>
    /// Defines the API of an NHibernate data repository.
    /// </summary>
    /// <typeparam name="TObject">The type of the objects stored in the repository.</typeparam>
    /// <typeparam name="TId">The type of the identifier of the objects stored in the repository.</typeparam>
    public interface IRepository<TObject, in TId>
    {
        /// <summary>
        /// Saves the specified object in the repository.
        /// </summary>
        /// <param name="obj">The object to save.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        void Save(TObject obj, ISession session, ITransaction transaction);

        /// <summary>
        /// Saves the specified object in the repository.
        /// </summary>
        /// <param name="obj">The object to save.</param>
        void Save(TObject obj);

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        void Delete(TObject obj, ISession session, ITransaction transaction);

        /// <summary>
        /// Deletes the specified object from the repository.
        /// </summary>
        /// <param name="obj">The object to delete from the repository.</param>
        void Delete(TObject obj);

        /// <summary>
        /// Gets an object from the repository with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object to get from the repository.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier.
        /// </returns>
        TObject GetById(TId objId, ISession session, ITransaction transaction);

        /// <summary>
        /// Gets an object from the repository with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object to get from the repository.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier.
        /// </returns>
        TObject GetById(TId objId);

        /// <summary>
        /// Gets the objects from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The identifiers of the objects to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>The domain objects with the specified identifiers.</returns>
        IEnumerable<TObject> GetMany(IEnumerable<TId> ids, ISession session, ITransaction transaction);

        /// <summary>
        /// Gets the objects from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The identifiers of the objects to get.</param>
        /// <returns>The domain objects with the specified identifiers.</returns>
        IEnumerable<TObject> GetMany(IEnumerable<TId> ids);

        /// <summary>
        /// Gets all object from the repository.
        /// </summary>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>A sequence of domain objects.</returns>
        IEnumerable<TObject> GetAll(ISession session, ITransaction transaction);

        /// <summary>
        /// Gets all object from the repository.
        /// </summary>
        /// <returns>A sequence of domain objects.</returns>
        IEnumerable<TObject> GetAll();
    }
}