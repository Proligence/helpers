﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVersionedRepository.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate
{
    using System;
    using System.Collections.Generic;
    using global::NHibernate;
    using Proligence.Helpers.Data;

    /// <summary>
    /// Defines the API of an NHibernate data repository which supports entity versioning.
    /// </summary>
    /// <typeparam name="TObject">The type of the objects stored in the repository.</typeparam>
    /// <typeparam name="TId">The type of the identifier of the objects stored in the repository.</typeparam>
    public interface IVersionedRepository<TObject, in TId> : IRepository<TObject, TId>
        where TObject : IVersioned<TId>
    {
        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="versionId">The identifier of the object version to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// version identifier.
        /// </returns>
        TObject GetByVersionId(TId versionId, ISession session, ITransaction transaction);

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="versionId">The identifier of the object version to get.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// version identifier.
        /// </returns>
        TObject GetByVersionId(TId versionId);

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="objId">The identifier of the object to get.</param>
        /// <param name="versionNumber">The number of the object version to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier and version number.
        /// </returns>
        TObject GetByIdAndVersionNumber(TId objId, int versionNumber, ISession session, ITransaction transaction);

        /// <summary>
        /// Gets the specified version of an object from the repository.
        /// </summary>
        /// <param name="objId">The identifier of the object to get.</param>
        /// <param name="versionNumber">The number of the object version to get.</param>
        /// <returns>
        /// The object retrieved from the repository or <c>null</c> if no object was found with the specified 
        /// identifier and version number.
        /// </returns>
        TObject GetByIdAndVersionNumber(TId objId, int versionNumber);

        /// <summary>
        /// Gets the object versions from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the objects to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>The domain object versions with the specified identifiers.</returns>
        IEnumerable<TObject> GetManyVersions(
            IEnumerable<TId> ids,
            ISession session,
            ITransaction transaction);

        /// <summary>
        /// Gets the object versions from the repository with the specified identifiers.
        /// </summary>
        /// <param name="ids">The version identifiers of the objects to get.</param>
        /// <returns>The domain object versions with the specified identifiers.</returns>
        IEnumerable<TObject> GetManyVersions(IEnumerable<TId> ids);

        /// <summary>
        /// Gets all versions of the object with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object which versions to get.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        /// <returns>
        /// All versions of the specified object or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        IEnumerable<TObject> GetAllVersions(TId objId, ISession session, ITransaction transaction);

        /// <summary>
        /// Gets all versions of the object with the specified identifier.
        /// </summary>
        /// <param name="objId">The identifier of the object which versions to get.</param>
        /// <returns>
        /// All versions of the specified object or an empty sequence if there is no object with the specified
        /// identifier.
        /// </returns>
        IEnumerable<TObject> GetAllVersions(TId objId);

        /// <summary>
        /// Restores the specified question version.
        /// </summary>
        /// <param name="versionId">The question version to restore.</param>
        /// <param name="session">The existing NHibernate session to use.</param>
        /// <param name="transaction">The existing transaction to use.</param>
        void RestoreVersion(TId versionId, ISession session, ITransaction transaction);

        /// <summary>
        /// Restores the specified question version.
        /// </summary>
        /// <param name="versionId">The question version to restore.</param>
        void RestoreVersion(TId versionId);
    }
}