﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MockExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Testing.UnitTests
{
    using System;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="Testing.MockExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class MockExtensionsUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="Testing.MockExtensions.AssertVerificationFailure"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified mock is <c>null</c>.
        /// </summary>
        [Test]
        public void AssertVerificationFailureWhenMockNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => Testing.MockExtensions.AssertVerificationFailure(null));

            Assert.That(exception.ParamName, Is.EqualTo("mock"));
        }

        /// <summary>
        /// Tests if the <see cref="Testing.MockExtensions.AssertVerificationFailure"/> method does not throws any
        /// exception when the specified mock is not verified successfully.
        /// </summary>
        [Test]
        public void AssertVerificationFailureWhenMockVerifiable()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestMethod(It.IsAny<int>())).Returns(0);

            Assert.DoesNotThrow(mock.AssertVerificationFailure);
        }

        /// <summary>
        /// Tests if the <see cref="Testing.MockExtensions.AssertVerificationFailure"/> method throws a
        /// <see cref="AssertionException"/> when the specified mock is verified successfully.
        /// </summary>
        [Test]
        public void AssertVerificationFailureWhenMockNotVerifiable()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestMethod(It.IsAny<int>())).Returns(0);

            mock.Object.TestMethod(7);

            Assert.Throws<AssertionException>(mock.AssertVerificationFailure);
        }

        /// <summary>
        /// Test class.
        /// </summary>
        public class TestClass
        {
            /// <summary>
            /// Test method.
            /// </summary>
            /// <param name="parameter">Test parameter.</param>
            /// <returns>Test parameter value.</returns>
            public virtual int TestMethod(int parameter)
            {
                return parameter;
            }
        }
    }
}
