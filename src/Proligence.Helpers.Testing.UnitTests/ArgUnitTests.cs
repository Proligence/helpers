﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Testing.UnitTests
{
    using System;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="Arg{T}"/> class.
    /// </summary>
    [TestFixture]
    public class ArgUnitTests
    {
        /// <summary>
        /// Tests the <see cref="Arg{T}.IsAny"/> method.
        /// </summary>
        [Test]
        public void IsAny()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.GuidMethod(Arg<Guid>.IsAny())).Returns(7);

            int result = mock.Object.GuidMethod(Guid.NewGuid());

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsAny{T2}"/> method when the specified value is of the specified subclass.
        /// </summary>
        [Test]
        public void IsAnySubclassWhenValueIsOfSubclass()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsAny<TestClass2>())).Returns(7);

            var value = new TestClass2();
            int result = mock.Object.TestClassMethod(value);

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsAny{T2}"/> method when the specified value is not of the specified subclass.
        /// </summary>
        [Test]
        public void IsAnySubclassWhenValueIsNotOfSubclass()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsAny<TestClass2>())).Returns(7);

            var value = new TestClass3();
            int result = mock.Object.TestClassMethod(value);

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsNotNull"/> method when the specified value is not null.
        /// </summary>
        [Test]
        public void IsNotNullWhenValueNotNull()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsNotNull())).Returns(7);

            int result = mock.Object.TestClassMethod(new TestClass2());

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsNotNull"/> method when the specified value is null.
        /// </summary>
        [Test]
        public void IsNotNullWhenValueNull()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsNotNull())).Returns(7);

            int result = mock.Object.TestClassMethod(null);

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Tests if the <see cref="Arg{T}.Is(Func{T,bool})"/> method throws an <see cref="ArgumentNullException"/>
        /// when the specified predicate is <c>null</c>.
        /// </summary>
        [Test]
        public void IsWhenPredicateNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => Arg<TestClass>.Is((Func<TestClass, bool>)null));

            Assert.That(exception.ParamName, Is.EqualTo("predicate"));
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.Is(Func{T,bool})"/> method when the specified value satisfies the predicate.
        /// </summary>
        [Test]
        public void IsWhenPredicateSatisfied()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestMethod(Arg<int>.Is(i => i > 2))).Returns(7);

            int result = mock.Object.TestMethod(3);

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.Is(Func{T,bool})"/> method when the specified value does not satisfy the
        /// predicate.
        /// </summary>
        [Test]
        public void IsWhenPredicateNotSatisfied()
        {
            var mock = new Mock<TestClass>();
            mock.Setup(x => x.TestMethod(Arg<int>.Is(i => i > 2))).Returns(7);

            int result = mock.Object.TestMethod(2);

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Tests if the <see cref="Arg{T}.Is(T)"/> method throws an <see cref="ArgumentNullException"/> when the
        /// specified object instance is <c>null</c>.
        /// </summary>
        [Test]
        public void IsWhenInstanceNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => Arg<TestClass>.Is((TestClass)null));

            Assert.That(exception.ParamName, Is.EqualTo("instance"));
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.Is(T)"/> method when the expected object instance is same as the specified
        /// value of the mocked parameter.
        /// </summary>
        [Test]
        public void IsWhenValueSameAsInstance()
        {
            var mock = new Mock<TestClass>();

            var value = new TestClass2(7);
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.Is(value))).Returns(7);

            int result = mock.Object.TestClassMethod(value);

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.Is(T)"/> method when the expected object instance is not same as the specified
        /// value of the mocked parameter.
        /// </summary>
        [Test]
        public void IsWhenValueNotSameAsInstance()
        {
            var mock = new Mock<TestClass>();

            var value = new TestClass2(7);
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.Is(value))).Returns(7);

            int result = mock.Object.TestClassMethod(new TestClass2(7));

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Tests if the <see cref="Arg{T}.IsEqual"/> method throws an <see cref="ArgumentNullException"/> when the
        /// specified object instance is <c>null</c>.
        /// </summary>
        [Test]
        public void IsEqualWhenInstanceNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => Arg<TestClass>.IsEqual(null));

            Assert.That(exception.ParamName, Is.EqualTo("instance"));
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsEqual"/> method when the expected object instance is not equal to the
        /// specified value of the mocked parameter.
        /// </summary>
        [Test]
        public void IsEqualWhenValueEqualToInstance()
        {
            var mock = new Mock<TestClass>();

            var value = new TestClass2(7);
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsEqual(value))).Returns(7);

            int result = mock.Object.TestClassMethod(new TestClass2(7));

            Assert.That(result, Is.EqualTo(7));
            mock.VerifyAll();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsEqual"/> method when the expected object instance is not same as the
        /// specified value of the mocked parameter.
        /// </summary>
        [Test]
        public void IsEqualWhenValueNotEqualToInstance()
        {
            var mock = new Mock<TestClass>();

            var value = new TestClass2(7);
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsEqual(value))).Returns(7);

            int result = mock.Object.TestClassMethod(new TestClass2(8));

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Tests the <see cref="Arg{T}.IsEqual"/> method when the specified value of the mocked parameter is
        /// <c>null</c>.
        /// </summary>
        [Test]
        public void IsEqualWhenValueNull()
        {
            var mock = new Mock<TestClass>();

            var value = new TestClass2(7);
            mock.Setup(x => x.TestClassMethod(Arg<TestClass>.IsEqual(value))).Returns(7);

            int result = mock.Object.TestClassMethod(null);

            Assert.That(result, Is.EqualTo(default(int)));
            mock.AssertVerificationFailure();
        }

        /// <summary>
        /// Test class.
        /// </summary>
        public abstract class TestClass
        {
            /// <summary>
            /// Test method with <see cref="Guid"/> parameter.
            /// </summary>
            /// <param name="parameter">Test parameter.</param>
            /// <returns>Hash code of the specified parameter.</returns>
            public virtual int GuidMethod(Guid parameter)
            {
                return parameter.GetHashCode();
            }

            /// <summary>
            /// Test method with <see cref="TestClass"/> parameter.
            /// </summary>
            /// <param name="parameter">Test parameter.</param>
            /// <returns>Hash code of the specified parameter.</returns>
            public virtual int TestClassMethod(TestClass parameter)
            {
                return parameter.GetHashCode();
            }

            /// <summary>
            /// Test method with <see cref="int"/> parameter.
            /// </summary>
            /// <param name="parameter">Test parameter.</param>
            /// <returns>Hash code of the specified parameter.</returns>
            public virtual int TestMethod(int parameter)
            {
                return parameter.GetHashCode();
            }
        }

        /// <summary>
        /// Concrete test class.
        /// </summary>
        public class TestClass2 : TestClass
        {
            /// <summary>
            /// The test value stored in the object.
            /// </summary>
            private readonly int value;

            /// <summary>
            /// Initializes a new instance of the <see cref="TestClass2"/> class.
            /// </summary>
            public TestClass2()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TestClass2"/> class.
            /// </summary>
            /// <param name="value">The value.</param>
            public TestClass2(int value)
            {
                this.value = value;
            }

            /// <summary>
            /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
            /// </summary>
            /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
            /// <returns>
            /// <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise,
            /// <c>false</c>.
            /// </returns>
            public override bool Equals(object obj)
            {
                return ((TestClass2)obj).value == this.value;
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash
            /// table. 
            /// </returns>
            public override int GetHashCode()
            {
                return this.value;
            }
        }

        /// <summary>
        /// Another concrete test class.
        /// </summary>
        public class TestClass3 : TestClass
        {
        }
    }
}