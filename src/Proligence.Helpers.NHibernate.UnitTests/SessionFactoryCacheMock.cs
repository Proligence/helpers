﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SessionFactoryCacheMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using FluentNHibernate.Cfg;
    using Moq;
    using global::NHibernate;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements a subclass of the <see cref="SessionFactoryCache"/> class for test purposes.
    /// </summary>
    public class SessionFactoryCacheMock : SessionFactoryCache
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionFactoryCacheMock"/> class.
        /// </summary>
        public SessionFactoryCacheMock()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionFactoryCacheMock"/> class.
        /// </summary>
        /// <param name="sessionFactory">The session factory.</param>
        public SessionFactoryCacheMock(ISessionFactory sessionFactory)
        {
            this.SetField("sessionFactory", sessionFactory);
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="SessionFactoryCacheMock"/> is disposed.
        /// </summary>
        /// <value><c>true</c> if disposed; otherwise, <c>false</c>.</value>
        public bool Disposed { get; private set; }

        /// <summary>
        /// Gets the configuration of the NHibernate session factory.
        /// </summary>
        protected override FluentConfiguration Configuration
        {
            get
            {
                return new Mock<FluentConfiguration>().Object;
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged
        /// resources.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.Disposed = true;
        }
    }
}