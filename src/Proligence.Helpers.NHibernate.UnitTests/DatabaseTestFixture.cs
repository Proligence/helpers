﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseTestFixture.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using global::NHibernate;
    using NUnit.Framework;

    /// <summary>
    /// The base class for test fixtures which use an in-memory SQLite database.
    /// </summary>
    public abstract class DatabaseTestFixture
    {
        /// <summary>
        /// The name of the SQL Server host.
        /// </summary>
        private readonly string sqlServer;

        /// <summary>
        /// The name of the SQL database.
        /// </summary>
        private readonly string sqlDatabase;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseTestFixture"/> class.
        /// </summary>
        protected DatabaseTestFixture()
            : this("localhost", "Helpers_UT")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseTestFixture"/> class.
        /// </summary>
        /// <param name="sqlServer">The SQL server.</param>
        /// <param name="sqlDatabase">The SQL database.</param>
        protected DatabaseTestFixture(string sqlServer, string sqlDatabase)
        {
            this.sqlServer = sqlServer;
            this.sqlDatabase = sqlDatabase;
        }

        /// <summary>
        /// Gets or sets the NHibernate mappings.
        /// </summary>
        public Type[] Mappings { get; set; }

        /// <summary>
        /// Gets the NHibernate session factory cache.
        /// </summary>
        public DefaultSessionFactoryCache SessionFactoryCache { get; private set; }

        /// <summary>
        /// Gets the NHibernate session.
        /// </summary>
        public ISession Session { get; private set; }

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public virtual void Setup()
        {
            this.SessionFactoryCache = new DefaultSessionFactoryCache(this.sqlServer, this.sqlDatabase);
            foreach (Type mapping in this.Mappings)
            {
                this.SessionFactoryCache.Mappings.Add(mapping);
            }

            this.Session = this.SessionFactoryCache.SessionFactory.OpenSession();
            this.SetupDatabase();
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public virtual void Teardown()
        {
            this.Session.Dispose();
        }

        /// <summary>
        /// Sets up the in-memory database before each test.
        /// </summary>
        protected virtual void SetupDatabase()
        {
        }
    }
}