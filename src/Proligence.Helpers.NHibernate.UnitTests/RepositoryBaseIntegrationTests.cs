﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryBaseIntegrationTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using global::Autofac;
    using FluentNHibernate.Mapping;
    using Moq;
    using NUnit.Framework;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="RepositoryBase{TObject,TData,TId}"/> class.
    /// </summary>
    [TestFixture]
    [Category("Integration")]
    public class RepositoryBaseIntegrationTests : DatabaseTestFixture
    {
        /// <summary>
        /// The tested <see cref="RepositoryBase{TObject,TData,TId}"/>.
        /// </summary>
        private TestRepository repository;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        public override void Setup()
        {
            this.Mappings = new[] { typeof(TestEntityDataMap) };
            base.Setup();

            var componentContext = new Mock<IComponentContext>();

            this.repository = new TestRepository(
                this.SessionFactoryCache,
                new SqlConnection(this.Session.Connection.ConnectionString),
                new CtorObjectFactory<TestEntityData>(),
                new CtorObjectFactory<TestEntity>(), 
                new DefaultObjectMapper<TestEntity, TestEntityData>(componentContext.Object), 
                new DefaultObjectMapper<TestEntityData, TestEntity>(componentContext.Object));
        }

        /// <summary>
        /// Tests if objects are saved correctly in the repository.
        /// </summary>
        [Test]
        public void SaveAndGetObjectFromRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };

            this.repository.Save(obj);

            obj = this.repository.GetById(obj.Id);
            Assert.That(obj.Id, Is.Not.EqualTo(Guid.Empty));
            Assert.That(obj.Col1, Is.EqualTo("foo"));
            Assert.That(obj.Col2, Is.EqualTo("bar"));
            Assert.That(obj.Col3, Is.EqualTo("baz"));
        }

        /// <summary>
        /// Tests if objects are updated correctly in the repository.
        /// </summary>
        [Test]
        public void UpdateObjectInRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };
            this.repository.Save(obj);
            Guid id = obj.Id;
            
            obj.Col1 = "baz";
            obj.Col2 = "quux";
            obj.Col3 = "foo";
            this.repository.Save(obj);

            obj = this.repository.GetById(obj.Id);
            Assert.That(obj.Id, Is.EqualTo(id));
            Assert.That(obj.Col1, Is.EqualTo("baz"));
            Assert.That(obj.Col2, Is.EqualTo("quux"));
            Assert.That(obj.Col3, Is.EqualTo("foo"));
        }

        /// <summary>
        /// Tests if objects are deleted correctly from the repository.
        /// </summary>
        [Test]
        public void DeleteObjectFromRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };
            this.repository.Save(obj);
            Guid id = obj.Id;

            this.repository.Delete(obj);

            obj = this.repository.GetById(id);
            Assert.That(obj, Is.Null);
        }
        
        /// <summary>
        /// Sets up the in-memory database before each test.
        /// </summary>
        protected override void SetupDatabase()
        {
            string sql = File.ReadAllText("RepositoryDatabase.sql");
            using (IDbCommand command = this.Session.Connection.CreateCommand())
            {
                command.Connection.ChangeDatabase("Helpers_UT");
                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Test entity.
        /// </summary>
        public class TestEntity : IIdentifiable<Guid>
        {
            /// <summary>
            /// Gets or sets the object's identifier.
            /// </summary>
            public Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the first test value.
            /// </summary>
            public string Col1 { get; set; }

            /// <summary>
            /// Gets or sets the second test value.
            /// </summary>
            public string Col2 { get; set; }

            /// <summary>
            /// Gets or sets the third test value.
            /// </summary>
            public string Col3 { get; set; }
        }

        /// <summary>
        /// Test entity data.
        /// </summary>
        public class TestEntityData : IIdentifiable<Guid>
        {
            /// <summary>
            /// Gets or sets the object's identifier.
            /// </summary>
            public virtual Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the first test value.
            /// </summary>
            public virtual string Col1 { get; set; }

            /// <summary>
            /// Gets or sets the second test value.
            /// </summary>
            public virtual string Col2 { get; set; }

            /// <summary>
            /// Gets or sets the third test value.
            /// </summary>
            public virtual string Col3 { get; set; }
        }

        /// <summary>
        /// NHibernate mapping for <see cref="TestEntityData"/>.
        /// </summary>
        public class TestEntityDataMap : ClassMap<TestEntityData>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestEntityDataMap"/> class.
            /// </summary>
            public TestEntityDataMap()
            {
                this.Table("Test");
                this.Polymorphism.Explicit();
                this.Id(x => x.Id).GeneratedBy.GuidComb();
                this.Map(x => x.Col1);
                this.Map(x => x.Col2);
                this.Map(x => x.Col3);
            }
        }

        /// <summary>
        /// Test repository.
        /// </summary>
        private class TestRepository : RepositoryBase<TestEntity, TestEntityData, Guid>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestRepository"/> class.
            /// </summary>
            /// <param name="factoryCache">The factory cache.</param>
            /// <param name="connection">The connection.</param>
            /// <param name="dataObjectFactory">The data object factory.</param>
            /// <param name="domainObjectFactory">The domain object factory.</param>
            /// <param name="dataObjectMapper">The data object mapper.</param>
            /// <param name="domainObjectMapper">The domain object mapper.</param>
            public TestRepository(
                SessionFactoryCache factoryCache,
                IDbConnection connection,
                IObjectFactory<TestEntityData> dataObjectFactory,
                IObjectFactory<TestEntity> domainObjectFactory,
                IObjectMapper<TestEntity, TestEntityData> dataObjectMapper,
                IObjectMapper<TestEntityData, TestEntity> domainObjectMapper)
                : base(
                    factoryCache,
                    connection,
                    dataObjectFactory,
                    domainObjectFactory,
                    dataObjectMapper,
                    domainObjectMapper)
            {
            }
        }
    }
}