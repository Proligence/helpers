﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VersionedRepositoryBaseIntegrationTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using global::Autofac;
    using FluentNHibernate.Mapping;
    using Moq;
    using NUnit.Framework;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="VersionedRepositoryBase{TObject,TData,TVersionData,TId}"/> class.
    /// </summary>
    [TestFixture]
    [Category("Integration")]
    public class VersionedRepositoryBaseIntegrationTests : DatabaseTestFixture
    {
        /// <summary>
        /// The tested <see cref="RepositoryBase{TObject,TData,TId}"/>.
        /// </summary>
        private TestRepository repository;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        public override void Setup()
        {
            this.Mappings = new[] { typeof(TestEntityDataMap), typeof(TestEntityVersionDataMap) };
            base.Setup();

            var componentContext = new Mock<IComponentContext>();

            this.repository = new TestRepository(
                this.SessionFactoryCache,
                new SqlConnection(this.Session.Connection.ConnectionString),
                new DefaultVersionManager<Guid, TestEntityData, TestEntityVersionData>(
                    this.SessionFactoryCache,
                    new SqlConnection(this.Session.Connection.ConnectionString),
                    new CtorObjectFactory<TestEntityVersionData>(), 
                    new DefaultObjectMapper<TestEntityData, TestEntityVersionData>(componentContext.Object)),
                new CtorObjectFactory<TestEntityData>(),
                new CtorObjectFactory<TestEntity>(),
                new DefaultObjectMapper<TestEntity, TestEntityData>(componentContext.Object),
                new DefaultObjectMapper<TestEntityData, TestEntity>(componentContext.Object),
                new DefaultObjectMapper<TestEntityVersionData, TestEntity>(componentContext.Object));
        }

        /// <summary>
        /// Tests if objects are saved correctly in the repository.
        /// </summary>
        [Test]
        public void SaveAndGetObjectFromRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };

            this.repository.Save(obj);

            obj = this.repository.GetById(obj.Id);
            Assert.That(obj.Id, Is.Not.EqualTo(Guid.Empty));
            Assert.That(obj.VersionId, Is.Not.EqualTo(Guid.Empty));
            Assert.That(obj.VersionId, Is.Not.EqualTo(obj.Id));
            Assert.That(obj.Version, Is.EqualTo(1));
            Assert.That(obj.Col1, Is.EqualTo("foo"));
            Assert.That(obj.Col2, Is.EqualTo("bar"));
            Assert.That(obj.Col3, Is.EqualTo("baz"));
        }

        /// <summary>
        /// Tests if objects are updated correctly in the repository.
        /// </summary>
        [Test]
        public void UpdateObjectInRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };
            this.repository.Save(obj);
            Guid id = obj.Id;
            Guid versionId = obj.VersionId;

            obj.Col1 = "baz";
            obj.Col2 = "quux";
            obj.Col3 = "foo";
            this.repository.Save(obj);

            obj = this.repository.GetById(obj.Id);
            Assert.That(obj.Id, Is.EqualTo(id));
            Assert.That(obj.VersionId, Is.Not.EqualTo(versionId));
            Assert.That(obj.Version, Is.EqualTo(2));
            Assert.That(obj.Col1, Is.EqualTo("baz"));
            Assert.That(obj.Col2, Is.EqualTo("quux"));
            Assert.That(obj.Col3, Is.EqualTo("foo"));

            TestEntity version1 = this.repository.GetByIdAndVersionNumber(id, 1);
            Assert.That(version1.Id, Is.EqualTo(id));
            Assert.That(version1.VersionId, Is.EqualTo(versionId));
            Assert.That(version1.Version, Is.EqualTo(1));
            Assert.That(version1.Col1, Is.EqualTo("foo"));
            Assert.That(version1.Col2, Is.EqualTo("bar"));
            Assert.That(version1.Col3, Is.EqualTo("baz"));

            TestEntity version2 = this.repository.GetByIdAndVersionNumber(id, 2);
            Assert.That(version2.Id, Is.EqualTo(id));
            Assert.That(version2.VersionId, Is.EqualTo(obj.VersionId));
            Assert.That(version2.Version, Is.EqualTo(2));
            Assert.That(version2.Col1, Is.EqualTo("baz"));
            Assert.That(version2.Col2, Is.EqualTo("quux"));
            Assert.That(version2.Col3, Is.EqualTo("foo"));
        }

        /// <summary>
        /// Tests if unchanged objects are not updated.
        /// </summary>
        [Test]
        public void UpdateUnchangedObject()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };
            this.repository.Save(obj);
            Guid id = obj.Id;
            Guid versionId = obj.VersionId;

            this.repository.Save(obj);

            obj = this.repository.GetById(obj.Id);
            Assert.That(obj.Id, Is.EqualTo(id));
            Assert.That(obj.VersionId, Is.EqualTo(versionId));
            Assert.That(obj.Version, Is.EqualTo(1));
            Assert.That(obj.Col1, Is.EqualTo("foo"));
            Assert.That(obj.Col2, Is.EqualTo("bar"));
            Assert.That(obj.Col3, Is.EqualTo("baz"));
        }

        /// <summary>
        /// Tests if objects are deleted correctly from the repository.
        /// </summary>
        [Test]
        public void DeleteObjectFromRepository()
        {
            var obj = new TestEntity { Col1 = "foo", Col2 = "bar", Col3 = "baz" };
            this.repository.Save(obj);
            Guid id = obj.Id;
            Guid versionId = obj.VersionId;

            this.repository.Delete(obj);

            obj = this.repository.GetById(id);
            Assert.That(obj, Is.Null);

            TestEntity version2 = this.repository.GetByIdAndVersionNumber(id, 2);
            Assert.That(version2.Id, Is.EqualTo(id));
            Assert.That(version2.VersionId, Is.Not.EqualTo(versionId));
            Assert.That(version2.Version, Is.EqualTo(2));
            Assert.That(version2.Col1, Is.EqualTo("foo"));
            Assert.That(version2.Col2, Is.EqualTo("bar"));
            Assert.That(version2.Col3, Is.EqualTo("baz"));
        }

        /// <summary>
        /// Tests if object versions are correctly retrieved by version ID.
        /// </summary>
        [Test]
        public void GetObjectVersionByVersionId()
        {
            var objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            var versionId = Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16");
            
            TestEntity version = this.repository.GetByVersionId(versionId);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(2));
            Assert.That(version.Col1, Is.EqualTo("One"));
            Assert.That(version.Col2, Is.EqualTo("Four"));
            Assert.That(version.Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Tests if object versions are correctly retrieved by object ID version number.
        /// </summary>
        [Test]
        public void GetObjectVersionByObjectIdAndVersionNumber()
        {
            var objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            var versionId = Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16");

            TestEntity version = this.repository.GetByIdAndVersionNumber(objectId, 2);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(2));
            Assert.That(version.Col1, Is.EqualTo("One"));
            Assert.That(version.Col2, Is.EqualTo("Four"));
            Assert.That(version.Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Tests if all versions of the object with the specified identifier are retrieved correctly.
        /// </summary>
        [Test]
        public void GetAllVersions()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");

            TestEntity[] versions = this.repository.GetAllVersions(objectId).ToArray();

            Assert.That(versions.Length, Is.EqualTo(3));

            Assert.That(versions[0].Id, Is.EqualTo(objectId));
            Assert.That(versions[0].VersionId, Is.EqualTo(Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718")));
            Assert.That(versions[0].Version, Is.EqualTo(3));
            Assert.That(versions[0].Col1, Is.EqualTo("Three"));
            Assert.That(versions[0].Col2, Is.EqualTo("Five"));
            Assert.That(versions[0].Col3, Is.EqualTo("Four"));

            Assert.That(versions[1].Id, Is.EqualTo(objectId));
            Assert.That(versions[1].VersionId, Is.EqualTo(Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16")));
            Assert.That(versions[1].Version, Is.EqualTo(2));
            Assert.That(versions[1].Col1, Is.EqualTo("One"));
            Assert.That(versions[1].Col2, Is.EqualTo("Four"));
            Assert.That(versions[1].Col3, Is.EqualTo("Three"));

            Assert.That(versions[2].Id, Is.EqualTo(objectId));
            Assert.That(versions[2].VersionId, Is.EqualTo(Guid.Parse("2A2CA386-153E-45F3-8E41-5989F93F974D")));
            Assert.That(versions[2].Version, Is.EqualTo(1));
            Assert.That(versions[2].Col1, Is.EqualTo("One"));
            Assert.That(versions[2].Col2, Is.EqualTo("Two"));
            Assert.That(versions[2].Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Sets up the in-memory database before each test.
        /// </summary>
        protected override void SetupDatabase()
        {
            string sql = File.ReadAllText("VersionedRepositoryDatabase.sql");
            using (IDbCommand command = this.Session.Connection.CreateCommand())
            {
                command.Connection.ChangeDatabase("Helpers_UT");
                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Test entity.
        /// </summary>
        public class TestEntity : IIdentifiable<Guid>, IVersioned<Guid>
        {
            /// <summary>
            /// Gets or sets the object's identifier.
            /// </summary>
            public Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the object's version identifier.
            /// </summary>
            public Guid VersionId { get; set; }

            /// <summary>
            /// Gets or sets the object's version number.
            /// </summary>
            public int Version { get; set; }

            /// <summary>
            /// Gets or sets the first test value.
            /// </summary>
            public string Col1 { get; set; }

            /// <summary>
            /// Gets or sets the second test value.
            /// </summary>
            public string Col2 { get; set; }

            /// <summary>
            /// Gets or sets the third test value.
            /// </summary>
            public string Col3 { get; set; }
        }

        /// <summary>
        /// Test entity data.
        /// </summary>
        public class TestEntityData : IIdentifiable<Guid>, IVersioned<Guid>
        {
            /// <summary>
            /// Gets or sets the object's identifier.
            /// </summary>
            public virtual Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the object's version identifier.
            /// </summary>
            public virtual Guid VersionId { get; set; }

            /// <summary>
            /// Gets or sets the object's version number.
            /// </summary>
            public virtual int Version { get; set; }

            /// <summary>
            /// Gets or sets the first test value.
            /// </summary>
            public virtual string Col1 { get; set; }

            /// <summary>
            /// Gets or sets the second test value.
            /// </summary>
            public virtual string Col2 { get; set; }

            /// <summary>
            /// Gets or sets the third test value.
            /// </summary>
            public virtual string Col3 { get; set; }
        }

        /// <summary>
        /// Test entity version data.
        /// </summary>
        public class TestEntityVersionData : TestEntityData
        {
        }

        /// <summary>
        /// NHibernate mapping for <see cref="TestEntityData"/>.
        /// </summary>
        public class TestEntityDataMap : ClassMap<TestEntityData>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestEntityDataMap"/> class.
            /// </summary>
            public TestEntityDataMap()
            {
                this.Table("Test");
                this.Polymorphism.Explicit();
                this.Id(x => x.Id).GeneratedBy.GuidComb();
                this.Map(x => x.VersionId);
                this.Map(x => x.Version);
                this.Map(x => x.Col1);
                this.Map(x => x.Col2);
                this.Map(x => x.Col3);
            }
        }

        /// <summary>
        /// NHibernate mapping for <see cref="TestEntityVersionDataMap"/>.
        /// </summary>
        public class TestEntityVersionDataMap : ClassMap<TestEntityVersionData>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestEntityVersionDataMap"/> class.
            /// </summary>
            public TestEntityVersionDataMap()
            {
                this.Table("TestVersions");
                this.Id(x => x.VersionId).GeneratedBy.GuidComb();
                this.Map(x => x.Id);
                this.Map(x => x.Version);
                this.Map(x => x.Col1);
                this.Map(x => x.Col2);
                this.Map(x => x.Col3);
            }
        }

        /// <summary>
        /// Test repository.
        /// </summary>
        private class TestRepository : VersionedRepositoryBase<TestEntity, TestEntityData, TestEntityVersionData, Guid>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestRepository"/> class.
            /// </summary>
            /// <param name="factoryCache">The factory cache.</param>
            /// <param name="connection">The connection.</param>
            /// <param name="versionManager">The version manager.</param>
            /// <param name="dataObjectFactory">The data object factory.</param>
            /// <param name="domainObjectFactory">The domain object factory.</param>
            /// <param name="dataObjectMapper">The data object mapper.</param>
            /// <param name="domainObjectMapper">The domain object mapper.</param>
            /// <param name="versionDataObjectMapper">The version data object mapper.</param>
            public TestRepository(
                SessionFactoryCache factoryCache,
                IDbConnection connection,
                IVersionManager<Guid, TestEntityData, TestEntityVersionData> versionManager,
                IObjectFactory<TestEntityData> dataObjectFactory,
                IObjectFactory<TestEntity> domainObjectFactory,
                IObjectMapper<TestEntity, TestEntityData> dataObjectMapper,
                IObjectMapper<TestEntityData, TestEntity> domainObjectMapper,
                IObjectMapper<TestEntityVersionData, TestEntity> versionDataObjectMapper)
                : base(
                    factoryCache,
                    connection,
                    versionManager,
                    dataObjectFactory,
                    domainObjectFactory,
                    dataObjectMapper,
                    domainObjectMapper,
                    versionDataObjectMapper)
            {
            }
        }
    }
}