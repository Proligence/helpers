﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NHibernateSessionHostUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using global::NHibernate;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="NHibernateSessionHost"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Test fixture.")]
    public class NHibernateSessionHostUnitTests
    {
        /// <summary>
        /// The SQL connection mock.
        /// </summary>
        private Mock<IDbConnection> connection;

        /// <summary>
        /// Indicates whether the mocked SQL connection is open.
        /// </summary>
        private bool connectionOpen;

        /// <summary>
        /// The NHibernate session factory mock.
        /// </summary>
        private Mock<ISessionFactory> sessionFactory;

        /// <summary>
        /// The NHibernate session mock.
        /// </summary>
        private Mock<ISession> session;

        /// <summary>
        /// Indicates whether the mocked NHibernate session was disposed.
        /// </summary>
        private bool sessionDisposed;

        /// <summary>
        /// Indicates whether the mocked NHibernate session was flushed.
        /// </summary>
        private bool sessionFlushed;

        /// <summary>
        /// The NHibernate transaction mock.
        /// </summary>
        private Mock<ITransaction> transaction;

        /// <summary>
        /// Indicates whether the mocked NHibernate transaction was committed.
        /// </summary>
        private bool transactionCommited;

        /// <summary>
        /// Indicates whether the mocked NHibernate transaction was rolled back.
        /// </summary>
        private bool transactionRollbacked;

        /// <summary>
        /// The NHibernate session factory cache mock.
        /// </summary>
        private SessionFactoryCacheMock sessionFactoryCache;

        /// <summary>
        /// The tested <see cref="NHibernateSessionHost"/> instance.
        /// </summary>
        private NHibernateSessionHostMock sessionHost;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.connection = new Mock<IDbConnection>();
            this.connection.Setup(c => c.Open()).Callback(() => this.connectionOpen = true);
            this.connection.Setup(c => c.Close()).Callback(() => this.connectionOpen = false);
            this.connection.Setup(c => c.Dispose()).Callback(() => this.connectionOpen = false);

            this.session = null;
            this.transaction = null;

            this.sessionFactory = new Mock<ISessionFactory>();
            this.sessionFactory.Setup(f => f.OpenSession(this.connection.Object)).Returns(this.CreateSessionMock);

            this.sessionFactoryCache = new SessionFactoryCacheMock(this.sessionFactory.Object);
            
            this.sessionHost = new NHibernateSessionHostMock(this.sessionFactoryCache, this.connection.Object);
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="NHibernateSessionHost"/> class properly initializes new
        /// instances.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", 
            Justification = "Test")]
        public void TestConstructor()
        {
            var host = new NHibernateSessionHostMock(this.sessionFactoryCache, this.connection.Object);
            
            Assert.That(host.FactoryCache, Is.SameAs(this.sessionFactoryCache));
            Assert.That(host.Connection, Is.SameAs(this.connection.Object));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="NHibernateSessionHost"/> class throws a 
        /// <see cref="ArgumentNullException"/> when the specified <see cref="SessionFactoryCache"/> is <c>null</c>.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", 
            Justification = "Test")]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Test")]
        public void TestConstructorWhenFactoryCacheNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new NHibernateSessionHostMock(null, this.connection.Object));

            Assert.That(exception.ParamName, Is.EqualTo("factoryCache"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="NHibernateSessionHost"/> class throws a 
        /// <see cref="ArgumentNullException"/> when the specified <see cref="IDbConnection"/> is <c>null</c>.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", 
            Justification = "Test")]
        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Justification = "Test")]
        public void TestConstructorWhenConnectionNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new NHibernateSessionHostMock(this.sessionFactoryCache, null));

            Assert.That(exception.ParamName, Is.EqualTo("connection"));
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession(Action{ISession})"/> method.
        /// </summary>
        [Test]
        public void UsingSessionWithoutResult()
        {
            bool actionCalled = false;
            this.sessionHost.InvokeUsingSession(
                s =>
                {
                    Assert.That(s, Is.Not.Null);
                    Assert.That(s, Is.EqualTo(this.session.Object));
                    Assert.That(this.transaction, Is.Null);
                    Assert.That(this.connectionOpen, Is.True);
                    Assert.That(this.sessionDisposed, Is.False);
                    Assert.That(this.sessionFlushed, Is.False);
                    actionCalled = true;
                });

            Assert.That(actionCalled, Is.True);
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.sessionFlushed, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession(Action{ISession})"/> method when an exception is
        /// thrown in the specified action.
        /// </summary>
        [Test]
        public void UsingSessionWithoutResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingSession(s => { throw exception; }));
            
            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession{TResult}(Func{ISession,TResult})"/> method.
        /// </summary>
        [Test]
        public void UsingSessionWithResult()
        {
            string result = this.sessionHost.InvokeUsingSession(
                s =>
                {
                    Assert.That(s, Is.Not.Null);
                    Assert.That(s, Is.EqualTo(this.session.Object));
                    Assert.That(this.transaction, Is.Null);
                    Assert.That(this.connectionOpen, Is.True);
                    Assert.That(this.sessionDisposed, Is.False);
                    Assert.That(this.sessionFlushed, Is.False);
                    return "Test";
                });

            Assert.That(result, Is.EqualTo("Test"));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.sessionFlushed, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession{TResult}(Func{ISession,TResult})"/> method when an
        /// exception is thrown in the specified action.
        /// </summary>
        [Test]
        public void UsingSessionWithResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingSession((Func<ISession, string>)(s => { throw exception; })));
            
            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession(Action{ISession,ITransaction})"/> method.
        /// </summary>
        [Test]
        public void UsingSessionAndTransactionWithoutResult()
        {
            bool actionCalled = false;
            this.sessionHost.InvokeUsingSession(
                (s, t) =>
                {
                    Assert.That(s, Is.Not.Null);
                    Assert.That(s, Is.EqualTo(this.session.Object));
                    Assert.That(t, Is.Not.Null);
                    Assert.That(t, Is.EqualTo(this.transaction.Object));
                    Assert.That(this.connectionOpen, Is.True);
                    Assert.That(this.sessionDisposed, Is.False);
                    Assert.That(this.sessionFlushed, Is.False);
                    Assert.That(this.transactionCommited, Is.False);
                    Assert.That(this.transactionRollbacked, Is.False);
                    actionCalled = true;
                });

            Assert.That(actionCalled, Is.True);
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.sessionFlushed, Is.True);
            Assert.That(this.transactionCommited, Is.True);
            Assert.That(this.transactionRollbacked, Is.False);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession(System.Action{ISession,ITransaction})"/> method
        /// when an exception is thrown in the specified action.
        /// </summary>
        [Test]
        public void UsingSessionAndTransactionWithoutResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingSession(
                    (s, t) =>
                    {
                        throw exception;
                    }));
            
            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.transactionCommited, Is.False);
            Assert.That(this.transactionRollbacked, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession{TResult}(Func{ISession,ITransaction,TResult})"/>
        /// method.
        /// </summary>
        [Test]
        public void UsingSessionAndTransactionWithResult()
        {
            string result = this.sessionHost.InvokeUsingSession(
                (s, t) =>
                {
                    Assert.That(s, Is.Not.Null);
                    Assert.That(s, Is.EqualTo(this.session.Object));
                    Assert.That(t, Is.Not.Null);
                    Assert.That(t, Is.EqualTo(this.transaction.Object));
                    Assert.That(this.connectionOpen, Is.True);
                    Assert.That(this.sessionDisposed, Is.False);
                    Assert.That(this.sessionFlushed, Is.False);
                    Assert.That(this.transactionCommited, Is.False);
                    Assert.That(this.transactionRollbacked, Is.False);
                    return "Test";
                });

            Assert.That(result, Is.EqualTo("Test"));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.sessionFlushed, Is.True);
            Assert.That(this.transactionCommited, Is.True);
            Assert.That(this.transactionRollbacked, Is.False);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingSession{TResult}(Func{ISession,TResult})"/> method when an
        /// exception is thrown in the specified action.
        /// </summary>
        [Test]
        public void UsingSessionAndTransactionWithResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingSession((Func<ISession, ITransaction, string>)(
                    (s, t) =>
                    {
                        throw exception;
                    })));
            
            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.connectionOpen, Is.False);
            Assert.That(this.sessionDisposed, Is.True);
            Assert.That(this.transactionCommited, Is.False);
            Assert.That(this.transactionRollbacked, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingTransaction"/> method.
        /// </summary>
        [Test]
        public void UsingTransactionWithoutResult()
        {
            bool actionCalled = false;
            this.sessionHost.InvokeUsingTransaction(
                this.CreateSessionMock(), 
                s =>
                {
                    Assert.That(s, Is.Not.Null);
                    Assert.That(s, Is.EqualTo(this.session.Object));
                    Assert.That(this.transactionCommited, Is.False);
                    Assert.That(this.transactionRollbacked, Is.False);
                    actionCalled = true;
                });

            Assert.That(actionCalled, Is.True);
            Assert.That(this.transactionCommited, Is.True);
            Assert.That(this.transactionRollbacked, Is.False);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingTransaction"/> method when an exception is thrown in the
        /// specified action.
        /// </summary>
        [Test]
        public void UsingTransactionWithoutResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingTransaction(this.CreateSessionMock(), s => { throw exception; }));

            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.transactionCommited, Is.False);
            Assert.That(this.transactionRollbacked, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingTransaction{TResult}"/> method.
        /// </summary>
        [Test]
        public void UsingTransactionWithResult()
        {
            Func<ISession, string> action = s =>
            {
                Assert.That(s, Is.Not.Null);
                Assert.That(s, Is.EqualTo(this.session.Object));
                Assert.That(this.transactionCommited, Is.False);
                Assert.That(this.transactionRollbacked, Is.False);
                return "Test";
            };

            string result = this.sessionHost.InvokeUsingTransaction(this.CreateSessionMock(), action);

            Assert.That(result, Is.EqualTo("Test"));
            Assert.That(this.transactionCommited, Is.True);
            Assert.That(this.transactionRollbacked, Is.False);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.UsingTransaction{TResult}"/> method when an exception is thrown
        /// in the specified action.
        /// </summary>
        [Test]
        public void UsingTransactionWithResultWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception");

            ISession sessionMock = this.CreateSessionMock();
            Func<ISession, string> action = s => { throw exception; };
            InvalidOperationException thrown = Assert.Throws<InvalidOperationException>(
                () => this.sessionHost.InvokeUsingTransaction(sessionMock, action));

            Assert.That(thrown, Is.SameAs(exception));
            Assert.That(this.transactionCommited, Is.False);
            Assert.That(this.transactionRollbacked, Is.True);
        }

        /// <summary>
        /// Tests the <see cref="NHibernateSessionHost.CreateConnection"/> method.
        /// </summary>
        [Test]
        public void TestCreateConnection()
        {
            IDbConnection conn = this.sessionHost.InvokeCreateConnection("Data Source=TEST");
            
            Assert.That(conn.ConnectionString, Is.EqualTo("Data Source=TEST"));
        }

        /// <summary>
        /// Tests if the <see cref="NHibernateSessionHost.Dispose"/> method properly disposes all disposable
        /// resources.
        /// </summary>
        [Test]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", 
            Justification = "Unit test.")]
        public void TestDispose()
        {
            var factoryCacheMock = new SessionFactoryCacheMock();

            bool connectionDisposed = false;
            this.connection.Setup(x => x.Dispose()).Callback(() => connectionDisposed = true);

            this.sessionHost = new NHibernateSessionHostMock(factoryCacheMock, this.connection.Object);

            this.sessionHost.Dispose();
            
            Assert.That(factoryCacheMock.Disposed, Is.True);
            Assert.That(connectionDisposed, Is.True);
        }

        /// <summary>
        /// Creates a new NHibernate session mock.
        /// </summary>
        /// <returns>The created mock.</returns>
        private ISession CreateSessionMock()
        {
            this.session = new Mock<ISession>();

            this.sessionDisposed = false;
            this.session.Setup(s => s.Dispose()).Callback(() => this.sessionDisposed = true);

            this.sessionFlushed = false;
            this.session.Setup(s => s.Flush()).Callback(() => this.sessionFlushed = true);

            this.session.Setup(s => s.BeginTransaction()).Returns(this.CreateTransactionMock);
                    
            return this.session.Object;
        }

        /// <summary>
        /// Creates a new NHibernate transaction mock.
        /// </summary>
        /// <returns>The created mock.</returns>
        private ITransaction CreateTransactionMock()
        {
            this.transaction = new Mock<ITransaction>();
                            
            this.transactionCommited = false;
            this.transaction.Setup(t => t.Commit()).Callback(() => this.transactionCommited = true);

            this.transactionRollbacked = false;
            this.transaction.Setup(t => t.Rollback()).Callback(() => this.transactionRollbacked = true);

            this.transaction.SetupGet(t => t.IsActive)
                .Returns(!this.transactionCommited && !this.transactionRollbacked);

            return this.transaction.Object;
        }
    }
}