﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SessionFactoryCacheUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System.Diagnostics.CodeAnalysis;
    using Moq;
    using global::NHibernate;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="SessionFactoryCache"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", 
        Justification = "Test fixture.")]
    public class SessionFactoryCacheUnitTests
    {
        /// <summary>
        /// The NHibernate session factory mock.
        /// </summary>
        private Mock<ISessionFactory> sessionFactory;

        /// <summary>
        /// The tested <see cref="SessionFactoryCache"/> instance.
        /// </summary>
        private SessionFactoryCacheMock factoryCache;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.sessionFactory = new Mock<ISessionFactory>();
            this.factoryCache = new SessionFactoryCacheMock(this.sessionFactory.Object);
        }
    
        /// <summary>
        /// Tests if the <see cref="SessionFactoryCache.Dispose"/> method properly disposes of all disposable
        /// resources.
        /// </summary>
        [Test]
        public void TestDispose()
        {
            bool sessionFactoryDisposed = false;
            this.sessionFactory.Setup(x => x.Dispose()).Callback(() => sessionFactoryDisposed = true);

            this.factoryCache.Dispose();

            Assert.That(sessionFactoryDisposed, Is.True);
        }
    }
}