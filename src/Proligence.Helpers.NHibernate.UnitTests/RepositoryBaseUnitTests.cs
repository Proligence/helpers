﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryBaseUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Data;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="RepositoryBase"/> class.
    /// </summary>
    [TestFixture]
    public class RepositoryBaseUnitTests
    {
        /// <summary>
        /// Session factory cache mock.
        /// </summary>
        private Mock<SessionFactoryCache> sessionFactoryCacheMock;

        /// <summary>
        /// SQL connection mock.
        /// </summary>
        private Mock<IDbConnection> connectionMock;

        /// <summary>
        /// The repository instance.
        /// </summary>
        private RepositoryMock repository;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.sessionFactoryCacheMock = new Mock<SessionFactoryCache>();
            this.connectionMock = new Mock<IDbConnection>();
            this.repository = new RepositoryMock(this.sessionFactoryCacheMock.Object, this.connectionMock.Object);
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="RepositoryBase"/> class correctly initializes the 
        /// <see cref="RepositoryBase.FactoryCache"/> property.
        /// </summary>
        [Test]
        public void SessionFactoryCacheInitialization()
        {
            Assert.That(this.repository.FactoryCache, Is.SameAs(this.sessionFactoryCacheMock.Object));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="RepositoryBase"/> class throws a 
        /// <see cref="ArgumentNullException"/> if the specified session factory cache is <c>null</c>.
        /// </summary>
        [Test]
        public void TryInitializeWithNullSessionFactoryCache()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => new RepositoryMock(null, this.connectionMock.Object));
            
            Assert.AreEqual(exception.ParamName, "factoryCache");
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="RepositoryBase"/> class correctly initializes the 
        /// <see cref="RepositoryBase.Connection"/> property.
        /// </summary>
        [Test]
        public void ConnectionInitialization()
        {
            Assert.That(this.repository.Connection, Is.SameAs(this.connectionMock.Object));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="RepositoryBase"/> class throws a 
        /// <see cref="ArgumentNullException"/> if the specified connection is <c>null</c>.
        /// </summary>
        [Test]
        public void TryInitializeWithNullConnection()
        {
            var exception = Assert.Throws<ArgumentNullException>(
                () => new RepositoryMock(this.sessionFactoryCacheMock.Object, null));

            Assert.AreEqual(exception.ParamName, "connection");
        }
    }
}