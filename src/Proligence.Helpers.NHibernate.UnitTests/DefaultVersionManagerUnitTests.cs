﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultVersionManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using global::Autofac;
    using FluentNHibernate.Mapping;
    using Moq;
    using NUnit.Framework;
    using Proligence.Helpers.Data;
    using Proligence.Helpers.Reflection;

    /// <summary>
    /// Implements unit tests for the <see cref="DefaultVersionManager{TId,TData,TDataVersion}"/> class.
    /// </summary>
    [TestFixture]
    [Category("Integration")]
    public class DefaultVersionManagerUnitTests : DatabaseTestFixture
    {
        /// <summary>
        /// The tested <see cref="DefaultVersionManager{TId,TData,TDataVersion}"/>.
        /// </summary>
        private DefaultVersionManager<Guid, TestEntityData, TestEntityVersionData> versionManager;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        public override void Setup()
        {
            this.Mappings = new[] { typeof(TestEntityDataMap), typeof(TestEntityVersionDataMap) };
            base.Setup();

            var componentContext = new Mock<IComponentContext>();

            this.versionManager = new DefaultVersionManager<Guid, TestEntityData, TestEntityVersionData>(
                this.SessionFactoryCache,
                new SqlConnection(this.Session.Connection.ConnectionString),
                new CtorObjectFactory<TestEntityVersionData>(), 
                new DefaultObjectMapper<TestEntityData, TestEntityVersionData>(componentContext.Object));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetVersion(TId)"/> method returns
        /// the data of the specified version when the version exists in the database.
        /// </summary>
        [Test]
        public void GetVersionByVersionIdShouldReturnVersionDataWhenVersionExists()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            Guid versionId = Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16");

            TestEntityVersionData version = this.versionManager.GetVersion(versionId);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(2));
            Assert.That(version.Col1, Is.EqualTo("One"));
            Assert.That(version.Col2, Is.EqualTo("Four"));
            Assert.That(version.Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetVersion(TId)"/> method returns
        /// <c>null</c> when the version does not exist in the database.
        /// </summary>
        [Test]
        public void GetVersionByVersionIdShouldReturnNullWhenVersionDoesNotExist()
        {
            TestEntityVersionData version = this.versionManager.GetVersion(Guid.NewGuid());

            Assert.That(version, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetVersion(TId,int)"/> method returns
        /// the data of the specified version when the version exists in the database.
        /// </summary>
        [Test]
        public void GetVersionByVersionNumberShouldReturnVersionDataWhenVersionExists()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            Guid versionId = Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16");

            TestEntityVersionData version = this.versionManager.GetVersion(objectId, 2);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(2));
            Assert.That(version.Col1, Is.EqualTo("One"));
            Assert.That(version.Col2, Is.EqualTo("Four"));
            Assert.That(version.Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetVersion(TId,int)"/> method returns
        /// <c>null</c> when the version does not exist in the database.
        /// </summary>
        [Test]
        public void GetVersionByVersionNumberShouldReturnNullWhenVersionDoesNotExist()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");

            TestEntityVersionData version = this.versionManager.GetVersion(objectId, 4);

            Assert.That(version, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetVersion(TId,int)"/> method returns
        /// <c>null</c> when the version does not exist in the database.
        /// </summary>
        [Test]
        public void GetVersionByVersionNumberShouldReturnNullWhenObjectDoesNotExist()
        {
            TestEntityVersionData version = this.versionManager.GetVersion(Guid.NewGuid(), 1);

            Assert.That(version, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetLatestVersion"/> method returns
        /// the data of the specified version when the version exists in the database.
        /// </summary>
        [Test]
        public void GetLatestVersionShouldReturnVersionDataWhenVersionExists()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            Guid versionId = Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718");

            TestEntityVersionData version = this.versionManager.GetLatestVersion(objectId);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(3));
            Assert.That(version.Col1, Is.EqualTo("Three"));
            Assert.That(version.Col2, Is.EqualTo("Five"));
            Assert.That(version.Col3, Is.EqualTo("Four"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetLatestVersion"/> method returns
        /// <c>null</c> when the object does not exist in the database.
        /// </summary>
        [Test]
        public void GetLatestVersionShouldReturnNullWhenObjectDoesNotExist()
        {
            TestEntityVersionData version = this.versionManager.GetLatestVersion(Guid.NewGuid());

            Assert.That(version, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetAllVersions"/> method returns
        /// all versions of the object with the specified identifier when the object exists in the database.
        /// </summary>
        [Test]
        public void GetAllVersionsShouldReturnAllVersionsOfObjectWhenObjectExists()
        {
            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            
            TestEntityVersionData[] versions = this.versionManager.GetAllVersions(objectId).ToArray();

            Assert.That(versions.Length, Is.EqualTo(3));

            Assert.That(versions[0].Id, Is.EqualTo(objectId));
            Assert.That(versions[0].VersionId, Is.EqualTo(Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718")));
            Assert.That(versions[0].Version, Is.EqualTo(3));
            Assert.That(versions[0].Col1, Is.EqualTo("Three"));
            Assert.That(versions[0].Col2, Is.EqualTo("Five"));
            Assert.That(versions[0].Col3, Is.EqualTo("Four"));

            Assert.That(versions[1].Id, Is.EqualTo(objectId));
            Assert.That(versions[1].VersionId, Is.EqualTo(Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16")));
            Assert.That(versions[1].Version, Is.EqualTo(2));
            Assert.That(versions[1].Col1, Is.EqualTo("One"));
            Assert.That(versions[1].Col2, Is.EqualTo("Four"));
            Assert.That(versions[1].Col3, Is.EqualTo("Three"));

            Assert.That(versions[2].Id, Is.EqualTo(objectId));
            Assert.That(versions[2].VersionId, Is.EqualTo(Guid.Parse("2A2CA386-153E-45F3-8E41-5989F93F974D")));
            Assert.That(versions[2].Version, Is.EqualTo(1));
            Assert.That(versions[2].Col1, Is.EqualTo("One"));
            Assert.That(versions[2].Col2, Is.EqualTo("Two"));
            Assert.That(versions[2].Col3, Is.EqualTo("Three"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.GetAllVersions"/> method returns
        /// an empty sequence when an object with the specified identifier does not exist in the database.
        /// </summary>
        [Test]
        public void GetAllVersionsShouldReturnEmptySequenceWhenObjectDoesNotExist()
        {
            IEnumerable<TestEntityVersionData> versions = this.versionManager.GetAllVersions(Guid.NewGuid());

            Assert.That(versions, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.CreateVersion"/> method creates a
        /// new version when there is an object in the database with the specified identifier.
        /// </summary>
        [Test]
        public void CreateVersionShouldCreateNewVersionWhenObjectExists()
        {
            // Perform manual update
            using (var command = this.Session.Connection.CreateCommand())
            {
                command.CommandText = 
                    "UPDATE dbo.Test SET Col1 = 'Foo', Col2 = 'Bar', Col3 = 'Baz'" +
                    "WHERE Id = '750005F7-53F4-48C3-B0BF-0B5F68DD8331'";
                command.ExecuteNonQuery();
            }

            Guid objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            TestEntityVersionData version = this.versionManager.CreateVersion(objectId);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.Not.EqualTo(Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718")));
            Assert.That(version.Version, Is.EqualTo(4));
            Assert.That(version.Col1, Is.EqualTo("Foo"));
            Assert.That(version.Col2, Is.EqualTo("Bar"));
            Assert.That(version.Col3, Is.EqualTo("Baz"));

            TestEntityData dataObject = this.Session.Get<TestEntityData>(objectId);
            Assert.That(dataObject.VersionId, Is.Not.EqualTo(Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718")));
            Assert.That(dataObject.Version, Is.EqualTo(4));
            Assert.That(dataObject.Col1, Is.EqualTo("Foo"));
            Assert.That(dataObject.Col2, Is.EqualTo("Bar"));
            Assert.That(dataObject.Col3, Is.EqualTo("Baz"));

            int versionCount;
            int objectCount;
            using (var command = this.Session.Connection.CreateCommand())
            {
                command.CommandText = 
                    "SELECT count(*) FROM dbo.TestVersions WHERE Id = '750005F7-53F4-48C3-B0BF-0B5F68DD8331'";
                versionCount = (int)command.ExecuteScalar();

                command.CommandText =
                    "SELECT count(*) FROM dbo.Test WHERE Id = '750005F7-53F4-48C3-B0BF-0B5F68DD8331'";
                objectCount = (int)command.ExecuteScalar();
            }

            Assert.That(versionCount, Is.EqualTo(4));
            Assert.That(objectCount, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.CreateVersion"/> method returns
        /// <c>null</c> when there is no object in the database with the specified identifier.
        /// </summary>
        [Test]
        public void CreateVersionShouldReturnNullWhenObjectDoesNotExist()
        {
            TestEntityVersionData version = this.versionManager.CreateVersion(Guid.NewGuid());

            Assert.That(version, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.SaveAsNewVersion"/> method correctly
        /// saves a new object to the database and creates its first version.
        /// </summary>
        [Test]
        public void SaveAsNewVersionShouldCreateNewObjectIfObjectDoesNotExist()
        {
            var objectData = new TestEntityData { Col1 = "foo", Col2 = "bar", Col3 = "baz" };

            TestEntityVersionData version = this.versionManager.SaveAsNewVersion(objectData);

            Assert.That(version.Id, Is.Not.EqualTo(Guid.Empty));
            Assert.That(version.VersionId, Is.Not.EqualTo(Guid.Empty));
            Assert.That(version.VersionId, Is.Not.EqualTo(version.Id));
            Assert.That(version.Version, Is.EqualTo(1));
            Assert.That(version.Col1, Is.EqualTo("foo"));
            Assert.That(version.Col2, Is.EqualTo("bar"));
            Assert.That(version.Col3, Is.EqualTo("baz"));

            var dataObject = this.Session.Get<TestEntityData>(version.Id);
            Assert.That(dataObject.Id, Is.EqualTo(version.Id));
            Assert.That(dataObject.VersionId, Is.EqualTo(version.VersionId));
            Assert.That(dataObject.Version, Is.EqualTo(1));
            Assert.That(dataObject.Col1, Is.EqualTo("foo"));
            Assert.That(dataObject.Col2, Is.EqualTo("bar"));
            Assert.That(dataObject.Col3, Is.EqualTo("baz"));

            var currentVersion = this.Session.Get<TestEntityVersionData>(version.VersionId);
            Assert.That(currentVersion.Id, Is.EqualTo(version.Id));
            Assert.That(currentVersion.VersionId, Is.EqualTo(version.VersionId));
            Assert.That(currentVersion.Version, Is.EqualTo(1));
            Assert.That(currentVersion.Col1, Is.EqualTo("foo"));
            Assert.That(currentVersion.Col2, Is.EqualTo("bar"));
            Assert.That(currentVersion.Col3, Is.EqualTo("baz"));

            int versionCount;
            int objectCount;
            using (var command = this.Session.Connection.CreateCommand())
            {
                command.CommandText =
                    "SELECT count(*) FROM dbo.TestVersions WHERE Id = '" + version.Id + "'";
                versionCount = (int)command.ExecuteScalar();

                command.CommandText =
                    "SELECT count(*) FROM dbo.Test WHERE Id = '" + version.Id + "'";
                objectCount = (int)command.ExecuteScalar();
            }

            Assert.That(versionCount, Is.EqualTo(1));
            Assert.That(objectCount, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.SaveAsNewVersion"/> method correctly
        /// updates an existing object in the database and creates its next version.
        /// </summary>
        [Test]
        public void SaveAsNewVersionShouldUpdateExistingObjectIfObjectExists()
        {
            var objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            var versionId = Guid.Parse("E517369C-E9A4-4C49-93CB-7B39BA0C0718");
            TestEntityData dataObject = this.Session.Get<TestEntityData>(objectId);
            dataObject.Col1 = "foo";
            dataObject.Col2 = "bar";
            dataObject.Col3 = "baz";

            TestEntityVersionData version = this.versionManager.SaveAsNewVersion(dataObject);

            Assert.That(version.Id, Is.EqualTo(objectId));
            Assert.That(version.VersionId, Is.Not.EqualTo(versionId));
            Assert.That(version.Version, Is.EqualTo(4));
            Assert.That(version.Col1, Is.EqualTo("foo"));
            Assert.That(version.Col2, Is.EqualTo("bar"));
            Assert.That(version.Col3, Is.EqualTo("baz"));

            var currentDataObject = this.Session.Get<TestEntityData>(version.Id);
            Assert.That(currentDataObject.Id, Is.EqualTo(objectId));
            Assert.That(currentDataObject.VersionId, Is.EqualTo(version.VersionId));
            Assert.That(currentDataObject.Version, Is.EqualTo(4));
            Assert.That(currentDataObject.Col1, Is.EqualTo("foo"));
            Assert.That(currentDataObject.Col2, Is.EqualTo("bar"));
            Assert.That(currentDataObject.Col3, Is.EqualTo("baz"));

            var currentVersion = this.Session.Get<TestEntityVersionData>(version.VersionId);
            Assert.That(currentVersion.Id, Is.EqualTo(version.Id));
            Assert.That(currentVersion.VersionId, Is.EqualTo(version.VersionId));
            Assert.That(currentVersion.Version, Is.EqualTo(4));
            Assert.That(currentVersion.Col1, Is.EqualTo("foo"));
            Assert.That(currentVersion.Col2, Is.EqualTo("bar"));
            Assert.That(currentVersion.Col3, Is.EqualTo("baz"));

            int versionCount;
            int objectCount;
            using (var command = this.Session.Connection.CreateCommand())
            {
                command.CommandText =
                    "SELECT count(*) FROM dbo.TestVersions WHERE Id = '750005F7-53F4-48C3-B0BF-0B5F68DD8331'";
                versionCount = (int)command.ExecuteScalar();

                command.CommandText =
                    "SELECT count(*) FROM dbo.Test WHERE Id = '750005F7-53F4-48C3-B0BF-0B5F68DD8331'";
                objectCount = (int)command.ExecuteScalar();
            }

            Assert.That(versionCount, Is.EqualTo(4));
            Assert.That(objectCount, Is.EqualTo(1));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultVersionManager{TId,TData,TDataVersion}.SaveAsNewVersion"/> method throws
        /// a <see cref="DBConcurrencyException"/> when an attempt is made to update an object version which is not
        /// the latest version.
        /// </summary>
        [Test]
        public void SaveAsNewVersionShouldThrowExceptionWhenVersionNotLatest()
        {
            var objectId = Guid.Parse("750005F7-53F4-48C3-B0BF-0B5F68DD8331");
            TestEntityData dataObject = this.Session.Get<TestEntityData>(objectId);
            dataObject.VersionId = Guid.Parse("757392EC-A439-4212-8FB8-1807DC886A16");  /* old version ID */
            dataObject.Col1 = "foo";
            dataObject.Col2 = "bar";
            dataObject.Col3 = "baz";

            var exception = Assert.Throws<DBConcurrencyException>(
                () => this.versionManager.SaveAsNewVersion(dataObject));

            Assert.That(
                exception.Message,
                Is.StringContaining("The update was performed on a version of the data which is no longer up-to-date."));
        }

        /// <summary>
        /// Sets up the in-memory database before each test.
        /// </summary>
        protected override void SetupDatabase()
        {
            string sql = File.ReadAllText("DefaultVersionManagerDatabase.sql");
            using (IDbCommand command = this.Session.Connection.CreateCommand())
            {
                command.Connection.ChangeDatabase("Helpers_UT");
                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Test entity data.
        /// </summary>
        public class TestEntityData : IIdentifiable<Guid>, IVersioned<Guid>
        {
            /// <summary>
            /// Gets or sets the object's identifier.
            /// </summary>
            public virtual Guid Id { get; set; }

            /// <summary>
            /// Gets or sets the object's version identifier.
            /// </summary>
            public virtual Guid VersionId { get; set; }

            /// <summary>
            /// Gets or sets the object's version number.
            /// </summary>
            public virtual int Version { get; set; }

            /// <summary>
            /// Gets or sets the first test value.
            /// </summary>
            public virtual string Col1 { get; set; }

            /// <summary>
            /// Gets or sets the second test value.
            /// </summary>
            public virtual string Col2 { get; set; }

            /// <summary>
            /// Gets or sets the third test value.
            /// </summary>
            public virtual string Col3 { get; set; }
        }

        /// <summary>
        /// Test entity version data.
        /// </summary>
        public class TestEntityVersionData : TestEntityData
        {
        }

        /// <summary>
        /// NHibernate mapping for <see cref="TestEntityData"/>.
        /// </summary>
        public class TestEntityDataMap : ClassMap<TestEntityData>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestEntityDataMap"/> class.
            /// </summary>
            public TestEntityDataMap()
            {
                this.Table("Test");
                this.Polymorphism.Explicit();
                this.Id(x => x.Id).GeneratedBy.GuidComb();
                this.Map(x => x.VersionId);
                this.Map(x => x.Version);
                this.Map(x => x.Col1);
                this.Map(x => x.Col2);
                this.Map(x => x.Col3);
            }
        }

        /// <summary>
        /// NHibernate mapping for <see cref="TestEntityVersionDataMap"/>.
        /// </summary>
        public class TestEntityVersionDataMap : ClassMap<TestEntityVersionData>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TestEntityVersionDataMap"/> class.
            /// </summary>
            public TestEntityVersionDataMap()
            {
                this.Table("TestVersions");
                this.Id(x => x.VersionId).GeneratedBy.GuidComb();
                this.Map(x => x.Id);
                this.Map(x => x.Version);
                this.Map(x => x.Col1);
                this.Map(x => x.Col2);
                this.Map(x => x.Col3);
            }
        }
    }
}