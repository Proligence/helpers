﻿------------------------------------------------------------------------------------------------------------------------
-- Copyright (C) Proligence
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
-- documentation files (the "Software"), to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
-- to permit persons to whom the Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all copies or substantial portions of
-- the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
-- THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
------------------------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Test]') AND type in (N'U'))
DROP TABLE dbo.Test

CREATE TABLE dbo.Test (
	Id uniqueidentifier NOT NULL,
	VersionId uniqueidentifier NULL,
	Version int NULL,
	Col1 varchar(100),
	Col2 varchar(100),
	Col3 varchar(100)
)

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TestVersions]') AND type in (N'U'))
DROP TABLE TestVersions

CREATE TABLE TestVersions (
	Id uniqueidentifier NOT NULL,
	VersionId uniqueidentifier NOT NULL,
	Version int NOT NULL,
	Col1 varchar(100),
	Col2 varchar(100),
	Col3 varchar(100)
)

INSERT INTO Test (Id, VersionId, Version, Col1, Col2, Col3)
VALUES ('750005F7-53F4-48C3-B0BF-0B5F68DD8331', 'E517369C-E9A4-4C49-93CB-7B39BA0C0718', 3, 'One', 'Five', 'Three');

INSERT INTO TestVersions (Id, VersionId, Version, Col1, Col2, Col3)
VALUES ('750005F7-53F4-48C3-B0BF-0B5F68DD8331', '2A2CA386-153E-45F3-8E41-5989F93F974D', 1, 'One', 'Two', 'Three');
INSERT INTO TestVersions (Id, VersionId, Version, Col1, Col2, Col3)
VALUES ('750005F7-53F4-48C3-B0BF-0B5F68DD8331', '757392EC-A439-4212-8FB8-1807DC886A16', 2, 'One', 'Four', 'Three');
INSERT INTO TestVersions (Id, VersionId, Version, Col1, Col2, Col3)
VALUES ('750005F7-53F4-48C3-B0BF-0B5F68DD8331', 'E517369C-E9A4-4C49-93CB-7B39BA0C0718', 3, 'Three', 'Five', 'Four');