﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NHibernateSessionHostMock.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.NHibernate.UnitTests
{
    using System;
    using System.Data;
    using global::NHibernate;

    /// <summary>
    /// Implements a subclass of the <see cref="NHibernateSessionHost"/> class for testing purposes.
    /// </summary>
    public class NHibernateSessionHostMock : NHibernateSessionHost
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NHibernateSessionHostMock"/> class.
        /// </summary>
        /// <param name="factoryCache">The object which caches the NHibernate session factory.</param>
        /// <param name="connection">The SQL Server connection instance.</param>
        public NHibernateSessionHostMock(SessionFactoryCache factoryCache, IDbConnection connection)
            : base(factoryCache, connection)
        {
        }

        /// <summary>
        /// Gets the object which caches the NHibernate session factory.
        /// </summary>
        public new SessionFactoryCache FactoryCache
        {
            get { return base.FactoryCache; }
        }

        /// <summary>
        /// Gets the connection to the SQL database.
        /// </summary>
        public new IDbConnection Connection
        {
            get { return base.Connection; }
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        public TResult InvokeUsingSession<TResult>(Func<ISession, TResult> action)
        {
            return this.UsingSession(action);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void InvokeUsingSession(Action<ISession> action)
        {
            this.UsingSession(action);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session and explicit transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        public TResult InvokeUsingSession<TResult>(Func<ISession, ITransaction, TResult> action)
        {
            return this.UsingSession(action);
        }

        /// <summary>
        /// Executes the specified action using a new NHibernate session and explicit transaction.
        /// </summary>
        /// <param name="action">The action to execute.</param>
        public void InvokeUsingSession(Action<ISession, ITransaction> action)
        {
            this.UsingSession(action);
        }

        /// <summary>
        /// Executes the specified action using an explicit transaction.
        /// </summary>
        /// <typeparam name="TResult">The type of the action's result.</typeparam>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        /// <returns>The result returned by the action.</returns>
        public TResult InvokeUsingTransaction<TResult>(ISession session, Func<ISession, TResult> action)
        {
            return this.UsingTransaction(session, action);
        }

        /// <summary>
        /// Executes the specified action using an explicit transaction.
        /// </summary>
        /// <param name="session">The NHibernate session.</param>
        /// <param name="action">The action to execute.</param>
        public void InvokeUsingTransaction(ISession session, Action<ISession> action)
        {
            this.UsingTransaction(session, action);
        }

        /// <summary>
        /// Creates a new SQL connection with the specified connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>The created connection object.</returns>
        public IDbConnection InvokeCreateConnection(string connectionString)
        {
            return base.CreateConnection(connectionString);
        }

        /// <summary>
        /// Creates a new SQL connection with the specified connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>The created connection object.</returns>
        protected override IDbConnection CreateConnection(string connectionString)
        {
            // Just returned the mocked connection.
            return this.Connection;
        }
    }
}