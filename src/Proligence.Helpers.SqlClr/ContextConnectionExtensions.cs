﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextConnectionExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Implements extension methods for the <see cref="IContextConnection"/> interface.
    /// </summary>
    public static class ContextConnectionExtensions
    {
        /// <summary>
        /// Initializes static members of the ContextConnectionExtensions class.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
            Justification = "By design.")]
        static ContextConnectionExtensions()
        {
            CreateCommand = connection => connection.CreateCommand();
        }

        /// <summary>
        /// Gets or sets the factory method which creates SQL commands.
        /// </summary>
        public static Func<IDbConnection, IDbCommand> CreateCommand { get; set; }

        /// <summary>
        /// Executes an SQL query which does not return any result.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="query">The SQL query to execute.</param>
        /// <param name="parameters">The parameters for the specified SQL query.</param>
        /// <returns>The number of rows affected by the executed query.</returns>
        public static int ExecuteNonQuery(
            this IContextConnection context, 
            string query,
            params SqlParameter[] parameters)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return ExecuteGeneric(
                context, 
                query, 
                CommandType.Text, 
                parameters, 
                command => command.ExecuteNonQuery());
        }

        /// <summary>
        /// Executes an SQL query which returns a single value.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="query">The SQL query to execute.</param>
        /// <param name="parameters">The parameters for the specified SQL query.</param>
        /// <returns>The result returned by the executed query.</returns>
        public static object ExecuteScalar(
            this IContextConnection context,
            string query,
            params SqlParameter[] parameters)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return ExecuteGeneric(
                context, 
                query, 
                CommandType.Text, 
                parameters, 
                command => command.ExecuteScalar());
        }

        /// <summary>
        /// Executes an SQL query which returns a single value.
        /// </summary>
        /// <typeparam name="TResult">The type of result returned by the query.</typeparam>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="query">The SQL query to execute.</param>
        /// <param name="parameters">The parameters for the specified SQL query.</param>
        /// <returns>The result returned by the executed query.</returns>
        public static TResult ExecuteScalar<TResult>(
            this IContextConnection context,
            string query,
            params SqlParameter[] parameters)
        {
            return (TResult)ExecuteScalar(context, query, parameters);
        }

        /// <summary>
        /// Executes an SQL query.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="query">The SQL query to execute.</param>
        /// <param name="parameters">The parameters for the specified SQL query.</param>
        /// <returns>A sequence of arrays which contain the values of each row returned by the query.</returns>
        public static IEnumerable<object[]> ExecuteQuery(
            this IContextConnection context,
            string query,
            params SqlParameter[] parameters)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return ExecuteQueryGeneric(context, query, CommandType.Text, parameters);
        }

        /// <summary>
        /// Executes an SQL procedure which does not return any result.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="procedureName">The name of the procedure to execute.</param>
        /// <param name="parameters">The parameters for the specified procedure.</param>
        /// <returns>The number of rows affected by the executed procedure.</returns>
        public static int ExecuteProcedure(
            this IContextConnection context,
            string procedureName,
            params SqlParameter[] parameters)
        {
            if (procedureName == null)
            {
                throw new ArgumentNullException("procedureName");
            }

            return ExecuteGeneric(
                context, 
                procedureName, 
                CommandType.StoredProcedure, 
                parameters, 
                command => command.ExecuteNonQuery());
        }

        /// <summary>
        /// Executes an SQL procedure which returns a single value.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="procedureName">The name of the procedure to execute.</param>
        /// <param name="parameters">The parameters for the specified procedure.</param>
        /// <returns>The result returned by the executed procedure.</returns>
        public static object ExecuteScalarProcedure(
            this IContextConnection context,
            string procedureName,
            params SqlParameter[] parameters)
        {
            if (procedureName == null)
            {
                throw new ArgumentNullException("procedureName");
            }

            return ExecuteGeneric(
                context, 
                procedureName, 
                CommandType.StoredProcedure, 
                parameters, 
                command => command.ExecuteScalar());
        }

        /// <summary>
        /// Executes an SQL procedure which returns a single value.
        /// </summary>
        /// <typeparam name="TResult">The type of result returned by the procedure.</typeparam>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="procedureName">The name of the procedure to execute.</param>
        /// <param name="parameters">The parameters for the specified procedure.</param>
        /// <returns>The result returned by the executed procedure.</returns>
        public static TResult ExecuteScalarProcedure<TResult>(
            this IContextConnection context,
            string procedureName,
            params SqlParameter[] parameters)
        {
            return (TResult)ExecuteScalarProcedure(context, procedureName, parameters);
        }

        /// <summary>
        /// Executes an SQL procedure.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="procedureName">The name of the procedure to execute.</param>
        /// <param name="parameters">The parameters for the specified procedure.</param>
        /// <returns>A sequence of arrays which contain the values of each row returned by the procedure.</returns>
        public static IEnumerable<object[]> ExecuteQueryProcedure(
            this IContextConnection context,
            string procedureName,
            params SqlParameter[] parameters)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (procedureName == null)
            {
                throw new ArgumentNullException("procedureName");
            }

            return ExecuteQueryGeneric(context, procedureName, CommandType.StoredProcedure, parameters);
        }

        /// <summary>
        /// Executes the specified delegate using an open context connection.
        /// </summary>
        /// <typeparam name="TResult">The type of result returned by the delegate.</typeparam>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="func">The delegate to invoke.</param>
        /// <returns>The result returned by the delegate.</returns>
        public static TResult UsingConnection<TResult>(
            this IContextConnection context, 
            Func<IDbConnection, TResult> func)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (func == null)
            {
                throw new ArgumentNullException("func");
            }

            IDbConnection connection = context.ContextConnection;
            bool connectionOpen = connection.State == ConnectionState.Open;

            if (!connectionOpen)
            {
                connection.Open();
            }

            try 
            {
                return func(connection);
            }
            finally
            {
                if ((!connectionOpen) && (connection.State == ConnectionState.Open))
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Executes the specified delegate on a <see cref="IDbCommand"/> object,
        /// </summary>
        /// <typeparam name="TResult">The type of result returned by the delegate.</typeparam>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="commandText">The query or name of the procedure to execute.</param>
        /// <param name="commandType">The type of the command to execute.</param>
        /// <param name="parameters">The parameters for the command.</param>
        /// <param name="func">The delegate to invoke.</param>
        /// <returns>The result returned by the delegate.</returns>
        private static TResult ExecuteGeneric<TResult>(
            IContextConnection context, 
            string commandText,
            CommandType commandType,
            IEnumerable<SqlParameter> parameters,
            Func<IDbCommand, TResult> func)
        {
            return UsingConnection(
                context,
                connection =>
                {
                    using (IDbCommand command = CreateCommand(context.ContextConnection))
                    {
                        SqlHelpers.SetupCommand(command, commandText, commandType, parameters);
                        return func(command);
                    }
                });
        }

        /// <summary>
        /// Executes the specified query or procedure and returns the result rows as a <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <param name="context">The <see cref="IContextConnection"/> object instance.</param>
        /// <param name="commandText">The query or name of the procedure to execute.</param>
        /// <param name="commandType">The type of the command to execute.</param>
        /// <param name="parameters">The parameters for the command.</param>
        /// <returns>Sequence of row values.</returns>
        private static IEnumerable<object[]> ExecuteQueryGeneric(
            IContextConnection context,
            string commandText,
            CommandType commandType,
            IEnumerable<SqlParameter> parameters)
        {
            bool connectionOpen = context.ContextConnection.State == ConnectionState.Open;

            if (!connectionOpen)
            {
                context.ContextConnection.Open();
            }

            try 
            {
                using (IDbCommand command = CreateCommand(context.ContextConnection))
                {
                    SqlHelpers.SetupCommand(command, commandText, commandType, parameters);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object[] arr = new object[reader.FieldCount];
                            reader.GetValues(arr);
                            yield return arr;
                        }
                    }
                }
            }
            finally
            {
                if (!connectionOpen)
                {
                    context.ContextConnection.Close();
                }
            }
        }
    }
}