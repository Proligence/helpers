﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrTrigger.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr
{
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.SqlServer.Server;

    /// <summary>
    /// The base class for classes which implement CLR SQL triggers.
    /// </summary>
    public abstract class ClrTrigger : ClrSqlObject
    {
        /// <summary>
        /// Gets the context of the trigger.
        /// </summary>
        public SqlTriggerContext Context { get; private set; }

        /// <summary>
        /// Executes the trigger.
        /// </summary>
        public void Execute()
        {
            this.Initialize();

            try
            {
                this.ExecuteInternal();
            }
            finally
            {
                this.Cleanup();
            }
        }

        /// <summary>
        /// Initializes the CLR trigger before performing its work.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            this.Context = this.GetTriggerContext();
        }

        /// <summary>
        /// Gets the context of the trigger.
        /// </summary>
        /// <returns>The <see cref="SqlTriggerContext"/> object which represents the trigger's context.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "By design.")]
        protected virtual SqlTriggerContext GetTriggerContext()
        {
            return SqlContext.TriggerContext;
        }

        /// <summary>
        /// Preforms the trigger's work.
        /// </summary>
        protected abstract void ExecuteInternal();
    }
}