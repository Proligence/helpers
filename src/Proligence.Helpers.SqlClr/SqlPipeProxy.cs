﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlPipeProxy.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using Microsoft.SqlServer.Server;

    /// <summary>
    /// Implements a proxy which forwards calls from <see cref="ISqlPipe"/> interface to a <see cref="SqlPipe"/>
    /// object.
    /// </summary>
    internal class SqlPipeProxy : ISqlPipe
    {
        /// <summary>
        /// The <see cref="SqlPipe"/> object to which calls will be forwarded.
        /// </summary>
        private readonly SqlPipe pipe;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlPipeProxy"/> class.
        /// </summary>
        /// <param name="pipe">The <see cref="SqlPipe"/> object to which calls will be forwarded.</param>
        public SqlPipeProxy(SqlPipe pipe)
        {
            if (pipe == null)
            {
                throw new ArgumentNullException("pipe");
            }

            this.pipe = pipe;
        }

        /// <summary>
        /// Gets a value indicating whether the pipe is in the mode of sending single result sets back to the client.
        /// </summary>
        /// <returns>
        /// <c>true</c> if the <see cref="ISqlPipe.SendResultsStart(SqlDataRecord)"/> method has been called and the
        /// pipe is in the mode of sending single result sets back to the client; otherwise <c>false</c>.
        /// </returns>
        public bool IsSendingResults
        {
            get
            {
                return this.pipe.IsSendingResults;
            }
        }

        /// <summary>
        /// Executes the command passed as a parameter and sends the results to the client.
        /// </summary>
        /// <param name="command">The <see cref="IDbCommand"/> object to be executed.</param>
        public void ExecuteAndSend(IDbCommand command)
        {
            this.pipe.ExecuteAndSend((SqlCommand)command);
        }

        /// <summary>
        /// Sends a string message directly to the client or current output consumer.
        /// </summary>
        /// <param name="message">The message string to be sent to the client.</param>
        public void Send(string message)
        {
            this.pipe.Send(message);
        }

        /// <summary>
        /// Sends a multirow result set directly to the client or current output consumer.
        /// </summary>
        /// <param name="reader">
        /// The multirow result set to be sent to the client: a <see cref="IDataReader"/> object.
        /// </param>
        public void Send(SqlDataReader reader)
        {
            this.pipe.Send(reader);
        }

        /// <summary>
        /// Sends a single-row result set directly to the client or current output consumer.
        /// </summary>
        /// <param name="record">
        /// The single-row result set sent to the client: a <see cref="SqlDataRecord"/> object.
        /// </param>
        public void Send(SqlDataRecord record)
        {
            this.pipe.Send(record);
        }

        /// <summary>
        /// Marks the beginning of a result set to be sent back to the client, and uses the record parameter to
        /// construct the metadata that describes the result set.
        /// </summary>
        /// <param name="record">
        /// A <see cref="SqlDataRecord"/> object from which metadata is extracted and used todescribe the result set.
        /// </param>
        public void SendResultsStart(SqlDataRecord record)
        {
            this.pipe.SendResultsStart(record);
        }

        /// <summary>
        /// Sends a single row of data back to the client.
        /// </summary>
        /// <param name="record">
        /// A <see cref="SqlDataRecord"/> object with the column values for the row to be sent to the client. The
        /// schema for the record must match the schema described by the metadata of the <see cref="SqlDataRecord"/>
        /// passed to the <see cref="ISqlPipe.SendResultsStart(SqlDataRecord)"/> method.
        /// </param>
        public void SendResultsRow(SqlDataRecord record)
        {
            this.pipe.SendResultsRow(record);
        }

        /// <summary>
        /// Marks the end of a result set, and returns the <see cref="ISqlPipe"/> instance back to the initial state.
        /// </summary>
        public void SendResultsEnd()
        {
            this.pipe.SendResultsEnd();
        }
    }
}