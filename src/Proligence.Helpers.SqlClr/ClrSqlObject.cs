﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrSqlObject.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using Microsoft.SqlServer.Server;

    /// <summary>
    /// The base class for SQL objects implemented in the CLR.
    /// </summary>
    public abstract class ClrSqlObject : IContextConnection, IDisposable
    {
        /// <summary>
        /// The object used to synchronize access to the <see cref="contextConnection"/> variable.
        /// </summary>
        private readonly object contextConnectionLock = new object();

        /// <summary>
        /// The object used to synchronize access to the <see cref="sqlPipe"/> variable.
        /// </summary>
        private readonly object sqlPipeLock = new object();

        /// <summary>
        /// The SQL context connection.
        /// </summary>
        private volatile IDbConnection contextConnection;

        /// <summary>
        /// The SQL data pipe.
        /// </summary>
        private volatile ISqlPipe sqlPipe;

        /// <summary>
        /// Gets the SQL context connection.
        /// </summary>
        public IDbConnection ContextConnection
        {
            get
            {
                if (this.contextConnection == null)
                {
                    lock (this.contextConnectionLock)
                    {
                        if (this.contextConnection == null)
                        {
                            this.contextConnection = this.CreateContextConnection();
                        }
                    }
                }

                return this.contextConnection;
            }
        }

        /// <summary>
        /// Gets the object which allows managed stored procedures running in-process on a SQL Server database to
        /// return results back to the caller.
        /// </summary>
        public ISqlPipe Pipe
        {
            get
            {
                if (this.sqlPipe == null)
                {
                    lock (this.sqlPipeLock)
                    {
                        if (this.sqlPipe == null)
                        {
                            this.sqlPipe = this.CreateSqlPipe();
                        }
                    }
                }

                return this.sqlPipe;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Initializes the CLR object before performing its work.
        /// </summary>
        protected virtual void Initialize()
        {
            if (this.ContextConnection.State != ConnectionState.Open)
            {
                this.ContextConnection.Open();
            }
        }

        /// <summary>
        /// Cleans up after the CLR objcet after it finishes its work.
        /// </summary>
        protected virtual void Cleanup()
        {
            if (this.ContextConnection.State == ConnectionState.Open)
            {
                this.ContextConnection.Close();
            }
        }

        /// <summary>
        /// Executes an SQL query and sends its results to the SQL context pipe.
        /// </summary>
        /// <param name="query">The SQL query to execute.</param>
        /// <param name="parameters">The parameters for the specified SQL query.</param>
        protected void ExecuteQueryToPipe(string query, params SqlParameter[] parameters)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            this.UsingConnection(
                connection =>
                {
                    using (var command = this.CreateCommand(this.ContextConnection))
                    {
                        SqlHelpers.SetupCommand(command, query, CommandType.Text, parameters);
                        this.Pipe.ExecuteAndSend(command);
                    }
                    
                    return 0;
                });
        }

        /// <summary>
        /// Executes an SQL procedure and sends its results to the SQL context pipe.
        /// </summary>
        /// <param name="procedureName">The name of the procedure to execute.</param>
        /// <param name="parameters">The parameters for the specified procedure.</param>
        protected void ExecuteProcedureToPipe(string procedureName, params SqlParameter[] parameters)
        {
            if (procedureName == null)
            {
                throw new ArgumentNullException("procedureName");
            }

            this.UsingConnection(
                connection =>
                {
                    using (var command = this.CreateCommand(this.ContextConnection))
                    {
                        SqlHelpers.SetupCommand(command, procedureName, CommandType.StoredProcedure, parameters);
                        this.Pipe.ExecuteAndSend(command);
                    }
                    
                    return 0;
                });
        }

        /// <summary>
        /// Creates a context SQL connection.
        /// </summary>
        /// <returns>The created connection.</returns>
        protected virtual IDbConnection CreateContextConnection()
        {
            return new SqlConnection("context connection=true");
        }

        /// <summary>
        /// Creates an instance of the SQL data pipe.
        /// </summary>
        /// <returns>The created instance.</returns>
        protected virtual ISqlPipe CreateSqlPipe()
        {
            return new SqlPipeProxy(SqlContext.Pipe);
        }

        /// <summary>
        /// Creates a new SQL command.
        /// </summary>
        /// <param name="connection">The connection to associate with the command.</param>
        /// <returns>The created command.</returns>
        protected virtual IDbCommand CreateCommand(IDbConnection connection)
        {
            return new SqlCommand(string.Empty, (SqlConnection)connection);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged 
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.ContextConnection != null)
                {
                    this.ContextConnection.Dispose();
                }
            }
        }
    }
}