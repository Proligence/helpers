﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClrFunction.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.SqlClr
{
    /// <summary>
    /// The base class for classes which implement CLR SQL user-defined functions.
    /// </summary>
    /// <typeparam name="T">The type of value returned by the function.</typeparam>
    public abstract class ClrFunction<T> : ClrSqlObject
    {
        /// <summary>
        /// Executes the user-defined function.
        /// </summary>
        /// <returns>The result calculated by the function.</returns>
        public T Execute()
        {
            this.Initialize();
            
            try
            {
                return this.CalculateResult();
            }
            finally
            {
                this.Cleanup();
            }
        }

        /// <summary>
        /// Preforms the user-defined function's work.
        /// </summary>
        /// <returns>The result calculated by the function.</returns>
        protected abstract T CalculateResult();
    }
}