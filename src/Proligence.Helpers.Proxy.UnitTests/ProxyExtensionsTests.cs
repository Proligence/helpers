﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyExtensionsTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Proxy.UnitTests
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Castle.DynamicProxy;
    using NUnit.Framework;
    using Proligence.Helpers.Reflection;

    // ReSharper disable InvokeAsExtensionMethod

    /// <summary>
    /// Implements unit tests for the <see cref="ObjectExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class ProxyExtensionsTests
    {
        /// <summary>
        /// Checks if created proxy is of a correct type.
        /// </summary>
        [Test]
        public void ProxyIsOfCorrectType()
        {
            ITestObject testObj = new TestObject();
            var proxy = testObj.Intercept(new TestInterceptor());
            Assert.That(proxy.GetType()
                .GetInterfaces()
                .Contains(typeof(ITestObject)));
        }

        /// <summary>
        /// Method call should be intercepted with invoking target.
        /// </summary>
        [Test]
        public void ShouldInterceptMethodCallAndInvokeTarget()
        {
            ITestObject testObj = new TestObject();
            var proxy = testObj.Intercept(new TestInterceptor());
            var retVal = proxy.TestMethod(string.Empty);
            Assert.That(retVal, Is.EqualTo("TestMethod"));
        }

        /// <summary>
        /// Method call should be intercepted, but target should not be called.
        /// </summary>
        [Test]
        public void ShouldInterceptMethodCallWithoutTarget()
        {
            var proxy = typeof(ITestObject).Proxy<ITestObject>(new TestInterfaceInterceptor());
            var retVal = proxy.TestMethod(string.Empty);
            Assert.That(retVal, Is.EqualTo("Intercepted"));
        }

        /// <summary>
        /// The class of the test type.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class TestObject : ITestObject
        {
            /// <summary>
            /// Test method.
            /// </summary>
            /// <param name="parameter">The parameter.</param>
            /// <returns>String value.</returns>
            public string TestMethod(string parameter)
            {
                return parameter;
            }
        }

        /// <summary>
        /// Test interceptor class.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class TestInterceptor : IInterceptor
        {
            /// <summary>
            /// Intercepts a method.
            /// </summary>
            /// <param name="invocation">
            /// The invocation object.
            /// </param>
            public void Intercept(IInvocation invocation)
            {
                if (invocation == null)
                {
                    throw new ArgumentNullException("invocation");
                }

                invocation.SetArgumentValue(0, invocation.Method.Name);
                invocation.Proceed();
            }
        }

        /// <summary>
        /// Test interceptor class.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class TestInterfaceInterceptor : IInterceptor
        {
            /// <summary>
            /// Intercepts a method.
            /// </summary>
            /// <param name="invocation">
            /// The invocation object.
            /// </param>
            public void Intercept(IInvocation invocation)
            {
                if (invocation == null)
                {
                    throw new ArgumentNullException("invocation");
                }

                invocation.ReturnValue = "Intercepted";
            }
        }
    }
}
