﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ShellProgressDialog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows
{
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using Proligence.Helpers.Windows.Interop;

    /// <summary>
    /// Provides a wrapper around the Windows shell progress dialog.
    /// </summary>
    public class ShellProgressDialog : Component
    {
        /// <summary>
        /// The type which represents the COM progress dialog type.
        /// </summary>
        private static readonly Type ProgressDialogType = Type.GetTypeFromCLSID(
            new Guid("{F8383852-FCD3-11d1-A6B9-006097DF5BD4}"));

        /// <summary>
        /// The progress dialog instance.
        /// </summary>
        private IProgressDialog instance;

        /// <summary>
        /// Indicates whether the dialog is currently being displayed.
        /// </summary>
        private bool displayed;

        /// <summary>
        /// Indicates whether the dialog was shown and then hidden.
        /// </summary>
        private bool hidden;

        /// <summary>
        /// Indicates whether the object was disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// The title of the progress dialog.
        /// </summary>
        private string title;

        /// <summary>
        /// The current progress value.
        /// </summary>
        private ulong currentProgress;

        /// <summary>
        /// The maximum progress value.
        /// </summary>
        private ulong totalProgress;

        /// <summary>
        /// The text of the first line.
        /// </summary>
        private string line1;

        /// <summary>
        /// The text of the second line.
        /// </summary>
        private string line2;

        /// <summary>
        /// The text of the third line.
        /// </summary>
        private string line3;

        /// <summary>
        /// The message to be displayed if the user cancels the operation.
        /// </summary>
        private string cancelMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShellProgressDialog"/> class.
        /// </summary>
        public ShellProgressDialog()
        {
            this.instance = (IProgressDialog)Activator.CreateInstance(ProgressDialogType);
        }

        /// <summary>
        /// Gets or sets the title of the progress dialog.
        /// </summary>
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.EnsureNotDisposed();
                this.title = value;

                if (this.displayed)
                {
                    this.instance.SetTitle(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the current progress value displayed in the dialog.
        /// </summary>
        public ulong CurrentProgress
        {
            get
            {
                return this.currentProgress;
            }

            set
            {
                this.EnsureNotDisposed();
                this.currentProgress = value;

                if (this.displayed)
                {
                    this.instance.SetProgress64(this.currentProgress, this.totalProgress);
                }
            }
        }

        /// <summary>
        /// Gets or sets the total progress value displayed in the dialog.
        /// </summary>
        public ulong TotalProgress
        {
            get
            {
                return this.totalProgress;
            }

            set
            {
                this.EnsureNotDisposed();
                this.totalProgress = value;

                if (this.displayed)
                {
                    this.instance.SetProgress64(this.currentProgress, this.totalProgress);
                }
            }
        }

        /// <summary>
        /// Gets or sets the text displayed in the first line of the progress dialog.
        /// </summary>
        public string Line1
        {
            get
            {
                return this.line1;
            }

            set
            {
                this.EnsureNotDisposed();
                this.line1 = value;

                if (this.displayed)
                {
                    this.instance.SetLine(1, value, this.CompactPaths, IntPtr.Zero);
                }
            }
        }

        /// <summary>
        /// Gets or sets the text displayed in the second line of the progress dialog.
        /// </summary>
        public string Line2
        {
            get
            {
                return this.line2;
            }

            set
            {
                this.EnsureNotDisposed();
                this.line2 = value;

                if (this.displayed)
                {
                    this.instance.SetLine(2, value, this.CompactPaths, IntPtr.Zero);
                }
            }
        }

        /// <summary>
        /// Gets or sets the text displayed in the third line of the progress dialog.
        /// </summary>
        public string Line3
        {
            get
            {
                return this.line3;
            }

            set
            {
                this.EnsureNotDisposed();
                this.line3 = value;

                if (this.displayed)
                {
                    this.instance.SetLine(3, value, this.CompactPaths, IntPtr.Zero);
                }
            }
        }

        /// <summary>
        /// Gets or sets the message to be displayed if the user cancels the operation.
        /// </summary>
        public string CancelMessage
        {
            get
            {
                return this.cancelMessage;
            }

            set
            {
                this.EnsureNotDisposed();
                this.cancelMessage = value;

                if (this.displayed)
                {
                    this.instance.SetCancelMsg(value, null);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the user has canceled the operation.
        /// </summary>
        public bool HasUserCanceled
        {
            get
            {
                if (!this.disposed && this.displayed)
                {
                    return this.instance.HasUserCancelled();
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether paths displayed in the dialog should be compacted.
        /// </summary>
        public bool CompactPaths { get; set; }

        /// <summary>
        /// Displays the progress dialog.
        /// </summary>
        public void Show()
        {
            this.Show(ProgressDialogFlags.Normal, this.GetParentHandle());
        }

        /// <summary>
        /// Displays the progress dialog.
        /// </summary>
        /// <param name="flags">Progress dialog flags.</param>
        public void Show(ProgressDialogFlags flags)
        {
            this.Show(flags, this.GetParentHandle());
        }

        /// <summary>
        /// Displays the progress dialog.
        /// </summary>
        /// <param name="parent">The handle of the parent window for the progress dialog.</param>
        public void Show(IntPtr parent)
        {
            this.Show(ProgressDialogFlags.Normal, parent);
        }

        /// <summary>
        /// Displays the progress dialog.
        /// </summary>
        /// <param name="flags">Progress dialog flags.</param>
        /// <param name="parent">The handle of the parent window for the progress dialog.</param>
        public void Show(ProgressDialogFlags flags, IntPtr parent)
        {
            this.EnsureNotDisposed();

            if (this.hidden)
            {
                // If the progress dialog was shown before and hidden, then we need to create a new dialog.
                Marshal.FinalReleaseComObject(this.instance);
                this.instance = (IProgressDialog)Activator.CreateInstance(ProgressDialogType);
                this.hidden = false;
            }

            this.instance.StartProgressDialog(parent, null, flags, IntPtr.Zero);

            if (this.title != null)
            {
                this.instance.SetProgress64(this.currentProgress, this.totalProgress);

                if (!string.IsNullOrEmpty(this.title))
                {
                    this.instance.SetTitle(this.title);
                }

                if (!string.IsNullOrEmpty(this.line1))
                {
                    this.instance.SetLine(1, this.line1, this.CompactPaths, IntPtr.Zero);
                }

                if (!string.IsNullOrEmpty(this.line2))
                {
                    this.instance.SetLine(2, this.line2, this.CompactPaths, IntPtr.Zero);
                }

                if (!string.IsNullOrEmpty(this.line3))
                {
                    this.instance.SetLine(3, this.line3, this.CompactPaths, IntPtr.Zero);
                }

                if (!string.IsNullOrEmpty(this.cancelMessage))
                {
                    this.instance.SetCancelMsg(this.cancelMessage, null);
                }
            }
            
            this.displayed = true;
        }

        /// <summary>
        /// Displays the progress dialog as a modal dialog.
        /// </summary>
        public void ShowModal()
        {
            this.Show(ProgressDialogFlags.Modal, this.GetParentHandle());
        }

        /// <summary>
        /// Displays the progress dialog.
        /// </summary>
        /// <param name="flags">Progress dialog flags.</param>
        public void ShowModal(ProgressDialogFlags flags)
        {
            this.Show(flags | ProgressDialogFlags.Modal, this.GetParentHandle());
        }

        /// <summary>
        /// Displays the progress dialog as a modal dialog.
        /// </summary>
        /// <param name="parent">The handle of the parent window for the progress dialog.</param>
        public void ShowModal(IntPtr parent)
        {
            this.Show(ProgressDialogFlags.Modal, parent);
        }

        /// <summary>
        /// Displays the progress dialog as a modal dialog.
        /// </summary>
        /// <param name="flags">Progress dialog flags.</param>
        /// <param name="parent">The handle of the parent window for the progress dialog.</param>
        public void ShowModal(ProgressDialogFlags flags, IntPtr parent)
        {
            this.Show(flags | ProgressDialogFlags.Modal, parent);
        }

        /// <summary>
        /// Hides the progress dialog.
        /// </summary>
        public void Hide()
        {
            this.EnsureNotDisposed();
            this.instance.StopProgressDialog();
            this.displayed = false;
            this.hidden = true;
        }

        /// <summary>
        /// Resets the timer which calculates the elapsed operation time.
        /// </summary>
        public void ResetTimer()
        {
            this.EnsureNotDisposed();
            this.instance.Timer(ProgressDialogTimerFlags.Reset, null);
        }

        /// <summary>
        /// Suspends the timer which calculates the elapsed operation time.
        /// </summary>
        public void PauseTimer()
        {
            this.EnsureNotDisposed();
            this.instance.Timer(ProgressDialogTimerFlags.Pause, null);
        }

        /// <summary>
        /// Resumes the timer which calculates the elapsed operation time.
        /// </summary>
        public void ResumeTimer()
        {
            this.EnsureNotDisposed();
            this.instance.Timer(ProgressDialogTimerFlags.Resume, null);
        }

        /// <summary>
        /// Sets the current progress displayed in the dialog.
        /// </summary>
        /// <param name="current">The current progress value.</param>
        /// <param name="total">The maximum progress value.</param>
        public void SetProgress(ulong current, ulong total)
        {
            this.EnsureNotDisposed();
            this.currentProgress = current;
            this.totalProgress = total;

            if (this.displayed)
            {
                this.instance.SetProgress64(current, total);
            }
        }

        /// <summary>
        /// Gets the handle of the parent window for which the progress dialog will be displayed.
        /// </summary>
        /// <returns>The handle of the parent window.</returns>
        protected virtual IntPtr GetParentHandle()
        {
            var activeForm = Form.ActiveForm;
            if (activeForm != null)
            {
                return activeForm.Handle;
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="Component" /> and optionally releases the managed
        /// resources.
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; false to release only unmanaged resources.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (this.displayed)
                {
                    this.instance.StopProgressDialog();
                }

                Marshal.FinalReleaseComObject(this.instance);
                this.disposed = true;
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Throws an <see cref="ObjectDisposedException"/> if the object was disposed.
        /// </summary>
        protected void EnsureNotDisposed()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException("ShellProgressDialog");
            }
        }
    }
}