﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressDialogFlags.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows.Interop
{
    using System;

    /// <summary>
    /// Defines progress dialog flags supported by the <see cref="IProgressDialog"/> interface.
    /// </summary>
    [Flags]
    public enum ProgressDialogFlags : uint
    {
        /// <summary>
        /// Normal progress dialog box behavior.
        /// </summary>
        Normal = 0x00000000,
        
        /// <summary>
        /// The progress dialog box will be modal to the window specified by <c>hwndParent</c>. By default, a progress
        /// dialog box is modeless.
        /// </summary>
        Modal = 0x00000001,
        
        /// <summary>Automatically estimate the remaining time and display the estimate on line 3. </summary>
        /// <remarks>
        /// If this flag is set, IProgressDialog::SetLine can be used only to display text on lines 1 and 2.
        /// </remarks>
        AutoTime = 0x00000002,
        
        /// <summary>
        /// Do not show the "time remaining" text.
        /// </summary>
        NoTime = 0x00000004,
        
        /// <summary>
        /// Do not display a minimize button on the dialog box's caption bar.
        /// </summary>
        NoMinimize = 0x00000008,
        
        /// <summary>
        /// Do not display a progress bar.
        /// </summary>
        /// <remarks>
        /// Typically, an application can quantitatively determine how much of the operation remains and periodically
        /// pass that value to IProgressDialog::SetProgress. The progress dialog box uses this information to update
        /// its progress bar. This flag is typically set when the calling application must wait for an operation to
        /// finish, but does not have any quantitative information it can use to update the dialog box.
        /// </remarks>
        NoProgressBar = 0x00000010,

        /// <summary>
        /// Windows Vista and later. Sets the progress bar to marquee mode. This causes the progress bar to scroll
        /// horizontally, similar to a marquee display. Use this when you wish to indicate that progress is being
        /// made, but the time required for the operation is unknown.
        /// </summary>
        MarqueeProgress = 0x00000020,

        /// <summary>
        /// Windows Vista and later. Do not display a cancel button. The operation cannot be canceled. Use this only
        /// when absolutely necessary.
        /// </summary>
        NoCancel = 0x00000040
    }
}