﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgressDialog.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows.Interop
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Exposes methods that provide options for an application to display a progress dialog box. This interface is
    /// exported by the progress dialog box object (<c>CLSID_ProgressDialog</c>). This object is a generic way to
    /// show a user how an operation is progressing. It is typically used when deleting, uploading, copying, moving,
    /// or downloading large numbers of files.
    /// </summary>
    [ComImport]
    [Guid("EBBC7C04-315E-11d2-B62F-006097DF5BD4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IProgressDialog
    {
        /// <summary>
        /// Starts the progress dialog box.
        /// </summary>
        /// <param name="hwndParent">A handle to the dialog box's parent window.</param>
        /// <param name="punkEnableModless">Reserved. Set to null.</param>
        /// <param name="flags">Flags that control the operation of the progress dialog box. </param>
        /// <param name="reserved">Reserved. Set to <c>IntPtr.Zero</c>.</param>
        void StartProgressDialog(
            IntPtr hwndParent,
            [MarshalAs(UnmanagedType.IUnknown)] object punkEnableModless,
            ProgressDialogFlags flags,
            IntPtr reserved);

        /// <summary>
        /// Stops the progress dialog box and removes it from the screen.
        /// </summary>
        void StopProgressDialog();

        /// <summary>
        /// Sets the title of the progress dialog box.
        /// </summary>
        /// <param name="title">
        /// A pointer to a null-terminated Unicode string that contains the dialog box title.
        /// </param>
        void SetTitle([MarshalAs(UnmanagedType.LPWStr)] string title);

        /// <summary>
        /// Specifies an Audio-Video Interleaved (AVI) clip that runs in the dialog box. Note: Note  This method is
        /// not supported in Windows Vista or later versions.
        /// </summary>
        /// <param name="instance">
        /// An instance handle to the module from which the AVI resource should be loaded.
        /// </param>
        /// <param name="id">
        /// An AVI resource identifier. To create this value, use the MAKEINTRESOURCE macro. The control loads the
        /// AVI resource from the module specified by <paramref name="instance"/>.
        /// </param>
        void SetAnimation(IntPtr instance, ushort id);

        /// <summary>
        /// Checks whether the user has canceled the operation.
        /// </summary>
        /// <returns>TRUE if the user has cancelled the operation; otherwise, FALSE.</returns>
        /// <remarks>
        /// The system does not send a message to the application when the user clicks the Cancel button. You must
        /// periodically use this function to poll the progress dialog box object to determine whether the operation
        /// has been canceled.
        /// </remarks>
        [PreserveSig]
        [return: MarshalAs(UnmanagedType.Bool)]
        bool HasUserCancelled();

        /// <summary>
        /// Updates the progress dialog box with the current state of the operation.
        /// </summary>
        /// <param name="completed">
        /// An application-defined value that indicates what proportion of the operation has been completed at the
        /// time the method was called.
        /// </param>
        /// <param name="total">
        /// An application-defined value that specifies what value <paramref name="completed"/> will have when the
        /// operation is complete.
        /// </param>
        void SetProgress(uint completed, uint total);

        /// <summary>
        /// Updates the progress dialog box with the current state of the operation.
        /// </summary>
        /// <param name="completed">
        /// An application-defined value that indicates what proportion of the operation has been completed at the
        /// time the method was called.
        /// </param>
        /// <param name="total">
        /// An application-defined value that specifies what value <paramref name="completed"/> will have when the
        /// operation is complete.
        /// </param>
        void SetProgress64(ulong completed, ulong total);

        /// <summary>
        /// Displays a message in the progress dialog.
        /// </summary>
        /// <param name="lineNum">
        /// The line number on which the text is to be displayed. Currently there are three lines—1, 2, and 3. If
        /// the <see cref="ProgressDialogFlags.AutoTime"/> flag was included in the flags parameter when
        /// <see cref="StartProgressDialog"/> was called, only lines 1 and 2 can be used. The estimated time will be
        /// displayed on line 3.
        /// </param>
        /// <param name="text">A null-terminated Unicode string that contains the text.</param>
        /// <param name="compactPath">
        /// TRUE to have path strings compacted if they are too large to fit on a line. The paths are compacted with
        /// <c>PathCompactPath</c>.
        /// </param>
        /// <param name="reserved">Reserved. Set to <c>IntPtr.Zero</c>.</param>
        /// <remarks>
        /// This function is typically used to display a message such as "Item XXX is now being processed." typically,
        /// messages are displayed on lines 1 and 2, with line 3 reserved for the estimated time.
        /// </remarks>
        void SetLine(
            uint lineNum,
            [MarshalAs(UnmanagedType.LPWStr)] string text,
            [MarshalAs(UnmanagedType.VariantBool)] bool compactPath,
            IntPtr reserved);

        /// <summary>
        /// Sets a message to be displayed if the user cancels the operation.
        /// </summary>
        /// <param name="cancelMessage">
        /// A pointer to a null-terminated Unicode string that contains the message to be displayed.
        /// </param>
        /// <param name="reserved">Reserved. Set to NULL.</param>
        /// <remarks>Even though the user clicks Cancel, the application cannot immediately call
        /// <see cref="StopProgressDialog"/> to close the dialog box. The application must wait until the next time
        /// it calls <see cref="HasUserCancelled"/> to discover that the user has canceled the operation. Since this
        /// delay might be significant, the progress dialog box provides the user with immediate feedback by clearing
        /// text lines 1 and 2 and displaying the cancel message on line 3. The message is intended to let the user
        /// know that the delay is normal and that the progress dialog box will be closed shortly. It is typically is
        /// set to something like "Please wait while ...".
        /// </remarks>
        void SetCancelMsg(
            [MarshalAs(UnmanagedType.LPWStr)] string cancelMessage,
            object reserved);

        /// <summary>
        /// Resets the progress dialog box timer to zero.
        /// </summary>
        /// <param name="timerAction">Flags that indicate the action to be taken by the timer.</param>
        /// <param name="reserved">Reserved. Set to NULL.</param>
        /// <remarks>
        /// The timer is used to estimate the remaining time. It is started when your application calls
        /// <see cref="StartProgressDialog"/>. Unless your application will start immediately, it should call
        /// <see cref="Timer"/> just before starting the operation. This practice ensures that the time estimates
        /// will be as accurate as possible. This method should not be called after the first call to
        /// <see cref="SetProgress"/>.
        /// </remarks>
        void Timer(ProgressDialogTimerFlags timerAction, object reserved);
    }
}