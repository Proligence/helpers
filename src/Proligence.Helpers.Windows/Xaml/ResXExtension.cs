﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResXExtension.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows.Xaml
{
    using System;
    using System.Resources;
    using System.Windows.Markup;

    /// <summary>
    /// Provides XAML bindings to RESX string resources.
    /// </summary>
    [MarkupExtensionReturnType(typeof(string))]
    public class ResXExtension : MarkupExtension
    {
        /// <summary>
        /// The key of the resource to get.
        /// </summary>
        private readonly string resourceKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResXExtension"/> class.
        /// </summary>
        /// <param name="resourceKey">The resource key.</param>
        public ResXExtension(string resourceKey)
        {
            this.resourceKey = resourceKey;
        }

        /// <summary>
        /// Gets or sets the resource manager which will be used to get resource values.
        /// </summary>
        public static ResourceManager ResourceManager { get; set; }

        /// <summary>
        /// When implemented in a derived class, returns an object that is set as the value of the target property for
        /// this markup extension.
        /// </summary>
        /// <param name="serviceProvider">Object that can provide services for the markup extension.</param>
        /// <returns>The object value to set on the property where the extension is applied.</returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ResourceManager != null)
            {
                return ResourceManager.GetString(this.resourceKey);
            }

            return this.resourceKey;
        }
    }
}