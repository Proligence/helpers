﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListViewComparer.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows.Forms
{
    using System;
    using System.Collections;
    using System.Windows.Forms;

    /// <summary>
    /// Implements a text-based object comparer.
    /// </summary>
    public class ListViewComparer : IComparer
    {
        /// <summary>
        /// Gets or sets the index of the list view column which will be sorted.
        /// </summary>
        public int SortColumn { get; set; }

        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        public SortOrder SortOrder { get; set; }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than
        /// the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />.
        /// </returns>
        public virtual int Compare(object x, object y)
        {
            var itemx = x as ListViewItem;
            var itemy = y as ListViewItem;

            if ((itemx != null) && (itemy != null))
            {
                return this.CompareItems(itemx, itemy);
            }

            return 0;
        }

        /// <summary>
        /// Compares two <see cref="ListViewItem"/> objects and returns a value indicating whether one is less than,
        /// equal to, or greater than the other.
        /// </summary>
        /// <param name="item1">The first item to compare.</param>
        /// <param name="item2">The second item to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="item1" /> and
        /// <paramref name="item2" />.
        /// </returns>
        protected virtual int CompareItems(ListViewItem item1, ListViewItem item2)
        {
            if (item1 == null)
            {
                throw new ArgumentNullException("item1");
            }

            if (item2 == null)
            {
                throw new ArgumentNullException("item2");
            }

            if (this.SortColumn == 0)
            {
                return this.CompareItemValues(item1.Text, item2.Text);
            }

            return this.CompareItemValues(
                item1.SubItems[this.SortColumn].Text,
                item2.SubItems[this.SortColumn].Text);
        }

        /// <summary>
        /// Compares two <see cref="ListViewItem"/> text values and returns a value indicating whether one is less
        /// than, equal to, or greater than the other.
        /// </summary>
        /// <param name="value1">The first item value to compare.</param>
        /// <param name="value2">The second item value to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="value1" /> and
        /// <paramref name="value2" />.
        /// </returns>
        protected virtual int CompareItemValues(string value1, string value2)
        {
            if (value1 == null)
            {
                throw new ArgumentNullException("value1");
            }

            if (value2 == null)
            {
                throw new ArgumentNullException("value2");
            }

            if (this.SortOrder == SortOrder.Descending)
            {
                return string.Compare(value1, value2, StringComparison.CurrentCulture);   
            }
            
            return string.Compare(value2, value1, StringComparison.CurrentCulture);
        }
    }
}