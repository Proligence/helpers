﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SortAdorner.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Windows.Wpf
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// Renders an arrow which indicates which column is being sorted.
    /// </summary>
    /// <remarks>
    /// Implementation based on:
    /// http://www.wpf-tutorial.com/listview-control/listview-how-to-column-sorting/
    /// </remarks>
    public class SortAdorner : Adorner
    {
        /// <summary>
        /// A geometry which renders the ascending sort icon.
        /// </summary>
        private static readonly Geometry AscGeometry = Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        /// <summary>
        /// A geometry which renders the descending sort icon.
        /// </summary>
        private static readonly Geometry DescGeometry = Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        /// <summary>
        /// Initializes a new instance of the <see cref="SortAdorner"/> class.
        /// </summary>
        /// <param name="adornedElement">The element to bind the adorner to.</param>
        /// <param name="direction">The sort direction indicated by the adorner.</param>
        public SortAdorner(UIElement adornedElement, ListSortDirection direction)
            : base(adornedElement)
        {
            this.Direction = direction;
        }

        /// <summary>
        /// Gets the sort direction indicated by the adorner.
        /// </summary>
        public ListSortDirection Direction { get; private set; }

        /// <summary>
        /// When overridden in a derived class, participates in rendering operations that are directed by the
        /// layout system. The rendering instructions for this element are not used directly when this method is
        /// invoked, and are instead preserved for later asynchronous use by layout and drawing.
        /// </summary>
        /// <param name="drawingContext">
        /// The drawing instructions for a specific element. This context is provided to the layout system.
        /// </param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException("drawingContext");
            }

            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width >= 20)
            {
                var transform = new TranslateTransform(
                AdornedElement.RenderSize.Width - 15,
                (AdornedElement.RenderSize.Height - 5) / 2);

                drawingContext.PushTransform(transform);

                Geometry geometry = this.Direction == ListSortDirection.Descending ? DescGeometry : AscGeometry;
                drawingContext.DrawGeometry(Brushes.Black, null, geometry);
                drawingContext.Pop();
            }
        }
    }
}