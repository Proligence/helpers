﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Retry.fs" company="Proligence">
//   Proligence Confidential, All Rights Reserved.
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Fs

open System
open System.Threading

/// Implements helper functions for automatic retrying failed actions
module Retry = 

    /// Performs the specified action and retries it if it fails, waiting a specified interval between retries.
    let rec retryWithInterval interval retries action =
        try
            action()
        with
        | ex ->
            if retries <= 0 then raise ex
            Thread.Sleep(TimeSpan.FromSeconds(float interval))
            retryWithInterval (interval * 2) (retries - 1) action

    /// Performs the specified action and retries it if it fails, waiting a specified interval between retries.
    let retry action =
        retryWithInterval 2 10 action