﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IArgumentValueParser.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System.Reflection;

    /// <summary>
    /// Defines the interface for classes which parse and convert argument property values.
    /// </summary>
    public interface IArgumentValueParser
    {
        /// <summary>
        /// Converts the specified value to the value type of a specified property.
        /// </summary>
        /// <param name="arg">
        /// The <see cref="ApplicationArgument"/> representing the argument whose value is to be converted.
        /// </param>
        /// <param name="pi">
        /// The <see cref="PropertyInfo"/> object of the property for which to convert the value.
        /// </param>
        /// <returns>The converted value.</returns>
        object GetArgumentValue(ApplicationArgument arg, PropertyInfo pi);
    }
}