﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ThrowHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;

    /// <summary>
    /// Helper utility class for throwing exceptions.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal static class ThrowHelper
    {
        /// <summary>
        /// Throws an <see cref="ArgumentParserMessageException"/> when an invalid application argument is detected.
        /// </summary>
        /// <param name="argumentName">The name of the argument.</param>
        public static void ThrowInvalidApplicationArgument(string argumentName)
        {
            throw new ArgumentParserMessageException(
                string.Format(CultureInfo.CurrentCulture, "Invalid argument: '{0}'.", argumentName),
                argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentParserMessageException"/> when a required argument argument is not specified.
        /// </summary>
        /// <param name="argumentName">The name of the argument.</param>
        public static void ThrowApplicationArgumentNotSpecified(string argumentName)
        {
            string message = string.Format(
                CultureInfo.CurrentCulture,
                "The required argument '{0}' was not specified.",
                argumentName);

            throw new ArgumentParserMessageException(message);
        }

        /// <summary>
        /// Gets the message to throw when an invalid argument value is detected.
        /// </summary>
        /// <param name="argumentName">The name of the argument.</param>
        public static void ThrowInvalidApplicationArgumentValue(string argumentName)
        {
            string message = string.Format(
                CultureInfo.CurrentCulture,
                "Invalid value for argument: '{0}'.",
                argumentName);

            throw new ArgumentParserMessageException(message, argumentName);
        }

        /// <summary>
        /// Throws an <see cref="ArgumentParserException"/> when an invalid switch is detected.
        /// </summary>
        /// <param name="switchName">Name of the invalid switch.</param>
        public static void ThrowInvalidSwitch(string switchName)
        {
            throw new ArgumentParserMessageException(
                string.Format(CultureInfo.CurrentCulture, "Invalid switch: '{0}'.", switchName),
                switchName);
        }
    }
}