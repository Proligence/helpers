// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentProvider.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------
 
namespace Proligence.Helpers.Console
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Parses application arguments into an object.
    /// </summary>
    /// <typeparam name="T">The type of the argument container object that will store all arguments.</typeparam>
    public class ArgumentProvider<T>
    {
        /// <summary>
        /// The object used to convert string arguments into objects.
        /// </summary>
        private readonly IArgumentParser argumentParser;

        /// <summary>
        /// The object used to parse the values of argument values.
        /// </summary>
        private readonly IArgumentValueParser valueParser;

        /// <summary>
        /// Contains the properties associated with arguments.
        /// </summary>
        private readonly IDictionary<string, PropertyInfo> argumentProperties;

        /// <summary>
        /// Contains the properties associated with switches.
        /// </summary>
        private readonly IDictionary<string, PropertyInfo> switchProperties;

        /// <summary>
        /// All defined arguments from the container object.
        /// </summary>
        private readonly IDictionary<string, ApplicationArgumentAttribute> definedArguments;
        
        /// <summary>
        /// All defined switches from the container object.
        /// </summary>
        private readonly IDictionary<string, ApplicationSwitchAttribute> definedSwitches;

        /// <summary>
        /// Contains a true value for every required argument.
        /// </summary>
        private IDictionary<string, bool> requiredArguments;

        /// <summary>
        /// Specifies whether the argument parser was initialized using the <see cref="Initialize"/> method.
        /// </summary>
        private bool initialized;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentProvider{T}"/> class.
        /// </summary>
        /// <param name="argumentParser">The object used to convert string arguments into objects.</param>
        /// <param name="valueParser">The object used to parse the values of argument properties.</param>
        public ArgumentProvider(IArgumentParser argumentParser, IArgumentValueParser valueParser)
        {
            this.argumentParser = argumentParser;
            this.valueParser = valueParser;
            this.definedArguments = new Dictionary<string, ApplicationArgumentAttribute>();
            this.argumentProperties = new Dictionary<string, PropertyInfo>();
            this.requiredArguments = new Dictionary<string, bool>();
            this.definedSwitches = new Dictionary<string, ApplicationSwitchAttribute>();
            this.switchProperties = new Dictionary<string, PropertyInfo>();
        }

        /// <summary>
        /// Initializes the argument parser.
        /// </summary>
        /// <exception cref="ArgumentParserException">
        /// An error occurred while reading the argument definitions.
        /// </exception>
        public void Initialize()
        {
            if (this.initialized)
            {
                throw new InvalidOperationException("The argument provider is already initialized.");
            }

            foreach (PropertyInfo pi in typeof(T).GetProperties())
            {
                // Read argument definitions
                var attributes = GetCustomAttributes<ApplicationArgumentAttribute>(pi);

                foreach (ApplicationArgumentAttribute attr in attributes)
                {
                    if (!this.definedArguments.ContainsKey(attr.Name) && !this.definedSwitches.ContainsKey(attr.Name))
                    {
                        this.definedArguments.Add(attr.Name, attr);
                        this.argumentProperties.Add(attr.Name, pi);

                        if (attr.Required)
                        {
                            this.requiredArguments.Add(attr.Name, true);
                        }
                    }
                    else
                    {
                        string message = string.Format(
                            CultureInfo.CurrentCulture,
                            "The argument name '{0}' was defined more than one time.",
                            attr.Name);
                        
                        throw new ArgumentParserException(message);
                    }
                }

                // Read switch definitions
                var switches = GetCustomAttributes<ApplicationSwitchAttribute>(pi);

                foreach (ApplicationSwitchAttribute attr in switches)
                {
                    if (pi.PropertyType != typeof(bool))
                    {
                        string message = string.Format(
                            CultureInfo.CurrentCulture,
                            "The switch argument's property '{0}' must be of Boolean type.",
                            pi.Name);
                        
                        throw new ArgumentParserException(message);
                    }

                    if (!this.definedSwitches.ContainsKey(attr.Name) && !this.definedArguments.ContainsKey(attr.Name))
                    {
                        this.definedSwitches.Add(attr.Name, attr);
                        this.switchProperties.Add(attr.Name, pi);
                    }
                    else
                    {
                        string message = string.Format(
                            CultureInfo.CurrentCulture,
                            "The argument name '{0}' was defined more than one time.",
                            attr.Name);

                        throw new ArgumentParserException(message);
                    }
                }
            }

            this.initialized = true;
        }

        /// <summary>
        /// Parses the specified application arguments into an object representing the parameters.
        /// </summary>
        /// <param name="args">An array of program arguments.</param>
        /// <param name="obj">The object to read the arguments into.</param>
        /// <exception cref="ArgumentParserMessageException">
        /// Parsing failed due to invalid input application arguments (display the message to the user).
        /// </exception>
        /// <remarks>
        /// The method uses reflection to retrieve all properties from the <paramref name="obj"/> object's type and
        /// map switches from the <paramref name="args"/> array to the properties.<br />
        /// Use <see cref="ApplicationArgumentAttribute"/> and <see cref="ApplicationSwitchAttribute"/> on properties
        /// in the <paramref name="obj"/> object to map argument names to properties.<br />
        /// The <paramref name="args"/> array should have the same format as the argument array passed to the
        /// application's <c>Main</c> method.
        /// </remarks>
        public void ParseArguments(string[] args, T obj)
        {
            if (!this.initialized)
            {
                throw new InvalidOperationException("The argument provider is not initialized.");
            }

            // Reset all required switches to true (because non where read yet)
            var newRequired = new Dictionary<string, bool>();

            foreach (string argName in this.requiredArguments.Keys)
            {
                newRequired.Add(argName, true);
            }

            this.requiredArguments = newRequired;

            // Set default values for all arguments and switches
            this.TurnOffAllSwitches(obj);

            // Parse the input string array
            ApplicationArgument[] setArguments;
            try
            {
                setArguments = this.argumentParser.GetArguments(args);
            }
            catch (ArgumentParserMessageException ex)
            {
                ThrowHelper.ThrowInvalidApplicationArgument(ex.ArgumentName);
                return;
            }

            // Set all arguments in the container object.
            foreach (ApplicationArgument arg in setArguments)
            {
                if (!this.argumentProperties.ContainsKey(arg.Name))
                {
                    ThrowHelper.ThrowInvalidApplicationArgument(arg.Name);
                }

                PropertyInfo pi = this.argumentProperties[arg.Name];

                object value = null;
                try
                {
                    value = this.valueParser.GetArgumentValue(arg, pi);
                }
                catch (ArgumentException)
                {
                    ThrowHelper.ThrowInvalidApplicationArgumentValue(arg.Name);
                }
                catch (FormatException)
                {
                    ThrowHelper.ThrowInvalidApplicationArgumentValue(arg.Name);
                }
                catch (OverflowException)
                {
                    ThrowHelper.ThrowInvalidApplicationArgumentValue(arg.Name);
                }

                pi.SetValue(obj, value, new object[0]);
                
                // Mark the required property as set
                if (this.requiredArguments.ContainsKey(arg.Name))
                {
                    this.requiredArguments.Remove(arg.Name);
                    this.requiredArguments.Add(arg.Name, false);
                }
            }

            // Check if all required arguments were set
            foreach (string argName in this.requiredArguments.Keys)
            {
                if (this.requiredArguments[argName])
                {
                    ThrowHelper.ThrowApplicationArgumentNotSpecified(argName);
                }
            }

            // Set all switches in the container object.
            ApplicationSwitchArgument[] setSwitches = this.argumentParser.GetSwitches(args);
            foreach (ApplicationSwitchArgument sw in setSwitches)
            {
                if (!this.switchProperties.ContainsKey(sw.Name))
                {
                    ThrowHelper.ThrowInvalidSwitch(sw.Name);
                }

                this.switchProperties[sw.Name].SetValue(obj, sw.Value, new object[0]);
            }
        }

        /// <summary>
        /// Gets the custom attributes.
        /// </summary>
        /// <typeparam name="TAttribute">The type of the attributes to get.</typeparam>
        /// <param name="propertyInfo">The property form which to get attributes.</param>
        /// <returns>An array of custom attributes.</returns>
        private static IEnumerable<TAttribute> GetCustomAttributes<TAttribute>(PropertyInfo propertyInfo)
        {
            return propertyInfo.GetCustomAttributes(typeof(TAttribute), true).Cast<TAttribute>();
        }

        /// <summary>
        /// Sets default values for all defined attributes and switches in the specified container object.
        /// </summary>
        /// <param name="obj">The container object to set default parameters on.</param>
        private void TurnOffAllSwitches(T obj)
        {
            foreach (PropertyInfo pi in this.switchProperties.Values)
            {
                pi.SetValue(obj, false, new object[0]);
            }
        }
    }
}