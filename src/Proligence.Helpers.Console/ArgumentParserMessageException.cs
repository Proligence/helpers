// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentParserMessageException.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// The exception is thrown when the <see cref="ArgumentProvider{T}"/> fail's to parse the application arguments
    /// because the input data is invalid. The message of this exception should be displayed to the user (it always
    /// contains a clear description of the error).
    /// </summary>
    [Serializable]
    public class ArgumentParserMessageException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public ArgumentParserMessageException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        /// <param name="message">The message describing the error which triggered the exception.</param>
        public ArgumentParserMessageException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        /// <param name="message">The message describing the error which triggered the exception.</param>
        /// <param name="argumentName">The name of the argument that generated this error.</param>
        public ArgumentParserMessageException(string message, string argumentName)
            : base(message)
        {
            this.ArgumentName = argumentName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        /// <param name="message">The message describing the error which triggered the exception.</param>
        /// <param name="inner">The exception that triggered this exception.</param>
        [ExcludeFromCodeCoverage]
        public ArgumentParserMessageException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        /// <param name="message">The message describing the error which triggered the exception.</param>
        /// <param name="inner">The exception that triggered this exception.</param>
        /// <param name="argumentName">The name of the argument that generated this error.</param>
        public ArgumentParserMessageException(string message, Exception inner, string argumentName)
            : base(message, inner)
        {
            this.ArgumentName = argumentName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentParserMessageException"/> class.
        /// </summary>
        /// <param name="info">
        /// The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="StreamingContext"/> that contains contextual information about the source or destination.
        /// </param>
        protected ArgumentParserMessageException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.ArgumentName = info.GetString("ArgumentName");
        }

        /// <summary>
        /// Gets the name of the argument that generated this error.
        /// </summary>
        public string ArgumentName { get; private set; }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="SerializationInfo" /> with information about the
        /// exception.
        /// </summary>
        /// <param name="info">
        /// The <see cref="SerializationInfo" /> that holds the serialized object data about the exception being
        /// thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="StreamingContext" /> that contains contextual information about the source or destination.
        /// </param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("ArgumentName", this.ArgumentName);
        }
    }
}