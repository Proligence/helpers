// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationSwitchArgument.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents an application argument that has a boolean type value (is set on or off).
    /// </summary>
    public class ApplicationSwitchArgument : ApplicationArgument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSwitchArgument"/> class.
        /// </summary>
        /// <param name="name">The name of the argument.</param>
        /// <param name="value">The value of the argument.</param>
        public ApplicationSwitchArgument(string name, bool value)
            : base(name, value)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the switch is set.
        /// </summary>
        /// <value><c>true</c> if the switch is set; otherwise, <c>false</c>.</value>
        public bool IsSet
        {
            get { return (bool)Value; }
            
            [ExcludeFromCodeCoverage]
            protected set { this.Value = value; }
        }
    }
}