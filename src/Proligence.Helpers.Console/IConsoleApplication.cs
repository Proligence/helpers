// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IConsoleApplication.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using global::Autofac;
    using NLog;

    /// <summary>
    /// The interface for console application base classes.
    /// </summary>
    /// <typeparam name="TArguments">The type which represents the application's arguments.</typeparam>
    public interface IConsoleApplication<out TArguments>
        where TArguments : new()
    {
        /// <summary>
        /// Gets the application's logger.
        /// </summary>
        Logger Logger { get; }

        /// <summary>
        /// Gets the application's dependency injection container.
        /// </summary>
        IContainer Container { get; }

        /// <summary>
        /// Gets the application's arguments.
        /// </summary>
        TArguments Arguments { get; }
    }
}