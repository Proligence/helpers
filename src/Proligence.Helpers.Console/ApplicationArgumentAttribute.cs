// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationArgumentAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System;

    /// <summary>
    /// Marks the specified property as an application argument for the <see cref="ArgumentProvider{T}"/> class.
    /// </summary>
    /// <remarks>
    /// The property must have a get and set accessor.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ApplicationArgumentAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationArgumentAttribute"/> class.
        /// </summary>
        /// <param name="name">The name of the argument.</param>
        /// <param name="required"><c>true</c> if the argument is required; otherwise, <c>false</c>.</param>
        public ApplicationArgumentAttribute(string name, bool required)
        {
            this.Name = name;
            this.Required = required;
        }

        /// <summary>
        /// Gets the name of the argument.
        /// </summary>
        /// <value>The name of the argument.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the argument is required.
        /// </summary>
        /// <value><c>true</c> if the argument is required; otherwise, <c>false</c>.</value>
        public bool Required { get; private set; }
    }
}