// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultArgumentParser.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------
 
namespace Proligence.Helpers.Console
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Parses application arguments in the Microsoft-style format.
    /// </summary>
    public class DefaultArgumentParser : IArgumentParser
    {
        /// <summary>
        /// Gets all arguments from the string array specified in the constructor.
        /// </summary>
        /// <param name="args">The application arguments.</param>
        /// <returns>An array of <see cref="ApplicationArgument"/> objects representing the parsed arguments.</returns>
        /// <exception cref="ArgumentParserMessageException">Invalid input data was supplied.</exception>
        public virtual ApplicationArgument[] GetArguments(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            var argsList = new List<ApplicationArgument>();

            foreach (string arg in args)
            {
                if (arg[0] != '/')
                {
                    throw new ArgumentParserMessageException("The specified argument '" + arg + "' is invalid.", arg);
                }

                int index = arg.IndexOf(':');
                if (index != -1)
                {
                    string name = arg.Substring(1, index - 1);
                    string value = arg.Substring(index + 1);

                    // Remove quotes wrapping the argument's value.
                    if (value.StartsWith("\"", StringComparison.Ordinal)
                        && value.EndsWith("\"", StringComparison.Ordinal))
                    {
                        value = value.Substring(1, value.Length - 2);
                    }

                    argsList.Add(new ApplicationArgument(name, value));
                }
            }

            ApplicationArgument[] arr = new ApplicationArgument[argsList.Count];
            argsList.CopyTo(arr, 0);

            return arr;
        }

        /// <summary>
        /// Gets all switch arguments from the string array specified in the constructor.
        /// </summary>
        /// <param name="args">The application arguments.</param>
        /// <returns>
        /// An array of <see cref="ApplicationSwitchArgument"/> objects representing the parsed arguments.
        /// </returns>
        /// <exception cref="ArgumentParserMessageException">Invalid input data was supplied.</exception>
        public virtual ApplicationSwitchArgument[] GetSwitches(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException("args");
            }

            var switchList = new List<ApplicationSwitchArgument>();

            foreach (string arg in args)
            {
                int index = arg.IndexOf(':');
                if (index == -1)
                {
                    if (arg[0] != '/')
                    {
                        throw new ArgumentParserMessageException(
                            "The specified switch '" + arg + "' is invalid.", arg);
                    }

                    switchList.Add(new ApplicationSwitchArgument(arg.Substring(1), true));
                }
            }

            ApplicationSwitchArgument[] arr = new ApplicationSwitchArgument[switchList.Count];
            switchList.CopyTo(arr, 0);
            
            return arr;
        }
    }
}