﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleApplication.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using global::Autofac;
    using NLog;
    using Proligence.Helpers.Autofac;

    /// <summary>
    /// The base class for console applications.
    /// </summary>
    /// <typeparam name="TArguments">The type which represents the application's arguments.</typeparam>
    public abstract class ConsoleApplication<TArguments> : IConsoleApplication<TArguments>
        where TArguments : new()
    {
        /// <summary>
        /// Gets the application's logger.
        /// </summary>
        public Logger Logger { get; private set; }

        /// <summary>
        /// Gets the application's dependency injection container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        /// Gets the application's arguments.
        /// </summary>
        public TArguments Arguments { get; private set; }

        /// <summary>
        /// Runs the application.
        /// </summary>
        /// <param name="args">The application's command-line arguments.</param>
        /// <returns>The application's exit code.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "By design.")]
        public int Run(string[] args)
        {
            Console.CancelKeyPress += this.OnCancelKeyPress;

            this.OnPreInitialize();
            this.Logger = this.InitializeLogger();

            this.Container = AppDomain.CurrentDomain.BuildIocContainer(
                new ContainerBuildOptions { RegisterAssemblyModules = false, RegisterAssemblyTypes = false },
                this.RegisterComponents);

            this.ConfigureContainer(this.Container);

            try
            {
                this.Arguments = this.ParseArguments(args);

                var builder = new ContainerBuilder();
                builder.Register(ctx => this.Arguments).As<TArguments>().SingleInstance();
                builder.Update(this.Container);
            }
            catch (ArgumentParserMessageException ex)
            {
                Console.WriteLine(ex.Message);
                this.OnShutdown();
                return -1;
            }
            catch (Exception ex)
            {
                this.Logger.Fatal(ex, "Failed to parse application arguments.");
                this.OnShutdown();
                return -2;
            }

            try
            {
                this.OnPostInitialize();
                this.Execute();
                return 0;
            }
            catch (Exception ex)
            {
                this.Logger.Fatal(ex, "Fatal error occurred.");
                this.OnFatalError(ex);
                return -2;
            }
            finally
            {
                this.OnShutdown();
            }
        }

        /// <summary>
        /// Called before the application is initialized.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void OnPreInitialize()
        {
        }

        /// <summary>
        /// Creates an initializes the application's logger.
        /// </summary>
        /// <returns>The created logger.</returns>
        protected virtual Logger InitializeLogger()
        {
            return LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Registers additional components in the application's dependency injection container.
        /// </summary>
        /// <param name="builder">The container builder.</param>
        protected virtual void RegisterComponents(ContainerBuilder builder)
        {
        }

        /// <summary>
        /// Called after the dependency injection container has been built.
        /// </summary>
        /// <param name="container">The application's dependency injection container.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void ConfigureContainer(IContainer container)
        {
        }

        /// <summary>
        /// Parses the application's command-line arguments.
        /// </summary>
        /// <param name="args">The command-line arguments to parse.</param>
        /// <returns>The <typeparamref name="TArguments"/> object which represents the parsed arguments.</returns>
        protected virtual TArguments ParseArguments(string[] args)
        {
            if (args == null)
            {
                args = new string[0];
            }
            
            var arguments = new TArguments();
            var argumentProvider = this.Container.Resolve<ArgumentProvider<TArguments>>();
            argumentProvider.Initialize();
            argumentProvider.ParseArguments(args, arguments);

            return arguments;
        }

        /// <summary>
        /// Called after the application has been initialized.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void OnPostInitialize()
        {
        }

        /// <summary>
        /// Called to execute application logic.
        /// </summary>
        protected abstract void Execute();

        /// <summary>
        /// Called when a fatal application error occurs.
        /// </summary>
        /// <param name="exception">The exception which caused the error.</param>
        protected virtual void OnFatalError(Exception exception)
        {
            if (exception != null)
            {
                Console.Error.WriteLine(exception.ToString());
            }
        }

        /// <summary>
        /// Occurs when the Control modifier key (Ctrl) and either the ConsoleKey.C console key (C) or the Break key
        /// are pressed simultaneously (Ctrl+C or Ctrl+Break).
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The <see cref="ConsoleCancelEventArgs"/> instance containing the event data.</param>
        [ExcludeFromCodeCoverage]
        protected virtual void OnCancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            this.OnShutdown();
        }

        /// <summary>
        /// Called when the application is being shut down.
        /// </summary>
        [ExcludeFromCodeCoverage]
        protected virtual void OnShutdown()
        {
        }
    }
}