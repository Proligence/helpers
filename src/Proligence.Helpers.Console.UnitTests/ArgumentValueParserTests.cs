﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentValueParserTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using System;
    using System.Reflection;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ArgumentValueParser"/> class.
    /// </summary>
    [TestFixture]
    public class ArgumentValueParserTests
    {
        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="ApplicationArgument"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldThrowExceptionWhenArgumentNull()
        {
            var parser = new ArgumentValueParser();
            var propertyInfo = typeof(TestClass).GetProperty("EnumProperty");

            var exception = Assert.Throws<ArgumentNullException>(
                () => parser.GetArgumentValue(null, propertyInfo));

            Assert.That(exception.ParamName, Is.EqualTo("arg"));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="PropertyInfo"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldThrowExceptionWhenPropertyInfoNull()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "Value1");

            var exception = Assert.Throws<ArgumentNullException>(
                () => parser.GetArgumentValue(argument, null));

            Assert.That(exception.ParamName, Is.EqualTo("pi"));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method correctly handles enumeration
        /// values.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldGetEnumValue()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "Value1");
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("EnumProperty");

            object result = parser.GetArgumentValue(argument, propertyInfo);

            Assert.That(result, Is.EqualTo(TestClass.TestEnum.Value1));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method throws an
        /// <see cref="ArgumentParserMessageException"/> when a value for an enumeration type property is <c>null</c>.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldThrowExceptionWhenEnumValueNull()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", null);
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("EnumProperty");

            var exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.GetArgumentValue(argument, propertyInfo));

            Assert.That(exception.Message, Is.EqualTo("Invalid value for argument: 'MyArg'."));
            Assert.That(exception.ArgumentName, Is.EqualTo("MyArg"));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method correctly handles
        /// <see cref="string"/> values.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldParseString()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "My text");
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("StringProperty");

            object result = parser.GetArgumentValue(argument, propertyInfo);

            Assert.That(result, Is.EqualTo("My text"));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method correctly handles
        /// <see cref="int"/> values.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldParseInteger()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "7");
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("IntProperty");

            object result = parser.GetArgumentValue(argument, propertyInfo);

            Assert.That(result, Is.EqualTo(7));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method correctly handles
        /// <see cref="bool"/> values.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldParseBoolean()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "True");
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("BoolProperty");

            object result = parser.GetArgumentValue(argument, propertyInfo);

            Assert.That(result, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentValueParser.GetArgumentValue"/> method throws an
        /// <see cref="ArgumentParserMessageException"/> when the specified value cannot be converted to the property's
        /// type.
        /// </summary>
        [Test]
        public void GetArgumentValueShouldThrowArgumentParserMessageExceptionWhenFailedToParseValue()
        {
            var parser = new ArgumentValueParser();
            var argument = new ApplicationArgument("MyArg", "Test");
            var obj = new TestClass();
            var propertyInfo = obj.GetType().GetProperty("IntProperty");

            var exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.GetArgumentValue(argument, propertyInfo));

            Assert.That(exception.Message, Is.EqualTo("Invalid value for argument: 'MyArg'."));
            Assert.That(exception.ArgumentName, Is.EqualTo("MyArg"));
        }

        /* ReSharper disable UnusedMember.Local */

        /// <summary>
        /// Test class.
        /// </summary>
        private class TestClass
        {
            /// <summary>
            /// Test enumeration.
            /// </summary>
            public enum TestEnum
            {
                /// <summary>
                /// Test enumeration value.
                /// </summary>
                Value1,

                /// <summary>
                /// Test enumeration value.
                /// </summary>
                Value2
            }

            /// <summary>
            /// Gets or sets the enumeration type value.
            /// </summary>
            public TestEnum EnumProperty { get; set; }

            /// <summary>
            /// Gets or sets the string value.
            /// </summary>
            public string StringProperty { get; set; }

            /// <summary>
            /// Gets or sets the integer value.
            /// </summary>
            public int IntProperty { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the boolean property is set.
            /// </summary>
            public bool BoolProperty { get; set; }
        }

        /* ReSharper restore UnusedMember.Local */
    }
}