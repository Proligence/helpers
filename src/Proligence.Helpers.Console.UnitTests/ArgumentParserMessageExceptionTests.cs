﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentParserMessageExceptionTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ArgumentParserMessageException"/> class.
    /// </summary>
    [TestFixture]
    public class ArgumentParserMessageExceptionTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="ArgumentParserMessageException"/> class correctly initializes
        /// a new object instance with a message and argument name.
        /// </summary>
        [Test]
        public void InitializeExceptionWithMessageAndArgument()
        {
            var exception = new ArgumentParserMessageException("My message.", "MyArgument");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.ArgumentName, Is.EqualTo("MyArgument"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="ArgumentParserMessageException"/> class correctly initializes
        /// a new object instance with a message, argument name and inner exception.
        /// </summary>
        [Test]
        public void InitializeExceptionWithMessageArgumentAndInnerException()
        {
            var inner = new InvalidOperationException("Test exception message.");
            var exception = new ArgumentParserMessageException("My message.", inner, "MyArgument");

            Assert.That(exception.Message, Is.EqualTo("My message."));
            Assert.That(exception.InnerException, Is.SameAs(inner));
            Assert.That(exception.ArgumentName, Is.EqualTo("MyArgument"));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentParserMessageException"/> is properly serialized and deserialized.
        /// </summary>
        [Test]
        public void TestExceptionSerialization()
        {
            var exception = new ArgumentParserMessageException("My message.", "MyArgument");

            ArgumentParserMessageException deserialized;
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, exception);
                stream.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                deserialized = (ArgumentParserMessageException)formatter.Deserialize(stream);
            }

            Assert.That(deserialized.Message, Is.EqualTo("My message."));
            Assert.That(deserialized.ArgumentName, Is.EqualTo("MyArgument"));
        }
    }
}