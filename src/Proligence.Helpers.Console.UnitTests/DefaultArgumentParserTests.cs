﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultArgumentParserTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="DefaultArgumentParser"/> class.
    /// </summary>
    [TestFixture]
    public class DefaultArgumentParserTests
    {
        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified arguments array is <c>null</c>.
        /// </summary>
        [Test]
        public void GetArgumentsShouldThrowExceptionWhenArgsNull()
        {
            var parser = new DefaultArgumentParser();

            var exception = Assert.Throws<ArgumentNullException>(
                () => parser.GetArguments(null));

            Assert.That(exception.ParamName, Is.EqualTo("args"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses an empty argument
        /// list.
        /// </summary>
        [Test]
        public void GetArgumentsShouldReturnEmptyArrayWhenNoArgument()
        {
            var args = new string[0];
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses an argument list
        /// with one argument.
        /// </summary>
        [Test]
        public void GetArgumentsShouldParseSingleArgument()
        {
            var args = new[] { "/Arg:TestValue" };
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result.Length, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Arg"));
            Assert.That(result[0].Value, Is.EqualTo("TestValue"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses an argument list
        /// with multiple arguments.
        /// </summary>
        [Test]
        public void GetArgumentsShouldParseMultipleArguments()
        {
            var args = new[] { "/Arg1:TestValue1", "/Arg2:TestValue2", "/Arg3:TestValue3" };
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result.Length, Is.EqualTo(3));
            Assert.That(result[0].Name, Is.EqualTo("Arg1"));
            Assert.That(result[0].Value, Is.EqualTo("TestValue1"));
            Assert.That(result[1].Name, Is.EqualTo("Arg2"));
            Assert.That(result[1].Value, Is.EqualTo("TestValue2"));
            Assert.That(result[2].Name, Is.EqualTo("Arg3"));
            Assert.That(result[2].Value, Is.EqualTo("TestValue3"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses an argument list
        /// with multiple switches and arguments.
        /// </summary>
        [Test]
        public void GetArgumentsShouldParseSwitchesMixedWithArguments()
        {
            var args = new[] { "/Arg1:Value1", "/Test1", "/Test2", "/Arg2:Value2", "/Test3" };
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result.Length, Is.EqualTo(2));
            Assert.That(result[0].Name, Is.EqualTo("Arg1"));
            Assert.That(result[0].Value, Is.EqualTo("Value1"));
            Assert.That(result[1].Name, Is.EqualTo("Arg2"));
            Assert.That(result[1].Value, Is.EqualTo("Value2"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses arguments with
        /// colons in the argument's value.
        /// </summary>
        [Test]
        public void GetArgumentsShouldParseArgumentWithColonInValue()
        {
            var args = new[] { "/Arg:Test:Value" };
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result.Length, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Arg"));
            Assert.That(result[0].Value, Is.EqualTo("Test:Value"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method correctly parses arguments with
        /// spaces in the argument's value.
        /// </summary>
        [Test]
        public void GetArgumentsShouldParseArgumentsWithSpaceInValue()
        {
            var args = new[] { "/Arg:\"Test Value\"" };
            var parser = new DefaultArgumentParser();

            ApplicationArgument[] result = parser.GetArguments(args);

            Assert.That(result.Length, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Arg"));
            Assert.That(result[0].Value, Is.EqualTo("Test Value"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method throws an
        /// <see cref="ArgumentParserMessageException"/> when the argument does not begin with a slash.
        /// </summary>
        [Test]
        public void GetArgumentsShouldThrowExceptionWhenArgumentDoesNotStartWithSlash()
        {
            var args = new[] { "Arg:TestValue" };
            var parser = new DefaultArgumentParser();

            var exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.GetArguments(args));

            Assert.That(exception.Message, Is.EqualTo("The specified argument 'Arg:TestValue' is invalid."));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetSwitches"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified arguments array is <c>null</c>.
        /// </summary>
        [Test]
        public void GetSwitchesShouldThrowExceptionWhenArgsNull()
        {
            var parser = new DefaultArgumentParser();

            var exception = Assert.Throws<ArgumentNullException>(
                () => parser.GetSwitches(null));

            Assert.That(exception.ParamName, Is.EqualTo("args"));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetSwitches"/> method correctly parses an empty argument
        /// list.
        /// </summary>
        [Test]
        public void GetSwitchesShouldReturnEmptyArrayWhenNoArgument()
        {
            var args = new string[0];
            var parser = new DefaultArgumentParser();

            ApplicationSwitchArgument[] result = parser.GetSwitches(args);

            Assert.That(result, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetSwitches"/> method correctly parses an argument list
        /// with one switch.
        /// </summary>
        [Test]
        public void GetSwitchesShouldParseSingleArgument()
        {
            var args = new[] { "/Test" };
            var parser = new DefaultArgumentParser();

            ApplicationSwitchArgument[] result = parser.GetSwitches(args);

            Assert.That(result.Length, Is.EqualTo(1));
            Assert.That(result[0].Name, Is.EqualTo("Test"));
            Assert.That(result[0].Value, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetSwitches"/> method correctly parses an argument list
        /// with multiple switches.
        /// </summary>
        [Test]
        public void GetSwitchesShouldParseMultipleArguments()
        {
            var args = new[] { "/Test1", "/Test2", "/Test3" };
            var parser = new DefaultArgumentParser();

            ApplicationSwitchArgument[] result = parser.GetSwitches(args);

            Assert.That(result.Length, Is.EqualTo(3));
            Assert.That(result[0].Name, Is.EqualTo("Test1"));
            Assert.That(result[0].Value, Is.EqualTo(true));
            Assert.That(result[1].Name, Is.EqualTo("Test2"));
            Assert.That(result[1].Value, Is.EqualTo(true));
            Assert.That(result[2].Name, Is.EqualTo("Test3"));
            Assert.That(result[2].Value, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetSwitches"/> method correctly parses an argument list
        /// with multiple switches and arguments.
        /// </summary>
        [Test]
        public void GetSwitchesShouldParseSwitchesMixedWithArguments()
        {
            var args = new[] { "/Arg1:Value1", "/Test1", "/Test2", "/Arg2:Value2", "/Test3" };
            var parser = new DefaultArgumentParser();

            ApplicationSwitchArgument[] result = parser.GetSwitches(args);

            Assert.That(result.Length, Is.EqualTo(3));
            Assert.That(result[0].Name, Is.EqualTo("Test1"));
            Assert.That(result[0].Value, Is.EqualTo(true));
            Assert.That(result[1].Name, Is.EqualTo("Test2"));
            Assert.That(result[1].Value, Is.EqualTo(true));
            Assert.That(result[2].Name, Is.EqualTo("Test3"));
            Assert.That(result[2].Value, Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="DefaultArgumentParser.GetArguments"/> method throws an
        /// <see cref="ArgumentParserMessageException"/> when the argument does not begin with a slash.
        /// </summary>
        [Test]
        public void GetSwitchesShouldThrowExceptionWhenSwitchDoesNotStartWithSlash()
        {
            var args = new[] { "Arg" };
            var parser = new DefaultArgumentParser();

            var exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.GetSwitches(args));

            Assert.That(exception.Message, Is.EqualTo("The specified switch 'Arg' is invalid."));
        }
    }
}