// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArgumentProviderTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ArgumentProvider{T}"/> class.
    /// </summary>
    [TestFixture]
    public class ArgumentProviderTests
    {
        /* ReSharper disable UnusedMember.Local */

        /// <summary>
        /// Test enumeration.
        /// </summary>
        private enum MyEnum
        {
            /// <summary>
            /// Test enumeration value 1.
            /// </summary>
            Item1,

            /// <summary>
            /// Test enumeration value 2.
            /// </summary>
            Item2,

            /// <summary>
            /// Test enumeration value 3.
            /// </summary>
            Item3
        }

        /* ReSharper restore UnusedMember.Local */

        /// <summary>
        /// Try to initialize the <see cref="ArgumentProvider{T}"/> with a parameters container
        /// which has a switch with a non-boolean value type.
        /// </summary>
        [Test]
        public void InitNonBooleanSwitch()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<InvalidParametersContainer>(argumentBuilder, propertyParser);

            ArgumentParserException exception = Assert.Throws<ArgumentParserException>(parser.Initialize);

            Assert.That(
                exception.Message,
                Is.EqualTo("The switch argument's property 'MySwitch' must be of Boolean type."));
        }

        /// <summary>
        /// Try to initialize the <see cref="ArgumentProvider{T}"/> with a parameters container
        /// which has two properties marked as arguments with the same name.
        /// </summary>
        [Test]
        public void InitDoubleArgument()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<DoubleArgContainer>(argumentBuilder, propertyParser);

            ArgumentParserException exception = Assert.Throws<ArgumentParserException>(parser.Initialize);

            Assert.That(
                exception.Message,
                Is.EqualTo("The argument name 'arg' was defined more than one time."));
        }

        /// <summary>
        /// Try to initialize the <see cref="ArgumentProvider{T}"/> with a parameters container
        /// which has two properties marked as switches with the same name.
        /// </summary>
        [Test]
        public void InitDoubleSwitch()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<DoubleSwitchContainer>(argumentBuilder, propertyParser);

            ArgumentParserException exception = Assert.Throws<ArgumentParserException>(parser.Initialize);

            Assert.That(
                exception.Message,
                Is.EqualTo("The argument name 'switch' was defined more than one time."));
        }

        /// <summary>
        /// Try to initialize the <see cref="ArgumentProvider{T}"/> with a parameters container
        /// which has one property marked as switch and another property marked as an argument with the same name.
        /// </summary>
        [Test]
        public void InitSwitchAndArgWithSameName()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<SwitchAndArgWithSameNameContainer>(argumentBuilder, propertyParser);

            ArgumentParserException exception = Assert.Throws<ArgumentParserException>(parser.Initialize);

            Assert.That(
                exception.Message,
                Is.EqualTo("The argument name 'arg' was defined more than one time."));
        }

        /// <summary>
        /// Tries to parse an argument with a non-primitive type.
        /// </summary>
        [Test]
        public void ParseNonBaseTypeArg()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NonBaseTypeContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/arg:" + new TimeSpan() };
            var obj = new NonBaseTypeContainer();

            ArgumentParserException exception = Assert.Throws<ArgumentParserException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(
                exception.Message,
                Is.EqualTo("The type of the property 'Arg' must be a primitive type or an enumeration."));
        }

        /// <summary>
        /// Parse an empty array of parameters.
        /// </summary>
        [Test]
        public void ParseEmpty()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            var args = new string[] { };
            var obj = new NoRequriedContainer();

            parser.ParseArguments(args, obj);

            Assert.That(obj.Integer, Is.EqualTo(0));
            Assert.That(obj.Str, Is.Null);
            Assert.That(obj.Sw, Is.False);
            Assert.That(obj.Sw2, Is.False);
        }

        /// <summary>
        /// Parse all required arguments.
        /// </summary>
        [Test]
        public void ParseRequiredArguments()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<ParametersContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/number:7" };
            var obj = new ParametersContainer();

            parser.ParseArguments(args, obj);

            Assert.That(obj.Integer, Is.EqualTo(7));
        }

        /// <summary>
        /// Parse optional arguments.
        /// </summary>
        [Test]
        public void ParseOptionalArguments()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/number:7", "/text:Ala ma kota" };
            var obj = new NoRequriedContainer();

            parser.ParseArguments(args, obj);

            Assert.That(obj.Integer, Is.EqualTo(7));
            Assert.That(obj.Str, Is.EqualTo("Ala ma kota"));
        }

        /// <summary>
        /// Parse arguments two times into the same container object.
        /// </summary>
        [Test]
        public void ParseArgumentTwoTimes()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            var obj = new NoRequriedContainer();
            
            string[] args = { "/number:7", "/text:Ala ma kota" };
            parser.ParseArguments(args, obj);
            Assert.That(obj.Integer, Is.EqualTo(7));
            Assert.That(obj.Str, Is.EqualTo("Ala ma kota"));

            string[] args2 = { "/text:Ala ma nowego kota" };
            parser.ParseArguments(args2, obj);
            Assert.That(obj.Str, Is.EqualTo("Ala ma nowego kota"));
        }

        /// <summary>
        /// Parse arguments together with switches.
        /// </summary>
        [Test]
        public void ParseArgumentsAndSwitches()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/number:7", "/switch", "/text:Ala ma kota" };
            var obj = new NoRequriedContainer();
            
            parser.ParseArguments(args, obj);

            Assert.That(obj.Integer, Is.EqualTo(7));
            Assert.That(obj.Sw, Is.True);
            Assert.That(obj.Str, Is.EqualTo("Ala ma kota"));
        }

        /// <summary>
        /// Parse an enum type property.
        /// </summary>
        [Test]
        public void ParseEnum()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<EnumContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/item:Item2" };
            var obj = new EnumContainer();
            
            parser.ParseArguments(args, obj);

            Assert.That(obj.En, Is.EqualTo(MyEnum.Item2));
        }

        /// <summary>
        /// Parse an application switch.
        /// </summary>
        [Test]
        public void ParseSwitch()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/switch" };
            var obj = new NoRequriedContainer { Sw = false };

            parser.ParseArguments(args, obj);

            Assert.That(obj.Sw, Is.True);
        }

        /// <summary>
        /// Parse switches two times into the same container object.
        /// </summary>
        [Test]
        public void ParseSwitchTwoTimes()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();
            
            var obj = new NoRequriedContainer();

            string[] args = { "/switch" };
            parser.ParseArguments(args, obj);
            Assert.That(obj.Sw, Is.True);
            Assert.That(obj.Sw2, Is.False);

            string[] args2 = { "/switch2" };
            parser.ParseArguments(args2, obj);
            Assert.That(obj.Sw, Is.False);
            Assert.That(obj.Sw2, Is.True);

            string[] args3 = { "/switch", "/switch2" };
            parser.ParseArguments(args3, obj);
            Assert.That(obj.Sw, Is.True);
            Assert.That(obj.Sw2, Is.True);
        }

        /// <summary>
        /// Parse switches in different order.
        /// </summary>
        [Test]
        public void ParseSwitchDiffOrder()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            var obj = new NoRequriedContainer();

            string[] args = { "/switch", "/switch2" };
            parser.ParseArguments(args, obj);
            Assert.That(obj.Sw, Is.True);
            Assert.That(obj.Sw2, Is.True);

            string[] args2 = { "/switch2", "/switch" };
            parser.ParseArguments(args2, obj);
            Assert.That(obj.Sw, Is.True);
            Assert.That(obj.Sw2, Is.True);
        }

        /// <summary>
        /// Parse an empty array of attributes.
        /// </summary>
        [Test]
        public void ParseEmptyArgs()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            var obj = new NoRequriedContainer();
            string[] args = { };
            
            parser.ParseArguments(args, obj);
            
            Assert.That(obj.Sw, Is.False);
            Assert.That(obj.Sw2, Is.False);
        }

        /// <summary>
        /// Try to parse arguments with a missing required argument.
        /// </summary>
        [Test]
        public void ParseWithoutRequiredArgument()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<ParametersContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { };
            var obj = new ParametersContainer();
            
            ArgumentParserMessageException exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(exception.Message, Is.EqualTo("The required argument 'number' was not specified."));
        }

        /// <summary>
        /// Try to parse an invalid argument.
        /// </summary>
        [Test]
        public void ParseInvalidArgument()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/xyz:123" };
            var obj = new NoRequriedContainer { Sw = false };
            
            ArgumentParserMessageException exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(exception.Message, Is.EqualTo("Invalid argument: 'xyz'."));
        }

        /// <summary>
        /// Try to parse an invalid switch.
        /// </summary>
        [Test]
        public void ParseInvalidSwitch()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/xyz" };
            var obj = new NoRequriedContainer { Sw = false };

            ArgumentParserMessageException exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(exception.Message, Is.EqualTo("Invalid switch: 'xyz'."));
        }

        /// <summary>
        /// Try to parse an attribute with an invalid value.
        /// </summary>
        [Test]
        public void ParseInvalidValue()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/number:ala" };
            var obj = new NoRequriedContainer { Sw = false };

            ArgumentParserMessageException exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(exception.Message, Is.EqualTo("Invalid value for argument: 'number'."));
        }

        /// <summary>
        /// Try to parse an enum type property with an invalid value.
        /// </summary>
        [Test]
        public void ParseInvalidEnum()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<EnumContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            string[] args = { "/item:ItemX" };
            var obj = new EnumContainer();

            ArgumentParserMessageException exception = Assert.Throws<ArgumentParserMessageException>(
                () => parser.ParseArguments(args, obj));

            Assert.That(exception.Message, Is.EqualTo("Invalid value for argument: 'item'."));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentProvider{T}.Initialize"/> method throws an
        /// <see cref="InvalidOperationException"/> when the parser is already initialized.
        /// </summary>
        [Test]
        public void InitializeShouldThrowExceptionWhenAlreadyInitialized()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);
            parser.Initialize();

            var exception = Assert.Throws<InvalidOperationException>(parser.Initialize);

            Assert.That(exception.Message, Is.EqualTo("The argument provider is already initialized."));
        }

        /// <summary>
        /// Tests if the <see cref="ArgumentProvider{T}.ParseArguments"/> method throws an
        /// <see cref="InvalidOperationException"/> when the parser is not initialized.
        /// </summary>
        [Test]
        public void ParseArgumentsShouldThrowExceptionWhenNotInitialized()
        {
            var argumentBuilder = new DefaultArgumentParser();
            var propertyParser = new ArgumentValueParser();
            var parser = new ArgumentProvider<NoRequriedContainer>(argumentBuilder, propertyParser);

            var exception = Assert.Throws<InvalidOperationException>(
                () => parser.ParseArguments(new string[0], new NoRequriedContainer()));

            Assert.That(exception.Message, Is.EqualTo("The argument provider is not initialized."));
        }

        /* ReSharper disable ClassNeverInstantiated.Local */
        /* ReSharper disable UnusedMember.Local */
        /* ReSharper disable UnusedAutoPropertyAccessor.Local */

        /// <summary>
        /// Empty class for test purposes.
        /// </summary>
        private class EmptyContainer
        {
        }

        /// <summary>
        /// Class with non-required application arguments.
        /// </summary>
        private class NoRequriedContainer
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            [ApplicationArgument("number", false)]
            public int Integer { get; set; }

            /// <summary>
            /// Gets or sets the text value.
            /// </summary>
            [ApplicationArgument("text", false)]
            public string Str { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the first switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch")]
            public bool Sw { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the second switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch2")]
            public bool Sw2 { get; set; }
        }

        /// <summary>
        /// Class with required application arguments.
        /// </summary>
        private class ParametersContainer
        {
            /// <summary>
            /// Gets or sets the number.
            /// </summary>
            [ApplicationArgument("number", true)]
            public int Integer { get; set; }

            /// <summary>
            /// Gets or sets the text value.
            /// </summary>
            [ApplicationArgument("text", false)]
            public string Str { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch")]
            public bool Sw { get; set; }
        }

        /// <summary>
        /// Class with an invalid switch property.
        /// </summary>
        private class InvalidParametersContainer
        {
            /// <summary>
            /// Gets or sets a value indicating whether the switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch")]
            public int MySwitch { get; set; }
        }

        /// <summary>
        /// Class with duplicate argument definition.
        /// </summary>
        private class DoubleArgContainer
        {
            /// <summary>
            /// Gets or sets the first argument.
            /// </summary>
            [ApplicationArgument("arg", false)]
            public int A { get; set; }

            /// <summary>
            /// Gets or sets the second argument, but with the same name as the first one.
            /// </summary>
            [ApplicationArgument("arg", false)]
            public int B { get; set; }
        }

        /// <summary>
        /// Class with duplicate switch definition.
        /// </summary>
        private class DoubleSwitchContainer
        {
            /// <summary>
            /// Gets or sets a value indicating whether the switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch")]
            public bool A { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the switch is enabled.
            /// </summary>
            [ApplicationSwitch("switch")]
            public bool B { get; set; }
        }

        /// <summary>
        /// Class with switch and argument with the same name.
        /// </summary>
        private class SwitchAndArgWithSameNameContainer
        {
            /// <summary>
            /// Gets or sets a value indicating whether the switch is enabled.
            /// </summary>
            [ApplicationSwitch("arg")]
            public bool A { get; set; }

            /// <summary>
            /// Gets or sets the second argument, but with the same name as the first one.
            /// </summary>
            [ApplicationArgument("arg", true)]
            public int B { get; set; }
        }
        
        /// <summary>
        /// Class with an argument property of unsupported type.
        /// </summary>
        private class NonBaseTypeContainer
        {
            /// <summary>
            /// Gets or sets the application argument with a non-base type.
            /// </summary>
            [ApplicationArgument("arg", false)]
            public TimeSpan Arg { get; set; }
        }

        /// <summary>
        /// Class with an enum argument.
        /// </summary>
        private class EnumContainer
        {
            /// <summary>
            /// Gets or sets the property with an enum type.
            /// </summary>
            [ApplicationArgument("item", false)]
            public MyEnum En { get; set; }
        }

        // ReSharper restore UnusedAutoPropertyAccessor.Local
        /* ReSharper restore UnusedMember.Local */
        /* ReSharper restore ClassNeverInstantiated.Local */
    }
}