﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleApplicationTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using System;
    using global::Autofac;
    using NUnit.Framework;
    
    /// <summary>
    /// Implements unit tests for the <see cref="ConsoleApplication{T}"/> class.
    /// </summary>
    [TestFixture]
    public class ConsoleApplicationTests
    {
        /// <summary>
        /// Test if the <see cref="ConsoleApplication{TArguments}.Run"/> method invokes the
        /// <see cref="ConsoleApplication{TArguments}.Execute"/> method.
        /// </summary>
        [Test]
        public void RunShouldInvokeExecute()
        {
            bool executed = false;
            var app = new ConsoleApplicationMock { ExecuteAction = () => { executed = true; } };
            app.Run(new string[0]);

            Assert.That(executed, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ConsoleApplication{TArguments}.Run"/> method invokes all hook methods.
        /// </summary>
        [Test]
        public void RunShouldInvokeHookMethods()
        {
            int counter = 0;
            bool preInitialize = false;
            bool configureContainer = false;
            bool parseArguments = false;
            bool postInitialize = false;
            bool shutdown = false;
            var arguments = new string[0];

            var app = new ConsoleApplicationMock
            {
                PreInitializeAction = () =>
                {
                    Assert.That(counter++, Is.EqualTo(0));
                    preInitialize = true;
                },
                ConfigureContainerAction = container =>
                {
                    Assert.That(container, Is.Not.Null);
                    Assert.That(counter++, Is.EqualTo(1));
                    configureContainer = true;
                },
                ParseArgumentsFunc = args =>
                {
                    Assert.That(args, Is.SameAs(arguments));
                    Assert.That(counter++, Is.EqualTo(2));
                    parseArguments = true;
                    return new TestArguments();
                },
                PostInitializeAction = () =>
                { 
                    Assert.That(counter++, Is.EqualTo(3));
                    postInitialize = true;
                },
                ShutdownAction = () =>
                {
                    Assert.That(counter++, Is.EqualTo(4));
                    shutdown = true;
                }
            };

            app.Run(arguments);

            Assert.That(preInitialize, Is.True);
            Assert.That(configureContainer, Is.True);
            Assert.That(parseArguments, Is.True);
            Assert.That(postInitialize, Is.True);
            Assert.That(shutdown, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ConsoleApplication{TArguments}.Run"/> method invokes the
        /// <see cref="ConsoleApplication{TArguments}.OnShutdown"/> method when an exception is thrown.
        /// </summary>
        [Test]
        public void RunShouldInvokeShutdownWhenExceptionThrown()
        {
            var exception = new InvalidOperationException("Test exception message.");
            bool shutdownInvoked = false;
            bool handlerInvoked = false;
            
            var app = new ConsoleApplicationMock
            {
                ExecuteAction = () => { throw exception; },
                ShutdownAction = () => { shutdownInvoked = true; },
                FatalErrorAction = ex =>
                {
                    Assert.That(ex, Is.SameAs(exception));
                    handlerInvoked = true;
                }
            };

            app.Run(new string[0]);

            Assert.That(shutdownInvoked, Is.True);
            Assert.That(handlerInvoked, Is.True);
        }

        /// <summary>
        /// Test arguments class.
        /// </summary>
        private class TestArguments
        {
        }

        /* ReSharper disable MemberCanBePrivate.Local */

        /// <summary>
        /// Console application class mock.
        /// </summary>
        private class ConsoleApplicationMock : ConsoleApplication<TestArguments>
        {
            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="OnPreInitialize"/>.
            /// </summary>
            public Action PreInitializeAction { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="ConfigureContainer"/>.
            /// </summary>
            public Action<IContainer> ConfigureContainerAction { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="ParseArguments"/>.
            /// </summary>
            public Func<string[], TestArguments> ParseArgumentsFunc { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="OnPostInitialize"/>.
            /// </summary>
            public Action PostInitializeAction { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="Execute"/>.
            /// </summary>
            public Action ExecuteAction { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="OnFatalError"/>.
            /// </summary>
            public Action<Exception> FatalErrorAction { get; set; }

            /// <summary>
            /// Gets or sets the action which is invoked by <see cref="OnShutdown"/>.
            /// </summary>
            public Action ShutdownAction { get; set; }

            /// <summary>
            /// Called before the application is initialized.
            /// </summary>
            protected override void OnPreInitialize()
            {
                if (this.PreInitializeAction != null)
                {
                    this.PreInitializeAction();
                }
            }

            /// <summary>
            /// Called after the dependency injection container has been built.
            /// </summary>
            /// <param name="container">The application's dependency injection container.</param>
            protected override void ConfigureContainer(IContainer container)
            {
                if (this.ConfigureContainerAction != null)
                {
                    this.ConfigureContainerAction(container);
                }
            }

            /// <summary>
            /// Parses the application's command-line arguments.
            /// </summary>
            /// <param name="args">The command-line arguments to parse.</param>
            /// <returns>The object which represents the parsed arguments.</returns>
            protected override TestArguments ParseArguments(string[] args)
            {
                if (this.ParseArgumentsFunc != null)
                {
                    return this.ParseArgumentsFunc(args);
                }

                return base.ParseArguments(args);
            }

            /// <summary>
            /// Called after the application has been initialized.
            /// </summary>
            protected override void OnPostInitialize()
            {
                if (this.PostInitializeAction != null)
                {
                    this.PostInitializeAction();
                }
            }

            /// <summary>
            /// Called to execute application logic.
            /// </summary>
            protected override void Execute()
            {
                if (this.ExecuteAction != null)
                {
                    this.ExecuteAction();
                }
            }

            /// <summary>
            /// Called when a fatal application error occurs.
            /// </summary>
            /// <param name="exception">The exception which caused the error.</param>
            protected override void OnFatalError(Exception exception)
            {
                if (this.FatalErrorAction != null)
                {
                    this.FatalErrorAction(exception);
                }

                base.OnFatalError(exception);
            }

            /// <summary>
            /// Called when the application is being shut down.
            /// </summary>
            protected override void OnShutdown()
            {
                if (this.ShutdownAction != null)
                {
                    this.ShutdownAction();
                }
            }

            protected override void RegisterComponents(ContainerBuilder builder)
            {
                base.RegisterComponents(builder);
                builder.RegisterModule<ConsoleApplicationModule>();
            }
        }

        /* ReSharper restore MemberCanBePrivate.Local */
    }
}