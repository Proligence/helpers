﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationSwitchArgumentTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
// THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Helpers.Console.UnitTests
{
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ApplicationSwitchArgument"/> class.
    /// </summary>
    [TestFixture]
    public class ApplicationSwitchArgumentTests
    {
        /// <summary>
        /// Tests if <see cref="ApplicationSwitchArgument.IsSet"/> returns <c>true</c> if the argument's value is
        /// <c>true</c>.
        /// </summary>
        [Test]
        public void IsSetShouldReturnTrueWhenArgumentValueTrue()
        {
            var sw = new ApplicationSwitchArgument("switch", true);

            Assert.That(sw.IsSet, Is.True);
        }

        /// <summary>
        /// Tests if <see cref="ApplicationSwitchArgument.IsSet"/> returns <c>false</c> if the argument's value is
        /// <c>false</c>.
        /// </summary>
        [Test]
        public void IsSetShouldReturnFalseWhenArgumentValueFalse()
        {
            var sw = new ApplicationSwitchArgument("switch", false);

            Assert.That(sw.IsSet, Is.False);
        }
    }
}